import unittest
from TestUtils import TestAST
from AST import *

class ASTGenSuite(unittest.TestCase):
    def test_1(self):
        """Simple program: class main {} """
        input = """
        class hihi {
            haha[5] main(){
                array : haha[6];
                a : int;
                a := 6;
                return array;
            }
        }
        class haha {
            x : int;
            y : float;
        }
        class kaka {
            z : int;
            void main(){
                s : hihi;
                z := s.main()[1].x;
            }
        }
        """
        expect = str(Program([
                                ClassDecl(Id("hihi"),[MethodDecl(Instance(),Id("main"),[],ArrayType(5,ClassType(Id("haha"))),Block([VarDecl(Id("array"),ArrayType(6,ClassType(Id("haha")))),VarDecl(Id("a"),IntType())],[Assign(Id("a"),IntLiteral(6)),Return(Id("array"))]))])
                                 ,
                                  ClassDecl(Id("haha"),[AttributeDecl(Instance(),VarDecl(Id("x"),IntType())),AttributeDecl(Instance(),VarDecl(Id("y"),FloatType()))])
                                 ,
                                  ClassDecl(Id("kaka"),[AttributeDecl(Instance(),VarDecl(Id("z"),IntType())),MethodDecl(Instance(),Id("main"),[],VoidType(),Block([VarDecl(Id("s"),ClassType(Id("hihi")))],[Assign(Id("z"),FieldAccess(ArrayCell(CallExpr(Id("s"),Id("main"),[]),IntLiteral(1)),Id("x")))]))])
                              ]))
        self.assertTrue(TestAST.test(input,expect,301))

