import unittest
from TestUtils import TestChecker
from AST import *


class CheckerSuite(unittest.TestCase):
    def test_401(self):
        input = Program([ClassDecl(Id("io"), []),
                         ClassDecl(Id("io"), [])])
        expect = "Redeclared Class: io"
        self.assertTrue(TestChecker.test(input, expect, 401))

    def test_402(self):
        input = Program([ClassDecl(Id("main"),
                                   [AttributeDecl(Instance(), VarDecl(Id("a"), IntType())),
                                    AttributeDecl(Static(), VarDecl(Id("a"), IntType()))])])
        expect = "Redeclared Attribute: a"
        self.assertTrue(TestChecker.test(input, expect, 402))

    def test_403(self):
        input = """
        class main extends d{}
        class a {}
        class b{}"""
        expect = "Undeclared Class: d"
        self.assertTrue(TestChecker.test(input, expect, 403))

    def test_404(self):
        input = """class abc {
            int factorial(n:int){
                if n then
                    io.writeStrLn("Expression is true");
            }
        }"""
        expect = "Type Mismatch In Statement: If(Id(n),CallStmt(Id(io),Id(writeStrLn),[StringLiteral(Expression is true)]))"
        self.assertTrue(TestChecker.test(input, expect, 404))

    def test_405(self):
        input = """class a {
            static x : int;
            y : float;
        }
        class b {
            y : int;
            y : float;

        }"""
        expect = "Redeclared Attribute: y"
        self.assertTrue(TestChecker.test(input, expect, 405))

    def test_406(self):
        input = """class a {
            static x : int;
            y : float;
        }
        class x {
        }"""
        expect = "[]"
        self.assertTrue(TestChecker.test(input, expect, 406))

    def test_407(self):
        input = """
        class main extends a{
            final int My1stCons = 1 + 5;
            static final int My2ndCons = 2;
            void A(){
            }
        }
        class a {
            static My1stCons, my3rdVar: Shape;
            static my2ndArray, my3rdArray: Shape[6];
        }
        class c{}"""
        expect = "Undeclared Class: Shape"
        self.assertTrue(TestChecker.test(input, expect, 407))

    def test_408(self):
        input = """
        class main extends getArea{
            final int My1stCons = 1 + 5;
            static final int My2ndCons = 2;
            a : getArea;
            b : main;
            void get(){
                s : int;
                this.My1stCons := this.b;
            }
        }
        class getArea {
            static my2ndVar, my3rdVar: Shape;
            static my2ndArray, my3rdArray: Shape[6];
            z : int;
        }
        class c{}"""
        expect = "Cannot Assign To Constant: AssignStmt(FieldAccess(SelfLiteral(),Id(My1stCons)),FieldAccess(SelfLiteral(),Id(b)))"
        self.assertTrue(TestChecker.test(input, expect, 408))

    def test_409(self):
        input = """
        class main extends getArea{
            final int My1stCons = 1 + 5;
            static final int My2ndCons = 2;
            a : getArea;
            b : main;
            void get(){
                s : int;
                final int hehe = this.a.my2ndVar + this.My2ndCons;
                this.a := this.b;
            }
        }
        class getArea {
            static my2ndVar, my3rdVar: Shape;
            static my2ndArray, my3rdArray: Shape[6];
            z : int;
        }
        class c{}"""
        expect = "Not Constant Expression: FieldAccess(FieldAccess(SelfLiteral(),Id(a)),Id(my2ndVar))"
        self.assertTrue(TestChecker.test(input, expect, 409))

    def test_410(self):
        input = """
        class main {
            a : int;
            b : int;
            int test(){
                return 2;
            }
            void get(){
                s : int;
                var : getArea;
                this.a := this.b + var.test();
            }
        }
        class getArea {
            static my2ndVar, my3rdVar: Shape;
            static hehe : int;
            int TaiVuong(){
                return 2;
            }
        }"""
        expect = "Undeclared Method: test"
        self.assertTrue(TestChecker.test(input, expect, 410))

    def test_411(self):
        input = """

        class main extends hehe {
            final int My1stCons = 1 + 5;
            static final int My2ndCons = 2;
            a : int;
            b : int;
            void get(){
                s : int;
                var : hehe;
                this.a := this.b + var.hihi(2,3);
            }
        }
        class hehe {
            static c : int;
            int hihi(a :int;b :int){
                continue;
            }
        }
        """
        expect = "Continue Not In Loop"
        self.assertTrue(TestChecker.test(input, expect, 411))

    def test_412(self):
        input = """
        class main extends hehe {
            final int My1stCons = 1 + 5;
            static final int My2ndCons = 2;
            a : int;
            b : int;
            void get(){
                s : int;
                var : hehe;
                this.a := this.b + var.main(2,3);
            }
        }
        class hehe {
            static c : int;
            int hihi(a :int;b :int){
                continue;
            }
        }
        """
        expect = "Undeclared Method: main"
        self.assertTrue(TestChecker.test(input, expect, 412))

    def test_413(self):
        input = """
        class main extends hehe {
            final int My1stCons = 1 + 5;
            static final int My2ndCons = 2;
            a : int;
            b : int;
            void get(){
                s : int;
                var : hehe;
                this.a := this.b + var.hihi(2,3);
            }
        }
        class hehe {
            static c : int;
            int hihi(a :int;b :int){
                return 2.3;
            }
        }
        """
        expect = "Type Mismatch In Statement: Return(FloatLiteral(2.3))"
        self.assertTrue(TestChecker.test(input, expect, 413))

    def test_414(self):
        input = """
        class Shape {
            length,width:float;
            float getArea() {}
            Shape(lengthParam,widthParam:float){
                this.length := lengthParam;
                this.width := widthParam;
            }
        }
        class Rectangle extends Shape {
            float callMethod(){
                return this.length*this.width;
        }
        }"""
        expect = "[]"
        self.assertTrue(TestChecker.test(input, expect, 414))

    def test_415(self):
        input = """
        class mot extends hai{
            attr1,attr2:float;
            float hihi() {}
            Shape(length,width:float){
                this.attr1 := this.a;
                this.attr2 := this.b;
            }
        }
        class hai extends ba{
            int haha(){
                return this.a*this.b;
            }
        }
        class ba{
            a,b,c :int;
        }"""
        expect = "[]"
        self.assertTrue(TestChecker.test(input, expect, 415))

    def test_416(self):
        input = """
        class Shape{
            attribute1,attribute2: int;
            float methodmotmot() {}
            Shape(lengthParam,widthParam:float){
                this.attribute1 := lengthParam + this.widthParam;
                this.attribute2 := widthParam;
            }
        }"""
        expect = "Undeclared Attribute: widthParam"
        self.assertTrue(TestChecker.test(input, expect, 416))

    def test_417(self):
        input = """class Shape {
                        length,width:float;
                        float getArea() {}
                        Shape(length,width:float){
                            this.length := length;
                            this.width := width;
                        }
                }
                class Rectangle extends Shape {
                    float getArea(){
                        return this.length*this.width;
                    }
                }
                class Triangle extends Shape {
                    float getArea(){
                        return this.length*this.width / 2;
                    }
                }
                class Example2 {
                    void main(){
                        s:Shape;
                        s := new Rectangle(3,4);
                        io.writeFloatLn(s.getArea());
                        s := new Triangle(3,4);
                    }
                }"""
        expect = "[]"
        self.assertTrue(TestChecker.test(input, expect, 417))

    def test_418(self):
        input = """
        class Shape{
            superman,hulk: int;
            float java() {}
            Shape(lengthParam,widthParam:float){
                lengthParam : float;
                width := 10;
            }
        }"""
        expect = "Redeclared Variable: lengthParam"
        self.assertTrue(TestChecker.test(input, expect, 418))

    def test_419(self):
        input = """
        class Shape {
                        length,width:float;
                        float getArea() {}
                        Shape(length,width:float){
                            this.length := length;
                            this.width := width;
                        }
                        Shape(){}
                }"""
        expect = "Redeclared Special Method: <init>"
        self.assertTrue(TestChecker.test(input, expect, 419))

    def test_420(self):
        input = """
        class Ronaldo {
                        shoot,head:int;
                        float score() {}
                        Ronaldo(){
                            str1 : string;
                            str2 : string;
                            haha : boolean;
                            if str1==str2 then {
                                haha := io.readStr();
                            }
                        }
                }"""
        expect = "Type Mismatch In Expression: BinaryOp(==,Id(str1),Id(str2))"
        self.assertTrue(TestChecker.test(input, expect, 420))

    def test_421(self):
        input = """
        class Ronaldo {
                        shoot,head:int;
                        float score() {}
                        Ronaldo(){
                            str1 : string;
                            str2 : string;
                            str3 : int;
                            str3 := str1 ^ str2;

                        }
                }"""
        expect = "Type Mismatch In Statement: AssignStmt(Id(str3),BinaryOp(^,Id(str1),Id(str2)))"
        self.assertTrue(TestChecker.test(input, expect, 421))

    def test_422(self):
        input = """
        class Ronaldo {
            shoot,head:int;
            float score() {}
            Ronaldo(){
                local1 : int[5];
                local2 : float[6];
                local1 := local2;
            }
        }
        class Messi {
            attr1 : string;
            attr2 : string;
            attr3 : int;
        }"""
        expect = "Type Mismatch In Statement: AssignStmt(Id(local1),Id(local2))"
        self.assertTrue(TestChecker.test(input, expect, 422))

    def test_423(self):
        input = """
        class hihi {
            void main(){
                a : int;
                var : a;
                var.abc := 6;
            }
        }
        class a {
            static abc : int;
        }"""
        expect = "[]"
        self.assertTrue(TestChecker.test(input, expect, 423))

    def test_424(self):
        input = """
        class hihi {
            int[5] main(){
                array : int[6];
                a : int;
                a := 6;
                return array;
            }
        }
        """
        expect = "Type Mismatch In Statement: Return(Id(array))"
        self.assertTrue(TestChecker.test(input, expect, 424))

    def test_425(self):
        input = """
        class hihi {
            final int immuAttribute = 0;
            haha[5] main(){
                array : haha[5];
                return array;
            }
        }
        class haha {
            static x: int;
            y: float;
        }
        class kaka {
            z: int;
            void main(){
                s : hihi;
                this.z := s.main()[1].x;
            }
        }
        """
        expect = "[]"
        self.assertTrue(TestChecker.test(input, expect, 425))

    def test_426(self):
        input = """
        class hihi {
            final int immuAttribute = 0;
            haha[5] main(){
                array : haha[5];
                return array;
            }
        }
        class haha {
            static x: int;
            y: float;
        }
        class kaka {
            z: string;
            void main(){
                s : hihi;
                this.z := s.main()[1].x;
            }
        }
        """
        expect = "Type Mismatch In Statement: AssignStmt(FieldAccess(SelfLiteral(),Id(z)),FieldAccess(ArrayCell(CallExpr(Id(s),Id(main),List()),IntLiteral(1)),Id(x)))"
        self.assertTrue(TestChecker.test(input, expect, 426))

    def test_427(self):
        input = """
        class hihi {
            final int immuAttribute = 0;
            int[5] main(){
                array : haha[5];
                return array;
            }
        }
        class haha {
            x: int;
            y: float;
        }
        class kaka {
            z: string;
            void main(){
                s : hihi;
                this.z := s.main()[1].x;
            }
        }
        """
        expect = "Type Mismatch In Expression: FieldAccess(ArrayCell(CallExpr(Id(s),Id(main),List()),IntLiteral(1)),Id(x))"
        self.assertTrue(TestChecker.test(input, expect, 427))

    def test_428(self):
        input = """
        class hihi {
            x: int;
            y: float;
            haha main(){
                var : haha;
                return var;
            }
        }
        class haha {
            static immuAttribute : int;
            static final int numOfShape = 0;
        }
        class kaka {
            z: string;
            void main(){
                s : hihi;
                this.z := s.main().numOfShape;
            }
        }
        """
        expect = "Type Mismatch In Statement: AssignStmt(FieldAccess(SelfLiteral(),Id(z)),FieldAccess(CallExpr(Id(s),Id(main),List()),Id(numOfShape)))"
        self.assertTrue(TestChecker.test(input, expect, 428))

    def test_429(self):
        input = """
        class hihi {
            x: int;
            y: float;
            haha main(){
                var : haha;
                return var;
            }
        }
        class haha {
            static immuAttribute : int;
            static final int numOfShape = 0;
        }
        class kaka {
            z: string;
            void main(){
                s : hihi;
                this.z := s.main().numOfShape;
            }
        }
        """
        expect = "Type Mismatch In Statement: AssignStmt(FieldAccess(SelfLiteral(),Id(z)),FieldAccess(CallExpr(Id(s),Id(main),List()),Id(numOfShape)))"
        self.assertTrue(TestChecker.test(input, expect, 429))

    def test_430(self):
        input = """
        class hihi {
            x: int;
            y: float;
            hehe main(){
                var : haha;
                return var;
            }
        }
        class haha {
            static immuAttribute : int;
            final int numOfShape = 0;
        }
        """
        expect = "Undeclared Class: hehe"
        self.assertTrue(TestChecker.test(input, expect, 430))

    def test_431(self):
        input = """
        class hihi {
            x: int;
            y: float;
            hehe[5] main(){
                var : haha[5];
                return var;
            }
        }
        class haha {
            static immuAttribute : int;
            final int numOfShape = 0;
        }
        """
        expect = "Undeclared Class: hehe"
        self.assertTrue(TestChecker.test(input, expect, 431))

    def test_432(self):
        input = """
        class Taivuong {
            x: int;
            y: float;
            void main(){
                var : int;
                if 1 + 4 then
                    io.writeIntLn(10);
            }
        }
        """
        expect = "Type Mismatch In Statement: If(BinaryOp(+,IntLiteral(1),IntLiteral(4)),CallStmt(Id(io),Id(writeIntLn),[IntLiteral(10)]))"
        self.assertTrue(TestChecker.test(input, expect, 432))

    def test_433(self):
        input = """
        class Taivuong {
            x: int;
            y: float;
            void main(){
                var : int;
                if 10.0 % 4.0 then
                    io.writeIntLn(10);
            }
        }
        """
        expect = "Type Mismatch In Expression: BinaryOp(%,FloatLiteral(10.0),FloatLiteral(4.0))"
        self.assertTrue(TestChecker.test(input, expect, 433))

    def test_434(self):
        input = """
        class Taivuong {
            final int My1stCons = 1 + this.main();
            x: int;
            y: float;
            int main(){
                var : int;
                if true then
                    io.writeIntLn(10);
                return var;
            }
        }
        """
        expect = "Not Constant Expression: CallExpr(SelfLiteral(),Id(main),List())"
        self.assertTrue(TestChecker.test(input, expect, 434))

    def test_435(self):
        input = """
        class Taivuong extends VD{
            final int My1stCons = 1 * 10 - 10 % 2;
            x: int;
            y: float;
            float main(){
                var : int;
                if true then
                    io.writeFloatLn(10.0);
                return var;
            }
        }
        class VD {
            static final int attr1 = 0;
            final int attr2 = 10 - 2;
            myArrayVar: int[5];
            static my2ndVar, my3rdVar: Taivuong;
            VD(){}
            VD(){}

        }
        """
        expect = "Redeclared Special Method: <init>"
        self.assertTrue(TestChecker.test(input, expect, 435))

    def test_436(self):
        input = """
        class Taivuong extends VD{
            final int My1stCons = 1 * 10 - 10 % 2;
            x: int;
            y: float;
            float main(){
                var : int;
                if true then
                    io.writeFloatLn(10.0);
                return var;
            }
        }
        class VD {
            static final int attr1 = 0;
            final int attr2 = 10 - 2;
            myArrayVar: int[5];
            static my2ndVar, my3rdVar: Taivuong;
            VD(name:string ; tuoi:int){
                this.attr1 := tuoi;
            }
        }
        """
        expect = "Cannot Assign To Constant: AssignStmt(FieldAccess(SelfLiteral(),Id(attr1)),Id(tuoi))"
        self.assertTrue(TestChecker.test(input, expect, 436))

    def test_437(self):
        input = """
        class Taivuong{
            final int My1stCons = 1 * 10 - 10 % 2;
            x: int;
            y: float;
            string main(){
                var : int;
                for var:=1 to 10 do
                    this.y := this.x;
                return var;
            }
        }
        """
        expect = "Type Mismatch In Statement: Return(Id(var))"
        self.assertTrue(TestChecker.test(input, expect, 437))

    def test_438(self):
        input = """
        class Taivuong{
            static final int Cons1 = 1 * 10 - 10 % 2;
            final float Const2 = 10.4 - 2.3;
            static x: int;
            y: float;
            string main(){
                var1 : int;
                var2 :string;
                for var1:=1 to 10 do
                    this.y := this.x;
                return var2;
            }
        }
        class Ronaldo {
            static final int Const1 = 13;
            final int Const2 = 12;
            static abc : Taivuong;
            void main (){
                name : string;
                tuoi : int;
                if this.Const2 > 10 then
                    tuoi := 10;
            }
        }
        class Messi {
            static gender : boolean;
            static final int abc = 10;
            Ronaldo dabonghay (){
                if this.abc >= 10 then{
                    i,x : int;
                    for i:=0 to 10 do{
                        s : Ronaldo;
                        x := s.abc;
                        io.writeInt(10);
                        if true then
                            io.writeInt(10);
                    }
                }
            }

        }
        """
        expect = "Type Mismatch In Statement: AssignStmt(Id(x),FieldAccess(Id(s),Id(abc)))"
        self.assertTrue(TestChecker.test(input, expect, 438))

    def test_439(self):
        input = """
        class Taivuong{
            final int My1stCons = "VoTaiVuong";
            x: int;
            y: float;
            int main(){
                var : int;
                for var:=1 to 10 do
                    this.y := this.x;
                return var;
            }
        }
        """
        expect = "Type Mismatch In Constant Declaration: ConstDecl(Id(My1stCons),IntType,StringLiteral(VoTaiVuong))"
        self.assertTrue(TestChecker.test(input, expect, 439))

    def test_440(self):
        input = """
        class Taivuong{
            final int My1stCons = 22;
            x: int;
            y: float;
            int main(){
                var : int;
                variable : BachKhoa;
                for var:=1 to 10 do{
                    this.y := this.x;
                    if true then{
                        var : int;
                        var := variable.main().x;
                    }
                }
                return var;
            }
        }
        class BachKhoa {
            static attribute : int;
            Taivuong main (){
                s : Taivuong;
                return this.attribute;
            }
        }
        """
        expect = "Type Mismatch In Statement: Return(FieldAccess(SelfLiteral(),Id(attribute)))"
        self.assertTrue(TestChecker.test(input, expect, 440))

    def test_441(self):
        input = """
        class Taivuong{
            final int My1stCons = 22;
            x: int;
            y: float;
            int main(){
                var : int;
                variable : BachKhoa;
                for var:=1 to 10 do{
                    this.y := this.x;
                    if true then{
                        var : int;
                        test1 : int;
                        test2 : float;
                        test3 : float;
                        var := variable.main().x;
                        test4 := test1 + test2;

                    }
                }
                return var;
            }
        }
        class BachKhoa {
            static attribute : int;
            Taivuong main (){
                s : Taivuong;
                return s;
            }
        }
        """
        expect = "Undeclared Identifier: test4"
        self.assertTrue(TestChecker.test(input, expect, 441))

    def test_442(self):
        input = """
        class Mess {
            void num() {}
            static num :int;
        }
        """
        expect = "Redeclared Attribute: num"
        self.assertTrue(TestChecker.test(input, expect, 442))

    def test_443(self):
        input = """
        class a{
            a:float;
        }
        class b{
            int b(){}
            static b : int;
        }
        class A{
            static b: int;
        }
        class B{
            static b: float;
        }
        """
        expect = "Redeclared Attribute: b"
        self.assertTrue(TestChecker.test(input, expect, 443))

    def test_444(self):
        input = """
        class a{
            a:float;
            b:hehe;
            b: int;
            void main(){
                this.b := nil;
            }
        }
        class hehe{
            c : string;
            d : boolean;
        }
        """
        expect = "Redeclared Attribute: b"
        self.assertTrue(TestChecker.test(input, expect, 444))

    def test_445(self):
        input = """
        class a{
            a:float;
            b:hehe;
            void main(){
                this.b := new hehe("TaiVuong",1);
            }
        }
        class hehe{
            name : string;
            isMale : boolean;
            hehe (name: string;isMale: boolean){
                this.name := name;
                this.isMale := isMale;
            }
        }
        """
        expect = "Type Mismatch In Expression: NewExpr(Id(hehe),List(StringLiteral(TaiVuong),IntLiteral(1)))"
        self.assertTrue(TestChecker.test(input, expect, 445))

    def test_446(self):
        input = """
        class a{
            void main(){
                a,b,c : string;
                d,e,f : int;
                g,h,i : float;
                t,u,v : boolean;
                f := a + b;
            }
        }
        """
        expect = "Type Mismatch In Expression: BinaryOp(+,Id(a),Id(b))"
        self.assertTrue(TestChecker.test(input, expect, 446))

    def test_447(self):
        input = """
        class a{
            void main(){
                a,b,c : string;
                d,e,f : int;
                g,h,i : float;
                t,u,v : boolean;
                if u && v then
                    io.writeBoolLn();
            }
        }
        """
        expect = "Type Mismatch In Statement: CallStmt(Id(io),Id(writeBoolLn),[])"
        self.assertTrue(TestChecker.test(input, expect, 447))

    def test_448(self):
        input = """
        class a{
            static attribute : a[5];
            static attribute2 : float;
            a test(){
                var : a;
                return var;
            }
        }
        class b{
            void main(){
                var : a;
                x : int;
                x := var.attribute[2].test().attribute2;
            }
        }
        """
        expect = "Type Mismatch In Statement: AssignStmt(Id(x),FieldAccess(CallExpr(ArrayCell(FieldAccess(Id(var),Id(attribute)),IntLiteral(2)),Id(test),List()),Id(attribute2)))"
        self.assertTrue(TestChecker.test(input, expect, 448))

    def test_449(self):
        input = """
        class a{
            void main(){
                io.readStr();
            }
        }
        """
        expect = "Type Mismatch In Statement: CallStmt(Id(io),Id(readStr),[])"
        self.assertTrue(TestChecker.test(input, expect, 449))

    def test_450(self):
        input = """
        class a{
            void main(){
            var : int;
                var := io.readStr();
            }
        }
        """
        expect = "Type Mismatch In Statement: AssignStmt(Id(var),CallExpr(Id(io),Id(readStr),List()))"
        self.assertTrue(TestChecker.test(input, expect, 450))

    def test_451(self):
        input = """
        class a{
            a:float;
            b:hehe;
            void main(){
                this.b := new hehe("TaiVuong","Hehe");
            }
        }
        class hehe{
            name : string;
            isMale : boolean;
            hehe (name: string;isMale: boolean){
                this.name := name;
                this.isMale := isMale;
            }
        }
        """
        expect = "Type Mismatch In Expression: NewExpr(Id(hehe),List(StringLiteral(TaiVuong),StringLiteral(Hehe)))"
        self.assertTrue(TestChecker.test(input, expect, 451))

    def test_452(self):
        input = """
        class Rivaldo{
            club : string;
            age : int;
            brazil : boolean;
            b:hehe;
            Rivaldo(club : string;age : int ){
                this.age := age;
                this.club := club;
            }
            int score (point :int){
                io.writeInt(point);
            }
        }
        class Romario{
            club : string;
            age : int;
            barca : boolean;
            hehe (name: string;isMale: boolean){
                variable : Rivaldo;
                variable := new Rivaldo("RealMadrich",30);
                variable.score();
            }
        }
        class Ronaldinho{
            club : string;
            age : int;
            real : boolean;
        }
        """
        expect = "Undeclared Class: hehe"
        self.assertTrue(TestChecker.test(input, expect, 452))

    def test_453(self):
        input = """
        class Rivaldo{
            club : string;
            age : int;
            brazil : boolean;
            b:Romario;
            Rivaldo(club : string;age : int ){
                this.age := age;
                this.club := club;
            }
            void score (point :int){
                io.writeInt(point);
            }
        }
        class Romario{
            club : string;
            age : int;
            barca : boolean;
            hehe (name: string;isMale: boolean){
                variable : Rivaldo;
                variable := new Rivaldo("RealMadrich",30);
                variable.score(3.0);
            }
        }
        class Ronaldinho{
            club : string;
            age : int;
            real : boolean;
        }
        """
        expect = "Type Mismatch In Statement: CallStmt(Id(variable),Id(score),[FloatLiteral(3.0)])"
        self.assertTrue(TestChecker.test(input, expect, 453))

    def test_454(self):
        input = """
        class Rivaldo{
            club : string;
            age : int;
            brazil : boolean;
            b:Romario;
            Rivaldo(club : string;age : int ){
                this.age := age;
                this.club := club;
            }
            string score (point :int){
                io.writeInt(point);
                return 3;
            }
        }
        class Romario{
            club : string;
            age : int;
            barca : boolean;
            hehe (name: string;isMale: boolean){
                variable : Rivaldo;
                variableR : Ronaldinho;
                variable := new Rivaldo("RealMadrich",30);
                variableR.age := variable.score(3);
            }
        }
        class Ronaldinho{
            club : string;
            static age : string;
            real : boolean;
        }
        """
        expect = "Type Mismatch In Statement: Return(IntLiteral(3))"
        self.assertTrue(TestChecker.test(input, expect, 454))

    def test_455(self):
        input = """
        class Rivaldo{
            club : string;
            age : int;
            brazil : boolean;
            Rivaldo(club : string;age : int ){
                this.age := age;
                this.club := club;
            }
            string score (point :int){
                this.age := age;
            }
        }
        """
        expect = "Undeclared Identifier: age"
        self.assertTrue(TestChecker.test(input, expect, 455))

    def test_456(self):
        input = """
        class Rivaldo{
            club : string;
            age : int;
            brazil : boolean;
            var : City;
            final int Constant = 10;
            static final int Imutablel = 10 + var.main();
            Rivaldo(club : string;age : int ){
                this.age := age;
                this.club := club;
            }
        }
        class City {
            int main (){
                return 2;
            }
        }
        """
        expect = "Not Constant Expression: CallExpr(Id(var),Id(main),List())"
        self.assertTrue(TestChecker.test(input, expect, 456))

    def test_457(self):
        input = """
        class Rivaldo{
            club : string;
            age : int;
            brazil : boolean;
            var : City;
            final int Constant = 10;
            static final int Imutablel = 10 * 10.2;
            Rivaldo(club : string;age : int ){
                this.age := age;
                this.club := club;
            }
        }
        class City {
            int main (){
                return 2;
            }
        }
        """
        expect = "Type Mismatch In Constant Declaration: ConstDecl(Id(Imutablel),IntType,BinaryOp(*,IntLiteral(10),FloatLiteral(10.2)))"
        self.assertTrue(TestChecker.test(input, expect, 457))

    def test_458(self):
        input = """
        class Rivaldo{
            club : string;
            age : int;
            brazil : boolean;
            var : City;
            final int Constant = 10;
            static final int Imutablel = 10 * 10;
            Rivaldo(club : string;age : int ){
                var : City;
                this.club := var.main();
            }
        }
        class City {
            int main (){
                return 2;
            }
        }
        """
        expect = "Type Mismatch In Statement: AssignStmt(FieldAccess(SelfLiteral(),Id(club)),CallExpr(Id(var),Id(main),List()))"
        self.assertTrue(TestChecker.test(input, expect, 458))

    def test_459(self):
        input = """
        class Rivaldo{
            club : string;
            age : int;
            brazil : boolean;
            final int Constant = 10;
            static final int Imutablel = 10 * 10;
            Rivaldo(club : string;age : int ){
                this.age := age;
                if true then{
                    io.writeStr("kakaka");
                }else{
                    var : string;
                    var := io.readInt();
                }
            }
        }
        """
        expect = "Type Mismatch In Statement: AssignStmt(Id(var),CallExpr(Id(io),Id(readInt),List()))"
        self.assertTrue(TestChecker.test(input, expect, 459))

    def test_460(self):
        input = """
        class Rivaldo{
            club : string;
            age : int;
            brazil : boolean;
            final int Constant = 10;
            static final int Imutablel = 10 * 12;
            Rivaldo(club : string;age : int ){
                this.Constant := 10;
            }
        }
        """
        expect = "Cannot Assign To Constant: AssignStmt(FieldAccess(SelfLiteral(),Id(Constant)),IntLiteral(10))"
        self.assertTrue(TestChecker.test(input, expect, 460))

    def test_461(self):
        input = """
        class Rivaldo{
            final int Constant = 10;
            hehe : int;
            static final int Imutablel = 10 + this.hehe;
        }
        """
        expect = "Not Constant Expression: FieldAccess(SelfLiteral(),Id(hehe))"
        self.assertTrue(TestChecker.test(input, expect, 461))

    def test_462(self):
        input = """
        class Rivaldo{
            final string Constant = "PPL";
            hehe : int;
            static final int Imutablel = 10 + this.Constant;
        }
        """
        expect = "Type Mismatch In Expression: BinaryOp(+,IntLiteral(10),FieldAccess(SelfLiteral(),Id(Constant)))"
        self.assertTrue(TestChecker.test(input, expect, 462))

    def test_463(self):
        input = """
        class Rivaldo{
            final string Constant = "PPL";
            hehe : int;
            var : TaiVuong;
            static final int Imutablel = 10 + this.var.hehe;
        }
        class TaiVuong{
            static final int Constant = 10;
            static hehe : int;
        }
        """
        expect = "Not Constant Expression: FieldAccess(FieldAccess(SelfLiteral(),Id(var)),Id(hehe))"
        self.assertTrue(TestChecker.test(input, expect, 463))

    def test_464(self):
        input = """
        class Rivaldo{
            final string Constant = "PPL";
            hehe : int;
            var : TaiVuong;
            static final int Imutablel = 10 + this.var.Constant;
        }
        class TaiVuong{
            static final int Constant = 10;
            static hehe : int;
            void main(){
                isKhongPhaiSoNguyenTo : boolean;
                var : int;
                i : int;
                var := io.readInt();
                for i:=2 to var do{
                    if var%2==0 then{
                        isKhongPhaiSoNguyenTo := 2;
                        break;
                    }
                }
            }
        }
        """
        expect = "Type Mismatch In Statement: AssignStmt(Id(isKhongPhaiSoNguyenTo),IntLiteral(2))"
        self.assertTrue(TestChecker.test(input, expect, 464))

    def test_465(self):
        input = """
        class Rivaldo{
            final string Constant = "PPL";
            hehe : int;
            var : TaiVuong;
            static final int Imutablel = 10 + this.var.Constant;
        }
        class TaiVuong{
            static final int Constant = 10;
            static hehe : int;
            void main(){
                isKhongPhaiSoNguyenTo : boolean;
                var : int;
                i : int;
                var := io.readInt();
                for i:=2 to var do{
                    if (var%i==0) && (1) then{
                        isKhongPhaiSoNguyenTo := true;
                        break;
                    }
                }
            }
        }
        """
        expect = "Type Mismatch In Expression: BinaryOp(&&,BinaryOp(==,BinaryOp(%,Id(var),Id(i)),IntLiteral(0)),IntLiteral(1))"
        self.assertTrue(TestChecker.test(input, expect, 465))

    def test_466(self):
        input = """
        class Rivaldo{
            final string Constant = "PPL";
            hehe : int;
            static final int Imutablel = -10 + "string";
        }
        """
        expect = "Type Mismatch In Expression: BinaryOp(+,UnaryOp(-,IntLiteral(10)),StringLiteral(string))"
        self.assertTrue(TestChecker.test(input, expect, 466))

    def test_467(self):
        input = """
        class Rivaldo{
            final string Constant = "PPL";
            hehe : int;
            static final int Imutablel = -10 + "string";
        }
        """
        expect = "Type Mismatch In Expression: BinaryOp(+,UnaryOp(-,IntLiteral(10)),StringLiteral(string))"
        self.assertTrue(TestChecker.test(input, expect, 467))

    def test_468(self):
        input = """
        class Rivaldo{
            final string Constant = "PPL";
            hehe : int;
            void main (){
                i : int;
                for i:=0 to 10 do{
                    var : float;
                    var : int;
                }
            }
        }
        """
        expect = "Redeclared Variable: var"
        self.assertTrue(TestChecker.test(input, expect, 468))

    def test_469(self):
        input = """
        class Rivaldo{
            final string Constant = "PPL";
            hehe : int;
            void main (){
                i : int;
                for i:=0 to 10 do{
                    var : float;
                    if 10>0 then{
                        hehe : float;
                        hehe : int;
                    }
                }
            }
        }
        """
        expect = "Redeclared Variable: hehe"
        self.assertTrue(TestChecker.test(input, expect, 469))

    def test_470(self):
        input = """
        class Rivaldo{
            final string Constant = 10;
            hehe : int;
            var : TaiVuong;
            static final int Imutablel = -10 + "string";
        }
        """
        expect = "Type Mismatch In Constant Declaration: ConstDecl(Id(Constant),StringType,IntLiteral(10))"
        self.assertTrue(TestChecker.test(input, expect, 470))

    def test_471(self):
        input = Program([ClassDecl(Id("main"), []),
                         ClassDecl(Id("main"), [])])
        expect = "Redeclared Class: main"
        self.assertTrue(TestChecker.test(input, expect, 471))

    def test_472(self):
        input = """
        class ABC{
            static AB : float;
            static BC : float;
            static CA : float;
            ABC (AB : int;BC : int;CA :int){
                this.AB := AB;
                this.BC := BC;
                this.CA := CA;
            }
        }
        class Test {
            isVuong,isDeu,isCan : boolean;
            instance : ABC;
            void main (){
                ABC := new ABC(3,3,3);
                if (ABC.AB == ABC.BC) && (ABC.BC == ABC.CA) then
                    io.writeStr("Day la tam giac deu");
            }
        }
        """
        expect = "Type Mismatch In Expression: BinaryOp(==,FieldAccess(Id(ABC),Id(AB)),FieldAccess(Id(ABC),Id(BC)))"
        self.assertTrue(TestChecker.test(input, expect, 472))

    def test_472(self):
        input = """
        class ABC{
            static AB : int;
            static BC : int;
            static CA : int;
            ABC (AB : int;BC : int;CA :int){
                this.AB := AB;
                this.BC := BC;
                this.CA := CA;
            }
        }
        class Test {
            isBinhThuong,isDeu,isCan : boolean;
            instance : ABC;
            void main (){
                ABC := new ABC(3,3,3);
                if (ABC.AB == ABC.BC) && (ABC.BC == ABC.CA) then
                    io.writeStr("Day la tam giac deu");
                else{
                    if (ABC.AB == ABC.BC) && (ABC.BC != ABC.CA) then
                        io.writeStr("Day la tam giac can");
                    else
                        io.writeStr();
                }
            }
        }
        """
        expect = "Type Mismatch In Statement: CallStmt(Id(io),Id(writeStr),[])"
        self.assertTrue(TestChecker.test(input, expect, 472))

    def test_473(self):
        input = """
        class ABC{
            static AB : int;
            static BC : int;
            static CA : int;
            ABC (AB : int;BC : int;CA :int){
                this.AB := AB;
                this.BC := BC;
                this.CA := CA;
            }
        }
        class Test {
            isBinhThuong,isDeu,isCan : boolean;
            instance : ABC;
            void main (){
                ABC := new ABC(3,3,3);
                if (ABC.AB == ABC.BC) && (ABC.BC == ABC.CA) then
                    io.writeStr("Day la tam giac deu");
                else{
                    if (ABC.AB == ABC.BC) && (ABC.BC != ABC.CA) then
                        io.writeStr("Day la tam giac can");
                    else{
                        i : int;
                        x : int;
                        x := 10 + 9 -4/4 * 12*(2+3)/ABC.AB-ABC.BC;
                        for i:=100 downto 0 do
                            io.writeStr("Day la tam giac binh thuong");

                    }
                }
            }
        }
        """
        expect = "Type Mismatch In Statement: AssignStmt(Id(x),BinaryOp(-,BinaryOp(-,BinaryOp(+,IntLiteral(10),IntLiteral(9)),BinaryOp(/,BinaryOp(*,BinaryOp(*,BinaryOp(/,IntLiteral(4),IntLiteral(4)),IntLiteral(12)),BinaryOp(+,IntLiteral(2),IntLiteral(3))),FieldAccess(Id(ABC),Id(AB)))),FieldAccess(Id(ABC),Id(BC))))"
        self.assertTrue(TestChecker.test(input, expect, 473))

    def test_474(self):
        input = """
        class ABC{
            static AB : string;
            static BC : string;
            static CA : string;
            ABC (AB : string;BC : string;CA :string){
                this.AB := AB;
                this.BC := BC;
                this.CA := CA;
                if this.AB == this.BC then
                    io.writeStr("oke");
            }
        }
        """
        expect = "Type Mismatch In Expression: BinaryOp(==,FieldAccess(SelfLiteral(),Id(AB)),FieldAccess(SelfLiteral(),Id(BC)))"
        self.assertTrue(TestChecker.test(input, expect, 474))

    def test_475(self):
        input = """
        class ABC{
            static AB : int;
            static final int e = 2;
            static BC : string;
            static CA : string;
            static hehe : ABC;
            static array : ABC[5];
            ABC (AB : int;BC : string;CA :string){
            }
            void main(){
                var : string;
                object : ABC;
                final int var = 1 + object.array[2].hehe.e;
            }
        }
        """
        expect = "Redeclared Constant: var"
        self.assertTrue(TestChecker.test(input, expect, 475))

    def test_476(self):
        input = """
        class ABC{
            void main(){
                x : int;
                y : int;
                z : int;
                for x:=1 to 100 do {
                    for y:=2 to 200 do{
                        for z:=0 downto -100 do{
                            continue;
                        }
                        break;
                    }
                    continue;
                }
                continue;
            }
        }
        """
        expect = "Continue Not In Loop"
        self.assertTrue(TestChecker.test(input, expect, 476))

    def test_477(self):
        input = """
        class Shape {
            length,width:float;
            float getArea() {}
            Shape(length,width:float){
                this.length := length;
                this.width := width;
            }
        }
        class Rectangle extends Shape {
            float getArea(){
                return this.length*this.width;
            }
        }
        class Triangle extends Shape {
            float getArea(){
                return this.length*this.width / 2;
            }
        }
        class Example2 {
            void main(){
                s:Shape;
                e:Rectangle;
                a,c:int;
                b:float[50];
                s := new Rectangle(3,4);
                io.writeFloatLn(s.getArea());
                s := new Triangle(3,4);
                io.writeFloatLn(s.getArea());
                for a:=1 to 100 do{
                    b[a] := 1.1;
                    if a%2 == 0 then{
                        for c:=7 downto 2 do{
                            b[a] := b[a]*c/a;
                        }
                    }
                    else
                        break;
                    if a>20 then {
                        break;
                    }
                }
                continue;
            }
        }
        """
        expect = "Continue Not In Loop"
        self.assertTrue(TestChecker.test(input, expect, 477))

    def test_478(self):
        input = """
        class ABC{
            int main(){
                x : int;
                y : int;
                z : int;
                for x:=1 to 100 do {
                    for y:=2 to 200 do{
                        for z:=0 downto -100 do{
                            continue;
                        }
                        break;
                    }
                    continue;
                }
                return x/y;
            }
        }
        """
        expect = "Type Mismatch In Statement: Return(BinaryOp(/,Id(x),Id(y)))"
        self.assertTrue(TestChecker.test(input, expect, 478))

    def test_479(self):
        input = """
        class ABC{
            int main(){
                i : int;
                for i:=10.2 to 100 do {
                    io.writeStr("hehe");
                }
            }
        }
        """
        expect = "Type Mismatch In Statement: For(Id(i),FloatLiteral(10.2),IntLiteral(100),True,Block(List(),List(CallStmt(Id(io),Id(writeStr),[StringLiteral(hehe)])))])"
        self.assertTrue(TestChecker.test(input, expect, 479))

    def test_480(self):
        input = """
        class ABC{
            int main(){
                i : int;
                for i:=10 to "hellofromVN" do {
                    io.writeStr("hehe");
                }
            }
        }
        """
        expect = "Type Mismatch In Statement: For(Id(i),IntLiteral(10),StringLiteral(hellofromVN),True,Block(List(),List(CallStmt(Id(io),Id(writeStr),[StringLiteral(hehe)])))])"
        self.assertTrue(TestChecker.test(input, expect, 480))

    def test_481(self):
        input = """
        class Example1 {
            int factorial(n:int){
                if n == 0 then return 1; else return n * this.factorial(n - 1);
            }
            void main(){
                x:string;
                x := io.readInt();
                io.writeIntLn(this.factorial(x));
            }
        }
        """
        expect = "Type Mismatch In Statement: AssignStmt(Id(x),CallExpr(Id(io),Id(readInt),List()))"
        self.assertTrue(TestChecker.test(input, expect, 481))

    def test_482(self):
        input = """
        class Example1 {
            int factorial(n:int){
                if n == 0 then return 1; else return n * this.factorial(n - 1);
            }
            void main(){
                x:int;
                x := io.readInt();
                io.writeStr(this.factorial(x));
            }
        }
        """
        expect = "Type Mismatch In Statement: CallStmt(Id(io),Id(writeStr),[CallExpr(SelfLiteral(),Id(factorial),List(Id(x)))])"
        self.assertTrue(TestChecker.test(input, expect, 482))

    def test_483(self):
        input = """
        class Shape {
            static final int numOfShape = 0;
            final int immuAttribute = 0;
            length,width: float;
            int static getNumOfShape() {
                return this.numOfShape;
            }
        }
        class Rectangle extends Shape {
            float getArea(){
                return this.length*this.width;
            }
            int getArea(){}
        }
        """
        expect = "Redeclared Method: getArea"
        self.assertTrue(TestChecker.test(input, expect, 483))

    def test_484(self):
        input = """
        class Shape {
            static final int numOfShape = 0;
            final int immuAttribute = 0;
            length,width: float;
            int static getNumOfShape() {
                return this.numOfShape;
            }
        }
        class Rectangle extends Shape {
            float getArea(){
                return this.length*this.width;
            }
            void main(){
                x : int;
                x := 1+2+"string";
            }
        }
        """
        expect = "Type Mismatch In Expression: BinaryOp(+,BinaryOp(+,IntLiteral(1),IntLiteral(2)),StringLiteral(string))"
        self.assertTrue(TestChecker.test(input, expect, 484))

    def test_485(self):
        input = """
        class Shape {
            static final int numOfShape = 0;
            final int immuAttribute = 0;
            length,width: float;
            int static getNumOfShape() {
                return this.numOfShape;
            }
        }
        class Rectangle extends Shape {
            float getArea(){
                return this.length*this.width;
            }
            void main(){
                var : int;
                var := this.length*this.width;
            }
        }
        """
        expect = "Type Mismatch In Statement: AssignStmt(Id(var),BinaryOp(*,FieldAccess(SelfLiteral(),Id(length)),FieldAccess(SelfLiteral(),Id(width))))"
        self.assertTrue(TestChecker.test(input, expect, 485))

    def test_486(self):
        input = """
        class Shape {
            static final int numOfShape = 0;
            final int immuAttribute = 0;
            length,width: float;
            int static getNumOfShape() {
                var : Shape;
                var := nil + new Shape();
            }
        }
        """
        expect = "Type Mismatch In Expression: BinaryOp(+,NullLiteral(),NewExpr(Id(Shape),List()))"
        self.assertTrue(TestChecker.test(input, expect, 486))

    def test_487(self):
        input = """
        class Shape {
            static final int numOfShape = 0;
            final int immuAttribute = 0;
            length,width: float;
            Shape(length : float;width : float){
                this.length := length;
                this.width := width;
            }
            int static getNumOfShape() {
                var : Shape;
                var := new Shape(2);
            }
        }
        """
        expect = "Type Mismatch In Expression: NewExpr(Id(Shape),List(IntLiteral(2)))"
        self.assertTrue(TestChecker.test(input, expect, 487))

    def test_488(self):
        input = """
        class Shape {
            static final int numOfShape = 0;
            final int immuAttribute = 0;
            length,width: float;
            Shape(length : float;width : float){
                this.length := length;
                this.width := width;
            }
            int static getNumOfShape() {
                var : Shape;
            }
        }
        class Hehe extends Shape{
            Hehe(x : int;y :int;z :int){
                io.writeStr("HelloWord");
            }
        }
        class Kaka extends Hehe{
            variable : Shape;
            void main(){
                this.variable := new Hehe(2,3);
            }
        }
        """
        expect = "Type Mismatch In Expression: NewExpr(Id(Hehe),List(IntLiteral(2),IntLiteral(3)))"
        self.assertTrue(TestChecker.test(input, expect, 488))

    def test_489(self):
        input = """
        class Shape {
            static final int numOfShape = 0;
            final int immuAttribute = 0;
            length,width: float;
            Shape(length : float;width : float){
                this.length := length;
                this.width := width;
            }
            int static getNumOfShape() {
                var : Shape;
            }
        }
        class Hehe extends Shape{
            Hehe(x : int;y :int;z :int){
                io.writeStr("HelloWord");
            }
        }
        class Kaka extends Hehe{
            variable : Shape;
            void main(){
                variable := new Hehe(2,3);
            }
        }
        """
        expect = "Undeclared Identifier: variable"
        self.assertTrue(TestChecker.test(input, expect, 489))

    def test_490(self):
        input = """
        class Shape {
            static final int numOfShape = 0;
            final string immuAttribute = "kcs";
            length,width: float;
            Shape(length : float;width : float){
                this.length := length;
                this.width := width;
            }
            int static getNumOfShape() {
                var : Shape;
            }
        }
        class Hehe extends Shape{
            Hehe(x : int;y :int;z :int){
                io.writeInt(this.immuAttribute);
            }
        }
        class Kaka extends Hehe{
            variable : Shape;
            void main(){
                this.variable := new Hehe(2,3,2);
            }
        }
        """
        expect = "Type Mismatch In Statement: CallStmt(Id(io),Id(writeInt),[FieldAccess(SelfLiteral(),Id(immuAttribute))])"
        self.assertTrue(TestChecker.test(input, expect, 490))

    def test_491(self):
        input = """
        class Shape {
            Shape : int;
            void main (){
                Shape : float;
                Shape : string;
            }
        }
        """
        expect = "Redeclared Variable: Shape"
        self.assertTrue(TestChecker.test(input, expect, 491))

    def test_492(self):
        input = """
        class Shape {
            Shape : int;
            Shape (){
                Shape : float;
                Shape : string;
            }
        }
        """
        expect = "Redeclared Variable: Shape"
        self.assertTrue(TestChecker.test(input, expect, 492))

    def test_493(self):
        input = """
        class TaiVuong {
            Shape : int;
            void Shape (){
                Shape : float;
            }
        }
        """
        expect = "Redeclared Method: Shape"
        self.assertTrue(TestChecker.test(input, expect, 493))

    def test_494(self):
        input = """
        class TaiVuong {
            Shape : int;
            void main(){
                if 1.2 != 3.0 then
                    io.writeInt(2);
            }
        }
        """
        expect = "Type Mismatch In Expression: BinaryOp(!=,FloatLiteral(1.2),FloatLiteral(3.0))"
        self.assertTrue(TestChecker.test(input, expect, 494))

    def test_495(self):
        input = """
        class TaiVuong {
            Shape : int;
            void main(){
                if 1.2 < 3.0 then
                    io.writeInt(2);
                else
                    if true then
                        if true then
                            if true then
                                io.writeInt("Hello");
            }
        }
        """
        expect = "Type Mismatch In Statement: CallStmt(Id(io),Id(writeInt),[StringLiteral(Hello)])"
        self.assertTrue(TestChecker.test(input, expect, 495))

    def test_496(self):
        input = """
        class TaiVuong {
            x : ThanhCong;
            void main(){
                x : int;
                x := this.x.hehe;
            }
        }
        class ThanhCong {
            static hehe : int;
            hehe : float;
        }
        """
        expect = "Redeclared Attribute: hehe"
        self.assertTrue(TestChecker.test(input, expect, 496))

    def test_497(self):
        input = """
        class TaiVuong {
            x : ThanhCong;
            void main(){
                x : int;
                x := this.x.papa;
            }
        }
        class ThanhCong {
            static hehe : int;
            haha : int;
        }
        """
        expect = "Undeclared Attribute: papa"
        self.assertTrue(TestChecker.test(input, expect, 497))

    def test_498(self):
        input = """
        class TaiVuong {
            x : ThanhCong;
            void main(){
                x : int;
                x := this.x.hehe;
                for x:=+10 to --10 do {
                    x := io.writeInt(--1) + 1;
                }
            }
        }
        class ThanhCong {
            static hehe : int;
            haha : int;
        }
        """
        expect = "Type Mismatch In Expression: CallExpr(Id(io),Id(writeInt),List(UnaryOp(-,UnaryOp(-,IntLiteral(1)))))"
        self.assertTrue(TestChecker.test(input, expect, 498))

    def test_499(self):
        input = """
        class TaiVuong {
            x : ThanhCong;
            void main(){
                x : int;
                x := this.x.hehe && true;
            }
        }
        class ThanhCong {
            static hehe : int;
            haha : int;
        }
        """
        expect = "Type Mismatch In Expression: BinaryOp(&&,FieldAccess(FieldAccess(SelfLiteral(),Id(x)),Id(hehe)),BooleanLiteral(True))"
        self.assertTrue(TestChecker.test(input, expect, 499))

    def test_500(self):
        input = """ 
        class TaiVuong extends Messi{
            static a : int;
            b : int;
            void main(){
                x : float;
                final int y = 1;
                object : Messi;
                object := new TaiVuong(2,2);
                x := object.width + object.height;
            }
            int kaka (a : int; b :float; c : string){
                x : int;
                Ronaldo : int;
                for x:=1 to 10 do {
                    kaka: int;
                    kake : float;
                    if true then {
                        io.writeInt(1);
                        x := object.b;
                    }
                } 

            }
        }
        class Ronaldo {
            static a : int;
            hehe : int;
            kaka : string;
            Ronaldo (a : int){
                if this.hehe == 1 then {
                    i : int;
                    for i:=1 to 100 do {
                        if i>=10 then {
                            if i < 20 then {
                                io.writeStr("Done");
                            }
                        }
                    }
                }
            }
        }
        class Messi {
            static final int a = 1;
            static b : string;
            height : int;
            width : float;
            Messi(height : int; width : float){
                this.height := height;
                this.width := width;
            }
            void print (){
                io.writeStr("kakaka");
            }
        }
        """
        expect = "Undeclared Identifier: object"
        self.assertTrue(TestChecker.test(input, expect, 500))
