"""
 * @author nhphung
"""
from AST import *
from Visitor import *
from Utils import Utils
from StaticError import *
from functools import reduce


class MType:
    def __init__(self, partype, rettype):
        self.partype = partype
        self.rettype = rettype


class Symbol:
    def __init__(self, name, mtype, value=None):
        self.name = name
        self.mtype = mtype
        self.value = value

class StaticChecker(BaseVisitor, Utils):
    global_envi = [Symbol("io", ClassType(Id("io"))),
                   Symbol("readInt", MType([], IntType())),
                   Symbol("writeInt", MType([IntType()], VoidType())),
                   Symbol("writeIntLn", MType([IntType()], VoidType())),
                   Symbol("readFloat", MType([], FloatType())),
                   Symbol("writeFloat", MType([FloatType()], VoidType())),
                   Symbol("writeFloatLn", MType([FloatType()], VoidType())),
                   Symbol("readBool", MType([], BoolType())),
                   Symbol("writeBool", MType([BoolType()], VoidType())),
                   Symbol("writeBoolLn", MType([BoolType()], VoidType())),
                   Symbol("readStr", MType([], StringType())),
                   Symbol("writeStr", MType([StringType()], VoidType())),
                   Symbol("writeStrLn", MType([StringType()], VoidType()))
                   ]

    def __init__(self, ast):
        self.ast = ast

    def check(self):
        return self.visit(self.ast, StaticChecker.global_envi)

    def visitProgram(self, ast, c):
        self.global_enviroment = self.global_envi.copy()
        self.tree_AST = ast
        ### all_class in program
        self.all_class = self.allClass(ast.decl,c) + [Symbol("io", ClassType(Id("io")))]
        reduce(lambda x, y: [self.visit(y, x + c)] + x, ast.decl, [])
        return []

    def visitClassDecl(self,ast,c):
        ### check parent name of class have exit
        self.checkHasClass(ast.parentname)
        sym = self.checkRedeclared(Symbol(ast.classname.name,ClassType(Id(ast.classname.name))),Class(),c)
        ### get current Object
        self.thisObject = sym
        ### enviroment for method = element has global scope + all mem of Class
        self.EnvironmentCanAcess = self.getAllMember(ast.memlist) + self.getMemberParent(ast) + self.findAllElementHasGlobalScope()
        reduce(lambda x, y: [self.visit(y, x)] + x , ast.memlist, [])
        return sym

    def visitAttributeDecl(self,ast,c):
        self.isAttribute = True
        sym = self.visit(ast.decl,c)
        self.isAttribute = False
        return sym

    def visitVarDecl(self,ast,c):
        if type(ast.varType) is ClassType:
            self.checkHasClass(ast.varType.classname)
        if type(ast.varType) is ArrayType and type(ast.varType.eleType) is ClassType:
            self.checkHasClass(ast.varType.eleType.classname)
        if self.isAttribute:
            return self.checkRedeclared(Symbol(ast.variable.name, ast.varType), Attribute(), c)
        elif self.isParam:
            return self.checkRedeclared(Symbol(ast.variable.name, ast.varType), Parameter(), c)
        else:
            return self.checkRedeclared(Symbol(ast.variable.name, ast.varType), Variable(), c)
    def visitConstDecl(self,ast,c):
        self.NotConstantExpression = True
        self.isFirst = True
        if not self.isEcceptableType(ast.constType,self.visit(ast.value,c)):
            raise TypeMismatchInConstant(ast)
        self.isFirst = False
        self.NotConstantExpression = False
        if self.isAttribute:
            return self.checkRedeclared(Symbol(ast.constant.name, ast.constType, ast.value), Attribute(), c)
        else:
            return self.checkRedeclared(Symbol(ast.constant.name, ast.constType, ast.value), Constant(), c)

    def visitMethodDecl(self,ast,c):
        if ast.name.name == "<init>":
            sym = self.checkRedeclared(Symbol(ast.name.name,MType([x.varType for x in ast.param],ast.returnType)),SpecialMethod(),c)
        else:
            sym = self.checkRedeclared(Symbol(ast.name.name, MType([x.varType for x in ast.param],ast.returnType)),Method(), c)
        self.isParam = True
        lstParam = reduce(lambda x, y: [self.visit(y, x)] + x, ast.param, [])
        self.isParam = False
        if type(ast.returnType) is ClassType:
            self.checkHasClass(ast.returnType.classname)
        elif type(ast.returnType) is ArrayType and type(ast.returnType.eleType) is ClassType:
            self.checkHasClass(ast.returnType.eleType.classname)
        self.returnTypeMethod = ast.returnType
        envi = self.all_class + lstParam
        self.listPatam = lstParam
        self.isBlockOfMethod = True
        self.visit(ast.body, envi)
        return sym

    def visitBlock(self,ast,c):
        if self.isBlockOfMethod:
            lstVarDecl = reduce(lambda x, y: [self.visit(y, x + self.listPatam)] + x, ast.decl, [])
            self.isBlockOfMethod = False
        else:
            lstVarDecl = reduce(lambda x, y: [self.visit(y, x)] + x , ast.decl, [])
        enviroForBlock = lstVarDecl + c
        for x in ast.stmt:
            self.visit(x, enviroForBlock)

    def visitAssign(self, ast, c):
        if self.LHSisConst(ast,c):
            raise CannotAssignToConstant(ast)
        Left = self.visit(ast.lhs, c)
        Right = self.visit(ast.exp, c)
        if not self.isEcceptableType(Left,Right):
            raise TypeMismatchInStatement(ast)

    def visitIf(self,ast,c):
        if type(self.visit(ast.expr,c)) is not BoolType:
            raise TypeMismatchInStatement(ast)
        self.visit(ast.thenStmt,c)
        if ast.elseStmt:
            self.visit(ast.elseStmt,c)

    def visitFor(self, ast, c):
        if not self.checkScalarVariable(ast.id.name,c):
            raise Undeclared(Identifier(),ast.id.name)
        if any(type(x) is not IntType for x in
               [self.visit(ast.id, c), self.visit(ast.expr1, c), self.visit(ast.expr2, c)]):
            raise TypeMismatchInStatement(ast)
        self.inLoopChecker += 1
        self.visit(ast.loop,c)
        self.inLoopChecker -= 1

    def visitBreak(self, ast, c):
        if self.inLoopChecker <= 0:
            raise BreakNotInLoop()

    def visitContinue(self, ast, c):
        if self.inLoopChecker <=0 :
            raise ContinueNotInLoop()
    def visitReturn(self,ast,c):
        expr = self.visit(ast.expr,c)
        if not self.isEcceptableType(self.returnTypeMethod,expr):
            raise TypeMismatchInStatement(ast)

    def visitCallStmt(self, ast, c):
        if self.NotConstantExpression:
            raise NotConstantExpression(ast)
        self.callObj(ast, c, True, False, False)

    def visitCallExpr(self,ast,c):
        if self.NotConstantExpression and self.isFirst:
            raise NotConstantExpression(ast)
        return self.callObj(ast, c, False, True, False)

    def visitNewExpr(self,ast,c):
        if self.NotConstantExpression and self.isFirst:
            raise NotConstantExpression(ast)
        self.checkHasClass(ast.classname)
        ### check constructor is right
        if not self.checkRightConstructor(ast,c):
            raise TypeMismatchInExpression(ast)
        ### return
        return ClassType(Id(ast.classname.name))

    def visitFieldAccess(self,ast,c):
        if self.isFirst:
            if self.NotConstantExpression and not self.checkIsImutableAttribute(ast):
                raise NotConstantExpression(ast)
            self.isFirst = False
        return self.callObj(ast, c, False, False, True)

    def visitId(self, ast, c):
        idFound = self.lookForElement(ast.name, c, lambda x: x.name)
        if idFound:
            return idFound.mtype
        else:
            raise Undeclared(Identifier(), ast.name)

    def visitArrayCell(self, ast, c):
        if self.NotConstantExpression and self.isFirst:
            raise NotConstantExpression(ast)
        typeArr = self.visit(ast.arr, c)
        if type(typeArr) is not ArrayType:
            raise TypeMismatchInExpression(ast)
        if type(self.visit(ast.idx, c)) is not IntType:
            raise TypeMismatchInExpression(ast)
        return typeArr.eleType

    def visitUnaryOp(self, ast, c):
        bodytype = self.visit(ast.body, c)
        if ast.op == '-' or ast.op == '+':
            if type(bodytype) is IntType:
                return IntType()
            if type(bodytype) is FloatType:
                return FloatType()
        if ast.op == '!' and type(bodytype) is BoolType:
            return BoolType()
        raise TypeMismatchInExpression(ast)

    def visitBinaryOp(self, ast, c):
        typeLeft = self.visit(ast.left, c)
        typeRight = self.visit(ast.right, c)
        if ast.op in ['&&','||','!'] and type(typeLeft) is BoolType and type(typeRight) is BoolType:
            return BoolType()
        if ast.op in ['<', '<=', '>=', '>'] and (
                type(typeLeft) is IntType or type(typeLeft) is FloatType) and (
                type(typeRight) is IntType or type(typeRight) is FloatType):
            return BoolType()
        if ast.op in ['==','!='] and type(typeLeft) is type(typeRight) and type(typeLeft) in [IntType,BoolType]:
            return BoolType()
        if ast.op == "^" and type(typeLeft) is StringType and type(typeRight) is StringType:
            return StringType()
        if ast.op in ['%', '\\'] and type(typeLeft) is IntType and type(typeRight) is IntType:
            return IntType()
        if ast.op in ['+', '-', '*', '/'] and any(type(typeLeft) is x for x in [IntType, FloatType]) and any(
                type(typeRight) is x for x in [IntType, FloatType]):
            if any(type(x) is FloatType for x in [typeLeft, typeRight]):
                return FloatType()
            else:
                if ast.op == '/':
                    return FloatType()
                else:
                    return IntType()
        raise TypeMismatchInExpression(ast)

    def visitBooleanLiteral(self, ast, c):
        return BoolType()

    def visitIntLiteral(self, ast, c):
        return IntType()

    def visitFloatLiteral(self, ast, c):
        return FloatType()

    def visitStringLiteral(self, ast, c):
        return StringType()

    def visitNullLiteral(self, ast, c):
        return ClassType(Id("NIL"))

    def visitSelfLiteral(self, ast, c):
        return ClassType(Id(self.thisObject.name))

    global_enviroment = []
    all_class = []
    returnTypeMethod = VoidType()
    EnvironmentCanAcess = []
    isBlockOfMethod = False
    listPatam = []
    thisObject = None
    isFirst = False
    isAttribute = False
    isParam = False
    isLhs = False
    NotConstantExpression = False
    inLoopChecker = 0
    tree_AST = None


    def checkRedeclared(self, sym, kind, envi):
        if self.lookForElement(sym.name, envi, lambda x: x.name):
            if self.isAttribute:
                raise Redeclared(Attribute(), sym.name)
            else:
                raise Redeclared(kind, sym.name)
        else:
            return sym


    def lookForElement(self, name, lst, func):
        for x in lst:
            if name == func(x):
                return x
        return None

    def findAllElementHasGlobalScope(self):
        tempList = []
        for x in self.tree_AST.decl:
            tempList += [Symbol(x.classname.name, ClassType(Id(x.classname.name)))]
            for y in x.memlist:
                if type(y) is MethodDecl:
                    tempList += [Symbol(y.name.name,MType([x.varType for x in y.param],y.returnType))]
                else :
                    if str(y.sikind) == "Static" and type(y.decl) is ConstDecl:
                        tempList += [Symbol(y.decl.constant.name,y.decl.constType,y.decl.value)]
                    elif str(y.sikind) == "Static" and type(y.decl) is VarDecl:
                        tempList += [Symbol(y.decl.variable.name,y.decl.varType)]
        return  tempList

    def getMemberParent(self,ast):
        memberOFparent = []
        if ast.parentname:
            nameParent = ast.parentname.name
            while nameParent :
                for class_mem in self.tree_AST.decl:
                    if nameParent == class_mem.classname.name:
                        if len(class_mem.memlist) > 0:
                            memberOFparent += self.getAllMember(class_mem.memlist)
                        if class_mem.parentname:
                            nameParent = class_mem.parentname.name
                        else:
                            nameParent = None

        return memberOFparent

    def getAllMember(self,ast):
        tempList = []
        for mem in ast:
            if type(mem) is MethodDecl:
                tempList += [Symbol(mem.name.name, MType([z.varType for z in mem.param], mem.returnType))]
            else:
                if type(mem.decl) is ConstDecl:
                    tempList += [Symbol(mem.decl.constant.name, mem.decl.constType, mem.decl.value)]
                else:
                    tempList += [Symbol(mem.decl.variable.name, mem.decl.varType)]
        return tempList

    def checkScalarVariable(self,name,c):
        for x in c:
            if x.name == name and type(x.mtype) is IntType:
                return True
        return False

    def allClass(self,ast,c):
        tempAll = []
        for x in ast:
            tempAll += [Symbol(x.classname.name, ClassType(Id(x.classname.name)))]
        return tempAll

    def checkHasClass(self,name):
        if name:
            check = False
            for x in self.all_class:
                if x.name == name.name:
                    check = True
            if not check:
                raise Undeclared(Class(),name.name)

    def isEcceptableType(self,Left,Right):
        if type(Left) is VoidType:
            return False
        if (type(Left) is type(Right) and type(Left) not in [ClassType,ArrayType]) or (type(Left) is FloatType and type(Right) is IntType):
            return True
        if type(Left) is type(Right) and type(Left) is ArrayType and Left.size == Right.size:
            return True
        if type(Left) is ClassType and type(Right) is ClassType:
            if Left.classname.name == Right.classname.name:
                return True
            elif Right.classname.name == "NIL":
                return True
            elif self.isSubToSupper(Left,Right):
                return True
        return False

    def checkIsImutableAttribute(self,ast):
        name = ast.fieldname.name
        for x in self.EnvironmentCanAcess:
            if x.name == name and x.value and type(x.mtype) is not MType:
                return True
        return False

    def isSubToSupper(self,Left,Right):
        parent= Left.classname.name
        child = Right.classname.name
        for x in self.tree_AST.decl:
            if x.parentname:
                if x.parentname.name == parent and x.classname.name == child:
                    return True
                elif x.classname.name == child:
                    child = x.parentname.name
        return False

    def getAttributeFromEnviroment(self, typeObj, name, isGetType, isGetReturnType, isGetParam):
        returnResult = None
        nameObject = typeObj.classname.name
        if nameObject == "io":
            for x in self.global_enviroment:
                if x.name == name and type(x.mtype) is MType:
                    if isGetReturnType:
                        returnResult = x.mtype.rettype
                        break
                    elif isGetParam:
                        returnResult = [y for y in x.mtype.partype]
                        break
        else:
            for x in self.tree_AST.decl:
                if x.classname.name == nameObject:
                    for y in self.getAllMember(x.memlist) + self.getMemberParent(x):
                        if isGetType:
                            if y.name == name :
                                returnResult = y.mtype
                                break
                        elif isGetReturnType:
                            if y.name == name :
                                returnResult = y.mtype.rettype
                                break
                        elif isGetParam :
                            if y.name == name :
                                returnResult = y.mtype.partype
                                break
        return returnResult

    def checkClassHasElement(self,typeObj, name, isMethodCheck):
        check = False
        nameObject = typeObj.classname.name
        if nameObject == "io":
            if isMethodCheck:
                for x in self.global_enviroment:
                    if x.name == name and type(x.mtype) is MType:
                        check = True
                        break
        else:
            for x in self.tree_AST.decl:
                if x.classname.name == nameObject:
                    for y in self.getAllMember(x.memlist) + self.getMemberParent(x):
                        if isMethodCheck:
                            if type(y.mtype) is MType and y.name == name :
                                check = True
                                break
                        else:
                            if type(y.mtype) is not MType:
                                if y.name == name :
                                    if self.isLhs and y.value:
                                        self.isLhs = False
                                    check = True
                                    break

        return check

    def checkRightConstructor(self,ast,c):
        listElement = []
        for x in self.tree_AST.decl:
            if x.classname.name == ast.classname.name:
                listElement = self.getAllMember(x.memlist) + self.getMemberParent(x)
        init = None
        for x in listElement:
            if x.name == "<init>":
                init = x
                break
        parameter = []
        argument = []
        if init:
            for x in x.mtype.partype:
                parameter.append(x)
            for y in ast.param:
                argument.append(self.visit(y,c))
            if len(parameter) != len(argument):
                return False
            else:
                for i in range(len(parameter)):
                    if not self.isEcceptableType(parameter[i], argument[i]):
                        return False
        else:
            if len(ast.param) != 0:
                return False
        return True

    def checkIsClassName(self,name,c):
        num = 0
        for x in c:
            if x.name == name:
                num = num + 1
        if num == 1:
            return True
        else:
            return False

    def checkStatic(self,nameObject,name):
        for x in self.tree_AST.decl:
            if x.classname.name == nameObject:
                for y in x.memlist:
                    if type(y.sikind)is Static:
                        if type(y) is AttributeDecl and type(y.decl) is VarDecl and y.decl.variable.name == name:
                            return True
                        if type(y) is AttributeDecl and type(y.decl) is ConstDecl and y.decl.constant.name == name:
                            return True
                        elif type(y) is MethodDecl and y.name.name == name:
                            return True
        return False

    def LHSisConst(self,ast,c):
        if type(ast.lhs) is Id:
            for x in c:
                if x.name == ast.lhs.name and x.value:
                    return True
        elif type(ast.lhs) is FieldAccess:
            self.isLhs = True
            self.visit(ast.lhs,c)
            if not self.isLhs:
                return True
            self.isLhs = True
        return False

    def callObj(self,ast,c,isCallStmt,isCallExpr,isFieldAcess):
        returntype = None
        ### check static attribute
        if type(ast.obj) is Id and ast.obj.name != "io" and self.checkIsClassName(ast.obj.name, c) and ast.obj.name in [x.name for x in self.all_class]:
                if (isCallExpr or isCallStmt) and not self.checkStatic(ast.obj.name, ast.method.name):
                    raise Undeclared(Method(), ast.method.name)
                elif isFieldAcess and not self.checkStatic(ast.obj.name, ast.fieldname.name):
                    raise Undeclared(Attribute(), ast.fieldname.name)
        ### check a.b() a is class ?
        typeObj = self.visit(ast.obj,c)
        if type(typeObj) is not ClassType:
            if isCallStmt:
                raise TypeMismatchInStatement(ast)
            else:
                raise TypeMismatchInExpression(ast)
        ### check a.b() b is method ?
        if isCallExpr or isCallStmt:
            if not self.checkClassHasElement(typeObj, ast.method.name, True):
                raise Undeclared(Method(), ast.method.name)

            ### check parameter and argument
            parameter = self.getAttributeFromEnviroment(typeObj, ast.method.name, False, False, True)
            parameterActual = []
            for x in ast.param:
                parameterActual.append(self.visit(x, c))
            if len(parameter) != len(parameterActual):
                raise TypeMismatchInStatement(ast)
            else:
                for i in range(len(parameter)):
                    if not self.isEcceptableType(parameter[i], parameterActual[i]):
                        if isCallStmt:
                            raise TypeMismatchInStatement(ast)
                        else:
                            raise TypeMismatchInExpression(ast)
            if isCallExpr:
                returntype = self.getAttributeFromEnviroment(typeObj, ast.method.name, False, True, False)
        ### if it is CallExpr , it must has returnType is not VoidType
        if isCallExpr:
            if type(self.getAttributeFromEnviroment(typeObj, ast.method.name, False, True, False)) is VoidType:
                raise TypeMismatchInExpression(ast)
        ### if it is CallStmt , it must has returnType is VoidType
        if isCallStmt:
            if type(self.getAttributeFromEnviroment(typeObj, ast.method.name, False, True, False)) is not VoidType:
                raise TypeMismatchInStatement(ast)

        if isFieldAcess:
            if not self.checkClassHasElement(typeObj, ast.fieldname.name, False):
                raise Undeclared(Attribute(), ast.fieldname.name)
            returntype = self.getAttributeFromEnviroment(typeObj, ast.fieldname.name, True, False, False)

        return returntype



















