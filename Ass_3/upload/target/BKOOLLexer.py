# Generated from main/bkool/parser/BKOOL.g4 by ANTLR 4.7.2
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys


from lexererr import *



def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2C")
        buf.write("\u01c1\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7")
        buf.write("\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r")
        buf.write("\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23")
        buf.write("\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30")
        buf.write("\4\31\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36")
        buf.write("\t\36\4\37\t\37\4 \t \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%")
        buf.write("\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4,\t,\4-\t-\4.")
        buf.write("\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64")
        buf.write("\t\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:")
        buf.write("\4;\t;\4<\t<\4=\t=\4>\t>\4?\t?\4@\t@\4A\tA\4B\tB\3\2\3")
        buf.write("\2\5\2\u0088\n\2\3\3\3\3\3\3\3\3\7\3\u008e\n\3\f\3\16")
        buf.write("\3\u0091\13\3\3\3\3\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4\7\4")
        buf.write("\u009c\n\4\f\4\16\4\u009f\13\4\3\4\3\4\3\5\3\5\3\5\3\5")
        buf.write("\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\7\3")
        buf.write("\7\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\t")
        buf.write("\3\n\3\n\3\n\3\n\3\n\3\n\3\13\3\13\3\13\3\13\3\f\3\f\3")
        buf.write("\f\3\f\3\f\3\r\3\r\3\r\3\r\3\r\3\r\3\16\3\16\3\16\3\17")
        buf.write("\3\17\3\17\3\17\3\17\3\17\3\17\3\20\3\20\3\20\3\20\3\20")
        buf.write("\3\20\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\22")
        buf.write("\3\22\3\22\3\22\3\23\3\23\3\23\3\23\3\23\3\24\3\24\3\24")
        buf.write("\3\24\3\24\3\24\3\24\3\25\3\25\3\25\3\26\3\26\3\26\3\26")
        buf.write("\3\27\3\27\3\27\3\27\3\27\3\27\3\30\3\30\3\30\3\31\3\31")
        buf.write("\3\31\3\31\3\31\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\33")
        buf.write("\3\33\3\33\3\33\3\33\3\34\3\34\3\34\3\34\3\34\3\34\3\34")
        buf.write("\3\35\3\35\3\36\3\36\3\36\3\37\3\37\3 \3 \3 \3!\3!\3!")
        buf.write("\3\"\3\"\3\"\3#\3#\3#\3$\3$\3$\3%\3%\3&\3&\3\'\3\'\3(")
        buf.write("\3(\3)\3)\3*\3*\3+\3+\3,\3,\3-\3-\3.\3.\3.\3/\3/\3\60")
        buf.write("\3\60\3\61\3\61\3\62\3\62\3\63\3\63\3\64\3\64\3\65\3\65")
        buf.write("\3\66\3\66\3\67\3\67\38\38\39\69\u0168\n9\r9\169\u0169")
        buf.write("\39\39\69\u016e\n9\r9\169\u016f\59\u0172\n9\39\59\u0175")
        buf.write("\n9\39\69\u0178\n9\r9\169\u0179\39\39\59\u017e\n9\3:\6")
        buf.write(":\u0181\n:\r:\16:\u0182\3;\3;\3;\3;\7;\u0189\n;\f;\16")
        buf.write(";\u018c\13;\3;\3;\3;\3<\3<\7<\u0193\n<\f<\16<\u0196\13")
        buf.write("<\3=\3=\3>\3>\5>\u019c\n>\3>\6>\u019f\n>\r>\16>\u01a0")
        buf.write("\3?\6?\u01a4\n?\r?\16?\u01a5\3?\3?\3@\3@\3@\3@\7@\u01ae")
        buf.write("\n@\f@\16@\u01b1\13@\3@\3@\3A\3A\7A\u01b7\nA\fA\16A\u01ba")
        buf.write("\13A\3A\3A\3A\3B\3B\3B\4\u008f\u01b8\2C\3\3\5\4\7\5\t")
        buf.write("\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17\35\20")
        buf.write("\37\21!\22#\23%\24\'\25)\26+\27-\30/\31\61\32\63\33\65")
        buf.write("\34\67\359\36;\37= ?!A\"C#E$G%I&K\'M(O)Q*S+U,W-Y.[/]\60")
        buf.write("_\61a\62c\63e\64g\65i\66k\67m8o9q:s;u<w=y>{?}@\177A\u0081")
        buf.write("B\u0083C\3\2\13\4\2\f\f\16\17\n\2$$))^^ddhhppttvv\7\2")
        buf.write("\n\f\16\17$$))^^\5\2C\\aac|\6\2\62;C\\aac|\3\2\62;\4\2")
        buf.write("GGgg\4\2--//\5\2\13\f\16\17\"\"\2\u01d3\2\3\3\2\2\2\2")
        buf.write("\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3")
        buf.write("\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2")
        buf.write("\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2")
        buf.write("\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3")
        buf.write("\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2/\3\2\2\2\2\61")
        buf.write("\3\2\2\2\2\63\3\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2\29\3\2")
        buf.write("\2\2\2;\3\2\2\2\2=\3\2\2\2\2?\3\2\2\2\2A\3\2\2\2\2C\3")
        buf.write("\2\2\2\2E\3\2\2\2\2G\3\2\2\2\2I\3\2\2\2\2K\3\2\2\2\2M")
        buf.write("\3\2\2\2\2O\3\2\2\2\2Q\3\2\2\2\2S\3\2\2\2\2U\3\2\2\2\2")
        buf.write("W\3\2\2\2\2Y\3\2\2\2\2[\3\2\2\2\2]\3\2\2\2\2_\3\2\2\2")
        buf.write("\2a\3\2\2\2\2c\3\2\2\2\2e\3\2\2\2\2g\3\2\2\2\2i\3\2\2")
        buf.write("\2\2k\3\2\2\2\2m\3\2\2\2\2o\3\2\2\2\2q\3\2\2\2\2s\3\2")
        buf.write("\2\2\2u\3\2\2\2\2w\3\2\2\2\2y\3\2\2\2\2{\3\2\2\2\2}\3")
        buf.write("\2\2\2\2\177\3\2\2\2\2\u0081\3\2\2\2\2\u0083\3\2\2\2\3")
        buf.write("\u0087\3\2\2\2\5\u0089\3\2\2\2\7\u0097\3\2\2\2\t\u00a2")
        buf.write("\3\2\2\2\13\u00aa\3\2\2\2\r\u00b2\3\2\2\2\17\u00b7\3\2")
        buf.write("\2\2\21\u00bb\3\2\2\2\23\u00c1\3\2\2\2\25\u00c7\3\2\2")
        buf.write("\2\27\u00cb\3\2\2\2\31\u00d0\3\2\2\2\33\u00d6\3\2\2\2")
        buf.write("\35\u00d9\3\2\2\2\37\u00e0\3\2\2\2!\u00e6\3\2\2\2#\u00ef")
        buf.write("\3\2\2\2%\u00f3\3\2\2\2\'\u00f8\3\2\2\2)\u00ff\3\2\2\2")
        buf.write("+\u0102\3\2\2\2-\u0106\3\2\2\2/\u010c\3\2\2\2\61\u010f")
        buf.write("\3\2\2\2\63\u0114\3\2\2\2\65\u011b\3\2\2\2\67\u0120\3")
        buf.write("\2\2\29\u0127\3\2\2\2;\u0129\3\2\2\2=\u012c\3\2\2\2?\u012e")
        buf.write("\3\2\2\2A\u0131\3\2\2\2C\u0134\3\2\2\2E\u0137\3\2\2\2")
        buf.write("G\u013a\3\2\2\2I\u013d\3\2\2\2K\u013f\3\2\2\2M\u0141\3")
        buf.write("\2\2\2O\u0143\3\2\2\2Q\u0145\3\2\2\2S\u0147\3\2\2\2U\u0149")
        buf.write("\3\2\2\2W\u014b\3\2\2\2Y\u014d\3\2\2\2[\u014f\3\2\2\2")
        buf.write("]\u0152\3\2\2\2_\u0154\3\2\2\2a\u0156\3\2\2\2c\u0158\3")
        buf.write("\2\2\2e\u015a\3\2\2\2g\u015c\3\2\2\2i\u015e\3\2\2\2k\u0160")
        buf.write("\3\2\2\2m\u0162\3\2\2\2o\u0164\3\2\2\2q\u017d\3\2\2\2")
        buf.write("s\u0180\3\2\2\2u\u0184\3\2\2\2w\u0190\3\2\2\2y\u0197\3")
        buf.write("\2\2\2{\u0199\3\2\2\2}\u01a3\3\2\2\2\177\u01a9\3\2\2\2")
        buf.write("\u0081\u01b4\3\2\2\2\u0083\u01be\3\2\2\2\u0085\u0088\5")
        buf.write("%\23\2\u0086\u0088\5-\27\2\u0087\u0085\3\2\2\2\u0087\u0086")
        buf.write("\3\2\2\2\u0088\4\3\2\2\2\u0089\u008a\7\61\2\2\u008a\u008b")
        buf.write("\7,\2\2\u008b\u008f\3\2\2\2\u008c\u008e\13\2\2\2\u008d")
        buf.write("\u008c\3\2\2\2\u008e\u0091\3\2\2\2\u008f\u0090\3\2\2\2")
        buf.write("\u008f\u008d\3\2\2\2\u0090\u0092\3\2\2\2\u0091\u008f\3")
        buf.write("\2\2\2\u0092\u0093\7,\2\2\u0093\u0094\7\61\2\2\u0094\u0095")
        buf.write("\3\2\2\2\u0095\u0096\b\3\2\2\u0096\6\3\2\2\2\u0097\u0098")
        buf.write("\7\'\2\2\u0098\u0099\7\'\2\2\u0099\u009d\3\2\2\2\u009a")
        buf.write("\u009c\n\2\2\2\u009b\u009a\3\2\2\2\u009c\u009f\3\2\2\2")
        buf.write("\u009d\u009b\3\2\2\2\u009d\u009e\3\2\2\2\u009e\u00a0\3")
        buf.write("\2\2\2\u009f\u009d\3\2\2\2\u00a0\u00a1\b\4\2\2\u00a1\b")
        buf.write("\3\2\2\2\u00a2\u00a3\7d\2\2\u00a3\u00a4\7q\2\2\u00a4\u00a5")
        buf.write("\7q\2\2\u00a5\u00a6\7n\2\2\u00a6\u00a7\7g\2\2\u00a7\u00a8")
        buf.write("\7c\2\2\u00a8\u00a9\7p\2\2\u00a9\n\3\2\2\2\u00aa\u00ab")
        buf.write("\7g\2\2\u00ab\u00ac\7z\2\2\u00ac\u00ad\7v\2\2\u00ad\u00ae")
        buf.write("\7g\2\2\u00ae\u00af\7p\2\2\u00af\u00b0\7f\2\2\u00b0\u00b1")
        buf.write("\7u\2\2\u00b1\f\3\2\2\2\u00b2\u00b3\7v\2\2\u00b3\u00b4")
        buf.write("\7j\2\2\u00b4\u00b5\7g\2\2\u00b5\u00b6\7p\2\2\u00b6\16")
        buf.write("\3\2\2\2\u00b7\u00b8\7p\2\2\u00b8\u00b9\7k\2\2\u00b9\u00ba")
        buf.write("\7n\2\2\u00ba\20\3\2\2\2\u00bb\u00bc\7d\2\2\u00bc\u00bd")
        buf.write("\7t\2\2\u00bd\u00be\7g\2\2\u00be\u00bf\7c\2\2\u00bf\u00c0")
        buf.write("\7m\2\2\u00c0\22\3\2\2\2\u00c1\u00c2\7h\2\2\u00c2\u00c3")
        buf.write("\7n\2\2\u00c3\u00c4\7q\2\2\u00c4\u00c5\7c\2\2\u00c5\u00c6")
        buf.write("\7v\2\2\u00c6\24\3\2\2\2\u00c7\u00c8\7h\2\2\u00c8\u00c9")
        buf.write("\7q\2\2\u00c9\u00ca\7t\2\2\u00ca\26\3\2\2\2\u00cb\u00cc")
        buf.write("\7v\2\2\u00cc\u00cd\7j\2\2\u00cd\u00ce\7k\2\2\u00ce\u00cf")
        buf.write("\7u\2\2\u00cf\30\3\2\2\2\u00d0\u00d1\7e\2\2\u00d1\u00d2")
        buf.write("\7n\2\2\u00d2\u00d3\7c\2\2\u00d3\u00d4\7u\2\2\u00d4\u00d5")
        buf.write("\7u\2\2\u00d5\32\3\2\2\2\u00d6\u00d7\7k\2\2\u00d7\u00d8")
        buf.write("\7h\2\2\u00d8\34\3\2\2\2\u00d9\u00da\7t\2\2\u00da\u00db")
        buf.write("\7g\2\2\u00db\u00dc\7v\2\2\u00dc\u00dd\7w\2\2\u00dd\u00de")
        buf.write("\7t\2\2\u00de\u00df\7p\2\2\u00df\36\3\2\2\2\u00e0\u00e1")
        buf.write("\7h\2\2\u00e1\u00e2\7k\2\2\u00e2\u00e3\7p\2\2\u00e3\u00e4")
        buf.write("\7c\2\2\u00e4\u00e5\7n\2\2\u00e5 \3\2\2\2\u00e6\u00e7")
        buf.write("\7e\2\2\u00e7\u00e8\7q\2\2\u00e8\u00e9\7p\2\2\u00e9\u00ea")
        buf.write("\7v\2\2\u00ea\u00eb\7k\2\2\u00eb\u00ec\7p\2\2\u00ec\u00ed")
        buf.write("\7w\2\2\u00ed\u00ee\7g\2\2\u00ee\"\3\2\2\2\u00ef\u00f0")
        buf.write("\7k\2\2\u00f0\u00f1\7p\2\2\u00f1\u00f2\7v\2\2\u00f2$\3")
        buf.write("\2\2\2\u00f3\u00f4\7v\2\2\u00f4\u00f5\7t\2\2\u00f5\u00f6")
        buf.write("\7w\2\2\u00f6\u00f7\7g\2\2\u00f7&\3\2\2\2\u00f8\u00f9")
        buf.write("\7u\2\2\u00f9\u00fa\7v\2\2\u00fa\u00fb\7c\2\2\u00fb\u00fc")
        buf.write("\7v\2\2\u00fc\u00fd\7k\2\2\u00fd\u00fe\7e\2\2\u00fe(\3")
        buf.write("\2\2\2\u00ff\u0100\7f\2\2\u0100\u0101\7q\2\2\u0101*\3")
        buf.write("\2\2\2\u0102\u0103\7p\2\2\u0103\u0104\7g\2\2\u0104\u0105")
        buf.write("\7y\2\2\u0105,\3\2\2\2\u0106\u0107\7h\2\2\u0107\u0108")
        buf.write("\7c\2\2\u0108\u0109\7n\2\2\u0109\u010a\7u\2\2\u010a\u010b")
        buf.write("\7g\2\2\u010b.\3\2\2\2\u010c\u010d\7v\2\2\u010d\u010e")
        buf.write("\7q\2\2\u010e\60\3\2\2\2\u010f\u0110\7g\2\2\u0110\u0111")
        buf.write("\7n\2\2\u0111\u0112\7u\2\2\u0112\u0113\7g\2\2\u0113\62")
        buf.write("\3\2\2\2\u0114\u0115\7u\2\2\u0115\u0116\7v\2\2\u0116\u0117")
        buf.write("\7t\2\2\u0117\u0118\7k\2\2\u0118\u0119\7p\2\2\u0119\u011a")
        buf.write("\7i\2\2\u011a\64\3\2\2\2\u011b\u011c\7x\2\2\u011c\u011d")
        buf.write("\7q\2\2\u011d\u011e\7k\2\2\u011e\u011f\7f\2\2\u011f\66")
        buf.write("\3\2\2\2\u0120\u0121\7f\2\2\u0121\u0122\7q\2\2\u0122\u0123")
        buf.write("\7y\2\2\u0123\u0124\7p\2\2\u0124\u0125\7v\2\2\u0125\u0126")
        buf.write("\7q\2\2\u01268\3\2\2\2\u0127\u0128\7>\2\2\u0128:\3\2\2")
        buf.write("\2\u0129\u012a\7>\2\2\u012a\u012b\7?\2\2\u012b<\3\2\2")
        buf.write("\2\u012c\u012d\7@\2\2\u012d>\3\2\2\2\u012e\u012f\7@\2")
        buf.write("\2\u012f\u0130\7?\2\2\u0130@\3\2\2\2\u0131\u0132\7#\2")
        buf.write("\2\u0132\u0133\7?\2\2\u0133B\3\2\2\2\u0134\u0135\7?\2")
        buf.write("\2\u0135\u0136\7?\2\2\u0136D\3\2\2\2\u0137\u0138\7~\2")
        buf.write("\2\u0138\u0139\7~\2\2\u0139F\3\2\2\2\u013a\u013b\7(\2")
        buf.write("\2\u013b\u013c\7(\2\2\u013cH\3\2\2\2\u013d\u013e\7-\2")
        buf.write("\2\u013eJ\3\2\2\2\u013f\u0140\7/\2\2\u0140L\3\2\2\2\u0141")
        buf.write("\u0142\7,\2\2\u0142N\3\2\2\2\u0143\u0144\7\61\2\2\u0144")
        buf.write("P\3\2\2\2\u0145\u0146\7^\2\2\u0146R\3\2\2\2\u0147\u0148")
        buf.write("\7\'\2\2\u0148T\3\2\2\2\u0149\u014a\7`\2\2\u014aV\3\2")
        buf.write("\2\2\u014b\u014c\7#\2\2\u014cX\3\2\2\2\u014d\u014e\7<")
        buf.write("\2\2\u014eZ\3\2\2\2\u014f\u0150\7<\2\2\u0150\u0151\7?")
        buf.write("\2\2\u0151\\\3\2\2\2\u0152\u0153\7?\2\2\u0153^\3\2\2\2")
        buf.write("\u0154\u0155\7]\2\2\u0155`\3\2\2\2\u0156\u0157\7_\2\2")
        buf.write("\u0157b\3\2\2\2\u0158\u0159\7*\2\2\u0159d\3\2\2\2\u015a")
        buf.write("\u015b\7+\2\2\u015bf\3\2\2\2\u015c\u015d\7}\2\2\u015d")
        buf.write("h\3\2\2\2\u015e\u015f\7\177\2\2\u015fj\3\2\2\2\u0160\u0161")
        buf.write("\7=\2\2\u0161l\3\2\2\2\u0162\u0163\7.\2\2\u0163n\3\2\2")
        buf.write("\2\u0164\u0165\7\60\2\2\u0165p\3\2\2\2\u0166\u0168\5y")
        buf.write("=\2\u0167\u0166\3\2\2\2\u0168\u0169\3\2\2\2\u0169\u0167")
        buf.write("\3\2\2\2\u0169\u016a\3\2\2\2\u016a\u016b\3\2\2\2\u016b")
        buf.write("\u0171\7\60\2\2\u016c\u016e\5y=\2\u016d\u016c\3\2\2\2")
        buf.write("\u016e\u016f\3\2\2\2\u016f\u016d\3\2\2\2\u016f\u0170\3")
        buf.write("\2\2\2\u0170\u0172\3\2\2\2\u0171\u016d\3\2\2\2\u0171\u0172")
        buf.write("\3\2\2\2\u0172\u0174\3\2\2\2\u0173\u0175\5{>\2\u0174\u0173")
        buf.write("\3\2\2\2\u0174\u0175\3\2\2\2\u0175\u017e\3\2\2\2\u0176")
        buf.write("\u0178\5y=\2\u0177\u0176\3\2\2\2\u0178\u0179\3\2\2\2\u0179")
        buf.write("\u0177\3\2\2\2\u0179\u017a\3\2\2\2\u017a\u017b\3\2\2\2")
        buf.write("\u017b\u017c\5{>\2\u017c\u017e\3\2\2\2\u017d\u0167\3\2")
        buf.write("\2\2\u017d\u0177\3\2\2\2\u017er\3\2\2\2\u017f\u0181\5")
        buf.write("y=\2\u0180\u017f\3\2\2\2\u0181\u0182\3\2\2\2\u0182\u0180")
        buf.write("\3\2\2\2\u0182\u0183\3\2\2\2\u0183t\3\2\2\2\u0184\u018a")
        buf.write("\7$\2\2\u0185\u0186\7^\2\2\u0186\u0189\t\3\2\2\u0187\u0189")
        buf.write("\n\4\2\2\u0188\u0185\3\2\2\2\u0188\u0187\3\2\2\2\u0189")
        buf.write("\u018c\3\2\2\2\u018a\u0188\3\2\2\2\u018a\u018b\3\2\2\2")
        buf.write("\u018b\u018d\3\2\2\2\u018c\u018a\3\2\2\2\u018d\u018e\7")
        buf.write("$\2\2\u018e\u018f\b;\3\2\u018fv\3\2\2\2\u0190\u0194\t")
        buf.write("\5\2\2\u0191\u0193\t\6\2\2\u0192\u0191\3\2\2\2\u0193\u0196")
        buf.write("\3\2\2\2\u0194\u0192\3\2\2\2\u0194\u0195\3\2\2\2\u0195")
        buf.write("x\3\2\2\2\u0196\u0194\3\2\2\2\u0197\u0198\t\7\2\2\u0198")
        buf.write("z\3\2\2\2\u0199\u019b\t\b\2\2\u019a\u019c\t\t\2\2\u019b")
        buf.write("\u019a\3\2\2\2\u019b\u019c\3\2\2\2\u019c\u019e\3\2\2\2")
        buf.write("\u019d\u019f\5y=\2\u019e\u019d\3\2\2\2\u019f\u01a0\3\2")
        buf.write("\2\2\u01a0\u019e\3\2\2\2\u01a0\u01a1\3\2\2\2\u01a1|\3")
        buf.write("\2\2\2\u01a2\u01a4\t\n\2\2\u01a3\u01a2\3\2\2\2\u01a4\u01a5")
        buf.write("\3\2\2\2\u01a5\u01a3\3\2\2\2\u01a5\u01a6\3\2\2\2\u01a6")
        buf.write("\u01a7\3\2\2\2\u01a7\u01a8\b?\2\2\u01a8~\3\2\2\2\u01a9")
        buf.write("\u01af\7$\2\2\u01aa\u01ab\7^\2\2\u01ab\u01ae\t\3\2\2\u01ac")
        buf.write("\u01ae\n\4\2\2\u01ad\u01aa\3\2\2\2\u01ad\u01ac\3\2\2\2")
        buf.write("\u01ae\u01b1\3\2\2\2\u01af\u01ad\3\2\2\2\u01af\u01b0\3")
        buf.write("\2\2\2\u01b0\u01b2\3\2\2\2\u01b1\u01af\3\2\2\2\u01b2\u01b3")
        buf.write("\b@\4\2\u01b3\u0080\3\2\2\2\u01b4\u01b8\7$\2\2\u01b5\u01b7")
        buf.write("\13\2\2\2\u01b6\u01b5\3\2\2\2\u01b7\u01ba\3\2\2\2\u01b8")
        buf.write("\u01b9\3\2\2\2\u01b8\u01b6\3\2\2\2\u01b9\u01bb\3\2\2\2")
        buf.write("\u01ba\u01b8\3\2\2\2\u01bb\u01bc\t\4\2\2\u01bc\u01bd\b")
        buf.write("A\5\2\u01bd\u0082\3\2\2\2\u01be\u01bf\13\2\2\2\u01bf\u01c0")
        buf.write("\bB\6\2\u01c0\u0084\3\2\2\2\26\2\u0087\u008f\u009d\u0169")
        buf.write("\u016f\u0171\u0174\u0179\u017d\u0182\u0188\u018a\u0194")
        buf.write("\u019b\u01a0\u01a5\u01ad\u01af\u01b8\7\b\2\2\3;\2\3@\3")
        buf.write("\3A\4\3B\5")
        return buf.getvalue()


class BKOOLLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    BOOLLIT = 1
    BLOCK_COMMENT = 2
    LINE_COMMENT = 3
    BOOLEAN = 4
    EXTENDS = 5
    THEN = 6
    NIL = 7
    BREAK = 8
    FLOAT = 9
    FOR = 10
    THIS = 11
    CLASS = 12
    IF = 13
    RETURN = 14
    FINAL = 15
    CONTINUE = 16
    INT = 17
    TRUE = 18
    STATIC = 19
    DO = 20
    NEW = 21
    FALSE = 22
    TO = 23
    ELSE = 24
    STRING = 25
    VOID = 26
    DOWNTO = 27
    LESSER_OP = 28
    LESSER_EQUAL_OP = 29
    GREATER_OP = 30
    GREATER_EQUAL_OP = 31
    NOT_EQUAL_OP = 32
    EQUAL_OP = 33
    OR_OP = 34
    AND_OP = 35
    ADD_OP = 36
    SUB_OP = 37
    MUL_OP = 38
    DIV_FLOAT_OP = 39
    DIV_INT_OP = 40
    MODUL_OP = 41
    CONCAT_OP = 42
    NOT_OP = 43
    DECLARE_OP = 44
    ASSIGN_OP = 45
    ASSIGN_OP_CONST = 46
    LSB = 47
    RSB = 48
    LB = 49
    RB = 50
    LP = 51
    RP = 52
    SEMICOLON = 53
    COMMA = 54
    DOT = 55
    FLOATLIT = 56
    INTEGERLIT = 57
    STRINGLIT = 58
    ID = 59
    DIGIT = 60
    EXPONENT = 61
    WS = 62
    UNCLOSE_STRING = 63
    ILLEGAL_ESCAPE = 64
    ERROR_CHAR = 65

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "'boolean'", "'extends'", "'then'", "'nil'", "'break'", "'float'", 
            "'for'", "'this'", "'class'", "'if'", "'return'", "'final'", 
            "'continue'", "'int'", "'true'", "'static'", "'do'", "'new'", 
            "'false'", "'to'", "'else'", "'string'", "'void'", "'downto'", 
            "'<'", "'<='", "'>'", "'>='", "'!='", "'=='", "'||'", "'&&'", 
            "'+'", "'-'", "'*'", "'/'", "'\\'", "'%'", "'^'", "'!'", "':'", 
            "':='", "'='", "'['", "']'", "'('", "')'", "'{'", "'}'", "';'", 
            "','", "'.'" ]

    symbolicNames = [ "<INVALID>",
            "BOOLLIT", "BLOCK_COMMENT", "LINE_COMMENT", "BOOLEAN", "EXTENDS", 
            "THEN", "NIL", "BREAK", "FLOAT", "FOR", "THIS", "CLASS", "IF", 
            "RETURN", "FINAL", "CONTINUE", "INT", "TRUE", "STATIC", "DO", 
            "NEW", "FALSE", "TO", "ELSE", "STRING", "VOID", "DOWNTO", "LESSER_OP", 
            "LESSER_EQUAL_OP", "GREATER_OP", "GREATER_EQUAL_OP", "NOT_EQUAL_OP", 
            "EQUAL_OP", "OR_OP", "AND_OP", "ADD_OP", "SUB_OP", "MUL_OP", 
            "DIV_FLOAT_OP", "DIV_INT_OP", "MODUL_OP", "CONCAT_OP", "NOT_OP", 
            "DECLARE_OP", "ASSIGN_OP", "ASSIGN_OP_CONST", "LSB", "RSB", 
            "LB", "RB", "LP", "RP", "SEMICOLON", "COMMA", "DOT", "FLOATLIT", 
            "INTEGERLIT", "STRINGLIT", "ID", "DIGIT", "EXPONENT", "WS", 
            "UNCLOSE_STRING", "ILLEGAL_ESCAPE", "ERROR_CHAR" ]

    ruleNames = [ "BOOLLIT", "BLOCK_COMMENT", "LINE_COMMENT", "BOOLEAN", 
                  "EXTENDS", "THEN", "NIL", "BREAK", "FLOAT", "FOR", "THIS", 
                  "CLASS", "IF", "RETURN", "FINAL", "CONTINUE", "INT", "TRUE", 
                  "STATIC", "DO", "NEW", "FALSE", "TO", "ELSE", "STRING", 
                  "VOID", "DOWNTO", "LESSER_OP", "LESSER_EQUAL_OP", "GREATER_OP", 
                  "GREATER_EQUAL_OP", "NOT_EQUAL_OP", "EQUAL_OP", "OR_OP", 
                  "AND_OP", "ADD_OP", "SUB_OP", "MUL_OP", "DIV_FLOAT_OP", 
                  "DIV_INT_OP", "MODUL_OP", "CONCAT_OP", "NOT_OP", "DECLARE_OP", 
                  "ASSIGN_OP", "ASSIGN_OP_CONST", "LSB", "RSB", "LB", "RB", 
                  "LP", "RP", "SEMICOLON", "COMMA", "DOT", "FLOATLIT", "INTEGERLIT", 
                  "STRINGLIT", "ID", "DIGIT", "EXPONENT", "WS", "UNCLOSE_STRING", 
                  "ILLEGAL_ESCAPE", "ERROR_CHAR" ]

    grammarFileName = "BKOOL.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.2")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


    def action(self, localctx:RuleContext, ruleIndex:int, actionIndex:int):
        if self._actions is None:
            actions = dict()
            actions[57] = self.STRINGLIT_action 
            actions[62] = self.UNCLOSE_STRING_action 
            actions[63] = self.ILLEGAL_ESCAPE_action 
            actions[64] = self.ERROR_CHAR_action 
            self._actions = actions
        action = self._actions.get(ruleIndex, None)
        if action is not None:
            action(localctx, actionIndex)
        else:
            raise Exception("No registered action for:" + str(ruleIndex))


    def STRINGLIT_action(self, localctx:RuleContext , actionIndex:int):
        if actionIndex == 0:

            	temp = Lexer.text.fget(self)
            	temp1 = (temp[1:len(temp)-1])
            	Lexer.text.fset(self, temp1)

     

    def UNCLOSE_STRING_action(self, localctx:RuleContext , actionIndex:int):
        if actionIndex == 1:

            	self.text=self.text[1:]
            	raise UncloseString(self.text)

     

    def ILLEGAL_ESCAPE_action(self, localctx:RuleContext , actionIndex:int):
        if actionIndex == 2:

            	self.text = self.text[1:]
            	raise IllegalEscape(self.text)

     

    def ERROR_CHAR_action(self, localctx:RuleContext , actionIndex:int):
        if actionIndex == 3:

            	raise ErrorToken(self.text)

     


