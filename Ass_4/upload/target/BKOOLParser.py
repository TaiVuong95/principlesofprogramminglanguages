# Generated from main/bkool/parser/BKOOL.g4 by ANTLR 4.7.2
# encoding: utf-8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3C")
        buf.write("\u0195\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16")
        buf.write("\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23\t\23")
        buf.write("\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31")
        buf.write("\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36")
        buf.write("\4\37\t\37\4 \t \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t")
        buf.write("&\4\'\t\'\4(\t(\4)\t)\4*\t*\3\2\6\2V\n\2\r\2\16\2W\3\3")
        buf.write("\3\3\3\3\3\3\5\3^\n\3\3\3\3\3\7\3b\n\3\f\3\16\3e\13\3")
        buf.write("\3\3\3\3\3\4\3\4\5\4k\n\4\3\5\3\5\5\5o\n\5\3\6\3\6\5\6")
        buf.write("s\n\6\3\7\5\7v\n\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\b\5\b")
        buf.write("\u0080\n\b\3\b\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\t")
        buf.write("\3\t\3\n\3\n\3\n\3\n\3\n\3\13\3\13\3\13\7\13\u0096\n\13")
        buf.write("\f\13\16\13\u0099\13\13\3\f\3\f\5\f\u009d\n\f\3\f\5\f")
        buf.write("\u00a0\n\f\3\f\3\f\3\f\5\f\u00a5\n\f\3\f\3\f\3\f\3\r\3")
        buf.write("\r\3\r\5\r\u00ad\n\r\3\16\3\16\5\16\u00b1\n\16\3\16\3")
        buf.write("\16\3\16\3\16\3\17\3\17\3\20\3\20\3\21\3\21\3\22\3\22")
        buf.write("\3\22\7\22\u00c0\n\22\f\22\16\22\u00c3\13\22\3\23\3\23")
        buf.write("\3\23\3\23\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\5\24")
        buf.write("\u00d1\n\24\3\25\3\25\7\25\u00d5\n\25\f\25\16\25\u00d8")
        buf.write("\13\25\3\25\7\25\u00db\n\25\f\25\16\25\u00de\13\25\3\25")
        buf.write("\3\25\3\26\3\26\3\26\3\26\3\26\3\27\3\27\3\27\3\30\3\30")
        buf.write("\3\30\3\31\3\31\3\31\3\31\3\32\3\32\3\32\3\32\3\32\3\32")
        buf.write("\5\32\u00f7\n\32\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3")
        buf.write("\33\3\33\3\34\3\34\3\34\3\34\3\34\5\34\u0107\n\34\3\34")
        buf.write("\3\34\3\34\3\35\3\35\3\35\3\35\3\35\5\35\u0111\n\35\3")
        buf.write("\36\3\36\3\36\3\36\3\36\5\36\u0118\n\36\3\37\3\37\3\37")
        buf.write("\3\37\3\37\3\37\7\37\u0120\n\37\f\37\16\37\u0123\13\37")
        buf.write("\3 \3 \3 \3 \3 \3 \7 \u012b\n \f \16 \u012e\13 \3!\3!")
        buf.write("\3!\3!\3!\3!\7!\u0136\n!\f!\16!\u0139\13!\3\"\3\"\3\"")
        buf.write("\3\"\3\"\3\"\7\"\u0141\n\"\f\"\16\"\u0144\13\"\3#\3#\3")
        buf.write("#\5#\u0149\n#\3$\3$\3$\5$\u014e\n$\3%\3%\3%\3%\3%\3%\3")
        buf.write("%\3%\3%\3%\3%\3%\3%\3%\3%\3%\5%\u0160\n%\3%\7%\u0163\n")
        buf.write("%\f%\16%\u0166\13%\3&\3&\3&\3&\3&\3&\3&\3&\5&\u0170\n")
        buf.write("&\3&\5&\u0173\n&\7&\u0175\n&\f&\16&\u0178\13&\3\'\3\'")
        buf.write("\3\'\3\'\5\'\u017e\n\'\3\'\3\'\5\'\u0182\n\'\3(\3(\3(")
        buf.write("\3(\3(\5(\u0189\n(\3)\3)\3*\3*\3*\7*\u0190\n*\f*\16*\u0193")
        buf.write("\13*\3*\2\b<>@BHJ+\2\4\6\b\n\f\16\20\22\24\26\30\32\34")
        buf.write("\36 \"$&(*,.\60\62\64\668:<>@BDFHJLNPR\2\n\6\2\6\6\13")
        buf.write("\13\23\23\33\33\4\2\31\31\35\35\3\2\36!\3\2\"#\3\2$%\3")
        buf.write("\2&\'\3\2(+\6\2\3\3\t\t\r\r:=\2\u019a\2U\3\2\2\2\4Y\3")
        buf.write("\2\2\2\6j\3\2\2\2\bn\3\2\2\2\nr\3\2\2\2\fu\3\2\2\2\16")
        buf.write("\177\3\2\2\2\20\u0086\3\2\2\2\22\u008d\3\2\2\2\24\u0092")
        buf.write("\3\2\2\2\26\u009c\3\2\2\2\30\u00ac\3\2\2\2\32\u00b0\3")
        buf.write("\2\2\2\34\u00b6\3\2\2\2\36\u00b8\3\2\2\2 \u00ba\3\2\2")
        buf.write("\2\"\u00bc\3\2\2\2$\u00c4\3\2\2\2&\u00d0\3\2\2\2(\u00d2")
        buf.write("\3\2\2\2*\u00e1\3\2\2\2,\u00e6\3\2\2\2.\u00e9\3\2\2\2")
        buf.write("\60\u00ec\3\2\2\2\62\u00f0\3\2\2\2\64\u00f8\3\2\2\2\66")
        buf.write("\u0101\3\2\2\28\u0110\3\2\2\2:\u0117\3\2\2\2<\u0119\3")
        buf.write("\2\2\2>\u0124\3\2\2\2@\u012f\3\2\2\2B\u013a\3\2\2\2D\u0148")
        buf.write("\3\2\2\2F\u014d\3\2\2\2H\u014f\3\2\2\2J\u0167\3\2\2\2")
        buf.write("L\u0181\3\2\2\2N\u0188\3\2\2\2P\u018a\3\2\2\2R\u018c\3")
        buf.write("\2\2\2TV\5\4\3\2UT\3\2\2\2VW\3\2\2\2WU\3\2\2\2WX\3\2\2")
        buf.write("\2X\3\3\2\2\2YZ\7\16\2\2Z]\7=\2\2[\\\7\7\2\2\\^\7=\2\2")
        buf.write("][\3\2\2\2]^\3\2\2\2^_\3\2\2\2_c\7\65\2\2`b\5\6\4\2a`")
        buf.write("\3\2\2\2be\3\2\2\2ca\3\2\2\2cd\3\2\2\2df\3\2\2\2ec\3\2")
        buf.write("\2\2fg\7\66\2\2g\5\3\2\2\2hk\5\b\5\2ik\5\26\f\2jh\3\2")
        buf.write("\2\2ji\3\2\2\2k\7\3\2\2\2lo\5\f\7\2mo\5\16\b\2nl\3\2\2")
        buf.write("\2nm\3\2\2\2o\t\3\2\2\2ps\5\20\t\2qs\5\22\n\2rp\3\2\2")
        buf.write("\2rq\3\2\2\2s\13\3\2\2\2tv\7\25\2\2ut\3\2\2\2uv\3\2\2")
        buf.write("\2vw\3\2\2\2wx\7\21\2\2xy\5\34\17\2yz\7=\2\2z{\7\60\2")
        buf.write("\2{|\58\35\2|}\7\67\2\2}\r\3\2\2\2~\u0080\7\25\2\2\177")
        buf.write("~\3\2\2\2\177\u0080\3\2\2\2\u0080\u0081\3\2\2\2\u0081")
        buf.write("\u0082\5\24\13\2\u0082\u0083\7.\2\2\u0083\u0084\5\30\r")
        buf.write("\2\u0084\u0085\7\67\2\2\u0085\17\3\2\2\2\u0086\u0087\7")
        buf.write("\21\2\2\u0087\u0088\5\34\17\2\u0088\u0089\7=\2\2\u0089")
        buf.write("\u008a\7\60\2\2\u008a\u008b\58\35\2\u008b\u008c\7\67\2")
        buf.write("\2\u008c\21\3\2\2\2\u008d\u008e\5\24\13\2\u008e\u008f")
        buf.write("\7.\2\2\u008f\u0090\5\30\r\2\u0090\u0091\7\67\2\2\u0091")
        buf.write("\23\3\2\2\2\u0092\u0097\7=\2\2\u0093\u0094\78\2\2\u0094")
        buf.write("\u0096\7=\2\2\u0095\u0093\3\2\2\2\u0096\u0099\3\2\2\2")
        buf.write("\u0097\u0095\3\2\2\2\u0097\u0098\3\2\2\2\u0098\25\3\2")
        buf.write("\2\2\u0099\u0097\3\2\2\2\u009a\u009d\5\30\r\2\u009b\u009d")
        buf.write("\5\36\20\2\u009c\u009a\3\2\2\2\u009c\u009b\3\2\2\2\u009c")
        buf.write("\u009d\3\2\2\2\u009d\u009f\3\2\2\2\u009e\u00a0\7\25\2")
        buf.write("\2\u009f\u009e\3\2\2\2\u009f\u00a0\3\2\2\2\u00a0\u00a1")
        buf.write("\3\2\2\2\u00a1\u00a2\7=\2\2\u00a2\u00a4\7\63\2\2\u00a3")
        buf.write("\u00a5\5\"\22\2\u00a4\u00a3\3\2\2\2\u00a4\u00a5\3\2\2")
        buf.write("\2\u00a5\u00a6\3\2\2\2\u00a6\u00a7\7\64\2\2\u00a7\u00a8")
        buf.write("\5(\25\2\u00a8\27\3\2\2\2\u00a9\u00ad\5\34\17\2\u00aa")
        buf.write("\u00ad\5\32\16\2\u00ab\u00ad\5 \21\2\u00ac\u00a9\3\2\2")
        buf.write("\2\u00ac\u00aa\3\2\2\2\u00ac\u00ab\3\2\2\2\u00ad\31\3")
        buf.write("\2\2\2\u00ae\u00b1\5\34\17\2\u00af\u00b1\5 \21\2\u00b0")
        buf.write("\u00ae\3\2\2\2\u00b0\u00af\3\2\2\2\u00b1\u00b2\3\2\2\2")
        buf.write("\u00b2\u00b3\7\61\2\2\u00b3\u00b4\7;\2\2\u00b4\u00b5\7")
        buf.write("\62\2\2\u00b5\33\3\2\2\2\u00b6\u00b7\t\2\2\2\u00b7\35")
        buf.write("\3\2\2\2\u00b8\u00b9\7\34\2\2\u00b9\37\3\2\2\2\u00ba\u00bb")
        buf.write("\7=\2\2\u00bb!\3\2\2\2\u00bc\u00c1\5$\23\2\u00bd\u00be")
        buf.write("\7\67\2\2\u00be\u00c0\5$\23\2\u00bf\u00bd\3\2\2\2\u00c0")
        buf.write("\u00c3\3\2\2\2\u00c1\u00bf\3\2\2\2\u00c1\u00c2\3\2\2\2")
        buf.write("\u00c2#\3\2\2\2\u00c3\u00c1\3\2\2\2\u00c4\u00c5\5\24\13")
        buf.write("\2\u00c5\u00c6\7.\2\2\u00c6\u00c7\5\30\r\2\u00c7%\3\2")
        buf.write("\2\2\u00c8\u00d1\5*\26\2\u00c9\u00d1\5,\27\2\u00ca\u00d1")
        buf.write("\5.\30\2\u00cb\u00d1\5\60\31\2\u00cc\u00d1\5\62\32\2\u00cd")
        buf.write("\u00d1\5\66\34\2\u00ce\u00d1\5\64\33\2\u00cf\u00d1\5(")
        buf.write("\25\2\u00d0\u00c8\3\2\2\2\u00d0\u00c9\3\2\2\2\u00d0\u00ca")
        buf.write("\3\2\2\2\u00d0\u00cb\3\2\2\2\u00d0\u00cc\3\2\2\2\u00d0")
        buf.write("\u00cd\3\2\2\2\u00d0\u00ce\3\2\2\2\u00d0\u00cf\3\2\2\2")
        buf.write("\u00d1\'\3\2\2\2\u00d2\u00d6\7\65\2\2\u00d3\u00d5\5\n")
        buf.write("\6\2\u00d4\u00d3\3\2\2\2\u00d5\u00d8\3\2\2\2\u00d6\u00d4")
        buf.write("\3\2\2\2\u00d6\u00d7\3\2\2\2\u00d7\u00dc\3\2\2\2\u00d8")
        buf.write("\u00d6\3\2\2\2\u00d9\u00db\5&\24\2\u00da\u00d9\3\2\2\2")
        buf.write("\u00db\u00de\3\2\2\2\u00dc\u00da\3\2\2\2\u00dc\u00dd\3")
        buf.write("\2\2\2\u00dd\u00df\3\2\2\2\u00de\u00dc\3\2\2\2\u00df\u00e0")
        buf.write("\7\66\2\2\u00e0)\3\2\2\2\u00e1\u00e2\5H%\2\u00e2\u00e3")
        buf.write("\7/\2\2\u00e3\u00e4\58\35\2\u00e4\u00e5\7\67\2\2\u00e5")
        buf.write("+\3\2\2\2\u00e6\u00e7\7\n\2\2\u00e7\u00e8\7\67\2\2\u00e8")
        buf.write("-\3\2\2\2\u00e9\u00ea\7\22\2\2\u00ea\u00eb\7\67\2\2\u00eb")
        buf.write("/\3\2\2\2\u00ec\u00ed\7\20\2\2\u00ed\u00ee\58\35\2\u00ee")
        buf.write("\u00ef\7\67\2\2\u00ef\61\3\2\2\2\u00f0\u00f1\7\17\2\2")
        buf.write("\u00f1\u00f2\58\35\2\u00f2\u00f3\7\b\2\2\u00f3\u00f6\5")
        buf.write("&\24\2\u00f4\u00f5\7\32\2\2\u00f5\u00f7\5&\24\2\u00f6")
        buf.write("\u00f4\3\2\2\2\u00f6\u00f7\3\2\2\2\u00f7\63\3\2\2\2\u00f8")
        buf.write("\u00f9\7\f\2\2\u00f9\u00fa\7=\2\2\u00fa\u00fb\7/\2\2\u00fb")
        buf.write("\u00fc\58\35\2\u00fc\u00fd\t\3\2\2\u00fd\u00fe\58\35\2")
        buf.write("\u00fe\u00ff\7\26\2\2\u00ff\u0100\5&\24\2\u0100\65\3\2")
        buf.write("\2\2\u0101\u0102\5H%\2\u0102\u0103\79\2\2\u0103\u0104")
        buf.write("\7=\2\2\u0104\u0106\7\63\2\2\u0105\u0107\5R*\2\u0106\u0105")
        buf.write("\3\2\2\2\u0106\u0107\3\2\2\2\u0107\u0108\3\2\2\2\u0108")
        buf.write("\u0109\7\64\2\2\u0109\u010a\7\67\2\2\u010a\67\3\2\2\2")
        buf.write("\u010b\u010c\5:\36\2\u010c\u010d\t\4\2\2\u010d\u010e\5")
        buf.write(":\36\2\u010e\u0111\3\2\2\2\u010f\u0111\5:\36\2\u0110\u010b")
        buf.write("\3\2\2\2\u0110\u010f\3\2\2\2\u01119\3\2\2\2\u0112\u0113")
        buf.write("\5<\37\2\u0113\u0114\t\5\2\2\u0114\u0115\5<\37\2\u0115")
        buf.write("\u0118\3\2\2\2\u0116\u0118\5<\37\2\u0117\u0112\3\2\2\2")
        buf.write("\u0117\u0116\3\2\2\2\u0118;\3\2\2\2\u0119\u011a\b\37\1")
        buf.write("\2\u011a\u011b\5> \2\u011b\u0121\3\2\2\2\u011c\u011d\f")
        buf.write("\4\2\2\u011d\u011e\t\6\2\2\u011e\u0120\5> \2\u011f\u011c")
        buf.write("\3\2\2\2\u0120\u0123\3\2\2\2\u0121\u011f\3\2\2\2\u0121")
        buf.write("\u0122\3\2\2\2\u0122=\3\2\2\2\u0123\u0121\3\2\2\2\u0124")
        buf.write("\u0125\b \1\2\u0125\u0126\5@!\2\u0126\u012c\3\2\2\2\u0127")
        buf.write("\u0128\f\4\2\2\u0128\u0129\t\7\2\2\u0129\u012b\5@!\2\u012a")
        buf.write("\u0127\3\2\2\2\u012b\u012e\3\2\2\2\u012c\u012a\3\2\2\2")
        buf.write("\u012c\u012d\3\2\2\2\u012d?\3\2\2\2\u012e\u012c\3\2\2")
        buf.write("\2\u012f\u0130\b!\1\2\u0130\u0131\5B\"\2\u0131\u0137\3")
        buf.write("\2\2\2\u0132\u0133\f\4\2\2\u0133\u0134\t\b\2\2\u0134\u0136")
        buf.write("\5B\"\2\u0135\u0132\3\2\2\2\u0136\u0139\3\2\2\2\u0137")
        buf.write("\u0135\3\2\2\2\u0137\u0138\3\2\2\2\u0138A\3\2\2\2\u0139")
        buf.write("\u0137\3\2\2\2\u013a\u013b\b\"\1\2\u013b\u013c\5D#\2\u013c")
        buf.write("\u0142\3\2\2\2\u013d\u013e\f\4\2\2\u013e\u013f\7,\2\2")
        buf.write("\u013f\u0141\5D#\2\u0140\u013d\3\2\2\2\u0141\u0144\3\2")
        buf.write("\2\2\u0142\u0140\3\2\2\2\u0142\u0143\3\2\2\2\u0143C\3")
        buf.write("\2\2\2\u0144\u0142\3\2\2\2\u0145\u0146\7-\2\2\u0146\u0149")
        buf.write("\5D#\2\u0147\u0149\5F$\2\u0148\u0145\3\2\2\2\u0148\u0147")
        buf.write("\3\2\2\2\u0149E\3\2\2\2\u014a\u014b\t\7\2\2\u014b\u014e")
        buf.write("\5F$\2\u014c\u014e\5H%\2\u014d\u014a\3\2\2\2\u014d\u014c")
        buf.write("\3\2\2\2\u014eG\3\2\2\2\u014f\u0150\b%\1\2\u0150\u0151")
        buf.write("\5J&\2\u0151\u0164\3\2\2\2\u0152\u0153\f\6\2\2\u0153\u0154")
        buf.write("\7\61\2\2\u0154\u0155\58\35\2\u0155\u0156\7\62\2\2\u0156")
        buf.write("\u0163\3\2\2\2\u0157\u0158\f\5\2\2\u0158\u0159\79\2\2")
        buf.write("\u0159\u0163\7=\2\2\u015a\u015b\f\4\2\2\u015b\u015c\7")
        buf.write("9\2\2\u015c\u015d\7=\2\2\u015d\u015f\7\63\2\2\u015e\u0160")
        buf.write("\5R*\2\u015f\u015e\3\2\2\2\u015f\u0160\3\2\2\2\u0160\u0161")
        buf.write("\3\2\2\2\u0161\u0163\7\64\2\2\u0162\u0152\3\2\2\2\u0162")
        buf.write("\u0157\3\2\2\2\u0162\u015a\3\2\2\2\u0163\u0166\3\2\2\2")
        buf.write("\u0164\u0162\3\2\2\2\u0164\u0165\3\2\2\2\u0165I\3\2\2")
        buf.write("\2\u0166\u0164\3\2\2\2\u0167\u0168\b&\1\2\u0168\u0169")
        buf.write("\5L\'\2\u0169\u0176\3\2\2\2\u016a\u016b\f\4\2\2\u016b")
        buf.write("\u016c\79\2\2\u016c\u0172\7=\2\2\u016d\u016f\7\63\2\2")
        buf.write("\u016e\u0170\5R*\2\u016f\u016e\3\2\2\2\u016f\u0170\3\2")
        buf.write("\2\2\u0170\u0171\3\2\2\2\u0171\u0173\7\64\2\2\u0172\u016d")
        buf.write("\3\2\2\2\u0172\u0173\3\2\2\2\u0173\u0175\3\2\2\2\u0174")
        buf.write("\u016a\3\2\2\2\u0175\u0178\3\2\2\2\u0176\u0174\3\2\2\2")
        buf.write("\u0176\u0177\3\2\2\2\u0177K\3\2\2\2\u0178\u0176\3\2\2")
        buf.write("\2\u0179\u017a\7\27\2\2\u017a\u017b\7=\2\2\u017b\u017d")
        buf.write("\7\63\2\2\u017c\u017e\5R*\2\u017d\u017c\3\2\2\2\u017d")
        buf.write("\u017e\3\2\2\2\u017e\u017f\3\2\2\2\u017f\u0182\7\64\2")
        buf.write("\2\u0180\u0182\5N(\2\u0181\u0179\3\2\2\2\u0181\u0180\3")
        buf.write("\2\2\2\u0182M\3\2\2\2\u0183\u0184\7\63\2\2\u0184\u0185")
        buf.write("\58\35\2\u0185\u0186\7\64\2\2\u0186\u0189\3\2\2\2\u0187")
        buf.write("\u0189\5P)\2\u0188\u0183\3\2\2\2\u0188\u0187\3\2\2\2\u0189")
        buf.write("O\3\2\2\2\u018a\u018b\t\t\2\2\u018bQ\3\2\2\2\u018c\u0191")
        buf.write("\58\35\2\u018d\u018e\78\2\2\u018e\u0190\58\35\2\u018f")
        buf.write("\u018d\3\2\2\2\u0190\u0193\3\2\2\2\u0191\u018f\3\2\2\2")
        buf.write("\u0191\u0192\3\2\2\2\u0192S\3\2\2\2\u0193\u0191\3\2\2")
        buf.write("\2(W]cjnru\177\u0097\u009c\u009f\u00a4\u00ac\u00b0\u00c1")
        buf.write("\u00d0\u00d6\u00dc\u00f6\u0106\u0110\u0117\u0121\u012c")
        buf.write("\u0137\u0142\u0148\u014d\u015f\u0162\u0164\u016f\u0172")
        buf.write("\u0176\u017d\u0181\u0188\u0191")
        return buf.getvalue()


class BKOOLParser ( Parser ):

    grammarFileName = "BKOOL.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "'boolean'", "'extends'", "'then'", "'nil'", "'break'", 
                     "'float'", "'for'", "'this'", "'class'", "'if'", "'return'", 
                     "'final'", "'continue'", "'int'", "'true'", "'static'", 
                     "'do'", "'new'", "'false'", "'to'", "'else'", "'string'", 
                     "'void'", "'downto'", "'<'", "'<='", "'>'", "'>='", 
                     "'!='", "'=='", "'||'", "'&&'", "'+'", "'-'", "'*'", 
                     "'/'", "'\\'", "'%'", "'^'", "'!'", "':'", "':='", 
                     "'='", "'['", "']'", "'('", "')'", "'{'", "'}'", "';'", 
                     "','", "'.'" ]

    symbolicNames = [ "<INVALID>", "BOOLLIT", "BLOCK_COMMENT", "LINE_COMMENT", 
                      "BOOLEAN", "EXTENDS", "THEN", "NIL", "BREAK", "FLOAT", 
                      "FOR", "THIS", "CLASS", "IF", "RETURN", "FINAL", "CONTINUE", 
                      "INT", "TRUE", "STATIC", "DO", "NEW", "FALSE", "TO", 
                      "ELSE", "STRING", "VOID", "DOWNTO", "LESSER_OP", "LESSER_EQUAL_OP", 
                      "GREATER_OP", "GREATER_EQUAL_OP", "NOT_EQUAL_OP", 
                      "EQUAL_OP", "OR_OP", "AND_OP", "ADD_OP", "SUB_OP", 
                      "MUL_OP", "DIV_FLOAT_OP", "DIV_INT_OP", "MODUL_OP", 
                      "CONCAT_OP", "NOT_OP", "DECLARE_OP", "ASSIGN_OP", 
                      "ASSIGN_OP_CONST", "LSB", "RSB", "LB", "RB", "LP", 
                      "RP", "SEMICOLON", "COMMA", "DOT", "FLOATLIT", "INTEGERLIT", 
                      "STRINGLIT", "ID", "DIGIT", "EXPONENT", "WS", "UNCLOSE_STRING", 
                      "ILLEGAL_ESCAPE", "ERROR_CHAR" ]

    RULE_program = 0
    RULE_class_decl = 1
    RULE_list_member = 2
    RULE_var_decl = 3
    RULE_local_decl = 4
    RULE_imutable_decl = 5
    RULE_mutable_decl = 6
    RULE_imutable_local = 7
    RULE_mutable_local = 8
    RULE_idlist = 9
    RULE_method_decl = 10
    RULE_type_var = 11
    RULE_arr_type = 12
    RULE_pri_type = 13
    RULE_void_type = 14
    RULE_class_type = 15
    RULE_many_para = 16
    RULE_one_para = 17
    RULE_stmt = 18
    RULE_block_stmt = 19
    RULE_assign_stmt = 20
    RULE_break_stmt = 21
    RULE_continue_stmt = 22
    RULE_return_stmt = 23
    RULE_if_stmt = 24
    RULE_for_stmt = 25
    RULE_call_stmt = 26
    RULE_exp = 27
    RULE_exp1 = 28
    RULE_exp2 = 29
    RULE_exp3 = 30
    RULE_exp4 = 31
    RULE_exp5 = 32
    RULE_exp6 = 33
    RULE_exp7 = 34
    RULE_exp8 = 35
    RULE_exp9 = 36
    RULE_exp10 = 37
    RULE_exp11 = 38
    RULE_element = 39
    RULE_arglist = 40

    ruleNames =  [ "program", "class_decl", "list_member", "var_decl", "local_decl", 
                   "imutable_decl", "mutable_decl", "imutable_local", "mutable_local", 
                   "idlist", "method_decl", "type_var", "arr_type", "pri_type", 
                   "void_type", "class_type", "many_para", "one_para", "stmt", 
                   "block_stmt", "assign_stmt", "break_stmt", "continue_stmt", 
                   "return_stmt", "if_stmt", "for_stmt", "call_stmt", "exp", 
                   "exp1", "exp2", "exp3", "exp4", "exp5", "exp6", "exp7", 
                   "exp8", "exp9", "exp10", "exp11", "element", "arglist" ]

    EOF = Token.EOF
    BOOLLIT=1
    BLOCK_COMMENT=2
    LINE_COMMENT=3
    BOOLEAN=4
    EXTENDS=5
    THEN=6
    NIL=7
    BREAK=8
    FLOAT=9
    FOR=10
    THIS=11
    CLASS=12
    IF=13
    RETURN=14
    FINAL=15
    CONTINUE=16
    INT=17
    TRUE=18
    STATIC=19
    DO=20
    NEW=21
    FALSE=22
    TO=23
    ELSE=24
    STRING=25
    VOID=26
    DOWNTO=27
    LESSER_OP=28
    LESSER_EQUAL_OP=29
    GREATER_OP=30
    GREATER_EQUAL_OP=31
    NOT_EQUAL_OP=32
    EQUAL_OP=33
    OR_OP=34
    AND_OP=35
    ADD_OP=36
    SUB_OP=37
    MUL_OP=38
    DIV_FLOAT_OP=39
    DIV_INT_OP=40
    MODUL_OP=41
    CONCAT_OP=42
    NOT_OP=43
    DECLARE_OP=44
    ASSIGN_OP=45
    ASSIGN_OP_CONST=46
    LSB=47
    RSB=48
    LB=49
    RB=50
    LP=51
    RP=52
    SEMICOLON=53
    COMMA=54
    DOT=55
    FLOATLIT=56
    INTEGERLIT=57
    STRINGLIT=58
    ID=59
    DIGIT=60
    EXPONENT=61
    WS=62
    UNCLOSE_STRING=63
    ILLEGAL_ESCAPE=64
    ERROR_CHAR=65

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.2")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class ProgramContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def class_decl(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(BKOOLParser.Class_declContext)
            else:
                return self.getTypedRuleContext(BKOOLParser.Class_declContext,i)


        def getRuleIndex(self):
            return BKOOLParser.RULE_program

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitProgram" ):
                return visitor.visitProgram(self)
            else:
                return visitor.visitChildren(self)




    def program(self):

        localctx = BKOOLParser.ProgramContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_program)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 83 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 82
                self.class_decl()
                self.state = 85 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not (_la==BKOOLParser.CLASS):
                    break

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Class_declContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def CLASS(self):
            return self.getToken(BKOOLParser.CLASS, 0)

        def ID(self, i:int=None):
            if i is None:
                return self.getTokens(BKOOLParser.ID)
            else:
                return self.getToken(BKOOLParser.ID, i)

        def LP(self):
            return self.getToken(BKOOLParser.LP, 0)

        def RP(self):
            return self.getToken(BKOOLParser.RP, 0)

        def EXTENDS(self):
            return self.getToken(BKOOLParser.EXTENDS, 0)

        def list_member(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(BKOOLParser.List_memberContext)
            else:
                return self.getTypedRuleContext(BKOOLParser.List_memberContext,i)


        def getRuleIndex(self):
            return BKOOLParser.RULE_class_decl

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitClass_decl" ):
                return visitor.visitClass_decl(self)
            else:
                return visitor.visitChildren(self)




    def class_decl(self):

        localctx = BKOOLParser.Class_declContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_class_decl)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 87
            self.match(BKOOLParser.CLASS)
            self.state = 88
            self.match(BKOOLParser.ID)
            self.state = 91
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==BKOOLParser.EXTENDS:
                self.state = 89
                self.match(BKOOLParser.EXTENDS)
                self.state = 90
                self.match(BKOOLParser.ID)


            self.state = 93
            self.match(BKOOLParser.LP)
            self.state = 97
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << BKOOLParser.BOOLEAN) | (1 << BKOOLParser.FLOAT) | (1 << BKOOLParser.FINAL) | (1 << BKOOLParser.INT) | (1 << BKOOLParser.STATIC) | (1 << BKOOLParser.STRING) | (1 << BKOOLParser.VOID) | (1 << BKOOLParser.ID))) != 0):
                self.state = 94
                self.list_member()
                self.state = 99
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 100
            self.match(BKOOLParser.RP)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class List_memberContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def var_decl(self):
            return self.getTypedRuleContext(BKOOLParser.Var_declContext,0)


        def method_decl(self):
            return self.getTypedRuleContext(BKOOLParser.Method_declContext,0)


        def getRuleIndex(self):
            return BKOOLParser.RULE_list_member

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitList_member" ):
                return visitor.visitList_member(self)
            else:
                return visitor.visitChildren(self)




    def list_member(self):

        localctx = BKOOLParser.List_memberContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_list_member)
        try:
            self.state = 104
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,3,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 102
                self.var_decl()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 103
                self.method_decl()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Var_declContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def imutable_decl(self):
            return self.getTypedRuleContext(BKOOLParser.Imutable_declContext,0)


        def mutable_decl(self):
            return self.getTypedRuleContext(BKOOLParser.Mutable_declContext,0)


        def getRuleIndex(self):
            return BKOOLParser.RULE_var_decl

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitVar_decl" ):
                return visitor.visitVar_decl(self)
            else:
                return visitor.visitChildren(self)




    def var_decl(self):

        localctx = BKOOLParser.Var_declContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_var_decl)
        try:
            self.state = 108
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,4,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 106
                self.imutable_decl()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 107
                self.mutable_decl()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Local_declContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def imutable_local(self):
            return self.getTypedRuleContext(BKOOLParser.Imutable_localContext,0)


        def mutable_local(self):
            return self.getTypedRuleContext(BKOOLParser.Mutable_localContext,0)


        def getRuleIndex(self):
            return BKOOLParser.RULE_local_decl

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitLocal_decl" ):
                return visitor.visitLocal_decl(self)
            else:
                return visitor.visitChildren(self)




    def local_decl(self):

        localctx = BKOOLParser.Local_declContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_local_decl)
        try:
            self.state = 112
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [BKOOLParser.FINAL]:
                self.enterOuterAlt(localctx, 1)
                self.state = 110
                self.imutable_local()
                pass
            elif token in [BKOOLParser.ID]:
                self.enterOuterAlt(localctx, 2)
                self.state = 111
                self.mutable_local()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Imutable_declContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def FINAL(self):
            return self.getToken(BKOOLParser.FINAL, 0)

        def pri_type(self):
            return self.getTypedRuleContext(BKOOLParser.Pri_typeContext,0)


        def ID(self):
            return self.getToken(BKOOLParser.ID, 0)

        def ASSIGN_OP_CONST(self):
            return self.getToken(BKOOLParser.ASSIGN_OP_CONST, 0)

        def exp(self):
            return self.getTypedRuleContext(BKOOLParser.ExpContext,0)


        def SEMICOLON(self):
            return self.getToken(BKOOLParser.SEMICOLON, 0)

        def STATIC(self):
            return self.getToken(BKOOLParser.STATIC, 0)

        def getRuleIndex(self):
            return BKOOLParser.RULE_imutable_decl

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitImutable_decl" ):
                return visitor.visitImutable_decl(self)
            else:
                return visitor.visitChildren(self)




    def imutable_decl(self):

        localctx = BKOOLParser.Imutable_declContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_imutable_decl)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 115
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==BKOOLParser.STATIC:
                self.state = 114
                self.match(BKOOLParser.STATIC)


            self.state = 117
            self.match(BKOOLParser.FINAL)
            self.state = 118
            self.pri_type()
            self.state = 119
            self.match(BKOOLParser.ID)
            self.state = 120
            self.match(BKOOLParser.ASSIGN_OP_CONST)
            self.state = 121
            self.exp()
            self.state = 122
            self.match(BKOOLParser.SEMICOLON)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Mutable_declContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def idlist(self):
            return self.getTypedRuleContext(BKOOLParser.IdlistContext,0)


        def DECLARE_OP(self):
            return self.getToken(BKOOLParser.DECLARE_OP, 0)

        def type_var(self):
            return self.getTypedRuleContext(BKOOLParser.Type_varContext,0)


        def SEMICOLON(self):
            return self.getToken(BKOOLParser.SEMICOLON, 0)

        def STATIC(self):
            return self.getToken(BKOOLParser.STATIC, 0)

        def getRuleIndex(self):
            return BKOOLParser.RULE_mutable_decl

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMutable_decl" ):
                return visitor.visitMutable_decl(self)
            else:
                return visitor.visitChildren(self)




    def mutable_decl(self):

        localctx = BKOOLParser.Mutable_declContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_mutable_decl)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 125
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==BKOOLParser.STATIC:
                self.state = 124
                self.match(BKOOLParser.STATIC)


            self.state = 127
            self.idlist()
            self.state = 128
            self.match(BKOOLParser.DECLARE_OP)
            self.state = 129
            self.type_var()
            self.state = 130
            self.match(BKOOLParser.SEMICOLON)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Imutable_localContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def FINAL(self):
            return self.getToken(BKOOLParser.FINAL, 0)

        def pri_type(self):
            return self.getTypedRuleContext(BKOOLParser.Pri_typeContext,0)


        def ID(self):
            return self.getToken(BKOOLParser.ID, 0)

        def ASSIGN_OP_CONST(self):
            return self.getToken(BKOOLParser.ASSIGN_OP_CONST, 0)

        def exp(self):
            return self.getTypedRuleContext(BKOOLParser.ExpContext,0)


        def SEMICOLON(self):
            return self.getToken(BKOOLParser.SEMICOLON, 0)

        def getRuleIndex(self):
            return BKOOLParser.RULE_imutable_local

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitImutable_local" ):
                return visitor.visitImutable_local(self)
            else:
                return visitor.visitChildren(self)




    def imutable_local(self):

        localctx = BKOOLParser.Imutable_localContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_imutable_local)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 132
            self.match(BKOOLParser.FINAL)
            self.state = 133
            self.pri_type()
            self.state = 134
            self.match(BKOOLParser.ID)
            self.state = 135
            self.match(BKOOLParser.ASSIGN_OP_CONST)
            self.state = 136
            self.exp()
            self.state = 137
            self.match(BKOOLParser.SEMICOLON)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Mutable_localContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def idlist(self):
            return self.getTypedRuleContext(BKOOLParser.IdlistContext,0)


        def DECLARE_OP(self):
            return self.getToken(BKOOLParser.DECLARE_OP, 0)

        def type_var(self):
            return self.getTypedRuleContext(BKOOLParser.Type_varContext,0)


        def SEMICOLON(self):
            return self.getToken(BKOOLParser.SEMICOLON, 0)

        def getRuleIndex(self):
            return BKOOLParser.RULE_mutable_local

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMutable_local" ):
                return visitor.visitMutable_local(self)
            else:
                return visitor.visitChildren(self)




    def mutable_local(self):

        localctx = BKOOLParser.Mutable_localContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_mutable_local)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 139
            self.idlist()
            self.state = 140
            self.match(BKOOLParser.DECLARE_OP)
            self.state = 141
            self.type_var()
            self.state = 142
            self.match(BKOOLParser.SEMICOLON)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class IdlistContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self, i:int=None):
            if i is None:
                return self.getTokens(BKOOLParser.ID)
            else:
                return self.getToken(BKOOLParser.ID, i)

        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(BKOOLParser.COMMA)
            else:
                return self.getToken(BKOOLParser.COMMA, i)

        def getRuleIndex(self):
            return BKOOLParser.RULE_idlist

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitIdlist" ):
                return visitor.visitIdlist(self)
            else:
                return visitor.visitChildren(self)




    def idlist(self):

        localctx = BKOOLParser.IdlistContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_idlist)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 144
            self.match(BKOOLParser.ID)
            self.state = 149
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==BKOOLParser.COMMA:
                self.state = 145
                self.match(BKOOLParser.COMMA)
                self.state = 146
                self.match(BKOOLParser.ID)
                self.state = 151
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Method_declContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(BKOOLParser.ID, 0)

        def LB(self):
            return self.getToken(BKOOLParser.LB, 0)

        def RB(self):
            return self.getToken(BKOOLParser.RB, 0)

        def block_stmt(self):
            return self.getTypedRuleContext(BKOOLParser.Block_stmtContext,0)


        def type_var(self):
            return self.getTypedRuleContext(BKOOLParser.Type_varContext,0)


        def void_type(self):
            return self.getTypedRuleContext(BKOOLParser.Void_typeContext,0)


        def STATIC(self):
            return self.getToken(BKOOLParser.STATIC, 0)

        def many_para(self):
            return self.getTypedRuleContext(BKOOLParser.Many_paraContext,0)


        def getRuleIndex(self):
            return BKOOLParser.RULE_method_decl

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMethod_decl" ):
                return visitor.visitMethod_decl(self)
            else:
                return visitor.visitChildren(self)




    def method_decl(self):

        localctx = BKOOLParser.Method_declContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_method_decl)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 154
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,9,self._ctx)
            if la_ == 1:
                self.state = 152
                self.type_var()

            elif la_ == 2:
                self.state = 153
                self.void_type()


            self.state = 157
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==BKOOLParser.STATIC:
                self.state = 156
                self.match(BKOOLParser.STATIC)


            self.state = 159
            self.match(BKOOLParser.ID)
            self.state = 160
            self.match(BKOOLParser.LB)
            self.state = 162
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==BKOOLParser.ID:
                self.state = 161
                self.many_para()


            self.state = 164
            self.match(BKOOLParser.RB)
            self.state = 165
            self.block_stmt()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Type_varContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def pri_type(self):
            return self.getTypedRuleContext(BKOOLParser.Pri_typeContext,0)


        def arr_type(self):
            return self.getTypedRuleContext(BKOOLParser.Arr_typeContext,0)


        def class_type(self):
            return self.getTypedRuleContext(BKOOLParser.Class_typeContext,0)


        def getRuleIndex(self):
            return BKOOLParser.RULE_type_var

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitType_var" ):
                return visitor.visitType_var(self)
            else:
                return visitor.visitChildren(self)




    def type_var(self):

        localctx = BKOOLParser.Type_varContext(self, self._ctx, self.state)
        self.enterRule(localctx, 22, self.RULE_type_var)
        try:
            self.state = 170
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,12,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 167
                self.pri_type()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 168
                self.arr_type()
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 169
                self.class_type()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Arr_typeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LSB(self):
            return self.getToken(BKOOLParser.LSB, 0)

        def INTEGERLIT(self):
            return self.getToken(BKOOLParser.INTEGERLIT, 0)

        def RSB(self):
            return self.getToken(BKOOLParser.RSB, 0)

        def pri_type(self):
            return self.getTypedRuleContext(BKOOLParser.Pri_typeContext,0)


        def class_type(self):
            return self.getTypedRuleContext(BKOOLParser.Class_typeContext,0)


        def getRuleIndex(self):
            return BKOOLParser.RULE_arr_type

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitArr_type" ):
                return visitor.visitArr_type(self)
            else:
                return visitor.visitChildren(self)




    def arr_type(self):

        localctx = BKOOLParser.Arr_typeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 24, self.RULE_arr_type)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 174
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [BKOOLParser.BOOLEAN, BKOOLParser.FLOAT, BKOOLParser.INT, BKOOLParser.STRING]:
                self.state = 172
                self.pri_type()
                pass
            elif token in [BKOOLParser.ID]:
                self.state = 173
                self.class_type()
                pass
            else:
                raise NoViableAltException(self)

            self.state = 176
            self.match(BKOOLParser.LSB)
            self.state = 177
            self.match(BKOOLParser.INTEGERLIT)
            self.state = 178
            self.match(BKOOLParser.RSB)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Pri_typeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def BOOLEAN(self):
            return self.getToken(BKOOLParser.BOOLEAN, 0)

        def INT(self):
            return self.getToken(BKOOLParser.INT, 0)

        def FLOAT(self):
            return self.getToken(BKOOLParser.FLOAT, 0)

        def STRING(self):
            return self.getToken(BKOOLParser.STRING, 0)

        def getRuleIndex(self):
            return BKOOLParser.RULE_pri_type

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitPri_type" ):
                return visitor.visitPri_type(self)
            else:
                return visitor.visitChildren(self)




    def pri_type(self):

        localctx = BKOOLParser.Pri_typeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 26, self.RULE_pri_type)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 180
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << BKOOLParser.BOOLEAN) | (1 << BKOOLParser.FLOAT) | (1 << BKOOLParser.INT) | (1 << BKOOLParser.STRING))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Void_typeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def VOID(self):
            return self.getToken(BKOOLParser.VOID, 0)

        def getRuleIndex(self):
            return BKOOLParser.RULE_void_type

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitVoid_type" ):
                return visitor.visitVoid_type(self)
            else:
                return visitor.visitChildren(self)




    def void_type(self):

        localctx = BKOOLParser.Void_typeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 28, self.RULE_void_type)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 182
            self.match(BKOOLParser.VOID)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Class_typeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(BKOOLParser.ID, 0)

        def getRuleIndex(self):
            return BKOOLParser.RULE_class_type

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitClass_type" ):
                return visitor.visitClass_type(self)
            else:
                return visitor.visitChildren(self)




    def class_type(self):

        localctx = BKOOLParser.Class_typeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 30, self.RULE_class_type)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 184
            self.match(BKOOLParser.ID)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Many_paraContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def one_para(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(BKOOLParser.One_paraContext)
            else:
                return self.getTypedRuleContext(BKOOLParser.One_paraContext,i)


        def SEMICOLON(self, i:int=None):
            if i is None:
                return self.getTokens(BKOOLParser.SEMICOLON)
            else:
                return self.getToken(BKOOLParser.SEMICOLON, i)

        def getRuleIndex(self):
            return BKOOLParser.RULE_many_para

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMany_para" ):
                return visitor.visitMany_para(self)
            else:
                return visitor.visitChildren(self)




    def many_para(self):

        localctx = BKOOLParser.Many_paraContext(self, self._ctx, self.state)
        self.enterRule(localctx, 32, self.RULE_many_para)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 186
            self.one_para()
            self.state = 191
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==BKOOLParser.SEMICOLON:
                self.state = 187
                self.match(BKOOLParser.SEMICOLON)
                self.state = 188
                self.one_para()
                self.state = 193
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class One_paraContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def idlist(self):
            return self.getTypedRuleContext(BKOOLParser.IdlistContext,0)


        def DECLARE_OP(self):
            return self.getToken(BKOOLParser.DECLARE_OP, 0)

        def type_var(self):
            return self.getTypedRuleContext(BKOOLParser.Type_varContext,0)


        def getRuleIndex(self):
            return BKOOLParser.RULE_one_para

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitOne_para" ):
                return visitor.visitOne_para(self)
            else:
                return visitor.visitChildren(self)




    def one_para(self):

        localctx = BKOOLParser.One_paraContext(self, self._ctx, self.state)
        self.enterRule(localctx, 34, self.RULE_one_para)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 194
            self.idlist()
            self.state = 195
            self.match(BKOOLParser.DECLARE_OP)
            self.state = 196
            self.type_var()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class StmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def assign_stmt(self):
            return self.getTypedRuleContext(BKOOLParser.Assign_stmtContext,0)


        def break_stmt(self):
            return self.getTypedRuleContext(BKOOLParser.Break_stmtContext,0)


        def continue_stmt(self):
            return self.getTypedRuleContext(BKOOLParser.Continue_stmtContext,0)


        def return_stmt(self):
            return self.getTypedRuleContext(BKOOLParser.Return_stmtContext,0)


        def if_stmt(self):
            return self.getTypedRuleContext(BKOOLParser.If_stmtContext,0)


        def call_stmt(self):
            return self.getTypedRuleContext(BKOOLParser.Call_stmtContext,0)


        def for_stmt(self):
            return self.getTypedRuleContext(BKOOLParser.For_stmtContext,0)


        def block_stmt(self):
            return self.getTypedRuleContext(BKOOLParser.Block_stmtContext,0)


        def getRuleIndex(self):
            return BKOOLParser.RULE_stmt

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitStmt" ):
                return visitor.visitStmt(self)
            else:
                return visitor.visitChildren(self)




    def stmt(self):

        localctx = BKOOLParser.StmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 36, self.RULE_stmt)
        try:
            self.state = 206
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,15,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 198
                self.assign_stmt()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 199
                self.break_stmt()
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 200
                self.continue_stmt()
                pass

            elif la_ == 4:
                self.enterOuterAlt(localctx, 4)
                self.state = 201
                self.return_stmt()
                pass

            elif la_ == 5:
                self.enterOuterAlt(localctx, 5)
                self.state = 202
                self.if_stmt()
                pass

            elif la_ == 6:
                self.enterOuterAlt(localctx, 6)
                self.state = 203
                self.call_stmt()
                pass

            elif la_ == 7:
                self.enterOuterAlt(localctx, 7)
                self.state = 204
                self.for_stmt()
                pass

            elif la_ == 8:
                self.enterOuterAlt(localctx, 8)
                self.state = 205
                self.block_stmt()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Block_stmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LP(self):
            return self.getToken(BKOOLParser.LP, 0)

        def RP(self):
            return self.getToken(BKOOLParser.RP, 0)

        def local_decl(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(BKOOLParser.Local_declContext)
            else:
                return self.getTypedRuleContext(BKOOLParser.Local_declContext,i)


        def stmt(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(BKOOLParser.StmtContext)
            else:
                return self.getTypedRuleContext(BKOOLParser.StmtContext,i)


        def getRuleIndex(self):
            return BKOOLParser.RULE_block_stmt

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBlock_stmt" ):
                return visitor.visitBlock_stmt(self)
            else:
                return visitor.visitChildren(self)




    def block_stmt(self):

        localctx = BKOOLParser.Block_stmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 38, self.RULE_block_stmt)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 208
            self.match(BKOOLParser.LP)
            self.state = 212
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,16,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 209
                    self.local_decl() 
                self.state = 214
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,16,self._ctx)

            self.state = 218
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << BKOOLParser.BOOLLIT) | (1 << BKOOLParser.NIL) | (1 << BKOOLParser.BREAK) | (1 << BKOOLParser.FOR) | (1 << BKOOLParser.THIS) | (1 << BKOOLParser.IF) | (1 << BKOOLParser.RETURN) | (1 << BKOOLParser.CONTINUE) | (1 << BKOOLParser.NEW) | (1 << BKOOLParser.LB) | (1 << BKOOLParser.LP) | (1 << BKOOLParser.FLOATLIT) | (1 << BKOOLParser.INTEGERLIT) | (1 << BKOOLParser.STRINGLIT) | (1 << BKOOLParser.ID))) != 0):
                self.state = 215
                self.stmt()
                self.state = 220
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 221
            self.match(BKOOLParser.RP)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Assign_stmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def exp8(self):
            return self.getTypedRuleContext(BKOOLParser.Exp8Context,0)


        def ASSIGN_OP(self):
            return self.getToken(BKOOLParser.ASSIGN_OP, 0)

        def exp(self):
            return self.getTypedRuleContext(BKOOLParser.ExpContext,0)


        def SEMICOLON(self):
            return self.getToken(BKOOLParser.SEMICOLON, 0)

        def getRuleIndex(self):
            return BKOOLParser.RULE_assign_stmt

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAssign_stmt" ):
                return visitor.visitAssign_stmt(self)
            else:
                return visitor.visitChildren(self)




    def assign_stmt(self):

        localctx = BKOOLParser.Assign_stmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 40, self.RULE_assign_stmt)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 223
            self.exp8(0)
            self.state = 224
            self.match(BKOOLParser.ASSIGN_OP)
            self.state = 225
            self.exp()
            self.state = 226
            self.match(BKOOLParser.SEMICOLON)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Break_stmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def BREAK(self):
            return self.getToken(BKOOLParser.BREAK, 0)

        def SEMICOLON(self):
            return self.getToken(BKOOLParser.SEMICOLON, 0)

        def getRuleIndex(self):
            return BKOOLParser.RULE_break_stmt

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBreak_stmt" ):
                return visitor.visitBreak_stmt(self)
            else:
                return visitor.visitChildren(self)




    def break_stmt(self):

        localctx = BKOOLParser.Break_stmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 42, self.RULE_break_stmt)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 228
            self.match(BKOOLParser.BREAK)
            self.state = 229
            self.match(BKOOLParser.SEMICOLON)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Continue_stmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def CONTINUE(self):
            return self.getToken(BKOOLParser.CONTINUE, 0)

        def SEMICOLON(self):
            return self.getToken(BKOOLParser.SEMICOLON, 0)

        def getRuleIndex(self):
            return BKOOLParser.RULE_continue_stmt

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitContinue_stmt" ):
                return visitor.visitContinue_stmt(self)
            else:
                return visitor.visitChildren(self)




    def continue_stmt(self):

        localctx = BKOOLParser.Continue_stmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 44, self.RULE_continue_stmt)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 231
            self.match(BKOOLParser.CONTINUE)
            self.state = 232
            self.match(BKOOLParser.SEMICOLON)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Return_stmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def RETURN(self):
            return self.getToken(BKOOLParser.RETURN, 0)

        def exp(self):
            return self.getTypedRuleContext(BKOOLParser.ExpContext,0)


        def SEMICOLON(self):
            return self.getToken(BKOOLParser.SEMICOLON, 0)

        def getRuleIndex(self):
            return BKOOLParser.RULE_return_stmt

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitReturn_stmt" ):
                return visitor.visitReturn_stmt(self)
            else:
                return visitor.visitChildren(self)




    def return_stmt(self):

        localctx = BKOOLParser.Return_stmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 46, self.RULE_return_stmt)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 234
            self.match(BKOOLParser.RETURN)
            self.state = 235
            self.exp()
            self.state = 236
            self.match(BKOOLParser.SEMICOLON)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class If_stmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def IF(self):
            return self.getToken(BKOOLParser.IF, 0)

        def exp(self):
            return self.getTypedRuleContext(BKOOLParser.ExpContext,0)


        def THEN(self):
            return self.getToken(BKOOLParser.THEN, 0)

        def stmt(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(BKOOLParser.StmtContext)
            else:
                return self.getTypedRuleContext(BKOOLParser.StmtContext,i)


        def ELSE(self):
            return self.getToken(BKOOLParser.ELSE, 0)

        def getRuleIndex(self):
            return BKOOLParser.RULE_if_stmt

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitIf_stmt" ):
                return visitor.visitIf_stmt(self)
            else:
                return visitor.visitChildren(self)




    def if_stmt(self):

        localctx = BKOOLParser.If_stmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 48, self.RULE_if_stmt)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 238
            self.match(BKOOLParser.IF)
            self.state = 239
            self.exp()
            self.state = 240
            self.match(BKOOLParser.THEN)
            self.state = 241
            self.stmt()
            self.state = 244
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,18,self._ctx)
            if la_ == 1:
                self.state = 242
                self.match(BKOOLParser.ELSE)
                self.state = 243
                self.stmt()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class For_stmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def FOR(self):
            return self.getToken(BKOOLParser.FOR, 0)

        def ID(self):
            return self.getToken(BKOOLParser.ID, 0)

        def ASSIGN_OP(self):
            return self.getToken(BKOOLParser.ASSIGN_OP, 0)

        def exp(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(BKOOLParser.ExpContext)
            else:
                return self.getTypedRuleContext(BKOOLParser.ExpContext,i)


        def DO(self):
            return self.getToken(BKOOLParser.DO, 0)

        def stmt(self):
            return self.getTypedRuleContext(BKOOLParser.StmtContext,0)


        def TO(self):
            return self.getToken(BKOOLParser.TO, 0)

        def DOWNTO(self):
            return self.getToken(BKOOLParser.DOWNTO, 0)

        def getRuleIndex(self):
            return BKOOLParser.RULE_for_stmt

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFor_stmt" ):
                return visitor.visitFor_stmt(self)
            else:
                return visitor.visitChildren(self)




    def for_stmt(self):

        localctx = BKOOLParser.For_stmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 50, self.RULE_for_stmt)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 246
            self.match(BKOOLParser.FOR)
            self.state = 247
            self.match(BKOOLParser.ID)
            self.state = 248
            self.match(BKOOLParser.ASSIGN_OP)
            self.state = 249
            self.exp()
            self.state = 250
            _la = self._input.LA(1)
            if not(_la==BKOOLParser.TO or _la==BKOOLParser.DOWNTO):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
            self.state = 251
            self.exp()
            self.state = 252
            self.match(BKOOLParser.DO)
            self.state = 253
            self.stmt()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Call_stmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def exp8(self):
            return self.getTypedRuleContext(BKOOLParser.Exp8Context,0)


        def DOT(self):
            return self.getToken(BKOOLParser.DOT, 0)

        def ID(self):
            return self.getToken(BKOOLParser.ID, 0)

        def LB(self):
            return self.getToken(BKOOLParser.LB, 0)

        def RB(self):
            return self.getToken(BKOOLParser.RB, 0)

        def SEMICOLON(self):
            return self.getToken(BKOOLParser.SEMICOLON, 0)

        def arglist(self):
            return self.getTypedRuleContext(BKOOLParser.ArglistContext,0)


        def getRuleIndex(self):
            return BKOOLParser.RULE_call_stmt

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitCall_stmt" ):
                return visitor.visitCall_stmt(self)
            else:
                return visitor.visitChildren(self)




    def call_stmt(self):

        localctx = BKOOLParser.Call_stmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 52, self.RULE_call_stmt)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 255
            self.exp8(0)
            self.state = 256
            self.match(BKOOLParser.DOT)
            self.state = 257
            self.match(BKOOLParser.ID)
            self.state = 258
            self.match(BKOOLParser.LB)
            self.state = 260
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << BKOOLParser.BOOLLIT) | (1 << BKOOLParser.NIL) | (1 << BKOOLParser.THIS) | (1 << BKOOLParser.NEW) | (1 << BKOOLParser.ADD_OP) | (1 << BKOOLParser.SUB_OP) | (1 << BKOOLParser.NOT_OP) | (1 << BKOOLParser.LB) | (1 << BKOOLParser.FLOATLIT) | (1 << BKOOLParser.INTEGERLIT) | (1 << BKOOLParser.STRINGLIT) | (1 << BKOOLParser.ID))) != 0):
                self.state = 259
                self.arglist()


            self.state = 262
            self.match(BKOOLParser.RB)
            self.state = 263
            self.match(BKOOLParser.SEMICOLON)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ExpContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def exp1(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(BKOOLParser.Exp1Context)
            else:
                return self.getTypedRuleContext(BKOOLParser.Exp1Context,i)


        def LESSER_OP(self):
            return self.getToken(BKOOLParser.LESSER_OP, 0)

        def LESSER_EQUAL_OP(self):
            return self.getToken(BKOOLParser.LESSER_EQUAL_OP, 0)

        def GREATER_OP(self):
            return self.getToken(BKOOLParser.GREATER_OP, 0)

        def GREATER_EQUAL_OP(self):
            return self.getToken(BKOOLParser.GREATER_EQUAL_OP, 0)

        def getRuleIndex(self):
            return BKOOLParser.RULE_exp

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExp" ):
                return visitor.visitExp(self)
            else:
                return visitor.visitChildren(self)




    def exp(self):

        localctx = BKOOLParser.ExpContext(self, self._ctx, self.state)
        self.enterRule(localctx, 54, self.RULE_exp)
        self._la = 0 # Token type
        try:
            self.state = 270
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,20,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 265
                self.exp1()
                self.state = 266
                _la = self._input.LA(1)
                if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << BKOOLParser.LESSER_OP) | (1 << BKOOLParser.LESSER_EQUAL_OP) | (1 << BKOOLParser.GREATER_OP) | (1 << BKOOLParser.GREATER_EQUAL_OP))) != 0)):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 267
                self.exp1()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 269
                self.exp1()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Exp1Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def exp2(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(BKOOLParser.Exp2Context)
            else:
                return self.getTypedRuleContext(BKOOLParser.Exp2Context,i)


        def EQUAL_OP(self):
            return self.getToken(BKOOLParser.EQUAL_OP, 0)

        def NOT_EQUAL_OP(self):
            return self.getToken(BKOOLParser.NOT_EQUAL_OP, 0)

        def getRuleIndex(self):
            return BKOOLParser.RULE_exp1

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExp1" ):
                return visitor.visitExp1(self)
            else:
                return visitor.visitChildren(self)




    def exp1(self):

        localctx = BKOOLParser.Exp1Context(self, self._ctx, self.state)
        self.enterRule(localctx, 56, self.RULE_exp1)
        self._la = 0 # Token type
        try:
            self.state = 277
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,21,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 272
                self.exp2(0)
                self.state = 273
                _la = self._input.LA(1)
                if not(_la==BKOOLParser.NOT_EQUAL_OP or _la==BKOOLParser.EQUAL_OP):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 274
                self.exp2(0)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 276
                self.exp2(0)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Exp2Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def exp3(self):
            return self.getTypedRuleContext(BKOOLParser.Exp3Context,0)


        def exp2(self):
            return self.getTypedRuleContext(BKOOLParser.Exp2Context,0)


        def AND_OP(self):
            return self.getToken(BKOOLParser.AND_OP, 0)

        def OR_OP(self):
            return self.getToken(BKOOLParser.OR_OP, 0)

        def getRuleIndex(self):
            return BKOOLParser.RULE_exp2

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExp2" ):
                return visitor.visitExp2(self)
            else:
                return visitor.visitChildren(self)



    def exp2(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = BKOOLParser.Exp2Context(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 58
        self.enterRecursionRule(localctx, 58, self.RULE_exp2, _p)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 280
            self.exp3(0)
            self._ctx.stop = self._input.LT(-1)
            self.state = 287
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,22,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    localctx = BKOOLParser.Exp2Context(self, _parentctx, _parentState)
                    self.pushNewRecursionContext(localctx, _startState, self.RULE_exp2)
                    self.state = 282
                    if not self.precpred(self._ctx, 2):
                        from antlr4.error.Errors import FailedPredicateException
                        raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                    self.state = 283
                    _la = self._input.LA(1)
                    if not(_la==BKOOLParser.OR_OP or _la==BKOOLParser.AND_OP):
                        self._errHandler.recoverInline(self)
                    else:
                        self._errHandler.reportMatch(self)
                        self.consume()
                    self.state = 284
                    self.exp3(0) 
                self.state = 289
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,22,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class Exp3Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def exp4(self):
            return self.getTypedRuleContext(BKOOLParser.Exp4Context,0)


        def exp3(self):
            return self.getTypedRuleContext(BKOOLParser.Exp3Context,0)


        def ADD_OP(self):
            return self.getToken(BKOOLParser.ADD_OP, 0)

        def SUB_OP(self):
            return self.getToken(BKOOLParser.SUB_OP, 0)

        def getRuleIndex(self):
            return BKOOLParser.RULE_exp3

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExp3" ):
                return visitor.visitExp3(self)
            else:
                return visitor.visitChildren(self)



    def exp3(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = BKOOLParser.Exp3Context(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 60
        self.enterRecursionRule(localctx, 60, self.RULE_exp3, _p)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 291
            self.exp4(0)
            self._ctx.stop = self._input.LT(-1)
            self.state = 298
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,23,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    localctx = BKOOLParser.Exp3Context(self, _parentctx, _parentState)
                    self.pushNewRecursionContext(localctx, _startState, self.RULE_exp3)
                    self.state = 293
                    if not self.precpred(self._ctx, 2):
                        from antlr4.error.Errors import FailedPredicateException
                        raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                    self.state = 294
                    _la = self._input.LA(1)
                    if not(_la==BKOOLParser.ADD_OP or _la==BKOOLParser.SUB_OP):
                        self._errHandler.recoverInline(self)
                    else:
                        self._errHandler.reportMatch(self)
                        self.consume()
                    self.state = 295
                    self.exp4(0) 
                self.state = 300
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,23,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class Exp4Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def exp5(self):
            return self.getTypedRuleContext(BKOOLParser.Exp5Context,0)


        def exp4(self):
            return self.getTypedRuleContext(BKOOLParser.Exp4Context,0)


        def MUL_OP(self):
            return self.getToken(BKOOLParser.MUL_OP, 0)

        def DIV_FLOAT_OP(self):
            return self.getToken(BKOOLParser.DIV_FLOAT_OP, 0)

        def DIV_INT_OP(self):
            return self.getToken(BKOOLParser.DIV_INT_OP, 0)

        def MODUL_OP(self):
            return self.getToken(BKOOLParser.MODUL_OP, 0)

        def getRuleIndex(self):
            return BKOOLParser.RULE_exp4

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExp4" ):
                return visitor.visitExp4(self)
            else:
                return visitor.visitChildren(self)



    def exp4(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = BKOOLParser.Exp4Context(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 62
        self.enterRecursionRule(localctx, 62, self.RULE_exp4, _p)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 302
            self.exp5(0)
            self._ctx.stop = self._input.LT(-1)
            self.state = 309
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,24,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    localctx = BKOOLParser.Exp4Context(self, _parentctx, _parentState)
                    self.pushNewRecursionContext(localctx, _startState, self.RULE_exp4)
                    self.state = 304
                    if not self.precpred(self._ctx, 2):
                        from antlr4.error.Errors import FailedPredicateException
                        raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                    self.state = 305
                    _la = self._input.LA(1)
                    if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << BKOOLParser.MUL_OP) | (1 << BKOOLParser.DIV_FLOAT_OP) | (1 << BKOOLParser.DIV_INT_OP) | (1 << BKOOLParser.MODUL_OP))) != 0)):
                        self._errHandler.recoverInline(self)
                    else:
                        self._errHandler.reportMatch(self)
                        self.consume()
                    self.state = 306
                    self.exp5(0) 
                self.state = 311
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,24,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class Exp5Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def exp6(self):
            return self.getTypedRuleContext(BKOOLParser.Exp6Context,0)


        def exp5(self):
            return self.getTypedRuleContext(BKOOLParser.Exp5Context,0)


        def CONCAT_OP(self):
            return self.getToken(BKOOLParser.CONCAT_OP, 0)

        def getRuleIndex(self):
            return BKOOLParser.RULE_exp5

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExp5" ):
                return visitor.visitExp5(self)
            else:
                return visitor.visitChildren(self)



    def exp5(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = BKOOLParser.Exp5Context(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 64
        self.enterRecursionRule(localctx, 64, self.RULE_exp5, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 313
            self.exp6()
            self._ctx.stop = self._input.LT(-1)
            self.state = 320
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,25,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    localctx = BKOOLParser.Exp5Context(self, _parentctx, _parentState)
                    self.pushNewRecursionContext(localctx, _startState, self.RULE_exp5)
                    self.state = 315
                    if not self.precpred(self._ctx, 2):
                        from antlr4.error.Errors import FailedPredicateException
                        raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                    self.state = 316
                    self.match(BKOOLParser.CONCAT_OP)
                    self.state = 317
                    self.exp6() 
                self.state = 322
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,25,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class Exp6Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NOT_OP(self):
            return self.getToken(BKOOLParser.NOT_OP, 0)

        def exp6(self):
            return self.getTypedRuleContext(BKOOLParser.Exp6Context,0)


        def exp7(self):
            return self.getTypedRuleContext(BKOOLParser.Exp7Context,0)


        def getRuleIndex(self):
            return BKOOLParser.RULE_exp6

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExp6" ):
                return visitor.visitExp6(self)
            else:
                return visitor.visitChildren(self)




    def exp6(self):

        localctx = BKOOLParser.Exp6Context(self, self._ctx, self.state)
        self.enterRule(localctx, 66, self.RULE_exp6)
        try:
            self.state = 326
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [BKOOLParser.NOT_OP]:
                self.enterOuterAlt(localctx, 1)
                self.state = 323
                self.match(BKOOLParser.NOT_OP)
                self.state = 324
                self.exp6()
                pass
            elif token in [BKOOLParser.BOOLLIT, BKOOLParser.NIL, BKOOLParser.THIS, BKOOLParser.NEW, BKOOLParser.ADD_OP, BKOOLParser.SUB_OP, BKOOLParser.LB, BKOOLParser.FLOATLIT, BKOOLParser.INTEGERLIT, BKOOLParser.STRINGLIT, BKOOLParser.ID]:
                self.enterOuterAlt(localctx, 2)
                self.state = 325
                self.exp7()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Exp7Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def exp7(self):
            return self.getTypedRuleContext(BKOOLParser.Exp7Context,0)


        def SUB_OP(self):
            return self.getToken(BKOOLParser.SUB_OP, 0)

        def ADD_OP(self):
            return self.getToken(BKOOLParser.ADD_OP, 0)

        def exp8(self):
            return self.getTypedRuleContext(BKOOLParser.Exp8Context,0)


        def getRuleIndex(self):
            return BKOOLParser.RULE_exp7

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExp7" ):
                return visitor.visitExp7(self)
            else:
                return visitor.visitChildren(self)




    def exp7(self):

        localctx = BKOOLParser.Exp7Context(self, self._ctx, self.state)
        self.enterRule(localctx, 68, self.RULE_exp7)
        self._la = 0 # Token type
        try:
            self.state = 331
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [BKOOLParser.ADD_OP, BKOOLParser.SUB_OP]:
                self.enterOuterAlt(localctx, 1)
                self.state = 328
                _la = self._input.LA(1)
                if not(_la==BKOOLParser.ADD_OP or _la==BKOOLParser.SUB_OP):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 329
                self.exp7()
                pass
            elif token in [BKOOLParser.BOOLLIT, BKOOLParser.NIL, BKOOLParser.THIS, BKOOLParser.NEW, BKOOLParser.LB, BKOOLParser.FLOATLIT, BKOOLParser.INTEGERLIT, BKOOLParser.STRINGLIT, BKOOLParser.ID]:
                self.enterOuterAlt(localctx, 2)
                self.state = 330
                self.exp8(0)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Exp8Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def exp9(self):
            return self.getTypedRuleContext(BKOOLParser.Exp9Context,0)


        def exp8(self):
            return self.getTypedRuleContext(BKOOLParser.Exp8Context,0)


        def LSB(self):
            return self.getToken(BKOOLParser.LSB, 0)

        def exp(self):
            return self.getTypedRuleContext(BKOOLParser.ExpContext,0)


        def RSB(self):
            return self.getToken(BKOOLParser.RSB, 0)

        def DOT(self):
            return self.getToken(BKOOLParser.DOT, 0)

        def ID(self):
            return self.getToken(BKOOLParser.ID, 0)

        def LB(self):
            return self.getToken(BKOOLParser.LB, 0)

        def RB(self):
            return self.getToken(BKOOLParser.RB, 0)

        def arglist(self):
            return self.getTypedRuleContext(BKOOLParser.ArglistContext,0)


        def getRuleIndex(self):
            return BKOOLParser.RULE_exp8

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExp8" ):
                return visitor.visitExp8(self)
            else:
                return visitor.visitChildren(self)



    def exp8(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = BKOOLParser.Exp8Context(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 70
        self.enterRecursionRule(localctx, 70, self.RULE_exp8, _p)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 334
            self.exp9(0)
            self._ctx.stop = self._input.LT(-1)
            self.state = 354
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,30,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    self.state = 352
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,29,self._ctx)
                    if la_ == 1:
                        localctx = BKOOLParser.Exp8Context(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_exp8)
                        self.state = 336
                        if not self.precpred(self._ctx, 4):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 4)")
                        self.state = 337
                        self.match(BKOOLParser.LSB)
                        self.state = 338
                        self.exp()
                        self.state = 339
                        self.match(BKOOLParser.RSB)
                        pass

                    elif la_ == 2:
                        localctx = BKOOLParser.Exp8Context(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_exp8)
                        self.state = 341
                        if not self.precpred(self._ctx, 3):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 3)")
                        self.state = 342
                        self.match(BKOOLParser.DOT)
                        self.state = 343
                        self.match(BKOOLParser.ID)
                        pass

                    elif la_ == 3:
                        localctx = BKOOLParser.Exp8Context(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_exp8)
                        self.state = 344
                        if not self.precpred(self._ctx, 2):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                        self.state = 345
                        self.match(BKOOLParser.DOT)
                        self.state = 346
                        self.match(BKOOLParser.ID)

                        self.state = 347
                        self.match(BKOOLParser.LB)
                        self.state = 349
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)
                        if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << BKOOLParser.BOOLLIT) | (1 << BKOOLParser.NIL) | (1 << BKOOLParser.THIS) | (1 << BKOOLParser.NEW) | (1 << BKOOLParser.ADD_OP) | (1 << BKOOLParser.SUB_OP) | (1 << BKOOLParser.NOT_OP) | (1 << BKOOLParser.LB) | (1 << BKOOLParser.FLOATLIT) | (1 << BKOOLParser.INTEGERLIT) | (1 << BKOOLParser.STRINGLIT) | (1 << BKOOLParser.ID))) != 0):
                            self.state = 348
                            self.arglist()


                        self.state = 351
                        self.match(BKOOLParser.RB)
                        pass

             
                self.state = 356
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,30,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class Exp9Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def exp10(self):
            return self.getTypedRuleContext(BKOOLParser.Exp10Context,0)


        def exp9(self):
            return self.getTypedRuleContext(BKOOLParser.Exp9Context,0)


        def DOT(self):
            return self.getToken(BKOOLParser.DOT, 0)

        def ID(self):
            return self.getToken(BKOOLParser.ID, 0)

        def LB(self):
            return self.getToken(BKOOLParser.LB, 0)

        def RB(self):
            return self.getToken(BKOOLParser.RB, 0)

        def arglist(self):
            return self.getTypedRuleContext(BKOOLParser.ArglistContext,0)


        def getRuleIndex(self):
            return BKOOLParser.RULE_exp9

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExp9" ):
                return visitor.visitExp9(self)
            else:
                return visitor.visitChildren(self)



    def exp9(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = BKOOLParser.Exp9Context(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 72
        self.enterRecursionRule(localctx, 72, self.RULE_exp9, _p)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 358
            self.exp10()
            self._ctx.stop = self._input.LT(-1)
            self.state = 372
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,33,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    localctx = BKOOLParser.Exp9Context(self, _parentctx, _parentState)
                    self.pushNewRecursionContext(localctx, _startState, self.RULE_exp9)
                    self.state = 360
                    if not self.precpred(self._ctx, 2):
                        from antlr4.error.Errors import FailedPredicateException
                        raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                    self.state = 361
                    self.match(BKOOLParser.DOT)
                    self.state = 362
                    self.match(BKOOLParser.ID)
                    self.state = 368
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,32,self._ctx)
                    if la_ == 1:
                        self.state = 363
                        self.match(BKOOLParser.LB)
                        self.state = 365
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)
                        if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << BKOOLParser.BOOLLIT) | (1 << BKOOLParser.NIL) | (1 << BKOOLParser.THIS) | (1 << BKOOLParser.NEW) | (1 << BKOOLParser.ADD_OP) | (1 << BKOOLParser.SUB_OP) | (1 << BKOOLParser.NOT_OP) | (1 << BKOOLParser.LB) | (1 << BKOOLParser.FLOATLIT) | (1 << BKOOLParser.INTEGERLIT) | (1 << BKOOLParser.STRINGLIT) | (1 << BKOOLParser.ID))) != 0):
                            self.state = 364
                            self.arglist()


                        self.state = 367
                        self.match(BKOOLParser.RB)

             
                self.state = 374
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,33,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class Exp10Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NEW(self):
            return self.getToken(BKOOLParser.NEW, 0)

        def ID(self):
            return self.getToken(BKOOLParser.ID, 0)

        def LB(self):
            return self.getToken(BKOOLParser.LB, 0)

        def RB(self):
            return self.getToken(BKOOLParser.RB, 0)

        def arglist(self):
            return self.getTypedRuleContext(BKOOLParser.ArglistContext,0)


        def exp11(self):
            return self.getTypedRuleContext(BKOOLParser.Exp11Context,0)


        def getRuleIndex(self):
            return BKOOLParser.RULE_exp10

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExp10" ):
                return visitor.visitExp10(self)
            else:
                return visitor.visitChildren(self)




    def exp10(self):

        localctx = BKOOLParser.Exp10Context(self, self._ctx, self.state)
        self.enterRule(localctx, 74, self.RULE_exp10)
        self._la = 0 # Token type
        try:
            self.state = 383
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [BKOOLParser.NEW]:
                self.enterOuterAlt(localctx, 1)
                self.state = 375
                self.match(BKOOLParser.NEW)
                self.state = 376
                self.match(BKOOLParser.ID)
                self.state = 377
                self.match(BKOOLParser.LB)
                self.state = 379
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << BKOOLParser.BOOLLIT) | (1 << BKOOLParser.NIL) | (1 << BKOOLParser.THIS) | (1 << BKOOLParser.NEW) | (1 << BKOOLParser.ADD_OP) | (1 << BKOOLParser.SUB_OP) | (1 << BKOOLParser.NOT_OP) | (1 << BKOOLParser.LB) | (1 << BKOOLParser.FLOATLIT) | (1 << BKOOLParser.INTEGERLIT) | (1 << BKOOLParser.STRINGLIT) | (1 << BKOOLParser.ID))) != 0):
                    self.state = 378
                    self.arglist()


                self.state = 381
                self.match(BKOOLParser.RB)
                pass
            elif token in [BKOOLParser.BOOLLIT, BKOOLParser.NIL, BKOOLParser.THIS, BKOOLParser.LB, BKOOLParser.FLOATLIT, BKOOLParser.INTEGERLIT, BKOOLParser.STRINGLIT, BKOOLParser.ID]:
                self.enterOuterAlt(localctx, 2)
                self.state = 382
                self.exp11()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Exp11Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LB(self):
            return self.getToken(BKOOLParser.LB, 0)

        def exp(self):
            return self.getTypedRuleContext(BKOOLParser.ExpContext,0)


        def RB(self):
            return self.getToken(BKOOLParser.RB, 0)

        def element(self):
            return self.getTypedRuleContext(BKOOLParser.ElementContext,0)


        def getRuleIndex(self):
            return BKOOLParser.RULE_exp11

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExp11" ):
                return visitor.visitExp11(self)
            else:
                return visitor.visitChildren(self)




    def exp11(self):

        localctx = BKOOLParser.Exp11Context(self, self._ctx, self.state)
        self.enterRule(localctx, 76, self.RULE_exp11)
        try:
            self.state = 390
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [BKOOLParser.LB]:
                self.enterOuterAlt(localctx, 1)
                self.state = 385
                self.match(BKOOLParser.LB)
                self.state = 386
                self.exp()
                self.state = 387
                self.match(BKOOLParser.RB)
                pass
            elif token in [BKOOLParser.BOOLLIT, BKOOLParser.NIL, BKOOLParser.THIS, BKOOLParser.FLOATLIT, BKOOLParser.INTEGERLIT, BKOOLParser.STRINGLIT, BKOOLParser.ID]:
                self.enterOuterAlt(localctx, 2)
                self.state = 389
                self.element()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ElementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def INTEGERLIT(self):
            return self.getToken(BKOOLParser.INTEGERLIT, 0)

        def FLOATLIT(self):
            return self.getToken(BKOOLParser.FLOATLIT, 0)

        def STRINGLIT(self):
            return self.getToken(BKOOLParser.STRINGLIT, 0)

        def BOOLLIT(self):
            return self.getToken(BKOOLParser.BOOLLIT, 0)

        def NIL(self):
            return self.getToken(BKOOLParser.NIL, 0)

        def ID(self):
            return self.getToken(BKOOLParser.ID, 0)

        def THIS(self):
            return self.getToken(BKOOLParser.THIS, 0)

        def getRuleIndex(self):
            return BKOOLParser.RULE_element

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitElement" ):
                return visitor.visitElement(self)
            else:
                return visitor.visitChildren(self)




    def element(self):

        localctx = BKOOLParser.ElementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 78, self.RULE_element)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 392
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << BKOOLParser.BOOLLIT) | (1 << BKOOLParser.NIL) | (1 << BKOOLParser.THIS) | (1 << BKOOLParser.FLOATLIT) | (1 << BKOOLParser.INTEGERLIT) | (1 << BKOOLParser.STRINGLIT) | (1 << BKOOLParser.ID))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ArglistContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def exp(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(BKOOLParser.ExpContext)
            else:
                return self.getTypedRuleContext(BKOOLParser.ExpContext,i)


        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(BKOOLParser.COMMA)
            else:
                return self.getToken(BKOOLParser.COMMA, i)

        def getRuleIndex(self):
            return BKOOLParser.RULE_arglist

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitArglist" ):
                return visitor.visitArglist(self)
            else:
                return visitor.visitChildren(self)




    def arglist(self):

        localctx = BKOOLParser.ArglistContext(self, self._ctx, self.state)
        self.enterRule(localctx, 80, self.RULE_arglist)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 394
            self.exp()
            self.state = 399
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==BKOOLParser.COMMA:
                self.state = 395
                self.match(BKOOLParser.COMMA)
                self.state = 396
                self.exp()
                self.state = 401
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx



    def sempred(self, localctx:RuleContext, ruleIndex:int, predIndex:int):
        if self._predicates == None:
            self._predicates = dict()
        self._predicates[29] = self.exp2_sempred
        self._predicates[30] = self.exp3_sempred
        self._predicates[31] = self.exp4_sempred
        self._predicates[32] = self.exp5_sempred
        self._predicates[35] = self.exp8_sempred
        self._predicates[36] = self.exp9_sempred
        pred = self._predicates.get(ruleIndex, None)
        if pred is None:
            raise Exception("No predicate with index:" + str(ruleIndex))
        else:
            return pred(localctx, predIndex)

    def exp2_sempred(self, localctx:Exp2Context, predIndex:int):
            if predIndex == 0:
                return self.precpred(self._ctx, 2)
         

    def exp3_sempred(self, localctx:Exp3Context, predIndex:int):
            if predIndex == 1:
                return self.precpred(self._ctx, 2)
         

    def exp4_sempred(self, localctx:Exp4Context, predIndex:int):
            if predIndex == 2:
                return self.precpred(self._ctx, 2)
         

    def exp5_sempred(self, localctx:Exp5Context, predIndex:int):
            if predIndex == 3:
                return self.precpred(self._ctx, 2)
         

    def exp8_sempred(self, localctx:Exp8Context, predIndex:int):
            if predIndex == 4:
                return self.precpred(self._ctx, 4)
         

            if predIndex == 5:
                return self.precpred(self._ctx, 3)
         

            if predIndex == 6:
                return self.precpred(self._ctx, 2)
         

    def exp9_sempred(self, localctx:Exp9Context, predIndex:int):
            if predIndex == 7:
                return self.precpred(self._ctx, 2)
         




