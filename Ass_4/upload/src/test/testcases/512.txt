
        class a {
            void main(){
                a : abc;
                a := new abc(12,4.0,true);
                a.hehe();
            }
        }
        class abc{
            x: int;
            y : float;
            z : boolean;
            abc(x : int; y : float; z : boolean){
                this.x := x;
                this.y := y;
                this.z := z;
            }
            void hehe(){
                io.writeBool(this.z);
            }
        }
        