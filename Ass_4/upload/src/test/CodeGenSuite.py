import unittest
from TestUtils import TestCodeGen
from AST import *


class CheckCodeGenSuite(unittest.TestCase):
    def test_1(self):
        input ="""
        class a {
            void main(){
                hehe : float;
                hehe := 10 / 2;
                io.writeFloat(hehe);
        }
    }
        """
        expect = "5.0"
        self.assertTrue(TestCodeGen.test(input,expect,501))

    def test_2(self):
        input ="""
        class Haha{
            height,width : int;
        }
        class a extends Haha{
            void main(){
                hehe : float;
                hehe := 10 / 2;
                io.writeFloat(hehe);
            }
        }
        """
        expect = "5.0"
        self.assertTrue(TestCodeGen.test(input,expect,502))

    def test_3(self):
        input ="""
        class Haha{
            height,width : int;
            Haha(height : int; width : int){
                this.height := height;
                this.width := width;
            }
        }
        class a extends Haha{
            void main(){
                hehe : float;
                hehe := 16 / 2;
                io.writeFloat(hehe);
            }
        }
        """
        expect = "8.0"
        self.assertTrue(TestCodeGen.test(input,expect,503))

    def test_4(self):
        input ="""
        class a {
            void main(){
                x : int;
                for x:=1 to 10 do{
                    io.writeInt(x);
                }
            }
        }
        """
        expect = "12345678910"
        self.assertTrue(TestCodeGen.test(input,expect,504))

    def test_5(self):
        input ="""
        class a {
            void main(){
                x : int;
                x := 2;
                if 1 < 2 then
                    io.writeInt(x);
            }
        }
        """
        expect = "2"
        self.assertTrue(TestCodeGen.test(input,expect,505))

    def test_6(self):
        input ="""
        class a {
            void main(){
                x : int;
                x := 10 % 3;
                io.writeInt(x);
            }
        }
        """
        expect = "1"
        self.assertTrue(TestCodeGen.test(input,expect,506))

    def test_7(self):
        input ="""
        class a {
            void main(){
                hehe : b;
                hehe := new b(12,23);
                io.writeInt(hehe.chieucao);
            }
        }
        class b{
            chieucao : int;
            cannang : int;
            b(chieucao :int; cannang: int){
                this.chieucao := chieucao;
                this.cannang := cannang;
            }
        }
        """
        expect = "1"
        self.assertTrue(TestCodeGen.test(input,expect,507))

    def test_7(self):
        input ="""
        class a {
            void main(){
                a : int;
                a := b.hehe(3);
                io.writeInt(a);
            }
        }
        class b{
            int static hehe(x : int){
                return 10;
            }
        }
        """
        expect = "10"
        self.assertTrue(TestCodeGen.test(input,expect,507))

    def test_8(self):
        input ="""
        class a {
            void main(){
                b.hehe(3);
            }
        }
        class b{
            void static hehe(x : int){
                io.writeInt(x);
            }
        }
        """
        expect = "3"
        self.assertTrue(TestCodeGen.test(input,expect,508))

    def test_9(self):
        input ="""
        class a {
            void main(){
                hehe : b;
                hehe := new b();
                hehe.hehe(15);
            }
        }
        class b{
            void hehe(x : int){
                io.writeInt(x);
            }
        }
        """
        expect = "15"
        self.assertTrue(TestCodeGen.test(input,expect,509))

    def test_10(self):
        input ="""
        class a {
            void main(){
                hehe : b;
                hehe := new b();
                hehe.hehe(15);
            }
        }
        class b{
            void hehe(x : int){
                io.writeInt(x);
            }
        }
        """
        expect = "15"
        self.assertTrue(TestCodeGen.test(input,expect,510))

    def test_11(self):
        input ="""
        class a {
            void main(){
                kaka : float;
                hehe : b;
                hehe := new b();
                kaka := hehe.hehe(3);
                io.writeFloat(kaka);
            }
        }
        class b{
            float hehe(x : int){
                return 2;
            }
        }
        """
        expect = "2.0"
        self.assertTrue(TestCodeGen.test(input,expect,511))

    def test_12(self):
        input ="""
        class a {
            void main(){
                a : abc;
                a := new abc(12,4.0,true);
                a.hehe();
            }
        }
        class abc{
            x: int;
            y : float;
            z : boolean;
            abc(x : int; y : float; z : boolean){
                this.x := x;
                this.y := y;
                this.z := z;
            }
            void hehe(){
                io.writeBool(this.z);
            }
        }
        """
        expect = "true"
        self.assertTrue(TestCodeGen.test(input,expect,512))

    def test_13(self):
        input ="""
        class a {
            void main(){
                a : abc;
                a := new abc(12,4,true);
                a.hehe();
            }
        }
        class abc{
            x: int;
            y : float;
            z : boolean;
            abc(x : int; y : float; z : boolean){
                this.x := x;
                this.y := y;
                this.z := z;
            }
            void hehe(){
                io.writeBool(this.z);
            }
        }
        """
        expect = "true"
        self.assertTrue(TestCodeGen.test(input,expect,513))

    def test_14(self):
        input ="""
        class a {
            void main(){
                i : int;
                for i:=1 to 10 do{
                    x: float;
                    x := 20;
                    io.writeFloat(x);
                }
            }
        }
        """
        expect = "20.020.020.020.020.020.020.020.020.020.0"
        self.assertTrue(TestCodeGen.test(input,expect,514))

    def test_15(self):
        input ="""
        class a extends b{
            void main(){
                i : int;
                for i:=1 to 10 do{
                    x: float;
                    x := 20;
                    io.writeFloat(x);
                }
            }
        }
        class b{
            static hehe : int;
            void zhz(){
                s: int;
            }
        }
        """
        expect = "20.020.020.020.020.020.020.020.020.020.0"
        self.assertTrue(TestCodeGen.test(input,expect,515))

    def test_16(self):
        input ="""
        class a extends b{
            void main(){
                i : int;
                for i:=1 to 10 do{
                    x: float;
                    x := 20;
                    io.writeFloat(x);
                }
            }
        }
        class b{
            static hehe : int;
            void zhz(){
                s: int;
            }
        }
        """
        expect = "20.020.020.020.020.020.020.020.020.020.0"
        self.assertTrue(TestCodeGen.test(input,expect,516))

    def test_17(self):
        input ="""
        class a extends b{
            void main(){
                variable : b;
                variable := new b(1,2,3);
                io.writeStr("successful");
            }
        }
        class b{
            mot, hai, ba : int;
            b(mot : int; hai: int; ba: int){
                this.mot := 1;
                this.hai := 2;
                this.ba := 3;
            }
        }
        """
        expect = "successful"
        self.assertTrue(TestCodeGen.test(input,expect,517))

    def test_18(self):
        input ="""
        class a extends b{
            void main(){
                x,y,z : int;
                x := 10 + 2;
                y := 10 / 2;
                z := 10 \ 2;
                io.writeFloat(x);
                io.writeFloat(y);
                io.writeFloat(z);
            }
        }
        """
        expect = "12.05.05.0"
        self.assertTrue(TestCodeGen.test(input,expect,518))

    def test_19(self):
        input ="""
        class a extends b{
            void main(){
                x : float;
                x := 10.32 / 1.5;
                io.writeFloat(x);
            }
        }
        """
        expect = "6.8799996"
        self.assertTrue(TestCodeGen.test(input,expect,519))

    def test_20(self):
        input ="""
        class a extends b{
            void main(){
                if 11 == 11 then {
                    io.writeInt(1);
                }
            }
        }
        """
        expect = "1"
        self.assertTrue(TestCodeGen.test(input,expect,520))

    def test_21(self):
        input ="""
        class a extends b{
            void main(){
                if (1<2) && (4==4) then {
                    io.writeInt(1);
                }
            }
        }
        """
        expect = "1"
        self.assertTrue(TestCodeGen.test(input,expect,521))

    def test_22(self):
        input ="""
        class a{
            void main(){
                maxVal : int;
                variable : b;
                variable := new b();
                maxVal := variable.FindMax(1,2);
                io.writeInt(maxVal);
            }
        }
        class b{
            hihi : int;
            int FindMax(a : int;b : int){
                if a>b then{
                    return a;
                }
                return b;
            }
        }
        """
        expect = "2"
        self.assertTrue(TestCodeGen.test(input,expect,522))

    def test_23(self):
        input ="""
        class a{
            void main(){
                maxVal : int;
                variable : b;
                variable := new b();
                maxVal := variable.FindMax(1,2,3);
                io.writeInt(maxVal);
            }
        }
        class b{
            hihi : int;
            int FindMax(a : int;b : int;c :int){
                if (a>b) && (a>c) then{
                    return a;
                }
                if (b>a) && (b>c) then{
                    return b;
                }
                return c;
            }
        }
        """
        expect = "3"
        self.assertTrue(TestCodeGen.test(input,expect,523))

    def test_24(self):
        input ="""
        class a{
            void main(){
                if 1 <= 2 then
                    io.writeInt(1);
            }
        }
        """
        expect = "1"
        self.assertTrue(TestCodeGen.test(input,expect,524))

    def test_25(self):
        input ="""
        class a{
            string hehe(){
                return "votaivuong";
            }
            void main(){
                if 1 <= 2 then{
                    object : a;
                    object := new a();
                    io.writeStr(object.hehe());
                }
            }
        }
        """
        expect = "votaivuong"
        self.assertTrue(TestCodeGen.test(input,expect,525))

    def test_26(self):
        input ="""
        class a{
            void main(){
                if 1 <= 2 then
                    io.writeInt(1);
            }
        }
        """
        expect = "1"
        self.assertTrue(TestCodeGen.test(input,expect,526))

    def test_27(self):
        input ="""
        class a{
            float hehe(){
                return 1;
            }
            void main(){
                if 1 <= 2 then{
                    object : a;
                    object := new a();
                    io.writeFloat(object.hehe());
                }
            }
        }
        """
        expect = "1.0"
        self.assertTrue(TestCodeGen.test(input,expect,527))

    def test_28(self):
        input ="""
        class a{
            y : float;
            x : float;
            float hehe(){
                return 1;
            }
            void main(){
                variable : a;
                variable := new a();
                variable.y := 2;
                variable.x := variable.y + 1;
                io.writeFloat(variable.x);
            }
        }
        """
        expect = "3.0"
        self.assertTrue(TestCodeGen.test(input,expect,528))

    def test_29(self):
        input ="""
        class a{
            static final int My1stCons = 1;
            x : float;
            float hehe(){
                return 1;
            }
            void main(){
                variable : a;
                variable := new a();
                variable.x := a.My1stCons + 1;
                io.writeFloat(variable.x);
            }
        }
        """
        expect = "2.0"
        self.assertTrue(TestCodeGen.test(input,expect,529))

    def test_30(self):
        input ="""
        class a{
            void main(){
                io.writeStr("mot"^"hai");
            }
        }
        """
        expect = "mothai"
        self.assertTrue(TestCodeGen.test(input,expect,530))

    def test_31(self):
        input ="""
        class a{
            void main(){
                x : int;
                for x:=1 to 10 do{
                    io.writeInt(x);
                }
            }
        }
        """
        expect = "12345678910"
        self.assertTrue(TestCodeGen.test(input,expect,531))

    def test_32(self):
        input ="""
        class a{
            void main(){
                x : int;
                x := -1;
                io.writeInt(x);
            }
        }
        """
        expect = "-1"
        self.assertTrue(TestCodeGen.test(input,expect,532))

    def test_33(self):
        input ="""
        class a{
            void main(){
                x : boolean;
                x := true;
                io.writeBool(!x);
            }
        }
        """
        expect = "false"
        self.assertTrue(TestCodeGen.test(input,expect,533))

    def test_34(self):
        input ="""
        class a{
            void main(){
                x : int;
                for x:=1 to 10 do{
                    if x==5 then
                        break;
                    io.writeInt(x);
                }
            }
        }
        """
        expect = "1234"
        self.assertTrue(TestCodeGen.test(input,expect,534))

    def test_35(self):
        input ="""
        class a{
            void main(){
                x : int;
                for x:=1 to 10 do{
                    if x == 4 then
                        continue;
                    io.writeInt(x);
                }
            }
        }
        """
        expect = "1235678910"
        self.assertTrue(TestCodeGen.test(input,expect,535))
