'''
 *   @author Nguyen Hua Phung
 *   @version 1.0
 *   23/10/2015
 *   This file provides a simple version of code generator
 *
'''
from Utils import *
from StaticCheck import *
from StaticError import *
from Emitter import Emitter
from Frame import Frame
from abc import ABC, abstractmethod
from functools import reduce

class MType(Type):
    
    def __init__(self,intype,outtype):
        #intype:list(Type)
        #outtype:Type
        self.partype = intype
        self.rettype = outtype

    def __str__(self):
        return "MType(["+",".join([str(x) for x in self.partype])+"],"+str(self.rettype)

    def accept(self, v, param):
        return None

class ArrayPointerType(Type):
    def __init__(self, ctype):
        #cname: String
        self.eleType = ctype

    def __str__(self):
        return "ArrayPointerType({0})".format(str(self.eleType))

    def accept(self, v, param):
        return None

class MethodEnv():
    def __init__(self, emit, classname, parentname, declist):
        #emit:Emitter
        #classname:String
        #parentname:String
        #declist:list(Decl)
        self.emit = emit
        self.classname = classname
        self.parentname = parentname
        self.declist = declist

class StmtEnv():
    def __init__(self, frame, sym, methodenv):
        #frame: Frame
        #sym: List[Symbol]

        self.frame = frame
        self.sym = sym
        self.method = methodenv

class ExprEnv():
    def __init__(self, isLeft, isFirst, stmt):
        #stmt: StmtEnv
        #isLeft: Boolean
        #isFirst: Boolean
        self.stmt = stmt
        self.isLeft = isLeft
        self.isFirst = isFirst

class Val(ABC):
    pass

class Index(Val):
    def __init__(self, value):
        #value: Int

        self.value = value

class CName(Val):
    def __init__(self, value):
        #value: String

        self.value = value

class Member:
    def __init__(self,name,skind,mtype,value = None):
        #name:String
        #skind:SIKind
        #kind:Kind
        #mtype:Type
        #value:Expr
        self.name = name
        self.skind = skind
        self.mtype = mtype
        self.value = value
class ClassData:
    def __init__(self,cname,pname,mem):
        #cname:String -- class name
        #pname:String -- parent name
        #mem:list(Member)
        self.cname = cname
        self.pname = pname
        self.mem = mem

class GlobalEnvironment(BaseVisitor):
    def __init__(self,env):
        #env:list(ClassData)
        self.env = env

    def visitProgram(self,ast:Program,o):
        return list(reduce(lambda x,y: self.visit(y,x),ast.decl,self.env))
    
    def visitClassDecl(self,ast:ClassDecl,o):
        return [ClassData(ast.classname.name,
                        ast.parentname.name if ast.parentname else "",
                        list(reduce(lambda x,y: self.visit(y,x),ast.memlist,[])))] + o
      
    def visitAttributeDecl(self,ast,o):
        name,mtype,e = self.visit(ast.decl,o)
        return [Member(ast.sikind,name,mtype,e)] + o

    def visitVarDecl(self,ast,o):
        return ast.variable.name,ast.varType,None

    def visitConstDecl(self,ast,o):
        return ast.constant.name,ast.constType,ast.value

    def visitMethodDecl(self,ast:MethodDecl,o):
        return [Member(ast.name.name,
                        ast.sikind,
                        MType([x.varType for x in ast.param],ast.returnType))] + o

class CodeGenerator(Utils):
    def __init__(self):
        self.libName = "io"

    def init(self):
        mem = [Member("readInt",Static(),MType([],IntType()),None),
               Member("writeInt",Static(),MType([IntType()],VoidType()),None),
               Member("writeIntLn",Static(),MType([IntType()],VoidType()),None),
               Member("readFloat", Static(), MType([], FloatType()), None),
               Member("writeFloat", Static(), MType([FloatType()], VoidType()), None),
               Member("writeFloatLn", Static(), MType([FloatType()], VoidType()), None),
               Member("readBool", Static(), MType([], BoolType()), None),
               Member("writeBool", Static(), MType([BoolType()], VoidType()), None),
               Member("writeBoolLn", Static(), MType([BoolType()], VoidType()), None),
               Member("readStr", Static(), MType([], StringType()), None),
               Member("writeStr", Static(), MType([StringType()], VoidType()), None),
               Member("writeStrLn", Static(), MType([StringType()], VoidType()), None)
               ]
        return [ClassData("io","",mem)]

    def gen(self, ast, dir_):
        #ast: AST
        #dir_: String

        gl = self.init()
        ge = GlobalEnvironment(gl)
        glenv = ge.visit(ast,None)
        gc = CodeGenVisitor(ast, glenv, dir_)
        gc.visit(ast, None)


class CodeGenVisitor(BaseVisitor, Utils):
    def __init__(self, astTree, env, dir_):
        #astTree: AST
        #env: List[ClassData]
        #dir_: File

        self.astTree = astTree
        self.env = env
        self.path = dir_
        

    def visitProgram(self, ast, c):
        #ast: Program
        #c: Any
        for x in ast.decl:
            self.visit(x,c)
        return c

    def visitClassDecl(self, ast, c):
        #ast:ClassDecl
        #c:Any
        emit = Emitter(self.path + "/" + ast.classname.name + ".j")
        parentname = ast.parentname.name if ast.parentname else "java.lang.Object"
        emit.printout(emit.emitPROLOG(ast.classname.name, parentname))
        e = MethodEnv(emit, ast.classname.name,parentname,[])
        list(map(lambda x: self.visit(x, e),ast.memlist))

        # generate default constructor
        self.Object = None
        if ast.parentname:
            self.Object = self.findInit(ast.parentname.name)
        if not self.isInited and (ast.parentname is None or (ast.parentname and self.Object is None)):
            self.genMETHOD(MethodDecl(Instance(),Id("<init>"), list(), None, Block(list(), list())), e, Frame("<init>", VoidType()))
        elif not self.isInited and self.Object is not None:
            self.genMETHOD(MethodDecl(Instance(), Id("<init>"), self.Object[0], None, Block(list(), list())), e, Frame("<init>", VoidType()))
        self.isInited = False
        emit.emitEPILOG()
        return c

    def visitMethodDecl(self, ast, o):
        #ast: MethodDecl
        #o: MethodEnv
        frame = Frame(ast.name, ast.returnType)
        self.genMETHOD(ast, o, frame)
        return o
    def visitAttributeDecl(self, ast, o):
        # ast: AttributeDecl
        # o: MethodEnv
        emit = o.emit
        if type(ast.sikind) is Static and type(ast.decl) is ConstDecl:
            emit.printout(emit.emitATTRIBUTESTATIC(ast.decl.constant.name, ast.decl.constType, True, ast.decl.value.value))
        elif type(ast.sikind) is Static and type(ast.decl) is VarDecl:
            emit.printout(emit.emitATTRIBUTESTATIC(ast.decl.variable.name, ast.decl.varType, False, ""))
        elif type(ast.sikind) is Instance and type(ast.decl) is ConstDecl:
            emit.printout(emit.emitATTRIBUTEINSTANCE(ast.decl.constant.name, ast.decl.constType, True, ""))
        else:
            emit.printout(emit.emitATTRIBUTEINSTANCE(ast.decl.variable.name, ast.decl.varType, False, ""))

    def visitVarDecl(self, ast, o):
        # ast : VarDecl
        # o : StmtEnv
        frame = o.frame
        sym = o.sym
        emit = o.method.emit
        if type(ast) is VarDecl:
            emit.printout(emit.emitVAR(frame.getNewIndex(), ast.variable.name, ast.varType, frame.getStartLabel(), frame.getEndLabel(), frame))
            sym = [Symbol(ast.variable.name, ast.varType, Index(frame.getCurrIndex() - 1))] + sym
        else:
            emit.printout(emit.emitVAR(frame.getNewIndex(), ast.constant.name, ast.constType, frame.getStartLabel(), frame.getEndLabel(), frame))
            sym = [Symbol(ast.constant.name, ast.constType, Index(frame.getCurrIndex() - 1))] + sym
        return StmtEnv(frame, sym, o.method)

    def genMETHOD(self, consdecl, o, frame):
        #consdecl: MethodDecl
        #o: MethodEnv
        #frame: Frame

        isInit = consdecl.returnType is None
        isMain = consdecl.name.name == "main" and len(consdecl.param) == 0 and type(consdecl.returnType) is VoidType

        returnType = VoidType() if isInit else consdecl.returnType
        methodName = "<init>" if isInit else consdecl.name.name
        intype =   [ArrayPointerType(StringType())] if isMain else [x.varType for x in consdecl.param]
        mtype = MType(intype, returnType)
        emit = o.emit
        emit.printout(emit.emitMETHOD(methodName, mtype, type(consdecl.sikind) is Static, frame))

        frame.enterScope(True)
        
        # Generate code for parameter declarations
        if (type(consdecl.sikind) is Instance):
            emit.printout(emit.emitVAR(frame.getNewIndex(),"this",ClassType(Id(o.classname)),frame.getStartLabel(),frame.getEndLabel(),frame))
        elif isMain:
            emit.printout(emit.emitVAR(frame.getNewIndex(), "args", ArrayPointerType(StringType()), frame.getStartLabel(), frame.getEndLabel(), frame))
        sym = []

        # Generate code for parameter
        for x in consdecl.param:
            emit.printout(emit.emitVAR(frame.getNewIndex(), x.variable.name, x.varType, frame.getStartLabel(), frame.getEndLabel(), frame))
            sym += [Symbol(x.variable.name, x.varType, Index(frame.getCurrIndex() - 1))]

        # Generate code for local declarations
        for x in consdecl.body.decl:
            if type(x) is VarDecl:
                emit.printout(emit.emitVAR(frame.getNewIndex(), x.variable.name, x.varType, frame.getStartLabel(), frame.getEndLabel(), frame))
                sym += [Symbol(x.variable.name, x.varType, Index(frame.getCurrIndex() - 1))]
            else:
                emit.printout(emit.emitVAR(frame.getNewIndex(), x.constant.name, x.constType, frame.getStartLabel(), frame.getEndLabel(), frame))
                sym += [Symbol(x.constant.name, x.constType, Index(frame.getCurrIndex() - 1))]

        body = consdecl.body
        emit.printout(emit.emitLABEL(frame.getStartLabel(), frame))

        if isInit:
            self.isInited = True
            emit.printout(emit.emitREADVAR("this", ClassType(Id(o.classname)), 0, frame))
            emit.printout(emit.emitINVOKESPECIAL(frame, o.parentname + "/<init>", MType([], VoidType())))

        # Generate code for statements
        list(map(lambda x: self.visit(x, StmtEnv(frame, sym, o)), body.stmt))

        emit.printout(emit.emitLABEL(frame.getEndLabel(), frame))
        if type(returnType) is VoidType:
            emit.printout(emit.emitRETURN(VoidType(), frame))
        emit.printout(emit.emitENDMETHOD(frame))
        frame.exitScope();


    ### ------------------------------ STMT ---------------------------------------

    def visitBlock(self, ast, o):
        # ast: Block
        # o : StmtEnv
        stmtEnv = o
        for x in ast.decl:
            stmtEnv = self.visit(x, stmtEnv)
        for y in ast.stmt:
            self.visit(y, stmtEnv)


    def visitAssign(self, ast, o):
        # ast : Assign
        # o : StmtEnv
        frame = o.frame
        emit = o.method.emit
        lc, lt, rc, rt, lc2, lt2 = "","","","","",""
        if type(ast.lhs) in [FieldAccess, ArrayCell]:
            self.isFieldAccess = True
            lc, lt = self.visit(ast.lhs, ExprEnv(False, True, o))
            self.isFieldAccess = False
            rc, rt = self.visit(ast.exp, ExprEnv(False, True, o))
            lc2, lt2 = self.visit(ast.lhs, ExprEnv(True, False, o))
            if type(lt) is type(rt):
                emit.printout(lc + rc + lc2)
            elif type(lt) is FloatType and type(rt) is IntType:
                emit.printout(lc + rc + emit.emitI2F(frame) + lc2)
        else:
            rc, rt = self.visit(ast.exp, ExprEnv(False, True, o))
            lc, lt = self.visit(ast.lhs, ExprEnv(True, True, o))
            if type(lt) is type(rt):
                emit.printout(rc + lc)
            elif type(lt) is FloatType and type(rt) is IntType:
                emit.printout(rc + emit.emitI2F(frame) + lc)

    def visitIf(self, ast, o):
        # ast : If ast
        # o : StmtEnv

        frame = o.frame
        emit = o.method.emit
        exp, typeExp = self.visit(ast.expr, ExprEnv(False, True, o))
        falseLabel = frame.getNewLabel()
        emit.printout(exp + emit.emitIFFALSE(falseLabel, frame))
        self.visit(ast.thenStmt, o)
        if not ast.elseStmt:
            emit.printout(emit.emitLABEL(falseLabel, frame))
        else:
            trueLabel = frame.getNewLabel()
            emit.printout(emit.emitGOTO(trueLabel, frame) + emit.emitLABEL(falseLabel, frame))
            self.visit(ast.elseStmt, o)
            emit.printout(emit.emitLABEL(trueLabel, frame))

    def visitFor(self, ast, o):
        # ast : For ast
        # o : StmtEnv

        if ast.up:
            up, op = '<=', '+'
        else:
            up, op = '>=', '-'
        frame = o.frame
        emit = o.method.emit
        buffer = ""
        labelFor = frame.getNewLabel()
        frame.enterLoop()
        expr1, lt = self.visit(ast.expr1, ExprEnv(False, True, o))
        expr2, rt = self.visit(ast.expr2, ExprEnv(False, True, o))
        buffer += expr1
        buffer += self.visit(ast.id, ExprEnv(True, True, o))[0]
        buffer += emit.emitLABEL(labelFor, frame)

        buffer += self.visit(ast.id, ExprEnv(False, True, o))[0]
        buffer += expr2
        buffer += emit.emitREOP(up, IntType(), frame)
        buffer += emit.emitIFFALSE(frame.getBreakLabel(), frame)
        emit.printout(buffer)

        self.visit(ast.loop, o)
        a = ""
        a += emit.emitLABEL(frame.getContinueLabel(), frame)

        a += self.visit(ast.id, ExprEnv(False, False, o))[0]
        a += emit.emitPUSHICONST(1, frame)
        a += emit.emitADDOP(op, IntType(), frame)
        a += self.visit(ast.id, ExprEnv(True, True, o))[0]

        a += emit.emitGOTO(labelFor, frame)
        a += emit.emitLABEL(frame.getBreakLabel(), frame)
        emit.printout(a)
        frame.exitLoop()

    def visitBreak(self, ast, o):
        # ast : Break
        # o : StmtEnv

        frame = o.frame
        emit = o.method.emit
        emit.printout(emit.emitGOTO(frame.getBreakLabel(), frame))

    def visitContinue(self, ast, o):
        # ast : Break
        # o : StmtEnv

        frame = o.frame
        emit = o.method.emit
        emit.printout(emit.emitGOTO(frame.getContinueLabel(), frame))

    def visitReturn(self, ast, o):
        # ast : Return
        # o : StmtEnv

        frame = o.frame
        emit = o.method.emit
        expr, typeExpr = self.visit(ast.expr, ExprEnv(False, True, o))
        emit.printout(expr)
        if type(typeExpr) is IntType and type(frame.returnType) is FloatType:
            emit.printout(emit.emitI2F(frame))
        emit.printout(emit.emitRETURN(frame.returnType, frame))

    def visitCallStmt(self, ast, o):
        #ast: CallStmt
        #o: StmtEnv
        emit = o.method.emit
        frame = o.frame

        cname, ctype = self.visit(ast.obj, ExprEnv(False, True, o))
        classname = cname
        isStatic = False
        if cname == "":
            isStatic = True
        if cname == "":
            cname = ast.obj.name
            ctype = ClassType(ast.obj)
        classData = self.lookup(ctype.classname.name, self.env, lambda x:x.cname)
        member = self.lookup(ast.method.name, classData.mem, lambda x:x.name)
        mtype = member.mtype
        i = 0
        in_ = ("", list())
        in_ = (in_[0] + classname, list())
        for x in ast.param:
            str1, typ1 = self.visit(x, ExprEnv(False, True, o))
            if type(typ1) is IntType and type(mtype.partype[i]) is FloatType:
                str1 += emit.emitI2F(frame)
            in_ = (in_[0] + str1, list())
            i += 1
        emit.printout(in_[0])
        if isStatic:
            emit.printout(emit.emitINVOKESTATIC(ctype.classname.name + "/" + ast.method.name, mtype, frame))
        else:
            emit.printout(emit.emitINVOKEVIRTUAL(ctype.classname.name + "/" + ast.method.name, mtype, frame))

    ### ------------------------------- EXPR --------------------------------------

    def visitBinaryOp(self, ast, o):
        # ast: BinaryOp
        # o : ExprEnv

        emit = o.stmt.method.emit
        frame = o.stmt.frame
        lc, lt = self.visit(ast.left, o)
        rc, rt = self.visit(ast.right, o)
        if ast.op == '\\':
            return lc + rc + emit.emitDIV(frame), IntType()
        elif ast.op == '%':
            return lc + rc + emit.emitMOD(frame), IntType()
        elif ast.op == '^':
            return emit.emitCONCAT("\""+ ast.left.value + ast.right.value +"\"", frame), StringType()
        elif ast.op in ['==', '!=']:
            if type(lt) is IntType:
                return lc + rc + emit.emitREOP(ast.op, lt, frame), IntType()
            if type(lt) is BoolType:
                return lc + rc + emit.emitREOP(ast.op, lt, frame), BoolType()
        elif ast.op in ['&&', '||']:
            typ = BoolType()
            buffer = ""
            LabelShort = frame.getNewLabel()
            LabelFinish = frame.getNewLabel()
            buffer += lc
            if ast.op == '&&':
                buffer += emit.emitIFFALSE(LabelShort, frame)
                buffer += rc
                buffer += emit.emitIFFALSE(LabelShort, frame)
                buffer += emit.emitPUSHZCONST("true", frame)
                frame.pop()
                buffer += emit.emitGOTO(LabelFinish, frame)
                buffer += emit.emitLABEL(LabelShort, frame)
                buffer += emit.emitPUSHZCONST("false", frame)
                buffer += emit.emitLABEL(LabelFinish, frame)
                return buffer, typ
            elif ast.op == '||':
                buffer += emit.emitIFTRUE(LabelShort, frame)
                buffer += rc
                buffer += emit.emitIFTRUE(LabelShort, frame)
                buffer += emit.emitPUSHZCONST("false", frame)
                frame.pop()
                buffer += emit.emitGOTO(LabelFinish, frame)
                buffer += emit.emitLABEL(LabelShort, frame)
                buffer += emit.emitPUSHZCONST("true", frame)
                buffer += emit.emitLABEL(LabelFinish, frame)
                return buffer, typ
        elif ast.op in ['<', '<=', '>=', '>']:
            if type(lt) is type(rt):
                if type(lt) is IntType:
                    return lc + rc + emit.emitREOP(ast.op, lt, frame), BoolType()
                else:
                    return lc + rc + emit.emitFREOP(ast.op, lt, frame), BoolType()
            else:
                if type(lt) is IntType:
                    return lc + emit.emitI2F(frame) + rc + emit.emitFREOP(ast.op, rt, frame), BoolType()
                else:
                    return lc + rc + emit.emitI2F(frame) + emit.emitFREOP(ast.op, lt, frame), BoolType()
        elif ast.op in ['+', '-']:
            if type(lt) is type(rt):
                return lc + rc + emit.emitADDOP(ast.op, lt, frame), lt
            else:
                if type(lt) is IntType:
                    return lc + emit.emitI2F(frame) + rc + emit.emitADDOP(ast.op, rt, frame), FloatType()
                else:
                    return lc + rc + emit.emitI2F(frame) + emit.emitADDOP(ast.op, lt, frame), FloatType()
        elif ast.op in ['*', '/']:
            if type(lt) is type(rt):
                return lc + rc + emit.emitMULOP(ast.op, lt, frame), lt
            else:
                if type(lt) is IntType:
                    return lc + emit.emitI2F(frame) + rc + emit.emitMULOP(ast.op, rt, frame), FloatType()
                else:
                    return lc + rc + emit.emitI2F(frame) + emit.emitMULOP(ast.op, lt, frame), FloatType()
        elif ast.op == '/':
            if type(lt) is IntType:
                lc = lc + emit.emitI2F(frame)
            if type(rt) is IntType:
                rc = rc + emit.emitI2F(frame)
            return lc + rc + emit.emitMULOP(ast.op, FloatType(), frame), FloatType()

    def visitUnaryOp(self, ast, o):
        # ast: UnaryOp
        # o : ExprEnv
        emit = o.stmt.method.emit
        frame = o.stmt.frame
        c, t = self.visit(ast.body, o)
        if ast.op == "-":
            return c + emit.emitNEGOP(t, frame), t
        else:
            return c + emit.emitNOT(t, frame), t

    def visitCallExpr(self, ast, o):
        # ast: CallStmt
        # o: ExprEnv
        emit = o.stmt.method.emit
        frame = o.stmt.frame

        cname, ctype = self.visit(ast.obj, o)
        classname = cname
        isStatic = False
        if cname == "":
            isStatic = True
        if cname == "":
            cname = ast.obj.name
            ctype = ClassType(ast.obj)
        classData = self.lookup(ctype.classname.name, self.env, lambda x: x.cname)
        member = self.lookup(ast.method.name, classData.mem, lambda x: x.name)
        mtype = member.mtype
        i = 0
        in_ = ("", list())
        in_ = (in_[0] + classname, list())
        for x in ast.param:
            str1, typ1 = self.visit(x, o)
            if type(typ1) is IntType and type(mtype.partype[i]) is FloatType:
                str1 += emit.emitI2F(frame)
            in_ = (in_[0] + str1, list())
            i += 1
        if isStatic:
            return in_[0] + emit.emitINVOKESTATIC(ctype.classname.name + "/" + ast.method.name, mtype, frame), member.mtype.rettype
        else:
            return in_[0] + emit.emitINVOKEVIRTUAL(ctype.classname.name + "/" + ast.method.name, mtype, frame), member.mtype.rettype

    def visitNewExpr(self, ast, o):
        # ast : NewExpr
        # o : ExprEnv
        emit = o.stmt.method.emit
        frame = o.stmt.frame
        buffer = ""
        buffer += emit.emitNEW(ast.classname.name, frame)
        buffer += emit.emitDUP(frame)
        temp = self.findInit(ast.classname.name)
        param = [] if temp is None else temp[0]
        in_ = ""
        i = 0
        for x in ast.param:
            str1, typ1 = self.visit(x, o)
            if type(param[i].varType) is FloatType and type(typ1) is IntType:
                in_ += str1 + emit.emitI2F(frame)
            else:
                in_ += str1
            i += 1

        buffer += in_
        buffer += emit.emitINVOKESPECIAL(frame, ast.classname.name + '/' + "<init>", MType([x.varType for x in param], VoidType()))
        return buffer, ClassType(ast.classname)
    ### ---------------------------------------- LHS -----------------------------------------

    def visitId(self, ast, o):
        #ast: Id
        #o: ExprEnv

        emit = o.stmt.method.emit
        frame = o.stmt.frame
        nenv = o.stmt.sym
        isLeft = o.isLeft
        sym = self.lookup(ast.name, nenv[::-1], lambda x: x.name)
        if sym is not None:
            if isLeft:
                return emit.emitWRITEVAR(sym.name, sym.mtype, sym.value.value, frame), sym.mtype
            else:
                return emit.emitREADVAR(sym.name, sym.mtype, sym.value.value, frame), sym.mtype
        else:
            return "", ""

    def visitFieldAccess(self, ast, o):
        # ast : FieldAccess
        # o : ExprEnv
        emit = o.stmt.method.emit
        frame = o.stmt.frame
        sym = o.stmt.sym
        isLeft = o.isLeft
        isFirst = o.isFirst
        isStatic, classname, typeField = self.checkIsStatic(ast.obj, ast.fieldname.name, sym, o, 1)
        buffer = ""
        if isFirst:
            buffer += self.visit(ast.obj, o)[0]
            if not isLeft and isStatic and not self.isFieldAccess:
                buffer += emit.emitGETSTATIC(classname + "." + ast.fieldname.name, typeField, frame)
            if not isLeft and not isStatic and not self.isFieldAccess:
                buffer += emit.emitGETFIELD(classname + "." + ast.fieldname.name, typeField, frame)
        else:
            if isLeft and isStatic:
                buffer += emit.emitPUTSTATIC(classname + "." + ast.fieldname.name, typeField, frame)
            if isLeft and not isStatic:
                buffer += emit.emitPUTFIELD(classname + "." + ast.fieldname.name, typeField, frame)
        return buffer, typeField

    ### --------------------------------------- Literals -------------------------------------

    def visitIntLiteral(self, ast, o):
        # ast: IntLiteral
        # o: ExprEnv

        emit = o.stmt.method.emit
        frame = o.stmt.frame
        return emit.emitPUSHICONST(ast.value, frame), IntType()

    def visitFloatLiteral(self, ast, o):
        # ast: FloatLiteral
        # o: ExprEnv

        emit = o.stmt.method.emit
        frame = o.stmt.frame
        return emit.emitPUSHFCONST(ast.value, frame), FloatType()

    def visitBooleanLiteral(self, ast, o):
        # ast: BooleanLiteral
        # o: ExprEnv

        emit = o.stmt.method.emit
        frame = o.stmt.frame
        return emit.emitPUSHZCONST(str(ast.value), frame), BoolType()

    def visitStringLiteral(self, ast, o):
        # ast: StringLiteral
        # o: ExprEnv

        emit = o.stmt.method.emit
        frame = o.stmt.frame
        return emit.emitPUSHSCONST(ast.value, frame), StringType()

    def visitSelfLiteral(self, ast, o):
        # ast: SelfLiteral
        # o: ExprEnv

        emit = o.stmt.method.emit
        frame = o.stmt.frame
        nameThisClass = o.stmt.method.classname
        return emit.emitREADVAR(nameThisClass, ClassType(Id(nameThisClass)), 0, frame), ClassType(Id(nameThisClass))

    def visitNullLiteral(self, ast, o):
        # ast: SelfLiteral
        # o: ExprEnv

        emit = o.stmt.method.emit
        frame = o.stmt.frame
        return emit.emitPUSHNULL(frame)

    ### ----------------------------------------------------------------------------------------------------------------
    Object = None
    isInited = False
    isFieldAccess = False
    ### ----------------------------------------------------------------------------------------------------------------
    def findInit(self, Parentname):
        for class_data in self.env:
            if Parentname == class_data.cname:
                for x in class_data.mem:
                    if type(x.mtype) is MType and x.name == "<init>":
                        temp = []
                        i = 0
                        for y in x.mtype.partype :
                            temp.append(VarDecl(Id("arg" + str(i)),y))
                            i += 1
                        return temp, VoidType()
                break
        return None

    def checkIsStatic(self, obj, fieldname, envi, o, case):
        # o : ExprEnv

        if type(obj) is SelfLiteral:
            className = o.stmt.method.classname
        else:
            obj = obj.name
            if obj not in [x.name for x in envi]:
                className = obj
            else:
                for x in envi:
                    if obj == x.name:
                        className = x.mtype.classname.name
        for y in self.astTree.decl:
            if className == y.classname.name:
                for z in y.memlist:
                    if case == 1 and type(z) is AttributeDecl and type(z.decl) is VarDecl and fieldname == z.decl.variable.name:
                        if type(z.sikind) is Static:
                            return True, className, z.decl.varType
                        else:
                            return False, className, z.decl.varType
                    elif case == 1 and type(z) is AttributeDecl and type(z.decl) is ConstDecl and fieldname == z.decl.constant.name:
                        if type(z.sikind) is Static:
                            return True, className, z.decl.constType
                        else:
                            return False, className, z.decl.constType
                    elif case == 2 and type(z) is MethodDecl and fieldname == z.name.name:
                        if type(z.sikind) is Static:
                            return True, className, z.returnType
                        else:
                            return False, className, z.returnType

    
