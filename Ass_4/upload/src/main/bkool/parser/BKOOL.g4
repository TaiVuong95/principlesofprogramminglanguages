grammar BKOOL;

@lexer::header {
from lexererr import *
}

options{
	language=Python3;
}
/***********************************************
********************PARSER**********************
***********************************************/

// ---------- PROGRAM -----------------------
program: class_decl+;

//----------- CLASS DECLARATION -------------
class_decl : CLASS ID (EXTENDS ID)? LP list_member* RP;
list_member : var_decl | method_decl;

// ---------- VARIABLE DECLARATION ----------
var_decl : imutable_decl | mutable_decl;
local_decl : imutable_local | mutable_local;
imutable_decl : STATIC? FINAL pri_type ID ASSIGN_OP_CONST exp SEMICOLON;
mutable_decl: STATIC? idlist DECLARE_OP type_var SEMICOLON;
imutable_local : FINAL pri_type ID ASSIGN_OP_CONST exp SEMICOLON;
mutable_local : idlist DECLARE_OP type_var SEMICOLON;
idlist: ID (COMMA ID)*;

//------------METHOD DECLARATION ------------
method_decl : (type_var | void_type)? STATIC? ID LB many_para? RB block_stmt;

// ---------- KIND OF TYPE ---------------------
type_var: pri_type | arr_type | class_type;
arr_type: (pri_type | class_type) LSB INTEGERLIT RSB;
pri_type: BOOLEAN | INT | FLOAT | STRING;
void_type : VOID;
class_type : ID;

// ------------ PARAMETER -----------------------
many_para: one_para (SEMICOLON one_para)*;
one_para: idlist DECLARE_OP type_var;

// ------------ STATEMENT -----------------------
stmt: assign_stmt | break_stmt | continue_stmt | return_stmt | if_stmt | call_stmt| for_stmt | block_stmt;
block_stmt : LP local_decl* stmt* RP;

assign_stmt: exp8 ASSIGN_OP exp SEMICOLON;

break_stmt: BREAK SEMICOLON;

continue_stmt: CONTINUE SEMICOLON;

return_stmt: RETURN exp SEMICOLON;

if_stmt: IF exp THEN stmt (ELSE stmt)?;

for_stmt: FOR ID ASSIGN_OP exp (TO | DOWNTO) exp DO stmt;

call_stmt : exp8 DOT ID LB (arglist)? RB SEMICOLON;

// ------------- EXPRESSION ----------------
exp : exp1 (LESSER_OP | LESSER_EQUAL_OP | GREATER_OP | GREATER_EQUAL_OP) exp1 | exp1;
exp1: exp2 (EQUAL_OP | NOT_EQUAL_OP) exp2 | exp2;
exp2: exp2 (AND_OP | OR_OP) exp3 | exp3;
exp3: exp3 (ADD_OP | SUB_OP) exp4 | exp4;
exp4: exp4 (MUL_OP | DIV_FLOAT_OP | DIV_INT_OP | MODUL_OP) exp5 | exp5;
exp5: exp5 CONCAT_OP exp6 | exp6;
exp6: NOT_OP exp6 | exp7;
exp7: (SUB_OP | ADD_OP) exp7 | exp8;
exp8: exp8 LSB exp RSB
    | exp8 DOT ID
    | exp8 DOT ID (LB arglist? RB)
    | exp9;
exp9: exp9 DOT ID (LB arglist? RB)? | exp10;
exp10: NEW ID LB arglist? RB | exp11;
exp11: LB exp RB | element;
element: INTEGERLIT | FLOATLIT | STRINGLIT | BOOLLIT | NIL | ID | THIS;
arglist: exp (COMMA exp)*;


/***********************************************
********************lEXER**********************
***********************************************/

// BOOLLIT
BOOLLIT: TRUE | FALSE;

// Comment
BLOCK_COMMENT : '/*' .*? '*/' -> skip;
LINE_COMMENT : '%%' ~[\r\n\f]* -> skip;

// Keywords
BOOLEAN     : 'boolean';
EXTENDS     : 'extends';
THEN        : 'then';
NIL         : 'nil';
BREAK       : 'break';
FLOAT       : 'float';
FOR         : 'for';
THIS        : 'this';
CLASS       : 'class';
IF          : 'if';
RETURN      : 'return';
FINAL       : 'final';
CONTINUE    : 'continue';
INT         : 'int';
TRUE        : 'true';
STATIC      : 'static';
DO          : 'do';
NEW         : 'new';
FALSE       : 'false';
TO          : 'to';
ELSE        : 'else';
STRING      : 'string';
VOID        : 'void';
DOWNTO      : 'downto';

// Operators
LESSER_OP     : '<';
LESSER_EQUAL_OP: '<=';
GREATER_OP  : '>';
GREATER_EQUAL_OP        : '>=';
NOT_EQUAL_OP: '!=';
EQUAL_OP    : '==';
OR_OP       : '||';
AND_OP      : '&&';
ADD_OP      : '+';
SUB_OP      : '-';
MUL_OP      : '*';
DIV_FLOAT_OP             : '/';
DIV_INT_OP               : '\\';
MODUL_OP    : '%';
CONCAT_OP   : '^';
NOT_OP      : '!';
DECLARE_OP  : ':';
ASSIGN_OP   : ':=';
ASSIGN_OP_CONST: '=';

// Separators
LSB     : '[';
RSB     : ']';
LB      : '(';
RB      : ')';
LP     : '{';
RP     : '}';
SEMICOLON: ';';
COMMA   : ',';
DOT     : '.';

// Literals
FLOATLIT: DIGIT+ '.' (DIGIT+)? EXPONENT? | DIGIT+ EXPONENT;

INTEGERLIT: DIGIT+;

STRINGLIT: '"'('\\' [bntfr'"\\]|(~[\b\n\t\f\r\\'"]))*'"'
{
	temp = Lexer.text.fget(self)
	temp1 = (temp[1:len(temp)-1])
	Lexer.text.fset(self, temp1)
};

// Identifier
ID : [a-zA-Z_][a-zA-Z_0-9]*;

DIGIT: [0-9];

EXPONENT: ('e' | 'E') ('-' | '+')? DIGIT+;

// White space
WS : [ \t\r\n\f]+ -> skip;

UNCLOSE_STRING: '"' ( '\\' [bfrnt'"\\] | ~[\b\n\t\f\r\\'"])*
{
	self.text=self.text[1:]
	raise UncloseString(self.text)
};

ILLEGAL_ESCAPE:	'"'.*? [\b\n\t\f\r\\'"]
{
	self.text = self.text[1:]
	raise IllegalEscape(self.text)
};

ERROR_CHAR: .
{
	raise ErrorToken(self.text)
};