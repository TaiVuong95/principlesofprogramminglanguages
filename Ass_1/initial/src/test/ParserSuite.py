import unittest
from TestUtils import TestParser

class ParserSuite(unittest.TestCase):
    #Test Variable  ...
    def test_1(self):
        input = """class Shape {
            length,width : float;
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,101))
    def test_2(self):
        input = """class Shape {
            x : int;
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,102))
    def test_3(self):
        input = """class Shape {
            x : float
        }
        """
        expect = "Error on line 3 col 8: }"
        self.assertTrue(TestParser.test(input,expect,103))
    def test_4(self):
        input = """class Triangle extends Shape {
           n : int[6];
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,104))
    def test_5(self):
        input = """class Triangle extends Shape {
           n int[6];
        }"""
        expect = "Error on line 2 col 13: int"
        self.assertTrue(TestParser.test(input,expect,105))
    def test_6(self):
        input = """class Triangle extends Shape {
            a,b,c : string;
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,106))
    def test_7(self):
        input = """class Triangle extends Shape {
            final int My1stCons = 1 + 5;
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,107))
    def test_8(self):
        input = """class A {
            static final int My2ndCons = 2;
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,108))
    def test_9(self):
        input = """class TaiVuong {
            static final My2ndCons = 2;
        }"""
        expect = "Error on line 2 col 25: My2ndCons"
        self.assertTrue(TestParser.test(input,expect,109))
    def test_10(self):
        input = """class TaiVuong {
            static final My2ndCons = 2;
        }"""
        expect = "Error on line 2 col 25: My2ndCons"
        self.assertTrue(TestParser.test(input,expect,110))
    # Test Method
    def test_11(self):
        input = """class TaiVuong {
            int factorial(n:int){}
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,111))
    def test_12(self):
        input = """class TaiVuong {
            int factorial(n:int{}
        }"""
        expect = "Error on line 2 col 31: {"
        self.assertTrue(TestParser.test(input,expect,112))
    def test_13(self):
        input = """class Example2 {
            int factorial(n:int){
        }"""
        expect = "Error on line 3 col 9: <EOF>"
        self.assertTrue(TestParser.test(input,expect,113))
    def test_14(self):
        input = """class Example2 {
            int factorial(n:int){
                if n==1 then return 1;
            }
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,114))
    def test_15(self):
        input = """class Example2 {
            int factorial(n:int){
                if 3>4<5 then return 1;
            }
        }"""
        expect = "Error on line 3 col 22: <"
        self.assertTrue(TestParser.test(input,expect,115))
    def test_16(self):
        input = """class Example2 {
            int factorial(n:int){
                if n==1 && a==2 then return 1;
            }
        }"""
        expect = "Error on line 3 col 28: =="
        self.assertTrue(TestParser.test(input,expect,116))
    def test_17(self):
        input = """class Example2 {
            int factorial(n:int){
                for i := 1 to 100 do {}
            }
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,117))
    def test_18(self):
        input = """class Example2 {
            int factorial(n:int){
                for i := 1 to 100 do {
                    Intarray[i] := i + 1;
                }
            }
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,118))
    def test_19(self):
        input = """class Example2 {
            int factorial(n:int){
                for i := 1 to 100 do {
                    Intarray[i] := i  1;
                }
            }
        }"""
        expect = "Error on line 4 col 38: 1"
        self.assertTrue(TestParser.test(input,expect,119))
    def test_20(self):
        input = """class Example2 {
            int Tai (){
                this.aPI := 3.14;
            }
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,120))
    def test_21(self):
        input = """class Example2 {
            int factorial(n:int){
                for i := 1 to 100 do {
                    io.writeIntLn(i);
                    Intarray[i] := i + 1;
                }
            }
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,121))
    def test_22(self):
        input = """class ABCD {
            int factorial(n:int){
                value := x.foo(5);
            }
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,122))
    def test_23(self):
        input = """class ABCD {
            int factorial(n:int){
                %%start of declaration part
                r,s:float;
                a,b:int[5];
                %%list of statements
                r:=2.0;
                s:=r*r*this.myPI;
                a[0]:= s;
            }
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,123))
    def test_24(self):
        input = """class ABCD {
            int factorial(n:int){
                value := x.foo(5);
                l[3] := value  2
            }
        }"""
        expect = "Error on line 4 col 31: 2"
        self.assertTrue(TestParser.test(input,expect,124))
    def test_25(self):
        input = """class A {
            int factorial(n:int){
                io.writeStrLn("Expression is true");
            }
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,125))
    def test_26(self):
        input = """class abc {
            int factorial(n:int){
                if flag then
                    io.writeStrLn("Expression is true");
                else
                    io.writeStrLn ("Expression is false");
            }
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,126))
    def test_27(self):
        input = """class B {
            int factorial(n:int){
                for x := 5 downto 2 do
                    io.writeIntLn(x);
                    i := 5;
            }
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,127))
    def test_28(self):
        input = """class A {
            int factorial(n:int){
                for x := 5 downto 2 do {
                    io.writeIntLn(x);
                    i := 5;
                }
            }
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,128))
    def test_29(self):
        input = """class abc {
            int factorial(n:int){
                for x := 5 downto 2 do {}
            }
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,129))
    def test_30(self):
        input = """class Example {
            int factorial(n:int){
                for x := 5 downto 2 do
                    x := 5; 
            }
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,130))
    def test_31(self):
        input = """class F {
            int factorial(n:int){
                if flag then
                    a := a + 7;
            }
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,131))
    def test_32(self):
        input = """class E {
            int factorial(n:int){
                if flag then{
                    i := 9;
                }
            }
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,132))
    def test_33(self):
        input = """class ABC { 
            int factorial(n:int){
                if flag then{
                    i := 9;
                }else{
                    a := 10;
                }
            }
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,133))
    def test_34(self):
        input = """class ABCD {
            int factorial(n:int){
                if flag then{
                    i := 9;
                }else 
                    a := 10;
            }
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,134))
    def test_35(self):
        input = """class ABC {
            int factorial(n:int){
                if true then{
                    i := 9;
                }
            }
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,135))
    def test_36(self):
        input = """class ABC {
            int factorial(n:int){
                if flag then
                    i := 9;
                }
            }
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,136))
    def test_37(self):
        input = """class Example1 {
            int factorial(n:int){
                if n == 0 then 
                    return 1; 
                else 
                    return n * this.factorial(n - 1);
            }
            void main(){
                x:int;
                x := io.readInt();
                io.writeIntLn(this.factorial(x));
            }
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,137))
    def test_38(self):
        input = """class Example1 {
            void main(){
                x:int;
                x := io.readInt();
                io.writeIntLn(this.factorial(x));
            }
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,138))
    def test_39(self):
        input = """class Shape {
            length,width:float;
            float getArea() {}
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,139))
    def test_40(self):
        input = """class Shape {
            length,width:float;
            float getArea() {}
            Shape(length,width:float){
                this.length := length;
                this.width := width;
            }
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,140))
    def test_41(self):
        input = """class Shape {
            length,width:float;
            float getArea() {}
            Shape(length,width:float){
                thislength := length;
                this.width := width;
            }
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,141))
    def test_42(self):
        input = """class Shape {
                        length,width:float;
                        float getArea() {}
                        Shape(length,width:float){
                            this length := length;
                            this.width := width;
                        }
                    }"""
        expect = "Error on line 5 col 33: length"
        self.assertTrue(TestParser.test(input,expect,142))
    def test_43(self):
        input = """class Rectangle extends Shape {
                        float getArea(){
                            return this.length * this.width;
                        }
                    }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,143))
    def test_44(self):
        input = """class Triangle extends Shape {
                        float getArea(){
                            return this.length*this.width / 2;
                        }
                }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,144))
    def test_45(self):
        input = """class Example2 {
                        void main(){
                            s:Shape;
                            s := new Rectangle(3,4);
                            io.writeFloatLn(s.getArea());
                            s := new Triangle(3,4);
                            io.writeFloatLn(s.getArea());
                        }
                    }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,145))
    def test_46(self):
        input = """class Rectangle extends Shape {
                        float getArea(){
                            return this.length  this.width;
                        }
                    }"""
        expect = "Error on line 3 col 48: this"
        self.assertTrue(TestParser.test(input,expect,146))
    def test_47(self):
        input = """class Rectangle extends Shape {
            float getArea(){
                return this.length +*this.width;
            }
        }"""
        expect = "Error on line 3 col 36: *"
        self.assertTrue(TestParser.test(input,expect,147))
    def test_48(self):
        input = """class hehe{}"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,148))
    def test_49(self):
        input = """class hehe (){
            length,width : float;
        }"""
        expect = "Error on line 1 col 11: ("
        self.assertTrue(TestParser.test(input,expect,149))
    def test_50(self):
        input = """class child extends parent{
            length,width : float;
        """
        expect = "Error on line 3 col 8: <EOF>"
        self.assertTrue(TestParser.test(input,expect,150))
    def test_51(self):
        input = """class Shape {
            static final int numOfShape = 0;
            final int immuAttribute = 0;
            length,width: float;
            int static getNumOfShape() {
                return numOfShape;
            }
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,151))
    def test_52(self):
        input = """class Shape {
            static final int numOfShape = 1 + 6;
            int static getNumOfShape() {
                return numOfShape;
            }
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,152))
    def test_53(self):
        input = """class Shape {
                        static final int numOfShape = 2 == 3;
                    }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,153))
    def test_54(self):
        input = """class ABC {
            static my2ndVar, my3rdVar: Shape;
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,154))
    def test_55(self):
        input = """class ABC {
            static my2ndArray, my3rdArray: Shape[6];
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,155))
    def test_56(self):
        input = """static final my2ndArray, my3rdArray: Shape[6];"""
        expect = "Error on line 1 col 0: static"
        self.assertTrue(TestParser.test(input,expect,156))
    def test_57(self):
        input = """class Shape {
            static final int numOfShape = 1 + 6;
            int static getNumOfShape() {
                a[3+x.foo(2)] := a[b[2]] +3;
            }
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,157))
    def test_58(self):
        input = """class Shape {
            static final int numOfShape = 1 + 6;
            int static getNumOfShape() {
                x.b[2] := x.m()[3];
            }
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,158))
    def test_59(self):
        input = """class Shape {
            static final int numOfShape = 1 + 6;
                int static getNumOfShape() {
                    x.b[2] := x.m()[];
                }
        }"""
        expect = "Error on line 4 col 36: ]"
        self.assertTrue(TestParser.test(input,expect,159))
    def test_60(self):
        input = """class Shape {
            int static getNumOfShape() {
                x.b[2] = x.m()[3];
            }
        }"""
        expect = "Error on line 3 col 23: ="
        self.assertTrue(TestParser.test(input,expect,160))
    def test_61(self):
        input = """class ABC {
            int factorial(n:int){
                for x := 5 downto 2 do {
                    if i==1 then
                        return 1;
                }
            }
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,161))
    def test_62(self):
        input = """class Here {
            int factorial(n:int){
                for x := 5 downto 2 do {
                    if i == 1 then 
                        continue;
                }
            }
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,162))
    def test_63(self):
        input = """class Test {
            a,b,c : int;
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,163))
    def test_64(self):
        input = """class ABC {
            a,b,c : string;
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,164))
    def test_65(self):
        input = """class ABC {
                static int My2ndCons = 2;
        }"""
        expect = "Error on line 2 col 23: int"
        self.assertTrue(TestParser.test(input,expect,165))
    def test_66(self):
        input = """class Shape {
            Shape() {
                x.b[2] := x.m()[3];
            }
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,166))
    def test_67(self):
        input = """class A {
            int factorial(n:int){
                for x := 5 downto 2 do {
                    if i == 1 then 
                        BrEak;
                }
            }
        }"""
        expect = "Error on line 5 col 29: ;"
        self.assertTrue(TestParser.test(input,expect,167))
    def test_68(self):
        input = """class Here {
            int factorial(n:int){
                for x := 5 downto 2 do {
                    if i == 1 then 
                        ConTinue;
                }
            
        }"""
        expect = "Error on line 5 col 32: ;"
        self.assertTrue(TestParser.test(input,expect,168))
    def test_69(self):
        input = """class Shape {
            int test() {
                return a.abc(1*2);
            }
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,169))
    def test_70(self):
        input = """class Shape {
            float test() {
                return a.abc(1,2);
            }
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,170))
    def test_71(self):
        input = """class TestConditional {
            int factorial(n:int){
                if n>=1 && a==2 then return 1;
            }
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,171))
    def test_72(self):
        input = """class O {
            int factorial(n:int){
                if n==b && c >=n then return 1;
            }
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,172))
    def test_73(self):
        input = """class Exmaple1 { 
            int factorial(n:int){
                if n==1||2 then return 1;
            }
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,173))
    def test_74(self):
        input = """class Example1 {
            int factorial(n:int){
                if n>=1 || a==2 then return 1;
            }
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,174))
    def test_75(self):
        input = """class kaka {
            int factorial(n:int){
                if true || a==2 then return 1;
            }
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,175))
    def test_76(self):
        input = """class Example1 {
            int factorial(n:int;x:float){
                if n == 0 then return 1; else return n * this.factorial(n - 1);
            }
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,176))
    def test_77(self):
        input = """class Example {
            x : int;
            y : float;
            Example(a:int;b:float){
                this.x := a;
                this.y := b;
            }
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,177))
    def test_78(self):
        input = """class hehe {
            int factorial(n:int){
                for x := 5 downto 2 do
                io.writeIntLn(x);
            }
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,178))
    def test_79(self):
        input = """class Example1 {
            int factorial(n:int){
                if n == 0 then 
                    return 1; 
                else 
                    return n * this.factorial(n - 1);
            }
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,179))
    def test_80(self):
        input = """class Test1 {
            int factorial(n:int){
                for x := 5 2 do
                    io.writeIntLn(x);
            }
        }"""
        expect = "Error on line 3 col 27: 2"
        self.assertTrue(TestParser.test(input,expect,180))
    def test_81(self):
        input = """class AbC {
            int factorial(n:int){
                for x := 5 to 2 do
                    a[2] := b[9] % 4;
            }
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,181))
    def test_82(self):
        input = """class aHala {
            int factorial(n:int){
                for x := 5 downto 2 do
                x := a ^ b;
            }
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,182))
    def test_83(self):
        input = """class A {
            float : length,width;
        }"""
        expect = "Error on line 2 col 18: :"
        self.assertTrue(TestParser.test(input,expect,183))
    def test_84(self):
        input = """class PPL {
            x int;
        }"""
        expect = "Error on line 2 col 14: int"
        self.assertTrue(TestParser.test(input,expect,184))
    def test_85(self):
        input = """class yyy {
            x : float
        }"""
        expect = "Error on line 3 col 8: }"
        self.assertTrue(TestParser.test(input,expect,185))
    def test_86(self):
        input = """class Test {
            int factorial(n:int){
                value := x.foo(5);
                l[3] := value * 2;      
        }"""
        expect = "Error on line 5 col 9: <EOF>"
        self.assertTrue(TestParser.test(input,expect,186))
    def test_87(self):
        input = """class z {
            int factorial(n:int){
                value := x.foo(5);
                l3] := value * 2;
            }
        }"""
        expect = "Error on line 4 col 18: ]"
        self.assertTrue(TestParser.test(input,expect,187))
    def test_88(self):
        input = """class x {
            final float My1stCons = 1 + 5;
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,188))
    def test_89(self):
        input = """class y {
            static final string My2ndCons = 2;
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,189))
    def test_90(self):
        input = """class Error {
            int factorial(n:int){
                if i % 2 == 0 then {
                    break;
                }
            }
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,190))
    def test_91(self):
        input = """class Example2 {
            void main(){
                s:Shape;
                this.printf("Chao Thay !");
            }
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,188))
    def test_92(self):
        input = """class xxx {
            int factorial(n:int){
                if n!=1 then return 2;
            } 
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,192))
    def test_93(self):
        input = """class TestFree extends Tai {
            int factorial(n:int){
                for i := 1 to 100 do {
                    for j :=2 downto -1 do {
                        if i==3 then {
                            x := 4;
                        }
                    }   
                }
            }
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,193))
    def test_94(self):
        input = """class aaa {
            int factorial(n:int){
                for i := 1 to 100 do {
                    Intarray[i] := i + 1*11;
                }
            }
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,194))
    def test_95(self):
        input = """class abc {
            int factorial(n:int){
                for i := 1 to 100 do {
                    Intarray[i] + a[7] := i + 1;
                }
            }
        }"""
        expect = "Error on line 4 col 32: +"
        self.assertTrue(TestParser.test(input,expect,195))
    def test_96(self):
        input = """class HinhThoi extends HinhVuong {
            int Tai (){
                this.aPI := 3.1.4;
            }
        }"""
        expect = "Error on line 3 col 32: 4"
        self.assertTrue(TestParser.test(input,expect,196))
    def test_97(self):
        input = """class abc extends xyz {
            int factorial(n:int){
                for i := 1 to 100 do {
                    io.writeIntLn(i);
                    Intarray[i] := i + 1;
                }
            }
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,121))
    def test_98(self):
        input = """class xyz extend abc {
            int factorial(n:int){
                value := x.foo(5);
                int Shape(){}
            }
        }"""
        expect = "Error on line 1 col 10: extend"
        self.assertTrue(TestParser.test(input,expect,198))
    def test_99(self):
        input = """class ABCD {
            int factorial(n:int){
                value := x.foo(5);
                l[3] := value * 2
            }
        }"""
        expect = "Error on line 5 col 12: }"
        self.assertTrue(TestParser.test(input,expect,199))
    def test_100(self):
        input = """class a {
            int factorial(n:int){
                continue;
                value := x.foo(5);
                l[3] := value + 2;
            }
        }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input,expect,200))


