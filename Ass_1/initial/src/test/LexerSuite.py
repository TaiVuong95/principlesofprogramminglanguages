import unittest
from TestUtils import TestLexer

class LexerSuite(unittest.TestCase):
    # Test Identifier ...
    def test_1(self):
        self.assertTrue(TestLexer.test("!true","if,<EOF>",101))
    def test_2(self):
        self.assertTrue(TestLexer.test("aCBbdc","aCBbdc,<EOF>",102))
    def test_3(self):
        self.assertTrue(TestLexer.test("aAsVN","aAsVN,<EOF>",103))
    def test_4(self):
        self.assertTrue(TestLexer.test("n>=1 && a==2","n,>=,1,&&,a,==,2,<EOF>",104))
    def test_5(self):
        self.assertTrue(TestLexer.test("3>4<5","3,>,4,<,5,<EOF>",105))
    def test_6(self):
        self.assertTrue(TestLexer.test("Vo Tai Vuong","Vo,Tai,Vuong,<EOF>",106))
    # Test Comment ...
    def test_7(self):
        self.assertTrue(TestLexer.test("/*halamadrid*/", "<EOF>", 107))
    def test_8(self):
        self.assertTrue(TestLexer.test("/*123ronaldo*/", "<EOF>", 108))
    def test_9(self):
        self.assertTrue(TestLexer.test("%%abc", "<EOF>", 109))
    def test_10(self):
        self.assertTrue(TestLexer.test("/*abcekf", "/,*,abcekf,<EOF>", 110))
    def test_11(self):
        self.assertTrue(TestLexer.test("%%abc/*de", "<EOF>", 111))
    def test_12(self):
        self.assertTrue(TestLexer.test("%%abc\na", "a,<EOF>", 112))
    def test_13(self):
        self.assertTrue(TestLexer.test("%%123a123123\n12345","12345,<EOF>",113))
    def test_14(self):
        self.assertTrue(TestLexer.test("456/*halo*/123a123", "456,123,a123,<EOF>", 114))
    def test_15(self):
        self.assertTrue(TestLexer.test("456%%halo123a123", "456,<EOF>", 115))
    def test_16(self):
        self.assertTrue(TestLexer.test("abc/*hala*/123", "abc,123,<EOF>", 116))
    # Test String ...
    def test_17(self):
        self.assertTrue(TestLexer.test("\"tai \\n vuong\"","tai \\n vuong,<EOF>", 117))
    def test_18(self):
        self.assertTrue(TestLexer.test("\"abc 1234\"", "abc 1234,<EOF>", 118))
    def test_19(self):
        self.assertTrue(TestLexer.test("\"xin chao", "Unclosed String: xin chao", 119))
    def test_20(self):
        self.assertTrue(TestLexer.test("\"xin\chao\"", "Illegal Escape In String: xin\\", 120))
    def test_21(self):
        self.assertTrue(TestLexer.test("\"viet nam vo dich AFF cup\"","viet nam vo dich AFF cup,<EOF>",121))
    def test_22(self):
        self.assertTrue(TestLexer.test("tai vuong %","tai,vuong,%,<EOF>",122))
    def test_23(self):
        self.assertTrue(TestLexer.test("adg # sdvb","adg,Error Token #",123))
    def test_24(self):
        self.assertTrue(TestLexer.test("*101#","*,101,Error Token #",124))
    def test_25(self):
        self.assertTrue(TestLexer.test("\"chao tam biet\";\"goodbye", "chao tam biet,;,Unclosed String: goodbye", 125))
    def test_26(self):
        self.assertTrue(TestLexer.test("a := \"chao tam biet\";\"goodbye", "a,:=,chao tam biet,;,Unclosed String: goodbye", 126))
    def test_27(self):
        self.assertTrue(TestLexer.test("\"abc   xyz\"", "abc   xyz,<EOF>", 127))
    # Test Float
    def test_28(self):
        self.assertTrue(TestLexer.test("1.2", "1.2,<EOF>", 128))
    def test_29(self):
        self.assertTrue(TestLexer.test(".1", ".,1,<EOF>", 129))
    def test_30(self):
        self.assertTrue(TestLexer.test("143e8", "143e8,<EOF>", 130))
    def test_31(self):
        self.assertTrue(TestLexer.test("9.0", "9.0,<EOF>", 131))
    def test_32(self):
        self.assertTrue(TestLexer.test("12e8", "12e8,<EOF>", 132))
    def test_33(self):
        self.assertTrue(TestLexer.test("1.", "1.,<EOF>", 133))
    def test_34(self):
        self.assertTrue(TestLexer.test("0.33E-3", "0.33E-3,<EOF>", 134))
    def test_35(self):
        self.assertTrue(TestLexer.test("128e+12", "128e+12,<EOF>", 135))
    def test_36(self):
        self.assertTrue(TestLexer.test(".12", ".,12,<EOF>", 136))
    def test_37(self):
        self.assertTrue(TestLexer.test("1.2.3.4", "1.2,.,3.4,<EOF>", 137))
    # Test KeyWord
    def test_38(self):
        self.assertTrue(TestLexer.test("boolean","boolean,<EOF>",138))
    def test_39(self):
        self.assertTrue(TestLexer.test("extends","extends,<EOF>",139))
    def test_40(self):
        self.assertTrue(TestLexer.test("then","then,<EOF>",140))
    def test_41(self):
        self.assertTrue(TestLexer.test("nil","nil,<EOF>",141))
    def test_42(self):
        self.assertTrue(TestLexer.test("break","break,<EOF>",142))
    def test_43(self):
        self.assertTrue(TestLexer.test("float","float,<EOF>",143))
    def test_44(self):
        self.assertTrue(TestLexer.test("for","for,<EOF>",144))
    def test_45(self):
        self.assertTrue(TestLexer.test("this","this,<EOF>",145))
    def test_46(self):
        self.assertTrue(TestLexer.test("class","class,<EOF>",146))
    def test_47(self):
        self.assertTrue(TestLexer.test("if","if,<EOF>",147))
    def test_48(self):
        self.assertTrue(TestLexer.test("return","return,<EOF>",148))
    def test_49(self):
        self.assertTrue(TestLexer.test("final","final,<EOF>",149))
    def test_50(self):
        self.assertTrue(TestLexer.test("continue","continue,<EOF>",150))
    def test_51(self):
        self.assertTrue(TestLexer.test("int","int,<EOF>",151))
    def test_52(self):
        self.assertTrue(TestLexer.test("true","true,<EOF>",152))
    def test_53(self):
        self.assertTrue(TestLexer.test("static","static,<EOF>",153))
    def test_54(self):
        self.assertTrue(TestLexer.test("do","do,<EOF>",154))
    def test_55(self):
        self.assertTrue(TestLexer.test("new","new,<EOF>",155))
    def test_56(self):
        self.assertTrue(TestLexer.test("false true","false,true,<EOF>",156))
    def test_57(self):
        self.assertTrue(TestLexer.test("to","to,<EOF>",157))
    def test_58(self):
        self.assertTrue(TestLexer.test("else","else,<EOF>",158))
    def test_59(self):
        self.assertTrue(TestLexer.test("string","string,<EOF>",159))
    def test_60(self):
        self.assertTrue(TestLexer.test("void","void,<EOF>",160))
    def test_61(self):
        self.assertTrue(TestLexer.test("downto","downto,<EOF>",161))
    #Test Sparator
    def test_62(self):
        self.assertTrue(TestLexer.test("((:=tai)", "(,(,:=,tai,),<EOF>",162))
    def test_63(self):
        self.assertTrue(TestLexer.test(".(vuong)", ".,(,vuong,),<EOF>", 163))
    def test_64(self):
        self.assertTrue(TestLexer.test("{;", "{,;,<EOF>", 164))
    def test_65(self):
        self.assertTrue(TestLexer.test("Toi: la nguoi viet nam", "Toi,:,la,nguoi,viet,nam,<EOF>", 165))
    def test_66(self):
        self.assertTrue(TestLexer.test("TPHCM (Viet Nam)", "TPHCM,(,Viet,Nam,),<EOF>", 166))
    def test_67(self):
        self.assertTrue(TestLexer.test("Khoa Khoa Hoc Ky Thuat May Tinh", "Khoa,Khoa,Hoc,Ky,Thuat,May,Tinh,<EOF>", 167))
    # Test Operator
    def test_68(self):
        self.assertTrue(TestLexer.test("^","^,<EOF>",168))
    def test_69(self):
        self.assertTrue(TestLexer.test("x = y * z","x,=,y,*,z,<EOF>",169))
    def test_70(self):
        self.assertTrue(TestLexer.test("ab = ba / y","ab,=,ba,/,y,<EOF>",170))
    def test_71(self):
        self.assertTrue(TestLexer.test("a = b % c","a,=,b,%,c,<EOF>",171))
    def test_72(self):
        self.assertTrue(TestLexer.test("a:int[5];","a,:,int,[,5,],;,<EOF>",172))
    def test_73(self):
        self.assertTrue(TestLexer.test("this.aPI := 3.14;","this,.,aPI,:=,3.14,;,<EOF>",173))
    def test_74(self):
        self.assertTrue(TestLexer.test("value := x.foo(2);","value,:=,x,.,foo,(,2,),;,<EOF>",174))
    def test_75(self):
        self.assertTrue(TestLexer.test("l[3] := value * 2;","l,[,3,],:=,value,*,2,;,<EOF>",175))
    def test_76(self):
        self.assertTrue(TestLexer.test("if flag then","if,flag,then,<EOF>",176))
    def test_77(self):
        self.assertTrue(TestLexer.test("Intarray[i] := i + 1;","Intarray,[,i,],:=,i,+,1,;,<EOF>",177))
    def test_78(self):
        self.assertTrue(TestLexer.test("Shape.getNumOfShape();","Shape,.,getNumOfShape,(,),;,<EOF>",178))
    def test_79(self):
        self.assertTrue(TestLexer.test("void main(){}","void,main,(,),{,},<EOF>",179))
    def test_80(self):
        self.assertTrue(TestLexer.test("int factorial(n:int){}","int,factorial,(,n,:,int,),{,},<EOF>",180))
    # Test Declaration
    def test_81(self):
        self.assertTrue(TestLexer.test("length,width:float;","length,,,width,:,float,;,<EOF>", 181))
    def test_82(self):
        self.assertTrue(TestLexer.test("s:Shape;", "s,:,Shape,;,<EOF>", 182))
    def test_83(self):
        self.assertTrue(TestLexer.test("s := new Triangle(3,4);", "s,:=,new,Triangle,(,3,,,4,),;,<EOF>", 183))
    def test_84(self):
        self.assertTrue(TestLexer.test("for x := 5 downto 2 do", "for,x,:=,5,downto,2,do,<EOF>", 184))
    def test_85(self):
        self.assertTrue(TestLexer.test("r,s:float;","r,,,s,:,float,;,<EOF>",185))
    def test_86(self):
        self.assertTrue(TestLexer.test("a,b:int[5];","a,,,b,:,int,[,5,],;,<EOF>",186))
    def test_87(self):
        self.assertTrue(TestLexer.test("r:=2.0;","r,:=,2.0,;,<EOF>",187))
    def test_88(self):
        self.assertTrue(TestLexer.test("s:=r*r*this.myPI;","s,:=,r,*,r,*,this,.,myPI,;,<EOF>",188))
    def test_89(self):
        self.assertTrue(TestLexer.test("io.writeFloatLn(s.getArea());","io,.,writeFloatLn,(,s,.,getArea,(,),),;,<EOF>", 189))
    def test_90(self):
        self.assertTrue(TestLexer.test("return this.length*this.width / 2;", "return,this,.,length,*,this,.,width,/,2,;,<EOF>", 190))
    #More Test
    def test_91(self):
        self.assertTrue(TestLexer.test("a[3+x.foo(2)]", "a,[,3,+,x,.,foo,(,2,),],<EOF>", 191))
    def test_92(self):
        self.assertTrue(TestLexer.test("a[b[2]] +3", "a,[,b,[,2,],],+,3,<EOF>", 192))
    def test_93(self):
        self.assertTrue(TestLexer.test("true false", "true,false,<EOF>", 193))
    def test_94(self):
        self.assertTrue(TestLexer.test("class Shape { n : int[6]}", "class,Shape,{,n,:,int,[,6,],},<EOF>", 194))
    def test_95(self):
        self.assertTrue(TestLexer.test("Shape(length,width:float){}", "Shape,(,length,,,width,:,float,),{,},<EOF>", 195))
    def test_96(self):
        self.assertTrue(TestLexer.test("this.length := length;", "this,.,length,:=,length,;,<EOF>", 196))
    def test_97(self):
        self.assertTrue(TestLexer.test("this.width := width;", "this,.,width,:=,width,;,<EOF>", 197))
    def test_98(self):
        self.assertTrue(TestLexer.test("writeInt", "writeInt,<EOF>", 198))
    def test_99(self):
        self.assertTrue(TestLexer.test("writeIntLn", "writeIntLn,<EOF>", 199))
    def test_100(self):
        self.assertTrue(TestLexer.test("this.aPI := 3.1.4;", "this,.,aPI,:=,3.1,.,4,;,<EOF>", 200))



