# Generated from main/bkool/parser/BKOOL.g4 by ANTLR 4.7.2
# encoding: utf-8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3C")
        buf.write("\u017c\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16")
        buf.write("\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23\t\23")
        buf.write("\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31")
        buf.write("\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36")
        buf.write("\4\37\t\37\4 \t \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t")
        buf.write("&\4\'\t\'\4(\t(\4)\t)\3\2\6\2T\n\2\r\2\16\2U\3\3\3\3\3")
        buf.write("\3\3\3\5\3\\\n\3\3\3\3\3\7\3`\n\3\f\3\16\3c\13\3\3\3\3")
        buf.write("\3\3\4\3\4\5\4i\n\4\3\5\3\5\5\5m\n\5\3\6\3\6\5\6q\n\6")
        buf.write("\3\7\5\7t\n\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\b\5\b~\n\b")
        buf.write("\3\b\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\n\3")
        buf.write("\n\3\n\3\n\3\n\3\13\3\13\3\13\7\13\u0094\n\13\f\13\16")
        buf.write("\13\u0097\13\13\3\f\3\f\5\f\u009b\n\f\3\f\5\f\u009e\n")
        buf.write("\f\3\f\3\f\3\f\5\f\u00a3\n\f\3\f\3\f\3\f\3\r\3\r\3\r\5")
        buf.write("\r\u00ab\n\r\3\16\3\16\5\16\u00af\n\16\3\16\3\16\3\16")
        buf.write("\3\16\3\17\3\17\3\20\3\20\3\21\3\21\3\22\3\22\3\22\7\22")
        buf.write("\u00be\n\22\f\22\16\22\u00c1\13\22\3\23\3\23\3\23\3\23")
        buf.write("\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\5\24\u00cf\n")
        buf.write("\24\3\25\3\25\7\25\u00d3\n\25\f\25\16\25\u00d6\13\25\3")
        buf.write("\25\7\25\u00d9\n\25\f\25\16\25\u00dc\13\25\3\25\3\25\3")
        buf.write("\26\3\26\3\26\3\26\3\26\3\27\3\27\3\27\3\30\3\30\3\30")
        buf.write("\3\31\3\31\3\31\3\31\3\32\3\32\3\32\3\32\3\32\3\32\5\32")
        buf.write("\u00f5\n\32\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3")
        buf.write("\33\3\34\3\34\3\34\3\34\3\34\5\34\u0105\n\34\3\34\3\34")
        buf.write("\3\34\3\35\3\35\3\35\3\35\3\35\5\35\u010f\n\35\3\36\3")
        buf.write("\36\3\36\3\36\3\36\5\36\u0116\n\36\3\37\3\37\3\37\3\37")
        buf.write("\3\37\3\37\7\37\u011e\n\37\f\37\16\37\u0121\13\37\3 \3")
        buf.write(" \3 \3 \3 \3 \7 \u0129\n \f \16 \u012c\13 \3!\3!\3!\3")
        buf.write("!\3!\3!\7!\u0134\n!\f!\16!\u0137\13!\3\"\3\"\3\"\3\"\3")
        buf.write("\"\3\"\7\"\u013f\n\"\f\"\16\"\u0142\13\"\3#\3#\3#\5#\u0147")
        buf.write("\n#\3$\3$\3$\5$\u014c\n$\3%\3%\3%\3%\3%\3%\5%\u0154\n")
        buf.write("%\3&\3&\3&\3&\3&\3&\3&\3&\5&\u015e\n&\3&\5&\u0161\n&\7")
        buf.write("&\u0163\n&\f&\16&\u0166\13&\3\'\3\'\3\'\3\'\5\'\u016c")
        buf.write("\n\'\3\'\3\'\5\'\u0170\n\'\3(\3(\3)\3)\3)\7)\u0177\n)")
        buf.write("\f)\16)\u017a\13)\3)\2\7<>@BJ*\2\4\6\b\n\f\16\20\22\24")
        buf.write("\26\30\32\34\36 \"$&(*,.\60\62\64\668:<>@BDFHJLNP\2\13")
        buf.write("\6\2\6\6\13\13\23\23\33\33\4\2\31\31\35\35\3\2\36!\3\2")
        buf.write("\"#\3\2$%\3\2&\'\3\2(+\4\2\'\'--\6\2\3\3\t\t\r\r:=\2\u017e")
        buf.write("\2S\3\2\2\2\4W\3\2\2\2\6h\3\2\2\2\bl\3\2\2\2\np\3\2\2")
        buf.write("\2\fs\3\2\2\2\16}\3\2\2\2\20\u0084\3\2\2\2\22\u008b\3")
        buf.write("\2\2\2\24\u0090\3\2\2\2\26\u009a\3\2\2\2\30\u00aa\3\2")
        buf.write("\2\2\32\u00ae\3\2\2\2\34\u00b4\3\2\2\2\36\u00b6\3\2\2")
        buf.write("\2 \u00b8\3\2\2\2\"\u00ba\3\2\2\2$\u00c2\3\2\2\2&\u00ce")
        buf.write("\3\2\2\2(\u00d0\3\2\2\2*\u00df\3\2\2\2,\u00e4\3\2\2\2")
        buf.write(".\u00e7\3\2\2\2\60\u00ea\3\2\2\2\62\u00ee\3\2\2\2\64\u00f6")
        buf.write("\3\2\2\2\66\u00ff\3\2\2\28\u010e\3\2\2\2:\u0115\3\2\2")
        buf.write("\2<\u0117\3\2\2\2>\u0122\3\2\2\2@\u012d\3\2\2\2B\u0138")
        buf.write("\3\2\2\2D\u0146\3\2\2\2F\u014b\3\2\2\2H\u0153\3\2\2\2")
        buf.write("J\u0155\3\2\2\2L\u016f\3\2\2\2N\u0171\3\2\2\2P\u0173\3")
        buf.write("\2\2\2RT\5\4\3\2SR\3\2\2\2TU\3\2\2\2US\3\2\2\2UV\3\2\2")
        buf.write("\2V\3\3\2\2\2WX\7\16\2\2X[\7=\2\2YZ\7\7\2\2Z\\\7=\2\2")
        buf.write("[Y\3\2\2\2[\\\3\2\2\2\\]\3\2\2\2]a\7\65\2\2^`\5\6\4\2")
        buf.write("_^\3\2\2\2`c\3\2\2\2a_\3\2\2\2ab\3\2\2\2bd\3\2\2\2ca\3")
        buf.write("\2\2\2de\7\66\2\2e\5\3\2\2\2fi\5\b\5\2gi\5\26\f\2hf\3")
        buf.write("\2\2\2hg\3\2\2\2i\7\3\2\2\2jm\5\f\7\2km\5\16\b\2lj\3\2")
        buf.write("\2\2lk\3\2\2\2m\t\3\2\2\2nq\5\20\t\2oq\5\22\n\2pn\3\2")
        buf.write("\2\2po\3\2\2\2q\13\3\2\2\2rt\7\25\2\2sr\3\2\2\2st\3\2")
        buf.write("\2\2tu\3\2\2\2uv\7\21\2\2vw\5\34\17\2wx\7=\2\2xy\7\60")
        buf.write("\2\2yz\58\35\2z{\7\67\2\2{\r\3\2\2\2|~\7\25\2\2}|\3\2")
        buf.write("\2\2}~\3\2\2\2~\177\3\2\2\2\177\u0080\5\24\13\2\u0080")
        buf.write("\u0081\7.\2\2\u0081\u0082\5\30\r\2\u0082\u0083\7\67\2")
        buf.write("\2\u0083\17\3\2\2\2\u0084\u0085\7\21\2\2\u0085\u0086\5")
        buf.write("\34\17\2\u0086\u0087\7=\2\2\u0087\u0088\7\60\2\2\u0088")
        buf.write("\u0089\58\35\2\u0089\u008a\7\67\2\2\u008a\21\3\2\2\2\u008b")
        buf.write("\u008c\5\24\13\2\u008c\u008d\7.\2\2\u008d\u008e\5\30\r")
        buf.write("\2\u008e\u008f\7\67\2\2\u008f\23\3\2\2\2\u0090\u0095\7")
        buf.write("=\2\2\u0091\u0092\78\2\2\u0092\u0094\7=\2\2\u0093\u0091")
        buf.write("\3\2\2\2\u0094\u0097\3\2\2\2\u0095\u0093\3\2\2\2\u0095")
        buf.write("\u0096\3\2\2\2\u0096\25\3\2\2\2\u0097\u0095\3\2\2\2\u0098")
        buf.write("\u009b\5\30\r\2\u0099\u009b\5\36\20\2\u009a\u0098\3\2")
        buf.write("\2\2\u009a\u0099\3\2\2\2\u009a\u009b\3\2\2\2\u009b\u009d")
        buf.write("\3\2\2\2\u009c\u009e\7\25\2\2\u009d\u009c\3\2\2\2\u009d")
        buf.write("\u009e\3\2\2\2\u009e\u009f\3\2\2\2\u009f\u00a0\7=\2\2")
        buf.write("\u00a0\u00a2\7\63\2\2\u00a1\u00a3\5\"\22\2\u00a2\u00a1")
        buf.write("\3\2\2\2\u00a2\u00a3\3\2\2\2\u00a3\u00a4\3\2\2\2\u00a4")
        buf.write("\u00a5\7\64\2\2\u00a5\u00a6\5(\25\2\u00a6\27\3\2\2\2\u00a7")
        buf.write("\u00ab\5\34\17\2\u00a8\u00ab\5\32\16\2\u00a9\u00ab\5 ")
        buf.write("\21\2\u00aa\u00a7\3\2\2\2\u00aa\u00a8\3\2\2\2\u00aa\u00a9")
        buf.write("\3\2\2\2\u00ab\31\3\2\2\2\u00ac\u00af\5\34\17\2\u00ad")
        buf.write("\u00af\5 \21\2\u00ae\u00ac\3\2\2\2\u00ae\u00ad\3\2\2\2")
        buf.write("\u00af\u00b0\3\2\2\2\u00b0\u00b1\7\61\2\2\u00b1\u00b2")
        buf.write("\7;\2\2\u00b2\u00b3\7\62\2\2\u00b3\33\3\2\2\2\u00b4\u00b5")
        buf.write("\t\2\2\2\u00b5\35\3\2\2\2\u00b6\u00b7\7\34\2\2\u00b7\37")
        buf.write("\3\2\2\2\u00b8\u00b9\7=\2\2\u00b9!\3\2\2\2\u00ba\u00bf")
        buf.write("\5$\23\2\u00bb\u00bc\7\67\2\2\u00bc\u00be\5$\23\2\u00bd")
        buf.write("\u00bb\3\2\2\2\u00be\u00c1\3\2\2\2\u00bf\u00bd\3\2\2\2")
        buf.write("\u00bf\u00c0\3\2\2\2\u00c0#\3\2\2\2\u00c1\u00bf\3\2\2")
        buf.write("\2\u00c2\u00c3\5\24\13\2\u00c3\u00c4\7.\2\2\u00c4\u00c5")
        buf.write("\5\30\r\2\u00c5%\3\2\2\2\u00c6\u00cf\5*\26\2\u00c7\u00cf")
        buf.write("\5,\27\2\u00c8\u00cf\5.\30\2\u00c9\u00cf\5\60\31\2\u00ca")
        buf.write("\u00cf\5\62\32\2\u00cb\u00cf\5\66\34\2\u00cc\u00cf\5\64")
        buf.write("\33\2\u00cd\u00cf\5(\25\2\u00ce\u00c6\3\2\2\2\u00ce\u00c7")
        buf.write("\3\2\2\2\u00ce\u00c8\3\2\2\2\u00ce\u00c9\3\2\2\2\u00ce")
        buf.write("\u00ca\3\2\2\2\u00ce\u00cb\3\2\2\2\u00ce\u00cc\3\2\2\2")
        buf.write("\u00ce\u00cd\3\2\2\2\u00cf\'\3\2\2\2\u00d0\u00d4\7\65")
        buf.write("\2\2\u00d1\u00d3\5\n\6\2\u00d2\u00d1\3\2\2\2\u00d3\u00d6")
        buf.write("\3\2\2\2\u00d4\u00d2\3\2\2\2\u00d4\u00d5\3\2\2\2\u00d5")
        buf.write("\u00da\3\2\2\2\u00d6\u00d4\3\2\2\2\u00d7\u00d9\5&\24\2")
        buf.write("\u00d8\u00d7\3\2\2\2\u00d9\u00dc\3\2\2\2\u00da\u00d8\3")
        buf.write("\2\2\2\u00da\u00db\3\2\2\2\u00db\u00dd\3\2\2\2\u00dc\u00da")
        buf.write("\3\2\2\2\u00dd\u00de\7\66\2\2\u00de)\3\2\2\2\u00df\u00e0")
        buf.write("\5H%\2\u00e0\u00e1\7/\2\2\u00e1\u00e2\58\35\2\u00e2\u00e3")
        buf.write("\7\67\2\2\u00e3+\3\2\2\2\u00e4\u00e5\7\n\2\2\u00e5\u00e6")
        buf.write("\7\67\2\2\u00e6-\3\2\2\2\u00e7\u00e8\7\22\2\2\u00e8\u00e9")
        buf.write("\7\67\2\2\u00e9/\3\2\2\2\u00ea\u00eb\7\20\2\2\u00eb\u00ec")
        buf.write("\58\35\2\u00ec\u00ed\7\67\2\2\u00ed\61\3\2\2\2\u00ee\u00ef")
        buf.write("\7\17\2\2\u00ef\u00f0\58\35\2\u00f0\u00f1\7\b\2\2\u00f1")
        buf.write("\u00f4\5&\24\2\u00f2\u00f3\7\32\2\2\u00f3\u00f5\5&\24")
        buf.write("\2\u00f4\u00f2\3\2\2\2\u00f4\u00f5\3\2\2\2\u00f5\63\3")
        buf.write("\2\2\2\u00f6\u00f7\7\f\2\2\u00f7\u00f8\7=\2\2\u00f8\u00f9")
        buf.write("\7/\2\2\u00f9\u00fa\58\35\2\u00fa\u00fb\t\3\2\2\u00fb")
        buf.write("\u00fc\58\35\2\u00fc\u00fd\7\26\2\2\u00fd\u00fe\5&\24")
        buf.write("\2\u00fe\65\3\2\2\2\u00ff\u0100\5J&\2\u0100\u0101\79\2")
        buf.write("\2\u0101\u0102\7=\2\2\u0102\u0104\7\63\2\2\u0103\u0105")
        buf.write("\5P)\2\u0104\u0103\3\2\2\2\u0104\u0105\3\2\2\2\u0105\u0106")
        buf.write("\3\2\2\2\u0106\u0107\7\64\2\2\u0107\u0108\7\67\2\2\u0108")
        buf.write("\67\3\2\2\2\u0109\u010a\5:\36\2\u010a\u010b\t\4\2\2\u010b")
        buf.write("\u010c\5:\36\2\u010c\u010f\3\2\2\2\u010d\u010f\5:\36\2")
        buf.write("\u010e\u0109\3\2\2\2\u010e\u010d\3\2\2\2\u010f9\3\2\2")
        buf.write("\2\u0110\u0111\5<\37\2\u0111\u0112\t\5\2\2\u0112\u0113")
        buf.write("\5<\37\2\u0113\u0116\3\2\2\2\u0114\u0116\5<\37\2\u0115")
        buf.write("\u0110\3\2\2\2\u0115\u0114\3\2\2\2\u0116;\3\2\2\2\u0117")
        buf.write("\u0118\b\37\1\2\u0118\u0119\5> \2\u0119\u011f\3\2\2\2")
        buf.write("\u011a\u011b\f\4\2\2\u011b\u011c\t\6\2\2\u011c\u011e\5")
        buf.write("> \2\u011d\u011a\3\2\2\2\u011e\u0121\3\2\2\2\u011f\u011d")
        buf.write("\3\2\2\2\u011f\u0120\3\2\2\2\u0120=\3\2\2\2\u0121\u011f")
        buf.write("\3\2\2\2\u0122\u0123\b \1\2\u0123\u0124\5@!\2\u0124\u012a")
        buf.write("\3\2\2\2\u0125\u0126\f\4\2\2\u0126\u0127\t\7\2\2\u0127")
        buf.write("\u0129\5@!\2\u0128\u0125\3\2\2\2\u0129\u012c\3\2\2\2\u012a")
        buf.write("\u0128\3\2\2\2\u012a\u012b\3\2\2\2\u012b?\3\2\2\2\u012c")
        buf.write("\u012a\3\2\2\2\u012d\u012e\b!\1\2\u012e\u012f\5B\"\2\u012f")
        buf.write("\u0135\3\2\2\2\u0130\u0131\f\4\2\2\u0131\u0132\t\b\2\2")
        buf.write("\u0132\u0134\5B\"\2\u0133\u0130\3\2\2\2\u0134\u0137\3")
        buf.write("\2\2\2\u0135\u0133\3\2\2\2\u0135\u0136\3\2\2\2\u0136A")
        buf.write("\3\2\2\2\u0137\u0135\3\2\2\2\u0138\u0139\b\"\1\2\u0139")
        buf.write("\u013a\5D#\2\u013a\u0140\3\2\2\2\u013b\u013c\f\4\2\2\u013c")
        buf.write("\u013d\7,\2\2\u013d\u013f\5D#\2\u013e\u013b\3\2\2\2\u013f")
        buf.write("\u0142\3\2\2\2\u0140\u013e\3\2\2\2\u0140\u0141\3\2\2\2")
        buf.write("\u0141C\3\2\2\2\u0142\u0140\3\2\2\2\u0143\u0144\7-\2\2")
        buf.write("\u0144\u0147\5D#\2\u0145\u0147\5F$\2\u0146\u0143\3\2\2")
        buf.write("\2\u0146\u0145\3\2\2\2\u0147E\3\2\2\2\u0148\u0149\t\t")
        buf.write("\2\2\u0149\u014c\5F$\2\u014a\u014c\5H%\2\u014b\u0148\3")
        buf.write("\2\2\2\u014b\u014a\3\2\2\2\u014cG\3\2\2\2\u014d\u014e")
        buf.write("\5J&\2\u014e\u014f\7\61\2\2\u014f\u0150\58\35\2\u0150")
        buf.write("\u0151\7\62\2\2\u0151\u0154\3\2\2\2\u0152\u0154\5J&\2")
        buf.write("\u0153\u014d\3\2\2\2\u0153\u0152\3\2\2\2\u0154I\3\2\2")
        buf.write("\2\u0155\u0156\b&\1\2\u0156\u0157\5L\'\2\u0157\u0164\3")
        buf.write("\2\2\2\u0158\u0159\f\4\2\2\u0159\u015a\79\2\2\u015a\u0160")
        buf.write("\7=\2\2\u015b\u015d\7\63\2\2\u015c\u015e\5P)\2\u015d\u015c")
        buf.write("\3\2\2\2\u015d\u015e\3\2\2\2\u015e\u015f\3\2\2\2\u015f")
        buf.write("\u0161\7\64\2\2\u0160\u015b\3\2\2\2\u0160\u0161\3\2\2")
        buf.write("\2\u0161\u0163\3\2\2\2\u0162\u0158\3\2\2\2\u0163\u0166")
        buf.write("\3\2\2\2\u0164\u0162\3\2\2\2\u0164\u0165\3\2\2\2\u0165")
        buf.write("K\3\2\2\2\u0166\u0164\3\2\2\2\u0167\u0168\7\27\2\2\u0168")
        buf.write("\u0169\7=\2\2\u0169\u016b\7\63\2\2\u016a\u016c\5P)\2\u016b")
        buf.write("\u016a\3\2\2\2\u016b\u016c\3\2\2\2\u016c\u016d\3\2\2\2")
        buf.write("\u016d\u0170\7\64\2\2\u016e\u0170\5N(\2\u016f\u0167\3")
        buf.write("\2\2\2\u016f\u016e\3\2\2\2\u0170M\3\2\2\2\u0171\u0172")
        buf.write("\t\n\2\2\u0172O\3\2\2\2\u0173\u0178\58\35\2\u0174\u0175")
        buf.write("\78\2\2\u0175\u0177\58\35\2\u0176\u0174\3\2\2\2\u0177")
        buf.write("\u017a\3\2\2\2\u0178\u0176\3\2\2\2\u0178\u0179\3\2\2\2")
        buf.write("\u0179Q\3\2\2\2\u017a\u0178\3\2\2\2%U[ahlps}\u0095\u009a")
        buf.write("\u009d\u00a2\u00aa\u00ae\u00bf\u00ce\u00d4\u00da\u00f4")
        buf.write("\u0104\u010e\u0115\u011f\u012a\u0135\u0140\u0146\u014b")
        buf.write("\u0153\u015d\u0160\u0164\u016b\u016f\u0178")
        return buf.getvalue()


class BKOOLParser ( Parser ):

    grammarFileName = "BKOOL.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "'boolean'", "'extends'", "'then'", "'nil'", "'break'", 
                     "'float'", "'for'", "'this'", "'class'", "'if'", "'return'", 
                     "'final'", "'continue'", "'int'", "'true'", "'static'", 
                     "'do'", "'new'", "'false'", "'to'", "'else'", "'string'", 
                     "'void'", "'downto'", "'<'", "'<='", "'>'", "'>='", 
                     "'!='", "'=='", "'||'", "'&&'", "'+'", "'-'", "'*'", 
                     "'/'", "'\\'", "'%'", "'^'", "'!'", "':'", "':='", 
                     "'='", "'['", "']'", "'('", "')'", "'{'", "'}'", "';'", 
                     "','", "'.'" ]

    symbolicNames = [ "<INVALID>", "BOOLLIT", "BLOCK_COMMENT", "LINE_COMMENT", 
                      "BOOLEAN", "EXTENDS", "THEN", "NIL", "BREAK", "FLOAT", 
                      "FOR", "THIS", "CLASS", "IF", "RETURN", "FINAL", "CONTINUE", 
                      "INT", "TRUE", "STATIC", "DO", "NEW", "FALSE", "TO", 
                      "ELSE", "STRING", "VOID", "DOWNTO", "LESSER_OP", "LESSER_EQUAL_OP", 
                      "GREATER_OP", "GREATER_EQUAL_OP", "NOT_EQUAL_OP", 
                      "EQUAL_OP", "OR_OP", "AND_OP", "ADD_OP", "SUB_OP", 
                      "MUL_OP", "DIV_FLOAT_OP", "DIV_INT_OP", "MODUL_OP", 
                      "CONCAT_OP", "NOT_OP", "DECLARE_OP", "ASSIGN_OP", 
                      "ASSIGN_OP_CONST", "LSB", "RSB", "LB", "RB", "LP", 
                      "RP", "SEMICOLON", "COMMA", "DOT", "FLOATLIT", "INTEGERLIT", 
                      "STRINGLIT", "ID", "DIGIT", "EXPONENT", "WS", "UNCLOSE_STRING", 
                      "ILLEGAL_ESCAPE", "ERROR_CHAR" ]

    RULE_program = 0
    RULE_class_decl = 1
    RULE_list_member = 2
    RULE_var_decl = 3
    RULE_local_decl = 4
    RULE_imutable_decl = 5
    RULE_mutable_decl = 6
    RULE_imutable_local = 7
    RULE_mutable_local = 8
    RULE_idlist = 9
    RULE_method_decl = 10
    RULE_type_var = 11
    RULE_arr_type = 12
    RULE_pri_type = 13
    RULE_void_type = 14
    RULE_class_type = 15
    RULE_many_para = 16
    RULE_one_para = 17
    RULE_stmt = 18
    RULE_block_stmt = 19
    RULE_assign_stmt = 20
    RULE_break_stmt = 21
    RULE_continue_stmt = 22
    RULE_return_stmt = 23
    RULE_if_stmt = 24
    RULE_for_stmt = 25
    RULE_call_stmt = 26
    RULE_exp = 27
    RULE_exp1 = 28
    RULE_exp2 = 29
    RULE_exp3 = 30
    RULE_exp4 = 31
    RULE_exp5 = 32
    RULE_exp6 = 33
    RULE_exp7 = 34
    RULE_exp8 = 35
    RULE_exp9 = 36
    RULE_exp10 = 37
    RULE_element = 38
    RULE_arglist = 39

    ruleNames =  [ "program", "class_decl", "list_member", "var_decl", "local_decl", 
                   "imutable_decl", "mutable_decl", "imutable_local", "mutable_local", 
                   "idlist", "method_decl", "type_var", "arr_type", "pri_type", 
                   "void_type", "class_type", "many_para", "one_para", "stmt", 
                   "block_stmt", "assign_stmt", "break_stmt", "continue_stmt", 
                   "return_stmt", "if_stmt", "for_stmt", "call_stmt", "exp", 
                   "exp1", "exp2", "exp3", "exp4", "exp5", "exp6", "exp7", 
                   "exp8", "exp9", "exp10", "element", "arglist" ]

    EOF = Token.EOF
    BOOLLIT=1
    BLOCK_COMMENT=2
    LINE_COMMENT=3
    BOOLEAN=4
    EXTENDS=5
    THEN=6
    NIL=7
    BREAK=8
    FLOAT=9
    FOR=10
    THIS=11
    CLASS=12
    IF=13
    RETURN=14
    FINAL=15
    CONTINUE=16
    INT=17
    TRUE=18
    STATIC=19
    DO=20
    NEW=21
    FALSE=22
    TO=23
    ELSE=24
    STRING=25
    VOID=26
    DOWNTO=27
    LESSER_OP=28
    LESSER_EQUAL_OP=29
    GREATER_OP=30
    GREATER_EQUAL_OP=31
    NOT_EQUAL_OP=32
    EQUAL_OP=33
    OR_OP=34
    AND_OP=35
    ADD_OP=36
    SUB_OP=37
    MUL_OP=38
    DIV_FLOAT_OP=39
    DIV_INT_OP=40
    MODUL_OP=41
    CONCAT_OP=42
    NOT_OP=43
    DECLARE_OP=44
    ASSIGN_OP=45
    ASSIGN_OP_CONST=46
    LSB=47
    RSB=48
    LB=49
    RB=50
    LP=51
    RP=52
    SEMICOLON=53
    COMMA=54
    DOT=55
    FLOATLIT=56
    INTEGERLIT=57
    STRINGLIT=58
    ID=59
    DIGIT=60
    EXPONENT=61
    WS=62
    UNCLOSE_STRING=63
    ILLEGAL_ESCAPE=64
    ERROR_CHAR=65

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.2")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class ProgramContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def class_decl(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(BKOOLParser.Class_declContext)
            else:
                return self.getTypedRuleContext(BKOOLParser.Class_declContext,i)


        def getRuleIndex(self):
            return BKOOLParser.RULE_program

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitProgram" ):
                return visitor.visitProgram(self)
            else:
                return visitor.visitChildren(self)




    def program(self):

        localctx = BKOOLParser.ProgramContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_program)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 81 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 80
                self.class_decl()
                self.state = 83 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not (_la==BKOOLParser.CLASS):
                    break

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Class_declContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def CLASS(self):
            return self.getToken(BKOOLParser.CLASS, 0)

        def ID(self, i:int=None):
            if i is None:
                return self.getTokens(BKOOLParser.ID)
            else:
                return self.getToken(BKOOLParser.ID, i)

        def LP(self):
            return self.getToken(BKOOLParser.LP, 0)

        def RP(self):
            return self.getToken(BKOOLParser.RP, 0)

        def EXTENDS(self):
            return self.getToken(BKOOLParser.EXTENDS, 0)

        def list_member(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(BKOOLParser.List_memberContext)
            else:
                return self.getTypedRuleContext(BKOOLParser.List_memberContext,i)


        def getRuleIndex(self):
            return BKOOLParser.RULE_class_decl

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitClass_decl" ):
                return visitor.visitClass_decl(self)
            else:
                return visitor.visitChildren(self)




    def class_decl(self):

        localctx = BKOOLParser.Class_declContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_class_decl)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 85
            self.match(BKOOLParser.CLASS)
            self.state = 86
            self.match(BKOOLParser.ID)
            self.state = 89
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==BKOOLParser.EXTENDS:
                self.state = 87
                self.match(BKOOLParser.EXTENDS)
                self.state = 88
                self.match(BKOOLParser.ID)


            self.state = 91
            self.match(BKOOLParser.LP)
            self.state = 95
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << BKOOLParser.BOOLEAN) | (1 << BKOOLParser.FLOAT) | (1 << BKOOLParser.FINAL) | (1 << BKOOLParser.INT) | (1 << BKOOLParser.STATIC) | (1 << BKOOLParser.STRING) | (1 << BKOOLParser.VOID) | (1 << BKOOLParser.ID))) != 0):
                self.state = 92
                self.list_member()
                self.state = 97
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 98
            self.match(BKOOLParser.RP)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class List_memberContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def var_decl(self):
            return self.getTypedRuleContext(BKOOLParser.Var_declContext,0)


        def method_decl(self):
            return self.getTypedRuleContext(BKOOLParser.Method_declContext,0)


        def getRuleIndex(self):
            return BKOOLParser.RULE_list_member

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitList_member" ):
                return visitor.visitList_member(self)
            else:
                return visitor.visitChildren(self)




    def list_member(self):

        localctx = BKOOLParser.List_memberContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_list_member)
        try:
            self.state = 102
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,3,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 100
                self.var_decl()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 101
                self.method_decl()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Var_declContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def imutable_decl(self):
            return self.getTypedRuleContext(BKOOLParser.Imutable_declContext,0)


        def mutable_decl(self):
            return self.getTypedRuleContext(BKOOLParser.Mutable_declContext,0)


        def getRuleIndex(self):
            return BKOOLParser.RULE_var_decl

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitVar_decl" ):
                return visitor.visitVar_decl(self)
            else:
                return visitor.visitChildren(self)




    def var_decl(self):

        localctx = BKOOLParser.Var_declContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_var_decl)
        try:
            self.state = 106
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,4,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 104
                self.imutable_decl()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 105
                self.mutable_decl()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Local_declContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def imutable_local(self):
            return self.getTypedRuleContext(BKOOLParser.Imutable_localContext,0)


        def mutable_local(self):
            return self.getTypedRuleContext(BKOOLParser.Mutable_localContext,0)


        def getRuleIndex(self):
            return BKOOLParser.RULE_local_decl

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitLocal_decl" ):
                return visitor.visitLocal_decl(self)
            else:
                return visitor.visitChildren(self)




    def local_decl(self):

        localctx = BKOOLParser.Local_declContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_local_decl)
        try:
            self.state = 110
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [BKOOLParser.FINAL]:
                self.enterOuterAlt(localctx, 1)
                self.state = 108
                self.imutable_local()
                pass
            elif token in [BKOOLParser.ID]:
                self.enterOuterAlt(localctx, 2)
                self.state = 109
                self.mutable_local()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Imutable_declContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def FINAL(self):
            return self.getToken(BKOOLParser.FINAL, 0)

        def pri_type(self):
            return self.getTypedRuleContext(BKOOLParser.Pri_typeContext,0)


        def ID(self):
            return self.getToken(BKOOLParser.ID, 0)

        def ASSIGN_OP_CONST(self):
            return self.getToken(BKOOLParser.ASSIGN_OP_CONST, 0)

        def exp(self):
            return self.getTypedRuleContext(BKOOLParser.ExpContext,0)


        def SEMICOLON(self):
            return self.getToken(BKOOLParser.SEMICOLON, 0)

        def STATIC(self):
            return self.getToken(BKOOLParser.STATIC, 0)

        def getRuleIndex(self):
            return BKOOLParser.RULE_imutable_decl

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitImutable_decl" ):
                return visitor.visitImutable_decl(self)
            else:
                return visitor.visitChildren(self)




    def imutable_decl(self):

        localctx = BKOOLParser.Imutable_declContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_imutable_decl)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 113
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==BKOOLParser.STATIC:
                self.state = 112
                self.match(BKOOLParser.STATIC)


            self.state = 115
            self.match(BKOOLParser.FINAL)
            self.state = 116
            self.pri_type()
            self.state = 117
            self.match(BKOOLParser.ID)
            self.state = 118
            self.match(BKOOLParser.ASSIGN_OP_CONST)
            self.state = 119
            self.exp()
            self.state = 120
            self.match(BKOOLParser.SEMICOLON)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Mutable_declContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def idlist(self):
            return self.getTypedRuleContext(BKOOLParser.IdlistContext,0)


        def DECLARE_OP(self):
            return self.getToken(BKOOLParser.DECLARE_OP, 0)

        def type_var(self):
            return self.getTypedRuleContext(BKOOLParser.Type_varContext,0)


        def SEMICOLON(self):
            return self.getToken(BKOOLParser.SEMICOLON, 0)

        def STATIC(self):
            return self.getToken(BKOOLParser.STATIC, 0)

        def getRuleIndex(self):
            return BKOOLParser.RULE_mutable_decl

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMutable_decl" ):
                return visitor.visitMutable_decl(self)
            else:
                return visitor.visitChildren(self)




    def mutable_decl(self):

        localctx = BKOOLParser.Mutable_declContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_mutable_decl)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 123
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==BKOOLParser.STATIC:
                self.state = 122
                self.match(BKOOLParser.STATIC)


            self.state = 125
            self.idlist()
            self.state = 126
            self.match(BKOOLParser.DECLARE_OP)
            self.state = 127
            self.type_var()
            self.state = 128
            self.match(BKOOLParser.SEMICOLON)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Imutable_localContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def FINAL(self):
            return self.getToken(BKOOLParser.FINAL, 0)

        def pri_type(self):
            return self.getTypedRuleContext(BKOOLParser.Pri_typeContext,0)


        def ID(self):
            return self.getToken(BKOOLParser.ID, 0)

        def ASSIGN_OP_CONST(self):
            return self.getToken(BKOOLParser.ASSIGN_OP_CONST, 0)

        def exp(self):
            return self.getTypedRuleContext(BKOOLParser.ExpContext,0)


        def SEMICOLON(self):
            return self.getToken(BKOOLParser.SEMICOLON, 0)

        def getRuleIndex(self):
            return BKOOLParser.RULE_imutable_local

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitImutable_local" ):
                return visitor.visitImutable_local(self)
            else:
                return visitor.visitChildren(self)




    def imutable_local(self):

        localctx = BKOOLParser.Imutable_localContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_imutable_local)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 130
            self.match(BKOOLParser.FINAL)
            self.state = 131
            self.pri_type()
            self.state = 132
            self.match(BKOOLParser.ID)
            self.state = 133
            self.match(BKOOLParser.ASSIGN_OP_CONST)
            self.state = 134
            self.exp()
            self.state = 135
            self.match(BKOOLParser.SEMICOLON)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Mutable_localContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def idlist(self):
            return self.getTypedRuleContext(BKOOLParser.IdlistContext,0)


        def DECLARE_OP(self):
            return self.getToken(BKOOLParser.DECLARE_OP, 0)

        def type_var(self):
            return self.getTypedRuleContext(BKOOLParser.Type_varContext,0)


        def SEMICOLON(self):
            return self.getToken(BKOOLParser.SEMICOLON, 0)

        def getRuleIndex(self):
            return BKOOLParser.RULE_mutable_local

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMutable_local" ):
                return visitor.visitMutable_local(self)
            else:
                return visitor.visitChildren(self)




    def mutable_local(self):

        localctx = BKOOLParser.Mutable_localContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_mutable_local)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 137
            self.idlist()
            self.state = 138
            self.match(BKOOLParser.DECLARE_OP)
            self.state = 139
            self.type_var()
            self.state = 140
            self.match(BKOOLParser.SEMICOLON)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class IdlistContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self, i:int=None):
            if i is None:
                return self.getTokens(BKOOLParser.ID)
            else:
                return self.getToken(BKOOLParser.ID, i)

        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(BKOOLParser.COMMA)
            else:
                return self.getToken(BKOOLParser.COMMA, i)

        def getRuleIndex(self):
            return BKOOLParser.RULE_idlist

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitIdlist" ):
                return visitor.visitIdlist(self)
            else:
                return visitor.visitChildren(self)




    def idlist(self):

        localctx = BKOOLParser.IdlistContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_idlist)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 142
            self.match(BKOOLParser.ID)
            self.state = 147
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==BKOOLParser.COMMA:
                self.state = 143
                self.match(BKOOLParser.COMMA)
                self.state = 144
                self.match(BKOOLParser.ID)
                self.state = 149
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Method_declContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(BKOOLParser.ID, 0)

        def LB(self):
            return self.getToken(BKOOLParser.LB, 0)

        def RB(self):
            return self.getToken(BKOOLParser.RB, 0)

        def block_stmt(self):
            return self.getTypedRuleContext(BKOOLParser.Block_stmtContext,0)


        def type_var(self):
            return self.getTypedRuleContext(BKOOLParser.Type_varContext,0)


        def void_type(self):
            return self.getTypedRuleContext(BKOOLParser.Void_typeContext,0)


        def STATIC(self):
            return self.getToken(BKOOLParser.STATIC, 0)

        def many_para(self):
            return self.getTypedRuleContext(BKOOLParser.Many_paraContext,0)


        def getRuleIndex(self):
            return BKOOLParser.RULE_method_decl

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMethod_decl" ):
                return visitor.visitMethod_decl(self)
            else:
                return visitor.visitChildren(self)




    def method_decl(self):

        localctx = BKOOLParser.Method_declContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_method_decl)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 152
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,9,self._ctx)
            if la_ == 1:
                self.state = 150
                self.type_var()

            elif la_ == 2:
                self.state = 151
                self.void_type()


            self.state = 155
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==BKOOLParser.STATIC:
                self.state = 154
                self.match(BKOOLParser.STATIC)


            self.state = 157
            self.match(BKOOLParser.ID)
            self.state = 158
            self.match(BKOOLParser.LB)
            self.state = 160
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==BKOOLParser.ID:
                self.state = 159
                self.many_para()


            self.state = 162
            self.match(BKOOLParser.RB)
            self.state = 163
            self.block_stmt()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Type_varContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def pri_type(self):
            return self.getTypedRuleContext(BKOOLParser.Pri_typeContext,0)


        def arr_type(self):
            return self.getTypedRuleContext(BKOOLParser.Arr_typeContext,0)


        def class_type(self):
            return self.getTypedRuleContext(BKOOLParser.Class_typeContext,0)


        def getRuleIndex(self):
            return BKOOLParser.RULE_type_var

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitType_var" ):
                return visitor.visitType_var(self)
            else:
                return visitor.visitChildren(self)




    def type_var(self):

        localctx = BKOOLParser.Type_varContext(self, self._ctx, self.state)
        self.enterRule(localctx, 22, self.RULE_type_var)
        try:
            self.state = 168
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,12,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 165
                self.pri_type()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 166
                self.arr_type()
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 167
                self.class_type()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Arr_typeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LSB(self):
            return self.getToken(BKOOLParser.LSB, 0)

        def INTEGERLIT(self):
            return self.getToken(BKOOLParser.INTEGERLIT, 0)

        def RSB(self):
            return self.getToken(BKOOLParser.RSB, 0)

        def pri_type(self):
            return self.getTypedRuleContext(BKOOLParser.Pri_typeContext,0)


        def class_type(self):
            return self.getTypedRuleContext(BKOOLParser.Class_typeContext,0)


        def getRuleIndex(self):
            return BKOOLParser.RULE_arr_type

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitArr_type" ):
                return visitor.visitArr_type(self)
            else:
                return visitor.visitChildren(self)




    def arr_type(self):

        localctx = BKOOLParser.Arr_typeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 24, self.RULE_arr_type)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 172
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [BKOOLParser.BOOLEAN, BKOOLParser.FLOAT, BKOOLParser.INT, BKOOLParser.STRING]:
                self.state = 170
                self.pri_type()
                pass
            elif token in [BKOOLParser.ID]:
                self.state = 171
                self.class_type()
                pass
            else:
                raise NoViableAltException(self)

            self.state = 174
            self.match(BKOOLParser.LSB)
            self.state = 175
            self.match(BKOOLParser.INTEGERLIT)
            self.state = 176
            self.match(BKOOLParser.RSB)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Pri_typeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def BOOLEAN(self):
            return self.getToken(BKOOLParser.BOOLEAN, 0)

        def INT(self):
            return self.getToken(BKOOLParser.INT, 0)

        def FLOAT(self):
            return self.getToken(BKOOLParser.FLOAT, 0)

        def STRING(self):
            return self.getToken(BKOOLParser.STRING, 0)

        def getRuleIndex(self):
            return BKOOLParser.RULE_pri_type

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitPri_type" ):
                return visitor.visitPri_type(self)
            else:
                return visitor.visitChildren(self)




    def pri_type(self):

        localctx = BKOOLParser.Pri_typeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 26, self.RULE_pri_type)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 178
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << BKOOLParser.BOOLEAN) | (1 << BKOOLParser.FLOAT) | (1 << BKOOLParser.INT) | (1 << BKOOLParser.STRING))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Void_typeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def VOID(self):
            return self.getToken(BKOOLParser.VOID, 0)

        def getRuleIndex(self):
            return BKOOLParser.RULE_void_type

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitVoid_type" ):
                return visitor.visitVoid_type(self)
            else:
                return visitor.visitChildren(self)




    def void_type(self):

        localctx = BKOOLParser.Void_typeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 28, self.RULE_void_type)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 180
            self.match(BKOOLParser.VOID)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Class_typeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(BKOOLParser.ID, 0)

        def getRuleIndex(self):
            return BKOOLParser.RULE_class_type

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitClass_type" ):
                return visitor.visitClass_type(self)
            else:
                return visitor.visitChildren(self)




    def class_type(self):

        localctx = BKOOLParser.Class_typeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 30, self.RULE_class_type)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 182
            self.match(BKOOLParser.ID)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Many_paraContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def one_para(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(BKOOLParser.One_paraContext)
            else:
                return self.getTypedRuleContext(BKOOLParser.One_paraContext,i)


        def SEMICOLON(self, i:int=None):
            if i is None:
                return self.getTokens(BKOOLParser.SEMICOLON)
            else:
                return self.getToken(BKOOLParser.SEMICOLON, i)

        def getRuleIndex(self):
            return BKOOLParser.RULE_many_para

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMany_para" ):
                return visitor.visitMany_para(self)
            else:
                return visitor.visitChildren(self)




    def many_para(self):

        localctx = BKOOLParser.Many_paraContext(self, self._ctx, self.state)
        self.enterRule(localctx, 32, self.RULE_many_para)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 184
            self.one_para()
            self.state = 189
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==BKOOLParser.SEMICOLON:
                self.state = 185
                self.match(BKOOLParser.SEMICOLON)
                self.state = 186
                self.one_para()
                self.state = 191
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class One_paraContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def idlist(self):
            return self.getTypedRuleContext(BKOOLParser.IdlistContext,0)


        def DECLARE_OP(self):
            return self.getToken(BKOOLParser.DECLARE_OP, 0)

        def type_var(self):
            return self.getTypedRuleContext(BKOOLParser.Type_varContext,0)


        def getRuleIndex(self):
            return BKOOLParser.RULE_one_para

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitOne_para" ):
                return visitor.visitOne_para(self)
            else:
                return visitor.visitChildren(self)




    def one_para(self):

        localctx = BKOOLParser.One_paraContext(self, self._ctx, self.state)
        self.enterRule(localctx, 34, self.RULE_one_para)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 192
            self.idlist()
            self.state = 193
            self.match(BKOOLParser.DECLARE_OP)
            self.state = 194
            self.type_var()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class StmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def assign_stmt(self):
            return self.getTypedRuleContext(BKOOLParser.Assign_stmtContext,0)


        def break_stmt(self):
            return self.getTypedRuleContext(BKOOLParser.Break_stmtContext,0)


        def continue_stmt(self):
            return self.getTypedRuleContext(BKOOLParser.Continue_stmtContext,0)


        def return_stmt(self):
            return self.getTypedRuleContext(BKOOLParser.Return_stmtContext,0)


        def if_stmt(self):
            return self.getTypedRuleContext(BKOOLParser.If_stmtContext,0)


        def call_stmt(self):
            return self.getTypedRuleContext(BKOOLParser.Call_stmtContext,0)


        def for_stmt(self):
            return self.getTypedRuleContext(BKOOLParser.For_stmtContext,0)


        def block_stmt(self):
            return self.getTypedRuleContext(BKOOLParser.Block_stmtContext,0)


        def getRuleIndex(self):
            return BKOOLParser.RULE_stmt

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitStmt" ):
                return visitor.visitStmt(self)
            else:
                return visitor.visitChildren(self)




    def stmt(self):

        localctx = BKOOLParser.StmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 36, self.RULE_stmt)
        try:
            self.state = 204
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,15,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 196
                self.assign_stmt()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 197
                self.break_stmt()
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 198
                self.continue_stmt()
                pass

            elif la_ == 4:
                self.enterOuterAlt(localctx, 4)
                self.state = 199
                self.return_stmt()
                pass

            elif la_ == 5:
                self.enterOuterAlt(localctx, 5)
                self.state = 200
                self.if_stmt()
                pass

            elif la_ == 6:
                self.enterOuterAlt(localctx, 6)
                self.state = 201
                self.call_stmt()
                pass

            elif la_ == 7:
                self.enterOuterAlt(localctx, 7)
                self.state = 202
                self.for_stmt()
                pass

            elif la_ == 8:
                self.enterOuterAlt(localctx, 8)
                self.state = 203
                self.block_stmt()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Block_stmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LP(self):
            return self.getToken(BKOOLParser.LP, 0)

        def RP(self):
            return self.getToken(BKOOLParser.RP, 0)

        def local_decl(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(BKOOLParser.Local_declContext)
            else:
                return self.getTypedRuleContext(BKOOLParser.Local_declContext,i)


        def stmt(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(BKOOLParser.StmtContext)
            else:
                return self.getTypedRuleContext(BKOOLParser.StmtContext,i)


        def getRuleIndex(self):
            return BKOOLParser.RULE_block_stmt

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBlock_stmt" ):
                return visitor.visitBlock_stmt(self)
            else:
                return visitor.visitChildren(self)




    def block_stmt(self):

        localctx = BKOOLParser.Block_stmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 38, self.RULE_block_stmt)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 206
            self.match(BKOOLParser.LP)
            self.state = 210
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,16,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 207
                    self.local_decl() 
                self.state = 212
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,16,self._ctx)

            self.state = 216
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << BKOOLParser.BOOLLIT) | (1 << BKOOLParser.NIL) | (1 << BKOOLParser.BREAK) | (1 << BKOOLParser.FOR) | (1 << BKOOLParser.THIS) | (1 << BKOOLParser.IF) | (1 << BKOOLParser.RETURN) | (1 << BKOOLParser.CONTINUE) | (1 << BKOOLParser.NEW) | (1 << BKOOLParser.LP) | (1 << BKOOLParser.FLOATLIT) | (1 << BKOOLParser.INTEGERLIT) | (1 << BKOOLParser.STRINGLIT) | (1 << BKOOLParser.ID))) != 0):
                self.state = 213
                self.stmt()
                self.state = 218
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 219
            self.match(BKOOLParser.RP)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Assign_stmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def exp8(self):
            return self.getTypedRuleContext(BKOOLParser.Exp8Context,0)


        def ASSIGN_OP(self):
            return self.getToken(BKOOLParser.ASSIGN_OP, 0)

        def exp(self):
            return self.getTypedRuleContext(BKOOLParser.ExpContext,0)


        def SEMICOLON(self):
            return self.getToken(BKOOLParser.SEMICOLON, 0)

        def getRuleIndex(self):
            return BKOOLParser.RULE_assign_stmt

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAssign_stmt" ):
                return visitor.visitAssign_stmt(self)
            else:
                return visitor.visitChildren(self)




    def assign_stmt(self):

        localctx = BKOOLParser.Assign_stmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 40, self.RULE_assign_stmt)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 221
            self.exp8()
            self.state = 222
            self.match(BKOOLParser.ASSIGN_OP)
            self.state = 223
            self.exp()
            self.state = 224
            self.match(BKOOLParser.SEMICOLON)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Break_stmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def BREAK(self):
            return self.getToken(BKOOLParser.BREAK, 0)

        def SEMICOLON(self):
            return self.getToken(BKOOLParser.SEMICOLON, 0)

        def getRuleIndex(self):
            return BKOOLParser.RULE_break_stmt

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBreak_stmt" ):
                return visitor.visitBreak_stmt(self)
            else:
                return visitor.visitChildren(self)




    def break_stmt(self):

        localctx = BKOOLParser.Break_stmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 42, self.RULE_break_stmt)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 226
            self.match(BKOOLParser.BREAK)
            self.state = 227
            self.match(BKOOLParser.SEMICOLON)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Continue_stmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def CONTINUE(self):
            return self.getToken(BKOOLParser.CONTINUE, 0)

        def SEMICOLON(self):
            return self.getToken(BKOOLParser.SEMICOLON, 0)

        def getRuleIndex(self):
            return BKOOLParser.RULE_continue_stmt

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitContinue_stmt" ):
                return visitor.visitContinue_stmt(self)
            else:
                return visitor.visitChildren(self)




    def continue_stmt(self):

        localctx = BKOOLParser.Continue_stmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 44, self.RULE_continue_stmt)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 229
            self.match(BKOOLParser.CONTINUE)
            self.state = 230
            self.match(BKOOLParser.SEMICOLON)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Return_stmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def RETURN(self):
            return self.getToken(BKOOLParser.RETURN, 0)

        def exp(self):
            return self.getTypedRuleContext(BKOOLParser.ExpContext,0)


        def SEMICOLON(self):
            return self.getToken(BKOOLParser.SEMICOLON, 0)

        def getRuleIndex(self):
            return BKOOLParser.RULE_return_stmt

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitReturn_stmt" ):
                return visitor.visitReturn_stmt(self)
            else:
                return visitor.visitChildren(self)




    def return_stmt(self):

        localctx = BKOOLParser.Return_stmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 46, self.RULE_return_stmt)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 232
            self.match(BKOOLParser.RETURN)
            self.state = 233
            self.exp()
            self.state = 234
            self.match(BKOOLParser.SEMICOLON)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class If_stmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def IF(self):
            return self.getToken(BKOOLParser.IF, 0)

        def exp(self):
            return self.getTypedRuleContext(BKOOLParser.ExpContext,0)


        def THEN(self):
            return self.getToken(BKOOLParser.THEN, 0)

        def stmt(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(BKOOLParser.StmtContext)
            else:
                return self.getTypedRuleContext(BKOOLParser.StmtContext,i)


        def ELSE(self):
            return self.getToken(BKOOLParser.ELSE, 0)

        def getRuleIndex(self):
            return BKOOLParser.RULE_if_stmt

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitIf_stmt" ):
                return visitor.visitIf_stmt(self)
            else:
                return visitor.visitChildren(self)




    def if_stmt(self):

        localctx = BKOOLParser.If_stmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 48, self.RULE_if_stmt)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 236
            self.match(BKOOLParser.IF)
            self.state = 237
            self.exp()
            self.state = 238
            self.match(BKOOLParser.THEN)
            self.state = 239
            self.stmt()
            self.state = 242
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,18,self._ctx)
            if la_ == 1:
                self.state = 240
                self.match(BKOOLParser.ELSE)
                self.state = 241
                self.stmt()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class For_stmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def FOR(self):
            return self.getToken(BKOOLParser.FOR, 0)

        def ID(self):
            return self.getToken(BKOOLParser.ID, 0)

        def ASSIGN_OP(self):
            return self.getToken(BKOOLParser.ASSIGN_OP, 0)

        def exp(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(BKOOLParser.ExpContext)
            else:
                return self.getTypedRuleContext(BKOOLParser.ExpContext,i)


        def DO(self):
            return self.getToken(BKOOLParser.DO, 0)

        def stmt(self):
            return self.getTypedRuleContext(BKOOLParser.StmtContext,0)


        def TO(self):
            return self.getToken(BKOOLParser.TO, 0)

        def DOWNTO(self):
            return self.getToken(BKOOLParser.DOWNTO, 0)

        def getRuleIndex(self):
            return BKOOLParser.RULE_for_stmt

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFor_stmt" ):
                return visitor.visitFor_stmt(self)
            else:
                return visitor.visitChildren(self)




    def for_stmt(self):

        localctx = BKOOLParser.For_stmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 50, self.RULE_for_stmt)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 244
            self.match(BKOOLParser.FOR)
            self.state = 245
            self.match(BKOOLParser.ID)
            self.state = 246
            self.match(BKOOLParser.ASSIGN_OP)
            self.state = 247
            self.exp()
            self.state = 248
            _la = self._input.LA(1)
            if not(_la==BKOOLParser.TO or _la==BKOOLParser.DOWNTO):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
            self.state = 249
            self.exp()
            self.state = 250
            self.match(BKOOLParser.DO)
            self.state = 251
            self.stmt()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Call_stmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def exp9(self):
            return self.getTypedRuleContext(BKOOLParser.Exp9Context,0)


        def DOT(self):
            return self.getToken(BKOOLParser.DOT, 0)

        def ID(self):
            return self.getToken(BKOOLParser.ID, 0)

        def LB(self):
            return self.getToken(BKOOLParser.LB, 0)

        def RB(self):
            return self.getToken(BKOOLParser.RB, 0)

        def SEMICOLON(self):
            return self.getToken(BKOOLParser.SEMICOLON, 0)

        def arglist(self):
            return self.getTypedRuleContext(BKOOLParser.ArglistContext,0)


        def getRuleIndex(self):
            return BKOOLParser.RULE_call_stmt

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitCall_stmt" ):
                return visitor.visitCall_stmt(self)
            else:
                return visitor.visitChildren(self)




    def call_stmt(self):

        localctx = BKOOLParser.Call_stmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 52, self.RULE_call_stmt)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 253
            self.exp9(0)
            self.state = 254
            self.match(BKOOLParser.DOT)
            self.state = 255
            self.match(BKOOLParser.ID)
            self.state = 256
            self.match(BKOOLParser.LB)
            self.state = 258
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << BKOOLParser.BOOLLIT) | (1 << BKOOLParser.NIL) | (1 << BKOOLParser.THIS) | (1 << BKOOLParser.NEW) | (1 << BKOOLParser.SUB_OP) | (1 << BKOOLParser.NOT_OP) | (1 << BKOOLParser.FLOATLIT) | (1 << BKOOLParser.INTEGERLIT) | (1 << BKOOLParser.STRINGLIT) | (1 << BKOOLParser.ID))) != 0):
                self.state = 257
                self.arglist()


            self.state = 260
            self.match(BKOOLParser.RB)
            self.state = 261
            self.match(BKOOLParser.SEMICOLON)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ExpContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def exp1(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(BKOOLParser.Exp1Context)
            else:
                return self.getTypedRuleContext(BKOOLParser.Exp1Context,i)


        def LESSER_OP(self):
            return self.getToken(BKOOLParser.LESSER_OP, 0)

        def LESSER_EQUAL_OP(self):
            return self.getToken(BKOOLParser.LESSER_EQUAL_OP, 0)

        def GREATER_OP(self):
            return self.getToken(BKOOLParser.GREATER_OP, 0)

        def GREATER_EQUAL_OP(self):
            return self.getToken(BKOOLParser.GREATER_EQUAL_OP, 0)

        def getRuleIndex(self):
            return BKOOLParser.RULE_exp

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExp" ):
                return visitor.visitExp(self)
            else:
                return visitor.visitChildren(self)




    def exp(self):

        localctx = BKOOLParser.ExpContext(self, self._ctx, self.state)
        self.enterRule(localctx, 54, self.RULE_exp)
        self._la = 0 # Token type
        try:
            self.state = 268
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,20,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 263
                self.exp1()
                self.state = 264
                _la = self._input.LA(1)
                if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << BKOOLParser.LESSER_OP) | (1 << BKOOLParser.LESSER_EQUAL_OP) | (1 << BKOOLParser.GREATER_OP) | (1 << BKOOLParser.GREATER_EQUAL_OP))) != 0)):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 265
                self.exp1()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 267
                self.exp1()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Exp1Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def exp2(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(BKOOLParser.Exp2Context)
            else:
                return self.getTypedRuleContext(BKOOLParser.Exp2Context,i)


        def EQUAL_OP(self):
            return self.getToken(BKOOLParser.EQUAL_OP, 0)

        def NOT_EQUAL_OP(self):
            return self.getToken(BKOOLParser.NOT_EQUAL_OP, 0)

        def getRuleIndex(self):
            return BKOOLParser.RULE_exp1

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExp1" ):
                return visitor.visitExp1(self)
            else:
                return visitor.visitChildren(self)




    def exp1(self):

        localctx = BKOOLParser.Exp1Context(self, self._ctx, self.state)
        self.enterRule(localctx, 56, self.RULE_exp1)
        self._la = 0 # Token type
        try:
            self.state = 275
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,21,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 270
                self.exp2(0)
                self.state = 271
                _la = self._input.LA(1)
                if not(_la==BKOOLParser.NOT_EQUAL_OP or _la==BKOOLParser.EQUAL_OP):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 272
                self.exp2(0)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 274
                self.exp2(0)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Exp2Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def exp3(self):
            return self.getTypedRuleContext(BKOOLParser.Exp3Context,0)


        def exp2(self):
            return self.getTypedRuleContext(BKOOLParser.Exp2Context,0)


        def AND_OP(self):
            return self.getToken(BKOOLParser.AND_OP, 0)

        def OR_OP(self):
            return self.getToken(BKOOLParser.OR_OP, 0)

        def getRuleIndex(self):
            return BKOOLParser.RULE_exp2

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExp2" ):
                return visitor.visitExp2(self)
            else:
                return visitor.visitChildren(self)



    def exp2(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = BKOOLParser.Exp2Context(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 58
        self.enterRecursionRule(localctx, 58, self.RULE_exp2, _p)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 278
            self.exp3(0)
            self._ctx.stop = self._input.LT(-1)
            self.state = 285
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,22,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    localctx = BKOOLParser.Exp2Context(self, _parentctx, _parentState)
                    self.pushNewRecursionContext(localctx, _startState, self.RULE_exp2)
                    self.state = 280
                    if not self.precpred(self._ctx, 2):
                        from antlr4.error.Errors import FailedPredicateException
                        raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                    self.state = 281
                    _la = self._input.LA(1)
                    if not(_la==BKOOLParser.OR_OP or _la==BKOOLParser.AND_OP):
                        self._errHandler.recoverInline(self)
                    else:
                        self._errHandler.reportMatch(self)
                        self.consume()
                    self.state = 282
                    self.exp3(0) 
                self.state = 287
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,22,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class Exp3Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def exp4(self):
            return self.getTypedRuleContext(BKOOLParser.Exp4Context,0)


        def exp3(self):
            return self.getTypedRuleContext(BKOOLParser.Exp3Context,0)


        def ADD_OP(self):
            return self.getToken(BKOOLParser.ADD_OP, 0)

        def SUB_OP(self):
            return self.getToken(BKOOLParser.SUB_OP, 0)

        def getRuleIndex(self):
            return BKOOLParser.RULE_exp3

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExp3" ):
                return visitor.visitExp3(self)
            else:
                return visitor.visitChildren(self)



    def exp3(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = BKOOLParser.Exp3Context(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 60
        self.enterRecursionRule(localctx, 60, self.RULE_exp3, _p)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 289
            self.exp4(0)
            self._ctx.stop = self._input.LT(-1)
            self.state = 296
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,23,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    localctx = BKOOLParser.Exp3Context(self, _parentctx, _parentState)
                    self.pushNewRecursionContext(localctx, _startState, self.RULE_exp3)
                    self.state = 291
                    if not self.precpred(self._ctx, 2):
                        from antlr4.error.Errors import FailedPredicateException
                        raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                    self.state = 292
                    _la = self._input.LA(1)
                    if not(_la==BKOOLParser.ADD_OP or _la==BKOOLParser.SUB_OP):
                        self._errHandler.recoverInline(self)
                    else:
                        self._errHandler.reportMatch(self)
                        self.consume()
                    self.state = 293
                    self.exp4(0) 
                self.state = 298
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,23,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class Exp4Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def exp5(self):
            return self.getTypedRuleContext(BKOOLParser.Exp5Context,0)


        def exp4(self):
            return self.getTypedRuleContext(BKOOLParser.Exp4Context,0)


        def MUL_OP(self):
            return self.getToken(BKOOLParser.MUL_OP, 0)

        def DIV_FLOAT_OP(self):
            return self.getToken(BKOOLParser.DIV_FLOAT_OP, 0)

        def DIV_INT_OP(self):
            return self.getToken(BKOOLParser.DIV_INT_OP, 0)

        def MODUL_OP(self):
            return self.getToken(BKOOLParser.MODUL_OP, 0)

        def getRuleIndex(self):
            return BKOOLParser.RULE_exp4

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExp4" ):
                return visitor.visitExp4(self)
            else:
                return visitor.visitChildren(self)



    def exp4(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = BKOOLParser.Exp4Context(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 62
        self.enterRecursionRule(localctx, 62, self.RULE_exp4, _p)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 300
            self.exp5(0)
            self._ctx.stop = self._input.LT(-1)
            self.state = 307
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,24,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    localctx = BKOOLParser.Exp4Context(self, _parentctx, _parentState)
                    self.pushNewRecursionContext(localctx, _startState, self.RULE_exp4)
                    self.state = 302
                    if not self.precpred(self._ctx, 2):
                        from antlr4.error.Errors import FailedPredicateException
                        raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                    self.state = 303
                    _la = self._input.LA(1)
                    if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << BKOOLParser.MUL_OP) | (1 << BKOOLParser.DIV_FLOAT_OP) | (1 << BKOOLParser.DIV_INT_OP) | (1 << BKOOLParser.MODUL_OP))) != 0)):
                        self._errHandler.recoverInline(self)
                    else:
                        self._errHandler.reportMatch(self)
                        self.consume()
                    self.state = 304
                    self.exp5(0) 
                self.state = 309
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,24,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class Exp5Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def exp6(self):
            return self.getTypedRuleContext(BKOOLParser.Exp6Context,0)


        def exp5(self):
            return self.getTypedRuleContext(BKOOLParser.Exp5Context,0)


        def CONCAT_OP(self):
            return self.getToken(BKOOLParser.CONCAT_OP, 0)

        def getRuleIndex(self):
            return BKOOLParser.RULE_exp5

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExp5" ):
                return visitor.visitExp5(self)
            else:
                return visitor.visitChildren(self)



    def exp5(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = BKOOLParser.Exp5Context(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 64
        self.enterRecursionRule(localctx, 64, self.RULE_exp5, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 311
            self.exp6()
            self._ctx.stop = self._input.LT(-1)
            self.state = 318
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,25,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    localctx = BKOOLParser.Exp5Context(self, _parentctx, _parentState)
                    self.pushNewRecursionContext(localctx, _startState, self.RULE_exp5)
                    self.state = 313
                    if not self.precpred(self._ctx, 2):
                        from antlr4.error.Errors import FailedPredicateException
                        raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                    self.state = 314
                    self.match(BKOOLParser.CONCAT_OP)
                    self.state = 315
                    self.exp6() 
                self.state = 320
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,25,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class Exp6Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NOT_OP(self):
            return self.getToken(BKOOLParser.NOT_OP, 0)

        def exp6(self):
            return self.getTypedRuleContext(BKOOLParser.Exp6Context,0)


        def exp7(self):
            return self.getTypedRuleContext(BKOOLParser.Exp7Context,0)


        def getRuleIndex(self):
            return BKOOLParser.RULE_exp6

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExp6" ):
                return visitor.visitExp6(self)
            else:
                return visitor.visitChildren(self)




    def exp6(self):

        localctx = BKOOLParser.Exp6Context(self, self._ctx, self.state)
        self.enterRule(localctx, 66, self.RULE_exp6)
        try:
            self.state = 324
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,26,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 321
                self.match(BKOOLParser.NOT_OP)
                self.state = 322
                self.exp6()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 323
                self.exp7()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Exp7Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def exp7(self):
            return self.getTypedRuleContext(BKOOLParser.Exp7Context,0)


        def SUB_OP(self):
            return self.getToken(BKOOLParser.SUB_OP, 0)

        def NOT_OP(self):
            return self.getToken(BKOOLParser.NOT_OP, 0)

        def exp8(self):
            return self.getTypedRuleContext(BKOOLParser.Exp8Context,0)


        def getRuleIndex(self):
            return BKOOLParser.RULE_exp7

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExp7" ):
                return visitor.visitExp7(self)
            else:
                return visitor.visitChildren(self)




    def exp7(self):

        localctx = BKOOLParser.Exp7Context(self, self._ctx, self.state)
        self.enterRule(localctx, 68, self.RULE_exp7)
        self._la = 0 # Token type
        try:
            self.state = 329
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [BKOOLParser.SUB_OP, BKOOLParser.NOT_OP]:
                self.enterOuterAlt(localctx, 1)
                self.state = 326
                _la = self._input.LA(1)
                if not(_la==BKOOLParser.SUB_OP or _la==BKOOLParser.NOT_OP):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 327
                self.exp7()
                pass
            elif token in [BKOOLParser.BOOLLIT, BKOOLParser.NIL, BKOOLParser.THIS, BKOOLParser.NEW, BKOOLParser.FLOATLIT, BKOOLParser.INTEGERLIT, BKOOLParser.STRINGLIT, BKOOLParser.ID]:
                self.enterOuterAlt(localctx, 2)
                self.state = 328
                self.exp8()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Exp8Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def exp9(self):
            return self.getTypedRuleContext(BKOOLParser.Exp9Context,0)


        def LSB(self):
            return self.getToken(BKOOLParser.LSB, 0)

        def exp(self):
            return self.getTypedRuleContext(BKOOLParser.ExpContext,0)


        def RSB(self):
            return self.getToken(BKOOLParser.RSB, 0)

        def getRuleIndex(self):
            return BKOOLParser.RULE_exp8

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExp8" ):
                return visitor.visitExp8(self)
            else:
                return visitor.visitChildren(self)




    def exp8(self):

        localctx = BKOOLParser.Exp8Context(self, self._ctx, self.state)
        self.enterRule(localctx, 70, self.RULE_exp8)
        try:
            self.state = 337
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,28,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 331
                self.exp9(0)
                self.state = 332
                self.match(BKOOLParser.LSB)
                self.state = 333
                self.exp()
                self.state = 334
                self.match(BKOOLParser.RSB)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 336
                self.exp9(0)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Exp9Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def exp10(self):
            return self.getTypedRuleContext(BKOOLParser.Exp10Context,0)


        def exp9(self):
            return self.getTypedRuleContext(BKOOLParser.Exp9Context,0)


        def DOT(self):
            return self.getToken(BKOOLParser.DOT, 0)

        def ID(self):
            return self.getToken(BKOOLParser.ID, 0)

        def LB(self):
            return self.getToken(BKOOLParser.LB, 0)

        def RB(self):
            return self.getToken(BKOOLParser.RB, 0)

        def arglist(self):
            return self.getTypedRuleContext(BKOOLParser.ArglistContext,0)


        def getRuleIndex(self):
            return BKOOLParser.RULE_exp9

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExp9" ):
                return visitor.visitExp9(self)
            else:
                return visitor.visitChildren(self)



    def exp9(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = BKOOLParser.Exp9Context(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 72
        self.enterRecursionRule(localctx, 72, self.RULE_exp9, _p)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 340
            self.exp10()
            self._ctx.stop = self._input.LT(-1)
            self.state = 354
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,31,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    localctx = BKOOLParser.Exp9Context(self, _parentctx, _parentState)
                    self.pushNewRecursionContext(localctx, _startState, self.RULE_exp9)
                    self.state = 342
                    if not self.precpred(self._ctx, 2):
                        from antlr4.error.Errors import FailedPredicateException
                        raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                    self.state = 343
                    self.match(BKOOLParser.DOT)
                    self.state = 344
                    self.match(BKOOLParser.ID)
                    self.state = 350
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,30,self._ctx)
                    if la_ == 1:
                        self.state = 345
                        self.match(BKOOLParser.LB)
                        self.state = 347
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)
                        if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << BKOOLParser.BOOLLIT) | (1 << BKOOLParser.NIL) | (1 << BKOOLParser.THIS) | (1 << BKOOLParser.NEW) | (1 << BKOOLParser.SUB_OP) | (1 << BKOOLParser.NOT_OP) | (1 << BKOOLParser.FLOATLIT) | (1 << BKOOLParser.INTEGERLIT) | (1 << BKOOLParser.STRINGLIT) | (1 << BKOOLParser.ID))) != 0):
                            self.state = 346
                            self.arglist()


                        self.state = 349
                        self.match(BKOOLParser.RB)

             
                self.state = 356
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,31,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class Exp10Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NEW(self):
            return self.getToken(BKOOLParser.NEW, 0)

        def ID(self):
            return self.getToken(BKOOLParser.ID, 0)

        def LB(self):
            return self.getToken(BKOOLParser.LB, 0)

        def RB(self):
            return self.getToken(BKOOLParser.RB, 0)

        def arglist(self):
            return self.getTypedRuleContext(BKOOLParser.ArglistContext,0)


        def element(self):
            return self.getTypedRuleContext(BKOOLParser.ElementContext,0)


        def getRuleIndex(self):
            return BKOOLParser.RULE_exp10

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExp10" ):
                return visitor.visitExp10(self)
            else:
                return visitor.visitChildren(self)




    def exp10(self):

        localctx = BKOOLParser.Exp10Context(self, self._ctx, self.state)
        self.enterRule(localctx, 74, self.RULE_exp10)
        self._la = 0 # Token type
        try:
            self.state = 365
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [BKOOLParser.NEW]:
                self.enterOuterAlt(localctx, 1)
                self.state = 357
                self.match(BKOOLParser.NEW)
                self.state = 358
                self.match(BKOOLParser.ID)
                self.state = 359
                self.match(BKOOLParser.LB)
                self.state = 361
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << BKOOLParser.BOOLLIT) | (1 << BKOOLParser.NIL) | (1 << BKOOLParser.THIS) | (1 << BKOOLParser.NEW) | (1 << BKOOLParser.SUB_OP) | (1 << BKOOLParser.NOT_OP) | (1 << BKOOLParser.FLOATLIT) | (1 << BKOOLParser.INTEGERLIT) | (1 << BKOOLParser.STRINGLIT) | (1 << BKOOLParser.ID))) != 0):
                    self.state = 360
                    self.arglist()


                self.state = 363
                self.match(BKOOLParser.RB)
                pass
            elif token in [BKOOLParser.BOOLLIT, BKOOLParser.NIL, BKOOLParser.THIS, BKOOLParser.FLOATLIT, BKOOLParser.INTEGERLIT, BKOOLParser.STRINGLIT, BKOOLParser.ID]:
                self.enterOuterAlt(localctx, 2)
                self.state = 364
                self.element()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ElementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def INTEGERLIT(self):
            return self.getToken(BKOOLParser.INTEGERLIT, 0)

        def FLOATLIT(self):
            return self.getToken(BKOOLParser.FLOATLIT, 0)

        def STRINGLIT(self):
            return self.getToken(BKOOLParser.STRINGLIT, 0)

        def BOOLLIT(self):
            return self.getToken(BKOOLParser.BOOLLIT, 0)

        def NIL(self):
            return self.getToken(BKOOLParser.NIL, 0)

        def ID(self):
            return self.getToken(BKOOLParser.ID, 0)

        def THIS(self):
            return self.getToken(BKOOLParser.THIS, 0)

        def getRuleIndex(self):
            return BKOOLParser.RULE_element

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitElement" ):
                return visitor.visitElement(self)
            else:
                return visitor.visitChildren(self)




    def element(self):

        localctx = BKOOLParser.ElementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 76, self.RULE_element)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 367
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << BKOOLParser.BOOLLIT) | (1 << BKOOLParser.NIL) | (1 << BKOOLParser.THIS) | (1 << BKOOLParser.FLOATLIT) | (1 << BKOOLParser.INTEGERLIT) | (1 << BKOOLParser.STRINGLIT) | (1 << BKOOLParser.ID))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ArglistContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def exp(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(BKOOLParser.ExpContext)
            else:
                return self.getTypedRuleContext(BKOOLParser.ExpContext,i)


        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(BKOOLParser.COMMA)
            else:
                return self.getToken(BKOOLParser.COMMA, i)

        def getRuleIndex(self):
            return BKOOLParser.RULE_arglist

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitArglist" ):
                return visitor.visitArglist(self)
            else:
                return visitor.visitChildren(self)




    def arglist(self):

        localctx = BKOOLParser.ArglistContext(self, self._ctx, self.state)
        self.enterRule(localctx, 78, self.RULE_arglist)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 369
            self.exp()
            self.state = 374
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==BKOOLParser.COMMA:
                self.state = 370
                self.match(BKOOLParser.COMMA)
                self.state = 371
                self.exp()
                self.state = 376
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx



    def sempred(self, localctx:RuleContext, ruleIndex:int, predIndex:int):
        if self._predicates == None:
            self._predicates = dict()
        self._predicates[29] = self.exp2_sempred
        self._predicates[30] = self.exp3_sempred
        self._predicates[31] = self.exp4_sempred
        self._predicates[32] = self.exp5_sempred
        self._predicates[36] = self.exp9_sempred
        pred = self._predicates.get(ruleIndex, None)
        if pred is None:
            raise Exception("No predicate with index:" + str(ruleIndex))
        else:
            return pred(localctx, predIndex)

    def exp2_sempred(self, localctx:Exp2Context, predIndex:int):
            if predIndex == 0:
                return self.precpred(self._ctx, 2)
         

    def exp3_sempred(self, localctx:Exp3Context, predIndex:int):
            if predIndex == 1:
                return self.precpred(self._ctx, 2)
         

    def exp4_sempred(self, localctx:Exp4Context, predIndex:int):
            if predIndex == 2:
                return self.precpred(self._ctx, 2)
         

    def exp5_sempred(self, localctx:Exp5Context, predIndex:int):
            if predIndex == 3:
                return self.precpred(self._ctx, 2)
         

    def exp9_sempred(self, localctx:Exp9Context, predIndex:int):
            if predIndex == 4:
                return self.precpred(self._ctx, 2)
         




