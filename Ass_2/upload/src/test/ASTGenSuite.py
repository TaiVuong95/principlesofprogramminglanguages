import unittest
from TestUtils import TestAST
from AST import *

class ASTGenSuite(unittest.TestCase):
    # def test_1(self):
    #     input = """class main {}"""
    #     expect = str(Program([ClassDecl(Id("main"),[])]))
    #     self.assertTrue(TestAST.test(input,expect,101))
    #
    # def test_2(self):
    #     input = """class main {}
    #         class notmain {}
    #     """
    #     expect = str(Program([ClassDecl(Id("main"),[]),ClassDecl(Id("notmain"),[])]))
    #     self.assertTrue(TestAST.test(input,expect,102))
    #
    # def test_3(self):
    #     input = """class main {
    #         a,b : int;
    #         final int abc = 8;
    #     }"""
    #     expect = str(Program([ClassDecl(Id("main"),[
    #         AttributeDecl(Instance(), VarDecl(Id("a"), IntType())),
    #         AttributeDecl(Instance(), VarDecl(Id("b"), IntType())),
    #         AttributeDecl(Instance(),ConstDecl(Id("abc"),IntType(),IntLiteral(8)))])]))
    #     self.assertTrue(TestAST.test(input,expect,103))
    #
    # def test_4(self):
    #     input = """class main {
    #         a:int;
    #         b:int;
    #     }"""
    #     expect = str(Program([ClassDecl(Id("main"),
    #         [AttributeDecl(Instance(),VarDecl(Id("a"),IntType())),
    #          AttributeDecl(Instance(),VarDecl(Id("b"),IntType()))])]))
    #     self.assertTrue(TestAST.test(input,expect,104))
    #
    # def test_5(self):
    #     input = """class Shape {
    #         length,width : float;
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Shape"),
    #         [AttributeDecl(Instance(),VarDecl(Id("length"),FloatType())),
    #          AttributeDecl(Instance(),VarDecl(Id("width"),FloatType()))])]))
    #     self.assertTrue(TestAST.test(input,expect,105))
    #
    # def test_6(self):
    #     input = """class Shape extends Square{
    #         length,width : float;
    #         size : int;
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Shape"),
    #         [AttributeDecl(Instance(),VarDecl(Id("length"),FloatType())),
    #          AttributeDecl(Instance(),VarDecl(Id("width"),FloatType())),
    #          AttributeDecl(Instance(),VarDecl(Id("size"),IntType()))],Id("Square"))]))
    #     self.assertTrue(TestAST.test(input,expect,106))
    #
    # def test_7(self):
    #     input = """class Rectangle extends Shape{
    #         length,width : float;
    #         static size,height : int;
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Rectangle"),
    #         [AttributeDecl(Instance(),VarDecl(Id("length"),FloatType())),
    #          AttributeDecl(Instance(),VarDecl(Id("width"),FloatType())),
    #          AttributeDecl(Static(), VarDecl(Id("size"), IntType())),
    #          AttributeDecl(Static(),VarDecl(Id("height"),IntType()))],Id("Shape"))]))
    #     self.assertTrue(TestAST.test(input,expect,107))
    #
    # def test_8(self):
    #     input = """class Triangle extends Shape {
    #        n : int[6];
    #     }"""
    #     expect = str(Program([ClassDecl(
    #         Id("Triangle"),
    #         [AttributeDecl(Instance(),VarDecl(Id("n"),ArrayType(6,IntType())))],
    #         Id("Shape"))]))
    #     self.assertTrue(TestAST.test(input,expect,108))
    #
    # def test_9(self):
    #     input = """class A {
    #         static final int My2ndCons = 2;
    #     }"""
    #     expect = str(Program([ClassDecl(Id("A"),
    #         [AttributeDecl(Static(),ConstDecl(Id("My2ndCons"),IntType(),IntLiteral(2)))])]))
    #     self.assertTrue(TestAST.test(input,expect,109))
    #
    # def test_10(self):
    #     input = """class TaiVuong extends ABC {
    #         int factorial(n:int){}
    #     }"""
    #     expect = str(Program([ClassDecl(Id("TaiVuong"),
    #         [MethodDecl(Instance(),Id("factorial"),[VarDecl(Id("n"),IntType())],IntType(),Block([],[]))],Id("ABC"))]))
    #     self.assertTrue(TestAST.test(input,expect,110))
    #
    # def test_11(self):
    #     input = """class Example2 extends ABC {
    #         int factorial(n:int){
    #             for i := 1 to 100 do{
    #                 if a<b then
    #                     break;
    #                 else
    #                     continue;
    #             }
    #         }
    #     }"""
    #     expect = str(
    #         Program([ClassDecl(Id("Example2"),
    #             [MethodDecl(
    #                     Instance(),
    #                     Id("factorial"),
    #                     [VarDecl(Id("n"),IntType())],
    #                     IntType(),
    #                     Block([],[For(Id("i"),IntLiteral(1),IntLiteral(100),True,Block([],[
    #                         If(BinaryOp("<",Id("a"),Id("b")),Break(),Continue())
    #                     ]))]))],
    #             Id("ABC"))]))
    #     self.assertTrue(TestAST.test(input,expect,111))
    #
    # def test_12(self):
    #     input = """class Example2 extends ABC {
    #         int factorial(n:int){
    #             for i := 1 to 100 do
    #                 a := 9;
    #         }
    #     }"""
    #     expect = str(Program([
    #         ClassDecl(
    #             Id("Example2"),
    #             [MethodDecl(
    #                     Instance(),
    #                     Id("factorial"),
    #                     [VarDecl(Id("n"),IntType())],
    #                     IntType(),
    #                     Block([],[For(Id("i"),IntLiteral(1),IntLiteral(100),True,Assign(Id("a"),IntLiteral(9)))]))],
    #             Id("ABC"))]))
    #     self.assertTrue(TestAST.test(input,expect,112))
    #
    # def test_13(self):
    #     input = """class Example2 {
    #         int factorial(n:int){
    #             for i := 1 to 100 do {
    #                 x := this.fun();
    #             }
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Example2"),
    #         [MethodDecl(Instance(),
    #                     Id("factorial"),
    #                     [VarDecl(Id("n"),IntType())],
    #                     IntType(),
    #                     Block([],[For(Id("i"),IntLiteral(1),IntLiteral(100),True,Block([],[
    #                         Assign(Id("x"),CallExpr(SelfLiteral(),Id("fun"),[]))
    #                     ]))]))])]))
    #     self.assertTrue(TestAST.test(input,expect,113))
    #
    # def test_14(self):
    #     input = """class Example2 {
    #         int testMethod (n:int;x:float){
    #             for i := 1 to 100 do {
    #                 Intarray[i] := i + 1;
    #             }
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Example2"),
    #         [MethodDecl(Instance(),
    #                     Id("testMethod"),
    #                     [VarDecl(Id("n"),IntType()),VarDecl(Id("x"),FloatType())],
    #                     IntType(),
    #                     Block([],[For(Id("i"),IntLiteral(1),IntLiteral(100),True,Block([],[Assign(ArrayCell(Id("Intarray"),Id("i")),BinaryOp("+",Id("i"),IntLiteral(1)))]))])
    #                     )])]))
    #     self.assertTrue(TestAST.test(input,expect,114))
    #
    # def test_15(self):
    #     input = """class PPL extends Kaka {
    #             length,width : float;
    #             final int abc = 1 + 5;
    #             int testMethod (n:int;x:float){
    #                 for i := 1 to 100 do {
    #                     Intarray[i] := i + 1;
    #                 }
    #             }
    #             int Tai (){
    #                 this.aPI := 3.14;
    #             }
    #         }"""
    #     expect = str(Program([ClassDecl(Id("PPL"),
    #                                         [   AttributeDecl(Instance(), VarDecl(Id("length"), FloatType())),
    #                                             AttributeDecl(Instance(), VarDecl(Id("width"), FloatType())),
    #                                             AttributeDecl(Instance(), ConstDecl(Id("abc"), IntType(),BinaryOp("+",IntLiteral(1),IntLiteral(5)))),
    #                                             MethodDecl(Instance(),
    #                                                     Id("testMethod"),
    #                                                     [VarDecl(Id("n"), IntType()),VarDecl(Id("x"), FloatType())],
    #                                                     IntType(),
    #                                                     Block([],[
    #                                                         For(Id("i"), IntLiteral(1), IntLiteral(100), True, Block([], [
    #                                                             Assign(ArrayCell(Id("Intarray"), Id("i")),BinaryOp("+", Id("i"), IntLiteral(1)))]))])
    #                                                     ),
    #                                             MethodDecl(Instance(),
    #                                                        Id("Tai"),
    #                                                        [],
    #                                                        IntType(),
    #                                                        Block([],[
    #                                                            Assign(FieldAccess(SelfLiteral(),Id("aPI")),FloatLiteral(3.14))
    #                                                        ]))],
    #                                         Id("Kaka"))]))
    #     self.assertTrue(TestAST.test(input, expect, 115))
    #
    # def test_16(self):
    #     input = """class Example2 {
    #         int factorial(n:int){
    #             for i := 1 to 100 do {
    #                 io.writeIntLn(i);
    #                 Intarray[i] := i + 1;
    #             }
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Example2"),
    #         [MethodDecl(Instance(),
    #                     Id("factorial"),
    #                     [VarDecl(Id("n"),IntType())],
    #                     IntType(),Block([],[
    #                         For(Id("i"),IntLiteral(1),IntLiteral(100),True,Block([],[
    #                             CallStmt(Id("io"),Id("writeIntLn"),[Id("i")]),
    #                             Assign(ArrayCell(Id("Intarray"),Id("i")),BinaryOp("+",Id("i"),IntLiteral(1))),
    #                             ]))])
    #                     )])]))
    #     self.assertTrue(TestAST.test(input,expect,116))
    #
    # def test_17(self):
    #     input = """class ABCD {
    #         void factorial(n:int){
    #             value := x.foo(5);
    #             a := new A() + new B() + 1;
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("ABCD"),
    #         [MethodDecl(Instance(),
    #                     Id("factorial"),
    #                     [VarDecl(Id("n"),IntType())],
    #                     VoidType(),Block([],[
    #                         Assign(Id("value"),CallExpr(Id("x"),Id("foo"),[IntLiteral(5)])),
    #                         Assign(Id("a"),BinaryOp("+",BinaryOp("+",NewExpr(Id("A"),[]),NewExpr(Id("B"),[])),IntLiteral(1)))
    #                     ]))])]))
    #     self.assertTrue(TestAST.test(input,expect,117))
    #
    # def test_18(self):
    #     input = """class ABCD {
    #         int hihi(n:int){
    #             %% start of declaration
    #             r,s : float;
    #             a,b: int[5];
    #             %% list of statements
    #             r := 2.0;
    #             s := r*r*this.myPI;
    #             a[0] := s;
    #
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("ABCD"),
    #         [MethodDecl(Instance(),
    #                     Id("hihi"),
    #                     [VarDecl(Id("n"),IntType())],
    #                     IntType(),
    #                     Block([
    #                         VarDecl(Id("r"), FloatType()),
    #                         VarDecl(Id("s"), FloatType()),
    #                         VarDecl(Id("a"), ArrayType(5, IntType())),
    #                         VarDecl(Id("b"), ArrayType(5, IntType()))
    #                     ],[
    #                         Assign(Id("r"),FloatLiteral(2.0)),
    #                         Assign(Id("s"),BinaryOp("*",BinaryOp("*",Id("r"),Id("r")),FieldAccess(SelfLiteral(),Id("myPI")))),
    #                         Assign(ArrayCell(Id("a"),IntLiteral(0)),Id("s"))
    #                     ])
    #                     )])]))
    #     self.assertTrue(TestAST.test(input,expect,118))
    #
    # def test_19(self):
    #     input = """class B {
    #         string factorial(n:int){
    #             for x := 6+d*a/b[7] downto 8*7/this.foo(8*7-9) do {
    #                 i := 5;
    #             }
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("B"),
    #         [MethodDecl(Instance(),
    #                     Id("factorial"),
    #                     [VarDecl(Id("n"),IntType())],
    #                     StringType(),
    #                     Block([],[For(Id("x"),
    #                                   BinaryOp("+",IntLiteral(6),BinaryOp("/",BinaryOp("*",Id("d"),Id("a")),ArrayCell(Id("b"),IntLiteral(7)))),
    #                                   BinaryOp("/",BinaryOp("*",IntLiteral(8),IntLiteral(7)),CallExpr(SelfLiteral(),Id("foo"),[BinaryOp("-",BinaryOp("*",IntLiteral(8),IntLiteral(7)),IntLiteral(9))]))
    #                                   ,False,Block([],[
    #                                         Assign(Id("i"),IntLiteral(5))
    #                         ]))])
    #                     )])]))
    #     self.assertTrue(TestAST.test(input,expect,119))
    #
    # def test_20(self):
    #     input = """class B {
    #             string factorial(n:int){
    #                 for x := this.foo(x+3)+6+d*a/b[7] downto -8*7/this.foo(8*7-9) do {
    #                     i := 5;
    #                 }
    #             }
    #         }"""
    #     expect = str(Program([ClassDecl(Id("B"),
    #                                         [MethodDecl(Instance(),
    #                                                     Id("factorial"),
    #                                                     [VarDecl(Id("n"), IntType())],
    #                                                     StringType(),
    #                                                     Block([], [For(Id("x"),BinaryOp("+",
    #                                                                     BinaryOp("+",CallExpr(SelfLiteral(),Id("foo"),[BinaryOp("+",Id("x"),IntLiteral(3))]),IntLiteral(6)),
    #                                                                    BinaryOp("/",BinaryOp("*",Id("d"),Id("a")),ArrayCell(Id("b"),IntLiteral(7)))),
    #                                                                    BinaryOp("/", BinaryOp("*", UnaryOp("-",IntLiteral(8)),IntLiteral(7)),CallExpr(SelfLiteral(), Id("foo"), [BinaryOp("-", BinaryOp("*",IntLiteral(8),IntLiteral(7)),IntLiteral(9))]))
    #                                                                    , False, Block([], [
    #                                                             Assign(Id("i"), IntLiteral(5))
    #                                                         ]))])
    #                                                     )])]))
    #     self.assertTrue(TestAST.test(input, expect, 119))
    #     # BinaryOp("/", CallExpr(SelfLiteral(), Id("foo"), [BinaryOp("+", Id("x"), IntLiteral(4))]), this.foo(x+4)
    #     #          BinaryOp("*", BinaryOp("+", IntLiteral(6), BinaryOp("/", Id("d"), Id("a"))),
    #     #                   ArrayCell(Id("b"), IntLiteral(7)))),
    #
    #     # CallExpr(SelfLiteral(), Id("foo"), [BinaryOp("+", Id("x"), IntLiteral(4))]),
    #
    # def test_21(self):
    #     input = """class ABC {
    #         int factorial(n:int){
    #             if flag then
    #                 i := 9;
    #             else
    #                 a := 10;
    #
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("ABC"),
    #         [MethodDecl(Instance(),
    #                     Id("factorial"),
    #                     [VarDecl(Id("n"),IntType())],
    #                     IntType(),
    #                     Block([],[
    #                         If(Id("flag"),Assign(Id("i"),IntLiteral(9)),Assign(Id("a"),IntLiteral(10)))
    #                     ])
    #                     )])]))
    #     self.assertTrue(TestAST.test(input,expect,121))
    #
    # def test_22(self):
    #     input = """class example {
    #         example(n:int){
    #             x:int;
    #             x := io.readInt();
    #             io.writeIntLn(this.factorial(x));
    #         }
    #         int main(){
    #             if n == 0 then
    #                 return 1;
    #             else
    #                 return n * this.factorial(n - 1);
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("example"),
    #         [MethodDecl(Instance(),
    #                     Id("<init>"),
    #                     [VarDecl(Id("n"),IntType())],
    #                     VoidType(),
    #                     Block([VarDecl(Id("x"),IntType())],[
    #                         Assign(Id("x"),CallExpr(Id("io"),Id("readInt"),[])),
    #                         CallStmt(Id("io"),Id("writeIntLn"),[CallExpr(SelfLiteral(),Id("factorial"),[Id("x")])])
    #                     ])
    #                     ),
    #          MethodDecl(Instance(),
    #                     Id("main"),
    #                     [],
    #                     IntType(),
    #                     Block([], [
    #                         If(BinaryOp("==",Id("n"),IntLiteral(0)),Return(IntLiteral(1)),Return(BinaryOp("*",Id("n"),CallExpr(SelfLiteral(),Id("factorial"),[BinaryOp("-",Id("n"),IntLiteral(1))]))))
    #                     ])
    #                     )
    #          ])]))
    #     self.assertTrue(TestAST.test(input,expect,122))
    #
    # def test_23(self):
    #     input = """class Example1 {
    #         void main(){
    #             x:int;
    #             x := io.readInt();
    #             io.writeIntLn(this.factorial(x));
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Example1"),
    #         [MethodDecl(Instance(),
    #                     Id("main"),
    #                     [],
    #                     VoidType(),
    #                     Block([VarDecl(Id("x"),IntType())],[
    #                         Assign(Id("x"),CallExpr(Id("io"),Id("readInt"),[])),
    #                         CallStmt(Id("io"),Id("writeIntLn"),[CallExpr(SelfLiteral(),Id("factorial"),[Id("x")])])
    #                     ])
    #                     )])]))
    #     self.assertTrue(TestAST.test(input,expect,123))
    #
    # def test_24(self):
    #     input = """class Example2 {
    #                     void main(){
    #                         s: Shape;
    #                         s := new Rectangle(3, 4);
    #                         io.writeFloatLn(s.getArea());
    #                         s := new Triangle(3, 4);
    #                         io.writeIntLn(s.getArea());
    #                     }
    #                 }"""
    #     expect = str(Program([ClassDecl(Id("Example2"),
    #         [MethodDecl(Instance(),
    #                     Id("main"),
    #                     [],
    #                     VoidType(),
    #                     Block([VarDecl(Id("s"),ClassType(Id("Shape")))],[
    #                         Assign(Id("s"),NewExpr(Id("Rectangle"),[IntLiteral(3),IntLiteral(4)])),
    #                         CallStmt(Id("io"),Id("writeFloatLn"),[CallExpr(Id("s"),Id("getArea"),[])]),
    #                         Assign(Id("s"), NewExpr(Id("Triangle"), [IntLiteral(3), IntLiteral(4)])),
    #                         CallStmt(Id("io"), Id("writeIntLn"), [CallExpr(Id("s"), Id("getArea"), [])])
    #                     ])
    #                     )])]))
    #     self.assertTrue(TestAST.test(input,expect,124))
    #
    # def test_25(self):
    #     input = """class Shape {
    #                     static final int numOfShape = 2 == 3;
    #                 }
    #                 class ABC extends DEF{
    #                     static final int numOfShape = 1 + 6;
    #                     int static getNumOfShape() {
    #                         a[3+x.foo(2)] := a[b[2]] + 3;
    #                     }
    #                 }"""
    #     expect = str(Program([
    #         ClassDecl(Id("Shape"),[
    #             AttributeDecl(Static(),ConstDecl(Id("numOfShape"),IntType(),BinaryOp("==",IntLiteral(2),IntLiteral(3))))
    #         ]),
    #         ClassDecl(Id("ABC"),
    #             [AttributeDecl(Static(),ConstDecl(Id("numOfShape"),IntType(),BinaryOp("+",IntLiteral(1),IntLiteral(6)))),
    #              MethodDecl(Static(),
    #                     Id("getNumOfShape"),
    #                     [],
    #                     IntType(),
    #                     Block([],[
    #                         Assign(ArrayCell(Id("a"),BinaryOp("+",IntLiteral(3),CallExpr(Id("x"),Id("foo"),[IntLiteral(2)]))),
    #                                BinaryOp("+",ArrayCell(Id("a"),ArrayCell(Id("b"),IntLiteral(2))),IntLiteral(3)))
    #                     ])
    #                     )],
    #             Id("DEF"))
    #         ]))
    #     self.assertTrue(TestAST.test(input,expect,125))
    #
    # def test_26(self):
    #     input = """class Shape {
    #         static final int numOfShape = 1 + 6;
    #         int static getNumOfShape() {
    #             x.b[2] := x.m()[3];
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Shape"),
    #         [AttributeDecl(Static(),ConstDecl(Id("numOfShape"),IntType(),BinaryOp("+",IntLiteral(1),IntLiteral(6)))),
    #          MethodDecl(Static(),
    #                     Id("getNumOfShape"),
    #                     [],
    #                     IntType(),
    #                     Block([],[
    #                         Assign(ArrayCell(FieldAccess(Id("x"),Id("b")),IntLiteral(2)),ArrayCell(CallExpr(Id("x"),Id("m"),[]),IntLiteral(3)))
    #                     ])
    #                     )])]))
    #     self.assertTrue(TestAST.test(input,expect,126))
    #
    # def test_27(self):
    #     input = """class Example2 {
    #         int testMethod (){
    #             x := !!!+---++-this.fun()[1].D();
    #             if (((a==b))) then
    #                 system.show();
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Example2"),
    #         [MethodDecl(Instance(),
    #                     Id("testMethod"),
    #                     [],
    #                     IntType(),
    #                     Block([],[
    #                         Assign(Id("x"),UnaryOp("!",UnaryOp("!",UnaryOp("!",UnaryOp("+",UnaryOp("-",UnaryOp("-",UnaryOp("-",UnaryOp("+",UnaryOp("+",UnaryOp("-", CallExpr(ArrayCell(CallExpr(SelfLiteral(),Id("fun"),[]),IntLiteral(1)),Id("D"),[])))))))))))),
    #                         If(BinaryOp("==",Id("a"),Id("b")),CallStmt(Id("system"),Id("show"),[]))
    #                     ])
    #                     )])]))
    #     self.assertTrue(TestAST.test(input,expect,127))
    #
    # def test_28(self):
    #     input = """class Example {
    #         x : int;
    #         y : float;
    #         Example(a:int;b:float){
    #             this.x := a;
    #             x := !!!this.foo()[10].A.A.A().A[1];
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Example"),
    #                                     [AttributeDecl(Instance(),VarDecl(Id("x"),IntType())),
    #                                      AttributeDecl(Instance(), VarDecl(Id("y"), FloatType())),
    #                                     MethodDecl(Instance(),
    #                                                 Id("<init>"),
    #                                                 [VarDecl(Id("a"),IntType()),VarDecl(Id("b"),FloatType())],
    #                                                 VoidType(),
    #                                                 Block([], [
    #                                                     Assign(FieldAccess(SelfLiteral(),Id("x")),Id("a")),
    #                                                     Assign(Id("x"),UnaryOp("!",UnaryOp("!",UnaryOp("!",ArrayCell(FieldAccess(CallExpr(FieldAccess(FieldAccess(ArrayCell(CallExpr(SelfLiteral(),Id("foo"),[]),IntLiteral(10)),Id("A")),Id("A")),Id("A"),[]),Id("A")), IntLiteral(1))))))
    #
    #                                                 ])
    #                                                 )])]))
    #     self.assertTrue(TestAST.test(input, expect, 128))
    #
    # def test_29(self):
    #     input = """class AbC {
    #         int factorial(n:int){
    #             for x := 5 to 2 do
    #                 a[2] := b[9] % 4 - 4 / f.foo(10);
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("AbC"),
    #                                     [MethodDecl(Instance(),
    #                                                 Id("factorial"),
    #                                                 [VarDecl(Id("n"),IntType())],
    #                                                 IntType(),
    #                                                 Block([], [
    #                                                     For(Id("x"),IntLiteral(5),IntLiteral(2),True,
    #                                                         Assign(ArrayCell(Id("a"),IntLiteral(2)),BinaryOp("-",BinaryOp("%",ArrayCell(Id("b"),IntLiteral(9)),IntLiteral(4)),BinaryOp("/",IntLiteral(4),CallExpr(Id("f"),Id("foo"),[IntLiteral(10)])))))
    #                                                 ])
    #                                                 )])]))
    #     self.assertTrue(TestAST.test(input, expect, 129))
    #
    # def test_30(self):
    #     input = """class A {
    #         int factorial(n:int){
    #             io.writeStrLn("Expression is true");
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("A"),
    #                                     [MethodDecl(Instance(),
    #                                                 Id("factorial"),
    #                                                 [VarDecl(Id("n"),IntType())],
    #                                                 IntType(),
    #                                                 Block([], [
    #                                                     CallStmt(Id("io"),Id("writeStrLn"),[StringLiteral("Expression is true")])
    #                                                 ])
    #                                                 )])]))
    #     self.assertTrue(TestAST.test(input, expect, 130))
    #
    # def test_31(self):
    #     input = """class abc {
    #         int factorial(n:int){
    #             if flag then
    #                 io.writeStrLn("Expression is true");
    #             else
    #                 io.writeStrLn("Expression is false");
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("abc"),
    #                                     [MethodDecl(Instance(),
    #                                                 Id("factorial"),
    #                                                 [VarDecl(Id("n"),IntType())],
    #                                                 IntType(),
    #                                                 Block([], [
    #                                                     If(Id("flag"),CallStmt(Id("io"),Id("writeStrLn"),[StringLiteral("Expression is true")]),
    #                                                        CallStmt(Id("io"),Id("writeStrLn"),[StringLiteral("Expression is false")]))
    #                                                 ])
    #                                                 ),
    #                                      ])]))
    #     self.assertTrue(TestAST.test(input, expect, 131))
    #
    # def test_32(self):
    #     input = """class Example2 {
    #                     void main(){
    #                         s:Shape;
    #                         s := nil;
    #                     }
    #                 }"""
    #     expect = str(Program([ClassDecl(Id("Example2"),
    #                                     [MethodDecl(Instance(),
    #                                                 Id("main"),
    #                                                 [],
    #                                                 VoidType(),
    #                                                 Block([VarDecl(Id("s"),ClassType(Id("Shape")))], [
    #                                                     Assign(Id("s"),NullLiteral())
    #                                                 ])
    #                                                 )])]))
    #     self.assertTrue(TestAST.test(input, expect, 132))
    #
    # def test_33(self):
    #     input = """class A extends B {
    #         my1stVar: int;
    #         myArrayVar: int[5];
    #         static my2ndVar,my3ndVar: Shape;
    #         static my2ndArray, my3ndArray: Shape[6];
    #         A(x: int;b: boolean){
    #             if x < y then
    #                 if a >= b then
    #                     for i:=2 to 5 do
    #                         io.print("kakaka");
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("A"),
    #                                     [AttributeDecl(Instance(),VarDecl(Id("my1stVar"),IntType())),
    #                                      AttributeDecl(Instance(), VarDecl(Id("myArrayVar"),ArrayType(5,IntType()))),
    #                                      AttributeDecl(Static(), VarDecl(Id("my2ndVar"), ClassType(Id("Shape")))),
    #                                      AttributeDecl(Static(), VarDecl(Id("my3ndVar"), ClassType(Id("Shape")))),
    #                                      AttributeDecl(Static(), VarDecl(Id("my2ndArray"), ArrayType(6,ClassType(Id("Shape"))))),
    #                                      AttributeDecl(Static(), VarDecl(Id("my3ndArray"), ArrayType(6, ClassType(Id("Shape"))))),
    #                                     MethodDecl(Instance(),
    #                                                 Id("<init>"),
    #                                                 [VarDecl(Id("x"),IntType()),VarDecl(Id("b"),BoolType())],
    #                                                 VoidType(),
    #                                                 Block([], [
    #                                                     If(BinaryOp("<", Id("x"), Id("y")), If(BinaryOp( ">=", Id("a"), Id("b")),
    #                                                     For(Id("i"), IntLiteral(2), IntLiteral(5), True,
    #                                                         CallStmt(Id("io"), Id("print"),
    #                                                                  [StringLiteral("kakaka")]))))
    #                                                 ])
    #                                                 )
    #                                      ],
    #                                     Id("B"))]))
    #     self.assertTrue(TestAST.test(input, expect, 133))
    #
    # def test_34(self):
    #     input = """class TaiVuong {
    #         void constructor(){
    #             str1 : string;
    #             str2 : string;
    #             result : string;
    #             for x:=1 to 5 do
    #                 result := "str1" ^ "str2";
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("TaiVuong"),
    #                                     [MethodDecl(Instance(),
    #                                                 Id("constructor"),
    #                                                 [],
    #                                                 VoidType(),
    #                                                 Block([
    #                                                     VarDecl(Id("str1"),StringType()),
    #                                                     VarDecl(Id("str2"),StringType()),
    #                                                     VarDecl(Id("result"),StringType())],
    #                                                 [
    #                                                     For(Id("x"),IntLiteral(1),IntLiteral(5),True,
    #                                                         Assign(Id("result"),BinaryOp("^",StringLiteral("str1"),StringLiteral("str2"))))
    #                                                 ])
    #                                                 )])]))
    #     self.assertTrue(TestAST.test(input, expect, 134))
    #
    # def test_35(self):
    #     input = """class Example {
    #         static final float const =  3 - 5*7*8;
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Example"),
    #                 [AttributeDecl(Static(),ConstDecl(Id("const"),FloatType(),BinaryOp("-", IntLiteral(3), BinaryOp("*",BinaryOp("*",IntLiteral( 5),IntLiteral(7)),IntLiteral(8)))))])]))
    #     self.assertTrue(TestAST.test(input, expect, 135))
    #
    # def test_36(self):
    #     input = """class Example {
    #         static final float const = a[1]+a[2]*a[3];
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Example"),
    #                 [AttributeDecl(Static(),ConstDecl(Id("const"),FloatType(),BinaryOp("+", ArrayCell(Id("a"),IntLiteral(1)), BinaryOp("*",ArrayCell(Id("a"),IntLiteral(2)),ArrayCell(Id("a"),IntLiteral(3))))))])]))
    #     self.assertTrue(TestAST.test(input, expect, 136))
    #
    # def test_37(self):
    #     input = """class Rectangle extends Shape {
    #         float getArea(){
    #             return this.length*this.width;
    #         }
    #     }
    #     class Triangle extends Shape {
    #         float getArea(){
    #             return this.length*this.width/2;
    #         }
    #     }"""
    #     expect = str(Program([
    #         ClassDecl(Id("Rectangle"),[
    #             MethodDecl(Instance(),Id("getArea"),[],FloatType(),Block([],[
    #                 Return(BinaryOp("*",FieldAccess(SelfLiteral(),Id("length")),FieldAccess(SelfLiteral(),Id("width"))))
    #             ]))
    #         ],Id("Shape")),
    #         ClassDecl(Id("Triangle"),[
    #             MethodDecl(Instance(), Id("getArea"), [], FloatType(), Block([], [
    #                 Return(BinaryOp("/", BinaryOp("*",FieldAccess(SelfLiteral(), Id("length")),
    #                                 FieldAccess(SelfLiteral(), Id("width"))),IntLiteral(2)))
    #             ]))
    #         ],Id("Shape"))
    #     ]))
    #     self.assertTrue(TestAST.test(input, expect, 137))
    #
    # def test_38(self):
    #     input = """class CauThu {
    #         ten : string;
    #         tuoi: int;
    #         void dabong(){
    #             io.writeStrLn("Scored");
    #         }
    #     }
    #     class Ronaldo {
    #         ronaldo : CauThu;
    #         void dabong (){
    #             for i:=1 to 100 do
    #                 ronaldo.dabong();
    #         }
    #         void main(){
    #             this.dabong();
    #         }
    #     }"""
    #     expect = str(Program([
    #         ClassDecl(Id("CauThu"),
    #                 [AttributeDecl(Instance(),VarDecl(Id("ten"),StringType())),
    #                  AttributeDecl(Instance(), VarDecl(Id("tuoi"), IntType())),
    #                  MethodDecl(Instance(),Id("dabong"),[],VoidType(),Block([],[
    #                      CallStmt(Id("io"), Id("writeStrLn"),[StringLiteral("Scored")])
    #                  ]))
    #                  ]),
    #         ClassDecl(Id("Ronaldo"),[
    #             AttributeDecl(Instance(), VarDecl(Id("ronaldo"), ClassType(Id("CauThu")))),
    #             MethodDecl(Instance(),Id("dabong"),[],VoidType(),Block([],[
    #                 For(Id("i"),IntLiteral(1),IntLiteral(100),True,CallStmt(Id("ronaldo"),Id("dabong"),[]))
    #             ])),
    #             MethodDecl(Instance(), Id("main"), [], VoidType(), Block([], [
    #                 CallStmt(SelfLiteral(),Id("dabong"),[])
    #             ]))
    #         ])
    #     ]))
    #     self.assertTrue(TestAST.test(input, expect, 138))
    #
    # def test_39(self):
    #     input = """class findMax {
    #         x : int;
    #         int findMax(array: int[8];length: int){
    #             max := array[0];
    #             for i:=0 to length do{
    #                 if max<array[i] then{
    #                     max := array[i];
    #                 }
    #             }
    #             return max;
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("findMax"),
    #                 [AttributeDecl(Instance(),VarDecl(Id("x"),IntType())),
    #                  MethodDecl(Instance(),Id("findMax"),[
    #                      VarDecl(Id("array"),ArrayType(8,IntType())),
    #                      VarDecl(Id("length"), IntType())
    #                  ],IntType(),Block([],[
    #                      Assign(Id("max"),ArrayCell(Id("array"),IntLiteral(0))),
    #                      For(Id("i"),IntLiteral(0),Id("length"),True,Block([],[
    #                          If(BinaryOp("<",Id("max"),ArrayCell(Id("array"),Id("i"))),Block([],[
    #                              Assign(Id("max"),ArrayCell(Id("array"),Id("i")))
    #                          ]))
    #                      ])),
    #                      Return(Id("max"))
    #                  ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 139))
    #
    # def test_40(self):
    #     input = """class Example {
    #         int main(){
    #             x : boolean;
    #             x := a == b || c;
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Example"),
    #                 [MethodDecl(Instance(),Id("main"),[],IntType(),Block([
    #                     VarDecl(Id("x"),BoolType())
    #                 ],[
    #                     Assign(Id("x"),BinaryOp("==",Id("a"),BinaryOp("||",Id("b"),Id("c"))))
    #                 ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 140))
    #
    # def test_41(self):
    #     input = """class Example {
    #         int main(){
    #             x : boolean;
    #             x := a % b  ^ c == d || e >= f;
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Example"),
    #                 [MethodDecl(Instance(),Id("main"),[],IntType(),Block([
    #                     VarDecl(Id("x"),BoolType())
    #                 ],[
    #                     Assign(Id("x"),BinaryOp(">=",BinaryOp("==",BinaryOp("%",Id("a"),BinaryOp("^",Id("b"),Id("c"))),BinaryOp("||",Id("d"),Id("e"))),Id("f")))
    #                 ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 141))
    #
    # def test_42(self):
    #     input = """class Example {
    #         int main(){
    #
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Example"),
    #                 [MethodDecl(Instance(),Id("main"),[],IntType(),Block([],[
    #
    #                 ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 142))
    #
    # def test_43(self):
    #     input = """class Example {
    #         static final float x = -1.0 + 3.8 -4.7;
    #         void main(){
    #             continue;
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Example"),
    #                                     [AttributeDecl(Static(),ConstDecl(Id("x"),FloatType(),BinaryOp("-",BinaryOp("+",UnaryOp("-",FloatLiteral(1.0)),FloatLiteral(3.8)),FloatLiteral(4.7)))),
    #                                      MethodDecl(Instance(), Id("main"), [], VoidType(), Block([], [
    #                                          Continue()
    #                                     ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 143))
    #
    # def test_44(self):
    #     input = """class a {
    #         int factorial(n:int){
    #             continue;
    #             value := x.foo(5);
    #             l[3] := value + 2;
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("a"),
    #                                     [MethodDecl(Instance(), Id("factorial"), [VarDecl(Id("n"),IntType())], IntType(), Block([], [
    #                                         Continue(),
    #                                         Assign(Id("value"),CallExpr(Id("x"),Id("foo"),[IntLiteral(5)])),
    #                                         Assign(ArrayCell(Id("l"),IntLiteral(3)), BinaryOp("+",Id("value"),IntLiteral(2)))
    #                                     ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 144))
    #
    # def test_45(self):
    #     input = """class SumOfOddArray {
    #                     array : int[100];
    #                     sum: int;
    #                     void count(){
    #                         sum := 0;
    #                         for i:= 0 to 99 do {
    #                             if c[i] % 2 == 1 then sum := sum + c[i];
    #                             if c[i] < 0 then break;
    #                         }
    #                         return sum;
    #                     }
    #                 }"""
    #     expect = str(Program([ClassDecl(Id("SumOfOddArray"),
    #                                     [AttributeDecl(Instance(),VarDecl(Id("array"),ArrayType(100,IntType()))),
    #                                      AttributeDecl(Instance(),VarDecl(Id("sum"),IntType())),
    #                                      MethodDecl(Instance(), Id("count"), [], VoidType(), Block([], [
    #                                          Assign(Id("sum"),IntLiteral(0)),
    #                                         For(Id("i"),IntLiteral(0),IntLiteral(99),True,Block([],[
    #                                             If(BinaryOp("==",BinaryOp("%",ArrayCell(Id("c"),Id("i")),IntLiteral(2)),IntLiteral(1)),Assign(Id("sum"),BinaryOp("+",Id("sum"),ArrayCell(Id("c"),Id("i"))))),
    #                                             If(BinaryOp("<",ArrayCell(Id("c"),Id("i")),IntLiteral(0)),Break())
    #                                         ])),
    #                                          Return(Id("sum"))
    #                                     ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 145))
    #
    # def test_46(self):
    #     input = """class main{
    #                     string hehe (){
    #                         this.foo(x+4);
    #                         return this.foo(x+3.7);
    #                     }
    #                 }"""
    #     expect = str(Program([ClassDecl(Id("main"),
    #                                     [MethodDecl(Instance(), Id("hehe"), [], StringType(), Block([], [
    #                                         CallStmt(SelfLiteral(),Id("foo"),[BinaryOp("+",Id("x"),IntLiteral(4))]),
    #                                         Return(CallExpr(SelfLiteral(),Id("foo"),[BinaryOp("+",Id("x"),FloatLiteral(3.7))]))
    #                                     ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 146))
    #
    # def test_47(self):
    #     input = """class Example {
    #         int main(){
    #             x := 5 + 5.777;
    #             if true then
    #                 io.print("kakaka");
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Example"),
    #                                     [MethodDecl(Instance(), Id("main"), [], IntType(), Block([], [
    #                                         Assign(Id("x"),BinaryOp("+",IntLiteral(5),FloatLiteral(5.777))),
    #                                         If(BooleanLiteral(True),CallStmt(Id("io"),Id("print"),[StringLiteral("kakaka")]))
    #                                     ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 147))
    #
    # def test_48(self):
    #     input = """class Boss{
    #                 Boss(){
    #                     if a > 3 then {
    #                         if a < 6 then
    #                             a := 1;
    #                         else
    #                             a := 3;
    #                     }
    #                     else
    #                         a := 6;
    #                 }
    #             }"""
    #     expect = str(Program([ClassDecl(Id("Boss"),
    #                                     [MethodDecl(Instance(), Id("<init>"), [], VoidType(), Block([], [
    #                                         If(BinaryOp(">",Id("a"),IntLiteral(3)),Block([],[If(BinaryOp("<",Id("a"),IntLiteral(6)),Assign(Id("a"),IntLiteral(1)),Assign(Id("a"),IntLiteral(3)))]),Assign(Id("a"),IntLiteral(6)))
    #                                     ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 148))
    #
    # def test_49(self):
    #     input = """class Boss{
    #                 Boss(){
    #                     if a > 3 then
    #                     if a < 6 then
    #                     a := 1;
    #                     else
    #                     a := 3;
    #                 }
    #             }"""
    #     expect = str(Program([ClassDecl(Id("Boss"),
    #                                     [MethodDecl(Instance(), Id("<init>"), [], VoidType(), Block([], [
    #                                         If(BinaryOp(">", Id("a"), IntLiteral(3)),If(BinaryOp("<", Id("a"), IntLiteral(6)),Assign(Id("a"),IntLiteral(1)),Assign(Id("a"),IntLiteral(3))))
    #                                     ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 149))
    #
    # def test_50(self):
    #     input = """class Example {
    #         int main(){
    #             if toinaymua then
    #                 io.print("toi nay o nha");
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Example"),
    #                                     [MethodDecl(Instance(), Id("main"), [], IntType(), Block([], [
    #                                         If(Id("toinaymua"),CallStmt(Id("io"),Id("print"),[StringLiteral("toi nay o nha")]))
    #                                     ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 150))
    #
    # def test_51(self):
    #     input = """class Example {
    #         int main(){
    #             x :=  -1.34 + this.x() + "str1" ^ array[this.foo(x+3)] || true && x >= 9;
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Example"),
    #                 [MethodDecl(Instance(),Id("main"),[],IntType(),Block([],[
    #                     Assign(Id("x"),BinaryOp(">=",
    #                                     BinaryOp("&&",
    #                                             BinaryOp("||",
    #                                                      BinaryOp("+",
    #                                                               BinaryOp("+",UnaryOp("-",FloatLiteral(1.34)),CallExpr(SelfLiteral(),Id("x"),[])),
    #                                                               BinaryOp("^",StringLiteral("str1"),ArrayCell(Id("array"),CallExpr(SelfLiteral(),Id("foo"),[BinaryOp("+",Id("x"),IntLiteral(3))]))))
    #                                                     ,BooleanLiteral(True)),
    #                                             Id("x")),
    #                                     IntLiteral(9)))
    #                 ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 151))
    #
    # def test_52(self):
    #     input = """class Farther {
    #         void swap(a: int;b: int){
    #             io.print(this.foo(2)[2].A.fun[4].D.B());
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Farther"),
    #                 [MethodDecl(Instance(),Id("swap"),[
    #                     VarDecl(Id("a"),IntType()),
    #                     VarDecl(Id("b"), IntType()),
    #                 ],VoidType(),Block([],[
    #                     CallStmt(Id("io"),Id("print"),[CallExpr(FieldAccess(ArrayCell(FieldAccess(FieldAccess(ArrayCell(CallExpr(SelfLiteral(),Id("foo"),[IntLiteral(2)]),IntLiteral(2)),Id("A")),Id("fun")),IntLiteral(4)),Id("D")),Id("B"),[])])
    #                 ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 152))
    #
    # def test_53(self):
    #     input = """class Example {
    #         int main(){
    #             for x:=99 downto 0 do
    #                 io.print("***");
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Example"),
    #                 [MethodDecl(Instance(),Id("main"),[],IntType(),Block([],[
    #                     For(Id("x"),IntLiteral(99),IntLiteral(0),False,CallStmt(Id("io"),Id("print"),[StringLiteral("***")]))
    #                 ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 153))
    #
    # def test_54(self):
    #     input = """class Example {
    #         int main(){
    #             for a :=  b*c - y/z to b/c + y*z
	# 		do
	# 			x := !!!!!!b ;
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Example"),
    #                 [MethodDecl(Instance(),Id("main"),[],IntType(),Block([],[
    #                     For(Id("a"), BinaryOp("-",BinaryOp("*",Id("b"),Id("c")),BinaryOp("/",Id("y"),Id("z"))),
    #                     BinaryOp("+",BinaryOp("/",Id("b"),Id("c")),BinaryOp("*",Id("y"),Id("z"))), True,
    #                         Assign(Id("x"),UnaryOp("!",UnaryOp("!",UnaryOp("!",UnaryOp("!",UnaryOp("!",UnaryOp("!",Id("b")))))))))
    #                 ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 154))
    #
    # def test_55(self):
    #     input = """class giacmotrua {
    #                     array : int[10];
    #                     %%this is comment
    #                     %% and it does nothing
    #                     /* this is same */
    #                     void giacmotrua(a:int; str: string){}
    #                     %%this is comment
    #                     %%and it does nothing
    #                     /* this is same */
    #                 }"""
    #     expect = str(Program([ClassDecl(Id("giacmotrua"),
    #                 [AttributeDecl(Instance(),VarDecl(Id("array"),ArrayType(10,IntType()))),
    #                  MethodDecl(Instance(),Id("giacmotrua"),[
    #                      VarDecl(Id("a"),IntType()),
    #                      VarDecl(Id("str"),StringType())
    #                  ],VoidType(),Block([],[]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 155))
    #
    # def test_56(self):
    #     input = """class Test {
    #         int factorial(n:int){
    #             value := x.foo(5);
    #             l[3] := value * 2;
    #             if a || b then
    #                 break;
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Test"),
    #                 [MethodDecl(Instance(),Id("factorial"),[VarDecl(Id("n"),IntType())],IntType(),Block([],[
    #                     Assign(Id("value"),CallExpr(Id("x"),Id("foo"),[IntLiteral(5)])),
    #                     Assign(ArrayCell(Id("l"),IntLiteral(3)),BinaryOp("*",Id("value"),IntLiteral(2))),
    #                     If(BinaryOp("||",Id("a"),Id("b")),Break())
    #                 ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 156))
    #
    # def test_57(self):
    #     input = """class CheckEventNumber {
    #         x : int;
    #         CheckEventNumber(x : int){
    #             this.x := x;
    #         }
    #         boolean check(){
    #             if this.x%2==0 then
    #                 return true;
    #             else
    #                 return false;
    #         }
    #     }
    #     class Me{
    #         x : int;
    #         int execute(){
    #             check : CheckEventNumber;
    #             x := 10;
    #             CheckEventNumber :=  new CheckEventNumber(x);
    #             if CheckEventNumber.check() then
    #                 io.print("OKE");
    #             else
    #                 io.print("Not OKE");
    #         }
    #     }"""
    #     expect = str(Program([
    #         ClassDecl(Id("CheckEventNumber"),
    #                 [AttributeDecl(Instance(),VarDecl(Id("x"),IntType())),
    #                  MethodDecl(Instance(),Id("<init>"),[VarDecl(Id("x"),IntType())],VoidType(),Block([],[
    #                     Assign(FieldAccess(SelfLiteral(),Id("x")),Id("x"))
    #                 ])),
    #                  MethodDecl(Instance(), Id("check"), [], BoolType(), Block([], [
    #                      If(BinaryOp("==",BinaryOp("%",FieldAccess(SelfLiteral(),Id("x")),IntLiteral(2)),IntLiteral(0)),
    #                         Return(BooleanLiteral(True)),
    #                         Return(BooleanLiteral(False)))
    #                  ]))
    #                  ])
    #                              ,
    #             ClassDecl(Id("Me"),[AttributeDecl(Instance(),VarDecl(Id("x"),IntType())),
    #                 MethodDecl(Instance(),Id("execute"),[],IntType(),Block([
    #                     VarDecl(Id("check"),ClassType(Id("CheckEventNumber")))
    #                 ],[
    #                     Assign(Id("x"),IntLiteral(10)),
    #                     Assign(Id("CheckEventNumber"),NewExpr(Id("CheckEventNumber"),[Id("x")])),
    #                     If(CallExpr(Id("CheckEventNumber"),Id("check"),[]),CallStmt(Id("io"),Id("print"),[StringLiteral("OKE")]),CallStmt(Id("io"),Id("print"),[StringLiteral("Not OKE")]))
    #                 ]))
    #             ])
    #                           ]))
    #     self.assertTrue(TestAST.test(input, expect, 157))
    #
    # def test_58(self):
    #     input = """class Example {
    #         static final int hahaha = 10*10;
    #         x : float;
    #         int main(){
    #             io.print("kakaka");
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Example"),
    #                 [AttributeDecl(Static(),ConstDecl(Id("hahaha"),IntType(),BinaryOp("*",IntLiteral(10),IntLiteral(10)))),
    #                  AttributeDecl(Instance(),VarDecl(Id("x"),FloatType())),
    #                  MethodDecl(Instance(),Id("main"),[],IntType(),Block([],[
    #                     CallStmt(Id("io"),Id("print"),[StringLiteral("kakaka")])
    #                 ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 158))
    #
    # def test_59(self):
    #     input = """class Example {
    #         int main(){
    #             {
    #                 {
    #                     io.print("hi");
    #                 }
    #             }
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Example"),
    #                 [MethodDecl(Instance(),Id("main"),[],IntType(),Block([],[
    #                     Block([],[
    #                         Block([],[
    #                             CallStmt(Id("io"),Id("print"),[StringLiteral("hi")])
    #                         ])
    #                     ])
    #                 ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 159))
    #
    # def test_60(self):
    #     input = """class Example {
    #         troimua : boolean;
    #         int main(){
    #             a := c^(b%a)*d;
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Example"),
    #                 [AttributeDecl(Instance(),VarDecl(Id("troimua"),BoolType())),
    #                  MethodDecl(Instance(),Id("main"),[],IntType(),Block([],[
    #                      Assign(Id("a"),BinaryOp("*",BinaryOp("^",Id("c"),BinaryOp("%",Id("b"),Id("a"))),Id("d")))
    #                 ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 160))
    #
    # def test_61(self):
    #     input = """class HinhThoi extends HinhVuong {
    #         int Tai (){
    #             this.aPI := 3.14;
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("HinhThoi"),
    #                 [MethodDecl(Instance(),Id("Tai"),[],IntType(),Block([],[
    #                     Assign(FieldAccess(SelfLiteral(),Id("aPI")),FloatLiteral(3.14))
    #                 ]))],Id("HinhVuong"))]))
    #     self.assertTrue(TestAST.test(input, expect, 161))
    #
    # def test_62(self):
    #     input = """class a {
    #         int factorial(n:int){
    #             x := a[1][2][3];
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("a"),
    #                 [MethodDecl(Instance(),Id("factorial"),[VarDecl(Id("n"),IntType())],IntType(),Block([],[
    #                     Assign(Id("x"),ArrayCell(ArrayCell(ArrayCell(Id("a"),IntLiteral(1)),IntLiteral(2)),IntLiteral(3)))
    #                 ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 162))
    #
    # def test_63(self):
    #     input = """class TestFree extends Tai{
    #         int factorial(){
    #             x := io.fun()[1].A.B().H.fun().F;
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("TestFree"),
    #                 [MethodDecl(Instance(),Id("factorial"),[],IntType(),Block([],[
    #                     Assign(Id("x"), FieldAccess(CallExpr(FieldAccess(
    #                         CallExpr(FieldAccess(ArrayCell(CallExpr(Id("io"),Id("fun"),[]),IntLiteral(1)),Id("A")),
    #                                  Id("B"),[]),Id("H")),Id("fun"),[]),Id("F")))
    #                 ]))],Id("Tai"))]))
    #     self.assertTrue(TestAST.test(input, expect, 163))
    #
    # def test_64(self):
    #     input = """class xxx {
    #         int factorial(n:int){
    #             this.A.B.C.D.E.F();
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("xxx"),
    #                 [MethodDecl(Instance(),Id("factorial"),[VarDecl(Id("n"),IntType())],IntType(),Block([],[
    #                     CallStmt(FieldAccess(FieldAccess(FieldAccess(FieldAccess(FieldAccess(SelfLiteral(),Id("A")),Id("B")),Id("C")),Id("D")),Id("E")),Id("F"),[])
    #                 ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 164))
    #
    # def test_65(self):
    #     input = """class Example2 {
    #         void main(){
    #             io.print(this.fun[1].A.D());
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Example2"),
    #                 [MethodDecl(Instance(),Id("main"),[],VoidType(),Block([
    #                 ],[
    #                     CallStmt(Id("io"),Id("print"),[CallExpr(FieldAccess(
    #                         ArrayCell(FieldAccess(SelfLiteral(),Id("fun")),IntLiteral(1)),Id("A")),Id("D"),[])])
    #                 ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 165))
    #
    # def test_66(self):
    #     input = """class Example {
    #         int main(){
    #             if (a>b) && (b>c) then
    #                 io.print("kaka");
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Example"),
    #                 [MethodDecl(Instance(),Id("main"),[],IntType(),Block([],[
    #                     If(BinaryOp("&&",BinaryOp(">",Id("a"),Id("b")),BinaryOp(">",Id("b"),Id("c"))),
    #                        CallStmt(Id("io"),Id("print"),[StringLiteral("kaka")]))
    #                 ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 166))
    #
    # def test_67(self):
    #     input = """class Here extends Now {
    #         Here(){
    #             io.print(this.fun().A);
    #             io.print(this.fun().B[2]);
    #             io.print(this.fun().C());
    #             io.print(this.fun[1].D());
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Here"),
    #                 [MethodDecl(Instance(),Id("<init>"),[],VoidType(),Block([],[
    #                     CallStmt(Id("io"),Id("print"),[FieldAccess(CallExpr(SelfLiteral(),Id("fun"),[]),Id("A"))]),
    #                     CallStmt(Id("io"), Id("print"), [ArrayCell(FieldAccess(CallExpr(SelfLiteral(), Id("fun"), []), Id("B")),IntLiteral(2))]),
    #                     CallStmt(Id("io"), Id("print"), [CallExpr(CallExpr(SelfLiteral(),Id("fun"),[]),Id("C"),[])]),
    #                     CallStmt(Id("io"), Id("print"), [CallExpr(ArrayCell(FieldAccess(SelfLiteral(),Id("fun")),IntLiteral(1)),Id("D"),[])])
    #                 ]))],
    #                 Id("Now"))]))
    #     self.assertTrue(TestAST.test(input, expect, 167))
    #
    # def test_68(self):
    #     input = """class Shape {
    #         length,width:float;
    #         float getArea() {}
    #         Shape(length,width:float){
    #             this.length := length;
    #             this.width := width;
    #         }
    #     }
    #     class Rectangle extends Shape {
    #         float getArea(){
    #             return this.length*this.width;
    #         }
    #     }
    #     class Triangle extends Shape {
    #         float getArea(){
    #             return this.length*this.width / 2;
    #         }
    #     }
    #     class Example2 {
    #         void main(){
    #             s:Shape;
    #             s := new Rectangle(3,4);
    #             io.writeFloatLn(s.getArea());
    #             s := new Triangle(3,4);
    #             io.writeFloatLn(s.getArea());
    #         }
    #     }"""
    #     expect = str(Program([
    #         ClassDecl(Id("Shape"),
    #                 [AttributeDecl(Instance(),VarDecl(Id("length"),FloatType())),
    #                   AttributeDecl(Instance(),VarDecl(Id("width"),FloatType())),
    #                   MethodDecl(Instance(),Id("getArea"),[],FloatType(),Block([],[])),
    #                   MethodDecl(Instance(),Id("<init>"), [VarDecl(Id("length"), FloatType()),VarDecl(Id("width"), FloatType())],VoidType(), Block([], [
    #                       Assign(FieldAccess(SelfLiteral(),Id("length")),Id("length")),
    #                       Assign(FieldAccess(SelfLiteral(),Id("width")), Id("width"))
    #                   ]))
    #                  ]),
    #         ClassDecl(Id("Rectangle"),[
    #             MethodDecl(Instance(),Id("getArea"),[],FloatType(),Block([],[
    #                 Return(BinaryOp("*", FieldAccess(SelfLiteral(), Id("length")),
    #                                 FieldAccess(SelfLiteral(), Id("width"))))
    #             ]))
    #         ],Id("Shape")),
    #         ClassDecl(Id("Triangle"), [
    #             MethodDecl(Instance(), Id("getArea"), [], FloatType(), Block([], [
    #                 Return(BinaryOp("/", BinaryOp("*", FieldAccess(SelfLiteral(), Id("length")),
    #                                               FieldAccess(SelfLiteral(), Id("width"))), IntLiteral(2)))
    #             ]))
    #         ], Id("Shape")),
    #         ClassDecl(Id("Example2"),[
    #             MethodDecl(Instance(),Id("main"),[],VoidType(),Block([
    #                 VarDecl(Id("s"),ClassType(Id("Shape")))
    #             ],[
    #                 Assign(Id("s"),NewExpr(Id("Rectangle"),[IntLiteral(3),IntLiteral(4)])),
    #                 CallStmt(Id("io"),Id("writeFloatLn"),[CallExpr(Id("s"),Id("getArea"),[])]),
    #                 Assign(Id("s"), NewExpr(Id("Triangle"), [IntLiteral(3), IntLiteral(4)])),
    #                 CallStmt(Id("io"), Id("writeFloatLn"), [CallExpr(Id("s"), Id("getArea"), [])])
    #             ]))
    #         ])
    #
    #     ]))
    #     self.assertTrue(TestAST.test(input, expect, 168))
    #
    # def test_69(self):
    #     input = """class Milk {
    #         int milo(){
    #             if hasmuchenertry then
    #                 io.print("buy");
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Milk"),
    #                 [MethodDecl(Instance(),Id("milo"),[],IntType(),Block([],[
    #                     If(Id("hasmuchenertry"),CallStmt(Id("io"),Id("print"),[StringLiteral("buy")]))
    #                 ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 169))
    #
    # def test_70(self):
    #     input = """class Example {
    #         int main(){
    #             if true then
    #                 continue;
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Example"),
    #                 [MethodDecl(Instance(),Id("main"),[],IntType(),Block([],[
    #                     If(BooleanLiteral(True),Continue())
    #                 ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 170))
    #
    # def test_71(self):
    #     input = """class Example {
    #         int main(){
    #             a[3+x.foo(2)] := a[b[2]] +3;/* This is a block comment, that
    #             may span in many lines*/
    #             a := 5;%%this is a line comment
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Example"),
    #                 [MethodDecl(Instance(),Id("main"),[],IntType(),Block([],[
    #                     Assign(ArrayCell(Id("a"),BinaryOp("+",IntLiteral(3),CallExpr(Id("x"),Id("foo"),[IntLiteral(2)]))),
    #                            BinaryOp("+",ArrayCell(Id("a"),ArrayCell(Id("b"),IntLiteral(2))),IntLiteral(3))),
    #                     Assign(Id("a"),IntLiteral(5))
    #                 ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 171))
    #
    # def test_72(self):
    #     input = """class TestComment {
    #         int hehe(){
    #             /* This is a block comment so %%has no meaning here */
    #             %%This is a line comment so /* has no meaning here
    #             if (a>b)>=c then
    #                 io.print("hala");
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("TestComment"),
    #                 [MethodDecl(Instance(),Id("hehe"),[],IntType(),Block([],[
    #                     If(BinaryOp(">=",BinaryOp(">",Id("a"),Id("b")),Id("c")),CallStmt(Id("io"),Id("print"),[StringLiteral("hala")]))
    #                 ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 172))
    #
    # def test_73(self):
    #     input = """class Example {
    #         int main(){
    #             abc := 0.33E-3 + 128e+42;
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Example"),
    #                 [MethodDecl(Instance(),Id("main"),[],IntType(),Block([],[
    #                     Assign(Id("abc"),BinaryOp("+",FloatLiteral(0.33E-3),FloatLiteral(128e+42)))
    #                 ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 173))
    #
    # def test_74(self):
    #     input = """class Kakaka {
    #         int beauty(){
    #             if 9.0 + 12e8 % 0.33E-3 || 128e+42 then
    #                 io.show("kaka");
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Kakaka"),
    #                 [MethodDecl(Instance(),Id("beauty"),[],IntType(),Block([],[
    #                     If(BinaryOp("||",BinaryOp("+",FloatLiteral(9.0),BinaryOp("%",FloatLiteral(12e8),FloatLiteral(0.33E-3))),FloatLiteral(128e+42)),
    #                        CallStmt(Id("io"),Id("show"),[StringLiteral("kaka")]))
    #
    #                 ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 174))
    #
    # def test_75(self):
    #     input = """class Assignment2 {
    #         int main(){
    #             this.aPI := 3.14 * x.foo(5) * value * 2;
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Assignment2"),
    #                 [MethodDecl(Instance(),Id("main"),[],IntType(),Block([],[
    #                     Assign(FieldAccess(SelfLiteral(),Id("aPI")),BinaryOp("*",
    #                                                                         BinaryOp("*",
    #                                                                                  BinaryOp("*", FloatLiteral(3.14), CallExpr(Id("x"), Id("foo"),[IntLiteral(5)])),Id("value")),IntLiteral(2)))
    #                 ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 175))
    #
    # def test_76(self):
    #     input = """class TestConst {
    #         static final float conts = 9.0 + 12e8 - 1. * 0.33E-3 / 128e+42;
    #         int main(){
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("TestConst"),
    #                 [
    #                     AttributeDecl(Static(),ConstDecl(Id("conts"),FloatType(),
    #                                                      BinaryOp("-",BinaryOp("+",FloatLiteral(9.0),FloatLiteral(12e8)),
    #                                                               BinaryOp("/",BinaryOp("*",FloatLiteral(1.),FloatLiteral(0.33E-3)),FloatLiteral(128e+42))))),
    #                     MethodDecl(Instance(),Id("main"),[],IntType(),Block([],[
    #
    #                 ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 176))
    #
    # def test_77(self):
    #     input = """class Example {
    #         int main(){
    #             for i := 1 to 1000 do {
    #                 io.writeIntLn(i);
    #                 Intarray[i] := i + 1;
    #             }
    #             for x := 5 downto -1000 do
    #                 io.writeIntLn(x);
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Example"),
    #                 [MethodDecl(Instance(),Id("main"),[],IntType(),Block([],[
    #                     For(Id("i"),IntLiteral(1),IntLiteral(1000),True,Block([],[
    #                         CallStmt(Id("io"),Id("writeIntLn"),[Id("i")]),
    #                         Assign(ArrayCell(Id("Intarray"),Id("i")),BinaryOp("+",Id("i"),IntLiteral(1)))
    #                     ])),
    #                     For(Id("x"),IntLiteral(5),UnaryOp("-",IntLiteral(1000)),False,CallStmt(Id("io"),Id("writeIntLn"),[Id("x")]))
    #                 ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 177))
    #
    # def test_78(self):
    #     input = """class Example {
    #         int main(){
    #             if x < 5 then
	# 			    a := b + 1 ;
	# 		    else
	# 			    if y < 10 then
	# 				    if z < 20 then
	# 					    b := b - 2 ;
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Example"),
    #                 [MethodDecl(Instance(),Id("main"),[],IntType(),Block([],[
    #                     If(BinaryOp("<",Id("x"),IntLiteral(5)),
    #                        Assign(Id("a"),BinaryOp("+",Id("b"),IntLiteral(1))),
    #                        If(BinaryOp("<",Id("y"),IntLiteral(10)),
    #                           If(BinaryOp("<",Id("z"),IntLiteral(20)),
    #                              Assign(Id("b"),BinaryOp("-",Id("b"),IntLiteral(2))))))
    #                 ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 178))
    #
    # def test_79(self):
    #     input = """class Example {
    #         int main(){
    #             for a :=  b*c - y/z to b/c + y*z do
	# 			    x := x + 1 ;
	# 			    x := io.fun[3];
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Example"),
    #                 [MethodDecl(Instance(),Id("main"),[],IntType(),Block([],[
    #                     For(Id("a"),BinaryOp("-",BinaryOp("*",Id("b"),Id("c")),BinaryOp("/",Id("y"),Id("z"))),
    #                         BinaryOp("+",BinaryOp("/",Id("b"),Id("c")),BinaryOp("*",Id("y"),Id("z"))),
    #                         True,Assign(Id("x"),BinaryOp("+",Id("x"),IntLiteral(1)))),
    #                     Assign(Id("x"),ArrayCell(FieldAccess(Id("io"),Id("fun")),IntLiteral(3)))
    #                 ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 179))
    #
    # def test_80(self):
    #     input = """class CMT {
    #         int main(){
    #             array : int[11];
    #             %%this is comment
    #             %%and it does nothing
    #             /* this is same */
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("CMT"),
    #                 [MethodDecl(Instance(),Id("main"),[],IntType(),Block([
    #                     VarDecl(Id("array"),ArrayType(11,IntType()))
    #                 ],[
    #
    #                 ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 180))
    #
    # def test_81(self):
    #     input = """class Example {
    #         int main(){
    #             s : Shape;
    #             if x%10+3 == 3 then
    #                 s := new Shape(0);
    #             else
    #                 s := nil;
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Example"),
    #                 [MethodDecl(Instance(),Id("main"),[],IntType(),Block([
    #                     VarDecl(Id("s"),ClassType(Id("Shape")))
    #                 ],[
    #                     If(BinaryOp("==", BinaryOp("+", BinaryOp("%", Id("x"), IntLiteral(10)), IntLiteral(3)),
    #                                 IntLiteral(3)), Assign(Id("s"), NewExpr(Id("Shape"), [IntLiteral(0)])),
    #                        Assign(Id("s"), NullLiteral()))
    #                 ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 181))
    #
    # def test_82(self):
    #     input = """class Example {
    #         int main(){
    #             a[a/b] := c*d / e % f && g ;
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Example"),
    #                 [MethodDecl(Instance(),Id("main"),[],IntType(),Block([],[
    #                     Assign(ArrayCell(Id("a"),BinaryOp("/",Id("a"),Id("b"))),
    #                            BinaryOp("&&",BinaryOp("%",BinaryOp("/",BinaryOp("*",Id("c"),Id("d")),Id("e")),Id("f")),Id("g")))
    #                 ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 182))
    #
    # def test_83(self):
    #     input = """class Example {
    #         int main(){
    #             break;
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Example"),
    #                 [MethodDecl(Instance(),Id("main"),[],IntType(),Block([],[
    #                     Break()
    #                 ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 183))
    #
    # def test_84(self):
    #     input = """class Example {
    #         int main(){
    #             tai.TaiVuong();
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Example"),
    #                 [MethodDecl(Instance(),Id("main"),[],IntType(),Block([],[
    #                     CallStmt(Id("tai"),Id("TaiVuong"),[])
    #                 ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 184))
    #
    # def test_85(self):
    #     input = """class Example {
    #         int main(){
    #             x.foo(x.foo(x.foo(x.foo(x.foo()))));
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Example"),
    #                 [MethodDecl(Instance(),Id("main"),[],IntType(),Block([],[
    #                     CallStmt(Id("x"),Id("foo"),[CallExpr(Id("x"),Id("foo"),[
    #                         CallExpr(Id("x"),Id("foo"),[
    #                             CallExpr(Id("x"),Id("foo"),[CallExpr(Id("x"),Id("foo"),[])])
    #                         ])
    #                     ])])
    #                 ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 185))
    #
    # def test_86(self):
    #     input = """class Example {
    #         int main(){
    #             io.writeStr("Nhap ten cua ban:");
    #             io.readlnt(name);
    #             io.writeStr("Nhap ho cua ban:");
    #             io.readlnt(surname);
    #             io.writeStr();/*new line*/
    #             io.writelnt();%%new line
    #             io.writeStr("Ten day du cua ban la : ",name," ",surname);
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Example"),
    #                 [MethodDecl(Instance(),Id("main"),[],IntType(),Block([],[
    #                     CallStmt(Id("io"),Id("writeStr"),[StringLiteral("Nhap ten cua ban:")]),
    #                     CallStmt(Id("io"), Id("readlnt"), [Id("name")]),
    #                     CallStmt(Id("io"), Id("writeStr"), [StringLiteral("Nhap ho cua ban:")]),
    #                     CallStmt(Id("io"), Id("readlnt"), [Id("surname")]),
    #                     CallStmt(Id("io"), Id("writeStr"), []),
    #                     CallStmt(Id("io"), Id("writelnt"), []),
    #                     CallStmt(Id("io"), Id("writeStr"), [StringLiteral("Ten day du cua ban la : "),Id("name"),StringLiteral(" "),Id("surname")]),
    #                 ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 186))
    #
    # def test_87(self):
    #     input = """class ascn {
    #         int main(){
    #             return !true;
    #             return -x;
    #             return -8;
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("ascn"),
    #                 [MethodDecl(Instance(),Id("main"),[],IntType(),Block([],[
    #                     Return(UnaryOp("!",BooleanLiteral(True))),
    #                     Return(UnaryOp("-", Id("x"))),
    #                     Return(UnaryOp("-", IntLiteral(8)))
    #                 ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 187))
    #
    # def test_88(self):
    #     input = """class Example {
    #         str1 : string;
    #         str2 : string;
    #         result : string;
    #         int main(){
    #             str1 := "votaivuong";
    #             str2 := "finalyear";
    #             result := str1^str2;
    #             if result == "votaivuongfinalyear" then
    #                 io.print("right");
    #             else
    #                 io.print("wrong");
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Example"),
    #                 [   AttributeDecl(Instance(),VarDecl(Id("str1"),StringType())),
    #                     AttributeDecl(Instance(),VarDecl(Id("str2"),StringType())),
    #                     AttributeDecl(Instance(),VarDecl(Id("result"),StringType())),
    #                     MethodDecl(Instance(),Id("main"),[],IntType(),Block([],[
    #                         Assign(Id("str1"),StringLiteral("votaivuong")),
    #                         Assign(Id("str2"), StringLiteral("finalyear")),
    #                         Assign(Id("result"),BinaryOp("^",Id("str1"),Id("str2"))),
    #                         If(BinaryOp("==",Id("result"),StringLiteral("votaivuongfinalyear")),
    #                         CallStmt(Id("io"),Id("print"),[StringLiteral("right")]),
    #                            CallStmt(Id("io"),Id("print"),[StringLiteral("wrong")]))
    #                 ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 188))
    #
    # def test_89(self):
    #     input = """class Example {
    #         int main(){
    #             if a > b then
    #                 c := c+c;
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Example"),
    #                 [MethodDecl(Instance(),Id("main"),[],IntType(),Block([],[
    #                     If(BinaryOp(">",Id("a"),Id("b")),Assign(Id("c"),BinaryOp("+",Id("c"),Id("c"))))
    #                 ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 189))
    #
    # def test_90(self):
    #     input = """class Example {
    #         int main(){
    #             x.foo(x+4);
    #             return x.foo(x+3);
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Example"),
    #                 [MethodDecl(Instance(),Id("main"),[],IntType(),Block([],[
    #                     CallStmt(Id("x"),Id("foo"),[BinaryOp("+",Id("x"),IntLiteral(4))]),
    #                     Return(CallExpr(Id("x"),Id("foo"),[BinaryOp("+",Id("x"),IntLiteral(3))]))
    #                 ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 190))
    #
    # def test_91(self):
    #     input = """class Example {
    #         int main(){
    #             a := "string1" ^ "string2" - a.foo() * array[20] > !b + -g / d && k != m;
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Example"),
    #                 [MethodDecl(Instance(),Id("main"),[],IntType(),Block([],[
    #                     Assign(Id("a"),BinaryOp(">",BinaryOp("-",
    #                                                     BinaryOp("^",StringLiteral("string1"),StringLiteral("string2")),
    #                                                     BinaryOp("*",CallExpr(Id("a"),Id("foo"),[]),ArrayCell(Id("array"),IntLiteral(20))))
    #                            ,
    #                                                 BinaryOp("!=",
    #                                              BinaryOp("&&",BinaryOp("+",UnaryOp("!",Id("b")),BinaryOp("/",UnaryOp("-",Id("g")),Id("d"))),Id("k"))
    #                                             ,Id("m"))
    #                            ))
    #                 ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 191))
    #
    # def test_92(self):
    #     input = """class Example {
    #         int main(){
    #             a := -9 - -10;
    #             b := +10 - +10;
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Example"),
    #                 [MethodDecl(Instance(),Id("main"),[],IntType(),Block([],[
    #                     Assign(Id("a"),BinaryOp("-",UnaryOp("-",IntLiteral(9)),UnaryOp("-",IntLiteral(10)))),
    #                     Assign(Id("b"), BinaryOp("-", UnaryOp("+", IntLiteral(10)), UnaryOp("+", IntLiteral(10))))
    #                 ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 192))
    #
    # def test_93(self):
    #     input = """class Example {
    #         static abc : string;
    #         static final int hehe = 1*3;
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Example"),
    #                 [AttributeDecl(Static(),VarDecl(Id("abc"),StringType())),
    #                  AttributeDecl(Static(),ConstDecl(Id("hehe"),IntType(),BinaryOp("*",IntLiteral(1),IntLiteral(3))))])]))
    #     self.assertTrue(TestAST.test(input, expect, 193))
    #
    # def test_94(self):
    #     input = """class Example {
    #         int main(){
    #             if a>b==c then
    #                 continue;
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Example"),
    #                 [MethodDecl(Instance(),Id("main"),[],IntType(),Block([],[
    #                     If(BinaryOp(">",Id("a"),BinaryOp("==",Id("b"),Id("c"))),Continue())
    #                 ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 194))
    #
    # def test_95(self):
    #     input = """class Example {
    #         int main(){
    #             for i:= -+-+9 to --+10 do
    #                 io.foo();
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Example"),
    #                 [MethodDecl(Instance(),Id("main"),[],IntType(),Block([],[
    #                     For(Id("i"),UnaryOp("-",UnaryOp("+",UnaryOp("-",UnaryOp("+",IntLiteral(9))))),UnaryOp("-",UnaryOp("-",UnaryOp("+",IntLiteral(10)))),True,
    #                         CallStmt(Id("io"),Id("foo"),[]))
    #                 ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 195))
    #
    # def test_96(self):
    #     input = """class Example {
    #         int main(){
    #             c := a> 0 && a%10;
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Example"),
    #                 [MethodDecl(Instance(),Id("main"),[],IntType(),Block([],[
    #                     Assign(Id("c"),BinaryOp(">",Id("a"),BinaryOp("&&",IntLiteral(0),BinaryOp("%",Id("a"),IntLiteral(10)))))
    #                 ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 196))
    #
    # def test_97(self):
    #     input = """class Example {
    #         int main(){
    #             if a * b / c - 10 then
    #                 continue;
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Example"),
    #                 [MethodDecl(Instance(),Id("main"),[],IntType(),Block([],[
    #                     If(BinaryOp("-",BinaryOp("/",BinaryOp("*",Id("a"),Id("b")),Id("c")),IntLiteral(10)),Continue())
    #                 ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 197))
    #
    # def test_98(self):
    #     input = """class Example {
    #         int main(){
    #             x := new A(new B(new C()));
    #             y := new A(new B(new C(this.foo())));
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Example"),
    #                 [MethodDecl(Instance(),Id("main"),[],IntType(),Block([],[
    #                     Assign(Id("x"),NewExpr(Id("A"),[NewExpr(Id("B"),[NewExpr(Id("C"),[])])])),
    #                     Assign(Id("y"), NewExpr(Id("A"), [NewExpr(Id("B"), [NewExpr(Id("C"),[
    #                         CallExpr(SelfLiteral(),Id("foo"),[])
    #                     ])])]))
    #                 ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 198))
    #
    # def test_99(self):
    #     input = """class Example {
    #         int main(){
    #             io.print("Hello PPL");
    #         }
    #     }"""
    #     expect = str(Program([ClassDecl(Id("Example"),
    #                 [MethodDecl(Instance(),Id("main"),[],IntType(),Block([],[
    #                     CallStmt(Id("io"),Id("print"),[StringLiteral("Hello PPL")])
    #                 ]))])]))
    #     self.assertTrue(TestAST.test(input, expect, 199))

    def test_100(self):
        input = """class hihi {
            void main(){
                a : int;
                a.abc := 6;
            }
        }"""
        expect = str(Program([ClassDecl(Id("hihi"),[MethodDecl(Static(),Id("main"),[],VoidType(),
                        Block([VarDecl(Id("a"),IntType())],
                              [Assign(FieldAccess(Id("a"),Id("abc")),IntLiteral(6))]))])]))
        self.assertTrue(TestAST.test(input, expect, 200))

