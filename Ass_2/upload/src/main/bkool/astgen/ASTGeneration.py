from BKOOLVisitor import BKOOLVisitor
from BKOOLParser import BKOOLParser
from AST import *
from functools import reduce

class ASTGeneration(BKOOLVisitor):

    def visitProgram(self,ctx:BKOOLParser.ProgramContext):
        return Program([self.visit(x) for x in ctx.class_decl()])

    def visitClass_decl(self,ctx:BKOOLParser.Class_declContext):
        memlist = []
        if ctx.list_member():
            for x in ctx.list_member():
                if type(self.visit(x)) == list:
                    memlist.extend(self.visit(x))
                else:
                    memlist.append(self.visit(x))

        if not ctx.EXTENDS():
            classname = Id(ctx.ID(0).getText())
            return ClassDecl(classname,memlist)
        else:
            classname = Id(ctx.ID(0).getText())
            parentname = Id(ctx.ID(1).getText())
            return ClassDecl(classname, memlist, parentname)

    def visitList_member(self,ctx:BKOOLParser.List_memberContext):
        return self.visit(ctx.getChild(0))

    def visitVar_decl(self,ctx:BKOOLParser.Var_declContext):
        return self.visit(ctx.getChild(0))

    def visitLocal_decl(self,ctx:BKOOLParser.Local_declContext):
        return self.visit(ctx.getChild(0))

    def visitImutable_decl(self,ctx:BKOOLParser.Imutable_declContext):
        constant = Id(ctx.ID().getText())
        constType = self.visit(ctx.pri_type())
        value = self.visit(ctx.exp())
        if not ctx.STATIC():
            return AttributeDecl(Instance(),ConstDecl(constant,constType,value))
        else:
            return AttributeDecl(Static(), ConstDecl(constant, constType, value))

    def visitMutable_decl(self,ctx:BKOOLParser.Mutable_declContext):
        list_VarDecl = list(map(lambda x: VarDecl(x, self.visit(ctx.type_var())), self.visit(ctx.idlist())))
        if ctx.STATIC():
            return list(map(lambda x : AttributeDecl(Static(),x),list_VarDecl))
        else:
            return list(map(lambda x: AttributeDecl(Instance(),x),list_VarDecl))

    def visitImutable_local(self,ctx:BKOOLParser.Imutable_localContext):
        constant = Id(ctx.ID().getText())
        constType = self.visit(ctx.pri_type())
        value = self.visit(ctx.exp())
        return ConstDecl(constant,constType,value)

    def visitMutable_local(self,ctx:BKOOLParser.Mutable_localContext):
        return list(map(lambda x: VarDecl(x, self.visit(ctx.type_var())), self.visit(ctx.idlist())))

    def visitIdlist(self,ctx:BKOOLParser.IdlistContext):
        return [Id(x.getText()) for x in ctx.ID()]

    def visitMethod_decl(self,ctx:BKOOLParser.Method_declContext):
        if not ctx.type_var() and not ctx.void_type():
            name = Id("<init>")
        else:
            name = Id(ctx.ID().getText())
        param = self.visit(ctx.many_para()) if ctx.many_para() else []
        returnType = self.visit(ctx.type_var()) if ctx.type_var() else VoidType()
        body = self.visit(ctx.block_stmt())
        if name.name == "main" and len(param) == 0 and type(returnType) is VoidType:
            return MethodDecl(Static(), name, param, returnType, body)
        elif not ctx.STATIC():
            return MethodDecl(Instance(),name,param,returnType,body)
        else:
            return MethodDecl(Static(), name, param, returnType, body)

    def visitType_var(self, ctx: BKOOLParser.Type_varContext):
        return self.visit(ctx.getChild(0))

    def visitArr_type(self, ctx: BKOOLParser.Arr_typeContext):
        size = int(ctx.INTEGERLIT().getText())
        eleType = self.visit(ctx.pri_type()) if ctx.pri_type() else self.visit(ctx.class_type())
        return ArrayType(size,eleType)

    def visitPri_type(self, ctx: BKOOLParser.Pri_typeContext):
        if ctx.BOOLEAN():
            return BoolType()
        elif ctx.INT():
            return IntType()
        elif ctx.FLOAT():
            return FloatType()
        else:
            return StringType()

    def visitVoid_type(self, ctx: BKOOLParser.Void_typeContext):
        return VoidType()

    def visitClass_type(self, ctx: BKOOLParser.Class_typeContext):
        return ClassType(Id(ctx.ID().getText()))

    def visitMany_para(self, ctx: BKOOLParser.Many_paraContext):
        return reduce(lambda x,y : x + y, [self.visit(x) for x in ctx.one_para()])

    def visitOne_para(self, ctx: BKOOLParser.One_paraContext):
        return list(map(lambda x : VarDecl(x,self.visit(ctx.type_var())),self.visit(ctx.idlist())))

    def visitStmt(self, ctx: BKOOLParser.StmtContext):
        return self.visit(ctx.getChild(0))

    def visitBlock_stmt(self, ctx: BKOOLParser.Block_stmtContext):
        list_LocalDecl = [];
        if ctx.local_decl():
            for x in ctx.local_decl():
                if type(self.visit(x)) == list:
                    list_LocalDecl.extend(self.visit(x))
                else:
                    list_LocalDecl.append(self.visit(x))
        list_Stmt = [] if not ctx.stmt() else [self.visit(x) for x in ctx.stmt()]
        return Block(list_LocalDecl,list_Stmt)

    def visitAssign_stmt(self, ctx: BKOOLParser.Assign_stmtContext):
        lhs = self.visit(ctx.exp8())
        exp = self.visit(ctx.exp())
        return Assign(lhs,exp)

    def visitBreak_stmt(self, ctx: BKOOLParser.Break_stmtContext):
        return Break()

    def visitContinue_stmt(self, ctx: BKOOLParser.Continue_stmtContext):
        return Continue()

    def visitReturn_stmt(self, ctx: BKOOLParser.Return_stmtContext):
        exp = self.visit(ctx.exp())
        return Return(exp)

    def visitIf_stmt(self, ctx: BKOOLParser.If_stmtContext):
        exp = self.visit(ctx.exp())
        if not ctx.ELSE():
            thenStmt = self.visit(ctx.stmt(0))
            return If(exp,thenStmt)
        else:
            thenStmt = self.visit(ctx.stmt(0))
            elseStmt = self.visit(ctx.stmt(1))
            return If(exp,thenStmt,elseStmt)

    def visitFor_stmt(self, ctx: BKOOLParser.For_stmtContext):
        id = Id(ctx.ID().getText())
        exp1 = self.visit(ctx.exp(0))
        exp2 = self.visit(ctx.exp(1))
        up = True if ctx.TO() else False
        loop = self.visit(ctx.stmt())
        return For(id,exp1,exp2,up,loop)

    def visitCall_stmt(self, ctx: BKOOLParser.Call_stmtContext):
        obj = self.visit(ctx.exp8())
        id = Id(ctx.ID().getText())
        param = [] if not ctx.arglist() else self.visit(ctx.arglist())
        return CallStmt(obj,id,param)

    def visitExp(self, ctx: BKOOLParser.ExpContext):
        if ctx.getChildCount() == 1:
            return self.visit(ctx.exp1(0))
        else:
            return BinaryOp(ctx.getChild(1).getText(), self.visit(ctx.exp1(0)), self.visit(ctx.exp1(1)))

    def visitExp1(self, ctx: BKOOLParser.Exp1Context):
        if ctx.getChildCount() == 1:
            return self.visit(ctx.exp2(0))
        else:
            return BinaryOp(ctx.getChild(1).getText(), self.visit(ctx.exp2(0)), self.visit(ctx.exp2(1)))

    def visitExp2(self, ctx: BKOOLParser.Exp2Context):
        if ctx.getChildCount() == 1:
            return self.visit(ctx.exp3())
        else:
            return BinaryOp(ctx.getChild(1).getText(), self.visit(ctx.exp2()), self.visit(ctx.exp3()))

    def visitExp3(self, ctx: BKOOLParser.Exp3Context):
        if ctx.getChildCount() == 1:
            return self.visit(ctx.exp4())
        else:
            return BinaryOp(ctx.getChild(1).getText(), self.visit(ctx.exp3()), self.visit(ctx.exp4()))

    def visitExp4(self, ctx: BKOOLParser.Exp4Context):
        if ctx.getChildCount() == 1:
            return self.visit(ctx.exp5())
        else:
            return BinaryOp(ctx.getChild(1).getText(), self.visit(ctx.exp4()), self.visit(ctx.exp5()))

    def visitExp5(self, ctx: BKOOLParser.Exp5Context):
        if ctx.getChildCount() == 1:
            return self.visit(ctx.exp6())
        else:
            return BinaryOp(ctx.getChild(1).getText(), self.visit(ctx.exp5()), self.visit(ctx.exp6()))

    def visitExp6(self, ctx: BKOOLParser.Exp6Context):
        if ctx.getChildCount() == 1:
            return self.visit(ctx.exp7())
        else:
            return UnaryOp(ctx.getChild(0).getText(), self.visit(ctx.exp6()))

    def visitExp7(self, ctx: BKOOLParser.Exp7Context):
        if ctx.getChildCount() == 1:
            return self.visit(ctx.exp8())
        else:
            return UnaryOp(ctx.getChild(0).getText(), self.visit(ctx.exp7()))

    def visitExp8(self, ctx: BKOOLParser.Exp8Context):
        if ctx.getChildCount() == 1:
            return self.visit(ctx.exp9())
        elif ctx.getChildCount() == 4:
            arr = self.visit(ctx.exp8())
            idx = self.visit(ctx.exp())
            return ArrayCell(arr,idx)
        elif ctx.getChildCount() == 3:
            arr = self.visit(ctx.exp8())
            id = Id(ctx.ID().getText())
            return FieldAccess(arr,id)
        else:
            ob = self.visit(ctx.exp8())
            method = Id(ctx.ID().getText())
            param = [] if not ctx.arglist() else self.visit(ctx.arglist())
            return CallExpr(ob,method,param)

    def visitExp9(self, ctx: BKOOLParser.Exp9Context):
        if ctx.getChildCount() == 1:
            return self.visit(ctx.exp10())
        else:
            if ctx.LB():
                obj = self.visit(ctx.exp9())
                id = Id(ctx.ID().getText())
                pram = [] if not ctx.arglist() else self.visit(ctx.arglist())
                return CallExpr(obj, id, pram)
            else:
                obj = self.visit(ctx.exp9())
                id = Id(ctx.ID().getText())
                return FieldAccess(obj,id)

    def visitExp10(self, ctx: BKOOLParser.Exp10Context):
        if ctx.getChildCount() == 1:
            return self.visit(ctx.exp11())
        else:
            id = Id(ctx.ID().getText())
            param = [] if not ctx.arglist() else self.visit(ctx.arglist())
            return NewExpr(id,param)

    def visitExp11(self, ctx : BKOOLParser.Exp11Context):
        if ctx.getChildCount() == 1:
            return self.visit(ctx.element())
        else:
            return self.visit(ctx.exp())

    def visitElement(self, ctx: BKOOLParser.ElementContext):
        value = ctx.getChild(0)
        if ctx.INTEGERLIT():
            return IntLiteral(int(value.getText()))
        elif ctx.FLOATLIT():
            return FloatLiteral(float(value.getText()))
        elif ctx.BOOLLIT():
            return BooleanLiteral(True if value.getText().lower() == "true".lower() else False)
        elif ctx.STRINGLIT():
            return StringLiteral(value.getText())
        elif ctx.ID():
            return Id(value.getText())
        elif ctx.NIL():
            return NullLiteral()
        else:
            return SelfLiteral()

    def visitArglist(self, ctx: BKOOLParser.ArglistContext):
        return [self.visit(x) for x in ctx.exp()]